<?php
	session_start();
	include_once("funciones.php");

	$idprofesor = $_SESSION['idusuario'];
	$idplanteo = $_POST['idplanteo'];

	if (isset($_POST['pregunta']) && !empty($_POST['pregunta'])) {
		if(isset($_POST['idpregunta']) && !empty($_POST['idpregunta'])){
			$idpregunta = $_POST['idpregunta'];
			$pregunta = ucfirst(trim($_POST['pregunta']));
			$link = trim($_POST['link']);
			$op1 = ucfirst(trim($_POST['opcion1']));
			$op2 = ucfirst(trim($_POST['opcion2']));
			$op3 = ucfirst(trim($_POST['opcion3']));
			$op4 = ucfirst(trim($_POST['opcion4']));
			$op5 = ucfirst(trim($_POST['opcion5']));
			$op6 = ucfirst(trim($_POST['opcion6']));
		@	$correcta = $_POST['radio'];
			if(!empty($op1) || !empty($op2) || !empty($op3) || !empty($op4) || !empty($op5) || !empty($op6)){
				$multiple = 1;
			} else {
				$multiple = 0;
			}
			$editar = "UPDATE preguntas
									SET
									descripcion = '$pregunta',
									link = '$link',
									multiple = '$multiple',
									opcion1 = '$op1',
									opcion2 = '$op2',
									opcion3 = '$op3',
									opcion4 = '$op4',
									opcion5 = '$op5',
									opcion6 = '$op6',
									correcta = '$correcta'";
			if(!empty($_FILES["archivo"]["name"])){
				$tamano = $_FILES["archivo"]['size'];
				$tipo = $_FILES["archivo"]['type'];
				$archivo = $_FILES["archivo"]['name'];
				$prefijo = substr(md5(uniqid(rand())),0,6);
				$nombreModificado = sanear_string($archivo);
				if ($_FILES["archivo"]["error"] > 0) {
						echo "Return Code: " . $_FILES["archivo"]["error"] . "<br>";
					} else {
						$nombrealeatorio = rand() * rand();
						$rutaarchivo = "archivos_planteos/preguntas/" . $nombrealeatorio . $nombreModificado;
						if (file_exists($rutaarchivo)) {
							unlink($rutaarchivo);
							move_uploaded_file($_FILES["archivo"]["tmp_name"], $rutaarchivo);
							$editar .= ", ruta = '$rutaarchivo'";
						} else {
							// Muevo la archivo desde su ubicacion temporal a la carpeta de archivos
							move_uploaded_file($_FILES["archivo"]["tmp_name"], $rutaarchivo);
							$editar .= ", ruta = '$rutaarchivo'";
						}
					}
			}
			$editar .= "WHERE idpregunta = '$idpregunta';";
			$carga = consulta($editar);
			if(!$carga){
				echo "Error: ".mysqli_error();
			} else {
				echo "La pregunta se modifico correctamente.";
			}
		} else {
			$pregunta = ucfirst(trim($_POST['pregunta']));
			$link = trim($_POST['link']);
			$op1 = ucfirst(trim($_POST['opcion1']));
			$op2 = ucfirst(trim($_POST['opcion2']));
			$op3 = ucfirst(trim($_POST['opcion3']));
			$op4 = ucfirst(trim($_POST['opcion4']));
			$op5 = ucfirst(trim($_POST['opcion5']));
			$op6 = ucfirst(trim($_POST['opcion6']));
			$correcta = $_POST['radio'];
			$rutaarchivo = "";
			if(!empty($op1) || !empty($op2) || !empty($op3) || !empty($op4) || !empty($op5) || !empty($op6)){
				$multiple = 1;
			} else {
				$multiple = 0;
			}
			if(!empty($_FILES["archivo"]["name"])){
				$tamano = $_FILES["archivo"]['size'];
				$tipo = $_FILES["archivo"]['type'];
				$archivo = $_FILES["archivo"]['name'];
				$prefijo = substr(md5(uniqid(rand())),0,6);
				$nombreModificado = sanear_string($archivo);
				if ($_FILES["archivo"]["error"] > 0) {
						echo "Return Code: " . $_FILES["archivo"]["error"] . "<br>";
					} else {
						$nombrealeatorio = rand() * rand();
						$rutaarchivo = "archivos_planteos/preguntas/" . $nombrealeatorio . $nombreModificado;
						if (file_exists($rutaarchivo)) {
							unlink($rutaarchivo);
							move_uploaded_file($_FILES["archivo"]["tmp_name"], $rutaarchivo);
						} else {
							// Muevo la archivo desde su ubicacion temporal a la carpeta de archivos
							move_uploaded_file($_FILES["archivo"]["tmp_name"], $rutaarchivo);
						}
					}
			}
			$carga = consulta("INSERT INTO preguntas (idplanteo, descripcion, ruta, link, multiple, opcion1, opcion2, opcion3, opcion4, opcion5, opcion6, correcta)
						VALUES ('$idplanteo','$pregunta','$rutaarchivo','$link','$multiple','$op1','$op2','$op3','$op4','$op5','$op6','$correcta')");
			if(!$carga){
				echo "Error: ".mysqli_error();
			} else {
				echo "La pregunta se cargo correctamente.";
			}
		}
	} else {
		echo "No se ha escrito ninguna pregunta.";
	}
		/*	if($_POST['limite'] == "true") {
			$fechalimite = fecha($_POST['fechalimite'],"-");
		}else{
			$fechalimite = "0000-00-00";
		};*/
?>
