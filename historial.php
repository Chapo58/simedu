<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Historial</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="inicio.php">Inicio</a>
            </li>
            <li><a href="perfil.php">Perfil de Usuario</a></li>
            <li class="active">Historial</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Historial</h1>

  <table id="historial" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Fecha</th>
                <th>Hora</th>
                <th>Accion</th>
                <th>Empresa</th>
				<th></th>
            </tr>
        </thead>
		<tfoot>
			<tr>
				<th></th>
                <th></th>
                <th></th>
                <th></th>
				<th></th>
            </tr>
		</tfoot>
        <tbody>
			<?php
				$con_hist=consulta("select * from historial where idusuario='$idusuario'");
				while ($h = mysqli_fetch_array($con_hist, MYSQLI_ASSOC)) {
					$empresa = $h['rsocialempresa'];
					$fecha = $h['fecha'];
					$hora = $h['hora'];
					$accion = $h['accion'];
					$dia = substr($fecha, -2);
					$mes   = substr($fecha, 5, 2);
					$ano = substr($fecha, 0, 4);
					$pfecha = $dia."/".$mes."/".$ano;
			?>
            <tr>
				<td><?php echo $pfecha; ?></td>
                <td><?php echo $hora; ?></td>
                <td><?php echo $accion; ?></td>
                <td><?php echo $empresa; ?></td>
				<td><?php echo $ano.$mes.$dia; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

  <script type="text/javascript">
  	$(document).ready(function() {
  		var table = $('#historial').DataTable({
  			initComplete: function () {
              this.api().columns(2).every( function () {
                  var column = this;
                  var select = $('<select><option value=""></option></select>')
                      .appendTo( $(column.footer()).empty() )
                      .on( 'change', function () {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                          );

                          column
                              .search( val ? '^'+val+'$' : '', true, false )
                              .draw();
                      } );

                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                  } );
              } );
  			},
  			"columnDefs": [
              {
                  "targets": [ 4 ],
                  "visible": false,
                  "searchable": false
              }
  			],
  			language: {
  				"sProcessing":     "Procesando...",
  				"sLengthMenu":     "Mostrar _MENU_ registros",
  				"sZeroRecords":    "No se encontraron resultados",
  				"sEmptyTable":     "Ningún dato disponible en esta tabla",
  				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
  				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
  				"sInfoPostFix":    "",
  				"sSearch":         "Buscar:",
  				"sUrl":            "",
  				"sInfoThousands":  ",",
  				"sLoadingRecords": "Cargando...",
  				"oPaginate": {
  					"sFirst":    "Primero",
  					"sLast":     "Último",
  					"sNext":     "Siguiente",
  					"sPrevious": "Anterior"
  				},
  				"oAria": {
  					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
  					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
  				}
  			},
  			"sScrollY":        "70vh",
  			"scrollCollapse": true
  		});
  		// Ordenar por columnas fecha y hora
  		table
  			.order( [ 4, 'desc' ], [ 1, 'desc' ] )
  			.draw();
  	});
  </script>
  <!-- ============================================================= -->
</body>
</html>
