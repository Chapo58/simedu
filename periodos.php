<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Periodos</title>
    <?php require_once('head.php'); ?>
	  <link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	  <link href="jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="simcontable.php">Simulador Contable</a>
            </li>
            <li class="active">Periodos</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

    <!-- Page Content -->
    <div id="divPeriodos">
         <h1 class="page-header">Activa y Agrega Periodos</h1>
		 <p align="right"><?php if(isset($_SESSION['descripcionperiodo'])) echo "<u>Periodo Activo</u>: ".$_SESSION['descripcionperiodo']; else echo "Ningun Periodo Activo."; ?></p>
		 <div class="bs-callout bs-callout-info">
            <h4><label for="idperiodo">Seleccionar Periodo</label></h4>
            <form action="periodos.php" method="POST">
				<select class="form-control" name="idperiodo" onchange="this.form.submit()" id="idperiodo">
				<option disabled selected>Selecciona un Periodo</option>
				<?php $con_per=consulta("SELECT * FROM periodos WHERE idusuario='$idusuario'");
					  while ($per = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
							$idperiodo = $per['idperiodo'];
							$descripcion = $per['descripcion'];
				?>
					<option
					value="<?php echo $idperiodo; ?>"
					><?php echo $descripcion ; ?></option>
				<?php } ?>
				</select>
			</form>
        </div>
		 <input type="button" id="btnNueva" onclick="$('#Periodos').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Crear nuevo Periodo">

        <div class="space50"></div>

        <div class="row">
            <div class="col-xs-12">
				<div id="Periodos" style="width: 100%;"></div>
            </div>
        </div>
    </div>
    <!-- Final Page Content -->

    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

  <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
  <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function () {

		    //Prepare jTable
			$('#Periodos').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Periodos',
				paging: true,
				sorting: true,
				defaultSorting: 'periodo ASC',
				actions: {
					listAction: 'PeriodosAct.php?action=list',
					createAction: 'PeriodosAct.php?action=create',
					updateAction: 'PeriodosAct.php?action=update',
					deleteAction: 'PeriodosAct.php?action=delete'
				},
				fields: {
					idperiodo: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					periodo: {
						title: 'Periodo',
						width: '20%',
						visibility: 'fixed'
					},
					descripcion: {
						title: 'Descripcion',
						width: '40%'
					},
					desde: {
						title: 'Desde',
						width: '20%',
						type: 'date',
						displayFormat: 'dd-mm-yy'
					},
					hasta: {
						title: 'Hasta',
						width: '20%',
						type: 'date',
						displayFormat: 'dd-mm-yy'
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay periodos cargados!',
					addNewRecord: 'Agregar Periodo',
					editRecord: 'Editar Periodo',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El periodo será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
          pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				},
				recordAdded(event, data) {
					$('#idperiodo').append($('<option>', {
						value: data.record.idperiodo,
						text: data.record.descripcion
					}));
				},
				recordDeleted(event, data) {
					$("#idperiodo option[value='"+data.record.idperiodo+"']").remove();
				},
				recordUpdated(event, data) {
					$("#idperiodo option[value='"+data.record.idperiodo+"']").text(data.record.denominacion);
				},
				formSubmitting(event, data) {
					var nuevodesde = data.form.find('input[name="desde"]').val();
					var nuevohasta = data.form.find('input[name="hasta"]').val();
					var nuevadescripcion = data.form.find('input[name="descripcion"]').val();
					var nuevoperiodo = data.form.find('input[name="periodo"]').val();
					var retorno = true;
					if(nuevodesde == "") {
						BootstrapDialog.alert('Debe tener una fecha inicial.');
						retorno = false;
					}
					if(nuevohasta == "") {
						BootstrapDialog.alert('Debe tener una fecha final.');
						retorno = false;
					}
					if(nuevadescripcion == "") {
						BootstrapDialog.alert('La descripcion no puede estar vacia.');
						retorno = false;
					}
					if(nuevoperiodo == "") {
						BootstrapDialog.alert('El periodo no puede estar vacio.');
						retorno = false;
					}
					return retorno;
				}
			});

			 $('#Periodos').jtable('load');

			 $.datepicker.regional['es'] = {
			 closeText: 'Cerrar',
			 prevText: '<Ant',
			 nextText: 'Sig>',
			 currentText: 'Hoy',
			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			 weekHeader: 'Sm',
			 dateFormat: 'dd/mm/yy',
			 firstDay: 1,
			 isRTL: false,
			 showMonthAfterYear: false,
			 yearSuffix: ''
			 };
			 $.datepicker.setDefaults($.datepicker.regional['es']);

			 <?php
				if(isset($_SESSION['idperiodo']) && !empty($_SESSION['idperiodo'])) {
					$id = $_SESSION['idperiodo'];
					echo "$('#idperiodo').val('$id');";
				}
			?>
	});
    </script>
</body>
</html>
