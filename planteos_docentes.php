<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Planteos</title>
    <?php require_once('head.php'); ?>
    <?php
      $con_sf=consulta("SELECT * FROM semaforos WHERE idprofesor='$idusuario'");
      if(mysqli_num_rows($con_sf)>0){
        $sf=mysqli_fetch_array($con_sf);
        $verde = $sf['verde'];
        $rojo = $sf['rojo'];
        $amarillo = $sf['amarillo'];
      } else {
        $verde = "";
        $rojo = "";
        $amarillo = "";
      }

    ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-cubes home-icon"></i>
              <a href="planteo.php">Planteos</a>
            </li>
            <li class="active">Definicion de Planteos</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Definicion de Planteos</h1>
  <div class="row">
  <div class="col-xs-6">
  <button type="button" data-toggle="modal" data-target="#modalcrear" class="btn btn-primary btn-lg" title="Crear">
		<span class="fa fa-plus-square"></span>&nbsp;&nbsp;Crear nuevo planteo
  </button>
  </div>
  <div class="col-xs-6">
  <button type="button" data-toggle="modal" data-target="#modalsemaforo" class="btn btn-info btn-lg" title="Semaforo">
		<span class="fa fa-lightbulb-o"></span>&nbsp;&nbsp;Semaforo
  </button>
  </div>
  </div>
  <div class="space20"></div>
	<?php
		$con_planteo_activo=consulta("SELECT * FROM planteos WHERE idprofesor='$idusuario' AND habilitado = 1");
		if(mysqli_num_rows($con_planteo_activo)==1){
		$pa=mysqli_fetch_array($con_planteo_activo);
		$idp = $pa['idplanteo'];
		$titulop = $pa['titulo'];
	?>
	<div class="row">
		<div class="col-xs-12">
			<div class="bs-callout bs-callout-primary">
				<h4>Planteo Activo</h4>
				<table style="font-size: large;" class="table table-bordered" cellspacing="0" width="100%">
        <tbody>
      <tr>
				<td width="80%"><?php echo $titulop; ?></td>
				<td class="text-center">
					<form action="planteos_editar.php" method="POST">
                        <button type="submit" class="btn btn-primary" title="Editar">
							<span class="fa fa-pencil"></span>&nbsp;<strong>Editar</strong>
						</button>
                        <input type="hidden" name="idplanteo" value="<?php echo $idp; ?>" >
                    </form>
				</td>
				<td class="text-center">
					<form action="PDFs/planteos.php" target="_blank" method="POST">
                        <button type="submit" class="btn btn-info" title="Listar">
							<span class="fa fa-file-pdf-o"></span>&nbsp;<strong>Listar</strong>
						</button>
                        <input type="hidden" name="idplanteo" value="<?php echo $idp; ?>" >
                    </form>
				</td>
				<td class="text-center">
					<form action="planteos_desactivar.php" method="POST">
            <button type="submit" class="btn btn-warning" title="Desactivar">
							<span class="fa fa-times"></span>&nbsp;<strong>Desactivar</strong>
						</button>
						<input type="hidden" name="idplanteo" value="<?php echo $idp; ?>" >
          </form>
				</td>
        <td class="text-center">
					<form action="planteos_correccion.php" method="POST">
            <button type="submit" class="btn btn-info" title="Corregir">
							<span class="fa fa-star-half-o"></span>&nbsp;<strong>Corregir</strong>
						</button>
						<input type="hidden" name="idplanteo" value="<?php echo $idp; ?>" >
          </form>
				</td>
      </tr>
        </tbody>
    </table>
			</div>
		</div>
	</div>
	<?php } ?>

        <div class="row">
            <div class="col-xs-12">
	<table style="font-size: large;" id="tabla" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Titulo</th>
				<th>Editar</th>
				<th>Listar</th>
				<th>Activar</th>
				<th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
			<?php
				$con_planteos=consulta("SELECT * FROM planteos WHERE idprofesor='$idusuario' AND habilitado = 0 ORDER BY idplanteo DESC");
				while ($p = mysqli_fetch_array($con_planteos, MYSQLI_ASSOC)) {
					$id = $p['idplanteo'];
					$titulo = $p['titulo'];
			?>
            <tr>
				<td width="80%"><?php echo $titulo; ?></td>
				<td class="text-center">
					<form action="planteos_editar.php" method="POST">
                        <button type="submit" class="btn btn-primary" title="Editar">
							<span class="fa fa-pencil" aria-hidden="true"></span>
						</button>
                        <input type="hidden" name="idplanteo" id="idplanteo" value="<?php echo $id; ?>" >
                    </form>
				</td>
				<td class="text-center">
					<form action="PDFs/planteos.php" target="_blank" method="POST">
                        <button type="submit" class="btn btn-info" title="Listar">
							<span class="fa fa-file-pdf-o" aria-hidden="true"></span>
						</button>
                        <input type="hidden" name="idplanteo" id="idplanteo" value="<?php echo $id; ?>" >
                    </form>
				</td>
				<td class="text-center">
					<form class="actform">
                        <button type="submit" class="btn btn-warning" title="Activar">
							<span class="fa fa-check" aria-hidden="true"></span>
						</button>
						<input type="hidden" id="actplanteo" name="actplanteo" value="<?php echo $id; ?>" >
                    </form>
				</td>
				<td class="text-center">
					<form class="delform">
                        <button type="submit" class="btn btn-danger" title="Eliminar">
							<span class="fa fa-trash-o" aria-hidden="true"></span>
						</button>
						<input type="hidden" id="delplanteo" name="delplanteo" value="<?php echo $id; ?>" >
                    </form>
				</td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
            </div>
        </div>
<div id="modalcrear" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Crear nuevo planteo</h4>
      </div>
      <div class="modal-body">
	        <form id="crear" class="form-horizontal" role="form">
			<div class="row">
	            <div class="col-md-12 col-xs-12">
					<div class="input-group">
					  <div class="input-group-addon"><strong>Titulo</strong></div>
					  <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Titulo del planteo">
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Crear</button>
			</form>
      </div>
    </div>

  </div>
</div>

<div id="modalsemaforo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Semaforo</h4>
      </div>
      <div class="modal-body">
	        <form id="semaforo" class="form-horizontal" role="form">
			<div class="row">
				<div class="col-xs-12">
					<div class="bs-callout bs-callout-success">
						<h4><label for="verde">Verde</label></h4>
						<div class="form-group">
						  <input type="text" id="verde" name="verde" placeholder="Leyenda" class="form-control" maxlength="100" value="<?php echo $verde; ?>">
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="bs-callout bs-callout-warning">
						<h4><label for="amarillo">Amarillo</label></h4>
						<div class="form-group">
						  <input type="text" id="amarillo" name="amarillo" placeholder="Leyenda" class="form-control" maxlength="100" value="<?php echo $amarillo; ?>">
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="bs-callout bs-callout-danger">
						<h4><label for="rojo">Rojo</label></h4>
						<div class="form-group">
						  <input type="text" id="rojo" name="rojo" placeholder="Leyenda" class="form-control" maxlength="100" value="<?php echo $rojo; ?>">
						</div>
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Guardar</button>
			</form>
      </div>
    </div>

  </div>
</div>


  <!-- Final Page Content -->

  <?php include "footer.php" ?>

<script type="text/javascript">

	$("#crear").on('submit',function(event){
		event.preventDefault();
		data = $(this).serialize();
		$.ajax({
			type: "POST",
			url: "planteos_crear.php",
			data: data
		}).done(function( msg ) {
			window.location.reload();
		});
	});

	$("#semaforo").on('submit',function(event){
		event.preventDefault();
		data = $(this).serialize();
		$.ajax({
			type: "POST",
			url: "planteos_semaforo.php",
			data: data
		}).done(function( msg ) {
			window.location.reload();
		});
	});

	$(document).ready(function() {

		var table = $('#tabla').DataTable({
			"bSort" : false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "No existen planteos cargados",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});

		$('#tabla').on('click', '.delform', function () {
			event.preventDefault();
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Planteo',
				message: '¿Esta seguro que desea eliminar este planteo?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(dialogItself){
						dialogItself.close();
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

		$('#tabla').on('click', '.actform', function () {
			event.preventDefault();
			data = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "planteos_activar.php",
				data: data
			}).done(function( msg ) {
				BootstrapDialog.show({
					message: msg,
					buttons: [{
						label: 'Aceptar',
						action: function() {
							window.location.reload();
						}
					}]
				});
			});
		});

	});
</script>
  <!-- ============================================================= -->
</body>
</html>
