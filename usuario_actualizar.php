<?php
	session_start();
	include "funciones.php";
		if(isset($_SESSION['email']) && isset($_SESSION['idusuario'])){
			$email = $_SESSION['email'];
			$idusuario = $_SESSION['idusuario'];
			$con_usr=consulta("select * from usuario where idusuario='$idusuario'");
			$u=mysqli_fetch_array($con_usr);
		} else {
			ir_a('index.php');
		};
	
	if (isset($_POST['email']) && !empty($_POST['email'])) {
		
			// Quito espacios en blanco
			$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
			$nombre = trim($_POST['nombre']);
			$apellido = trim($_POST['apellido']);
			$fechanac = $_POST['fechanac'];
			$sexo = $_POST['sexo'];
			$dir = trim($_POST['direccion']);
			$loc = trim($_POST['localidad']);
			$prov = trim($_POST['provincia']);
			$face = trim($_POST['facebook']);
			$pais = $_POST['pais'];
			$tel = $_POST['telefono'];
			
			// Paso a mayusculas
			$nombre = ucfirst($nombre);
			$apellido = ucfirst($apellido);
			$dir = ucfirst($dir);
			$loc = ucfirst($loc);
			$prov = ucfirst($prov);
			
		$con_u=consulta("select email from usuario where email='$email'");
		$us=mysqli_num_rows($con_u);
		if($us>0 && $email!=$u['email']){
			echo "El Email ingresado ya existe. Ingrese uno diferente";
		} elseif($email === false) {
			echo "El Email ingresado no es valido. Ingrese uno diferente";
		} elseif(testfecha($fechanac)===false) {
			echo "La fecha ingresada no es correcta.";
		} else { 
			//$fechanac = date("Y-m-d", strtotime($fechanac));
			$editar = "UPDATE usuario
								SET
								email = '$email',
								nombre = '$nombre',
								apellido = '$apellido',
								sexo = '$sexo',
								direccion = '$dir',
								ciudad = '$loc',
								provincia = '$prov',
								pais = '$pais',
								telefono = '$tel',
								facebook = '$face'";
			if(isset($_POST['fechanac']) && !empty($_POST['fechanac'])){
				$fechanac = str_replace("/","",$fechanac);
				$a�o = substr($fechanac, -4);
				$mes = substr($fechanac, -6, 2);
				$dia = substr($fechanac, -8, 2);
				$fechanac = $a�o."-".$mes."-".$dia;
				$editar .= ", fechanacimiento = '$fechanac'";
			}

			if(!empty($_FILES["imagen"]["name"])){
				if ((($_FILES["imagen"]["type"] == "image/gif")
					|| ($_FILES["imagen"]["type"] == "image/jpeg")
					|| ($_FILES["imagen"]["type"] == "image/jpg")
					|| ($_FILES["imagen"]["type"] == "image/png"))) {
					if ($_FILES["imagen"]["error"] > 0) {
						echo "Return Code: " . $_FILES["imagen"]["error"] . "<br>";
					} else {
						$extension = substr($_FILES["imagen"]["type"], 6);
						$rutaimagen = "images/imagenesperfil/" . $idusuario . '.' . $extension;
						if (file_exists($rutaimagen)) {
							unlink($rutaimagen);
							move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
							$editar .= ", imagen = '$rutaimagen'";
						} else {
							// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
							move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
							$editar .= ", imagen = '$rutaimagen'";
						}
					}
				} else {
					echo "Archivo Inv�lido";
				}
			}
			
			$editar .= "WHERE idusuario = '$idusuario'";
			
			$actualizardatos=consulta($editar);
			if(!$actualizardatos){
				echo "Mensaje: ".mysqli_error();
				mensaje("Error");
			} else {
				echo "Los datos se actualizaron con exito.";
				$_SESSION['email'] = $email;
				actualizarhistorial("Actualizo su informacion de perfil");
			}
		}
	} else {
		ir_a("perfil.php");
	}
?>