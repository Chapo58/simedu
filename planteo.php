<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Simulador Ludico</title>
    <?php require_once('head.php'); ?>
    <?php
      $con_p=consulta("SELECT * FROM planteos WHERE idprofesor='$idprofe' AND habilitado = 1");
      if(mysqli_num_rows($con_p)>0){
        $hayplanteo = true;
        $p=mysqli_fetch_array($con_p);
        $titulo = $p['titulo'];
        $idplanteo = $p['idplanteo'];
        if($p['limite'] == "0000-00-00"){
          $blimite = false;
        } else {
          $limite = date("d/m/Y",strtotime($p['limite']));
          $blimite = true;
        }
        $color = $p['semaforo'];
        if($color){
          $bsemaforo = true;
          $con_semaforo=consulta("SELECT $color FROM semaforos WHERE idprofesor='$idprofe'");
          $s=mysqli_fetch_array($con_semaforo);
          $semaforo = $s["$color"];
          if($color == "verde"){
            $color = "success";
            $alert = "#49A749";
          }elseif($color == "amarillo"){
            $color = "warning";
            $alert = "#F0AD4E";
          } else {
            $color = "danger";
            $alert = "#D9534F";
          }
        } else {
          $bsemaforo = false;
        }
        $con_m=consulta("SELECT multiple FROM preguntas WHERE idplanteo='$idplanteo' AND multiple = 1");
        if(mysqli_num_rows($con_m)>0){
          $bmultiple = true;
        } else {
          $bmultiple = false;
        }
      } else {
        $hayplanteo = false;
        $titulo = "Planteo";
      }

    ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-cubes home-icon"></i>
              <a href="#">Planteo</a>
            </li>
            <li class="active"><?php echo $titulo; ?></li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header"><?php echo $titulo; ?></h1>
	<?php if($hayplanteo){ ?>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<?php
				$indice = 0;
				$con_preguntas=consulta("SELECT preguntas.*, respuestas.respuesta, respuestas.opcion
          FROM preguntas LEFT JOIN respuestas ON preguntas.idpregunta = respuestas.idpregunta AND respuestas.idalumno = '$idusuario'
          WHERE preguntas.idplanteo='$idplanteo' ORDER BY preguntas.idpregunta ASC");
				while ($pr = mysqli_fetch_array($con_preguntas, MYSQLI_ASSOC)) {
					$descripcion = $pr['descripcion'];
					$idpregunta = $pr['idpregunta'];
					$multiple = $pr['multiple'];
					$indice++;
          if($pr['respuesta'] || $pr['opcion']){
            $clase = "respondido";
          } else {
            $clase = "orange-circle-button";
          }
			?>
			<button class="<?php echo $clase; ?>" onclick="traerDatos(<?php echo $idpregunta.",".$indice; ?>)" data-toggle="modal" data-target="#modalpreguntas">
				<?php echo $indice; ?>
			</button>
			<?php } ?>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br>

<?php $con_ar=consulta("SELECT * FROM archivos WHERE idplanteo='$idplanteo'");
		if(mysqli_num_rows($con_ar)>0){ ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">
			<span class="glyphicon glyphicon-dash"></span> Archivos Adjuntos
		</h3>
	</div>
	<div class="panel-body">
		<div class="row">
		<?php while ($ar = mysqli_fetch_array($con_ar, MYSQLI_ASSOC)) {
				if(esimagen($ar['ruta'])){
					$icono = "fa-picture-o";
				} else {
					$icono = "fa-file";
				}
		?>
			<div class="col-lg-2">
				<a href="<?php echo $ar['ruta']; ?>" class="btn btn-primary btn-lg btn-block dash-widget" role="button" style="padding:2px;">
					<div id="box_1"><span class="fa <?php echo $icono; ?> fa-3x"></span></div>
					<div id="box_2" class="icon-label"><?php echo $ar['nombre']; ?></div>
					<button class="btn btn-default btn-block">Ver</button>
				</a>
			</div>
		<?php } ?>
		</div>
	</div>
</div>
<?php } ?>

	<div class="row">
        <div class="col-lg-12">
			<?php if($blimite) { ?>
            <button onclick="tiempo()" class="btn btn-sq btn-danger" data-toggle="tooltip" title="Tiempo Limite">
                <i class="fa fa-clock-o fa-5x"></i>
            </button>
			<?php }
				if($bsemaforo) { ?>
			<button onclick="semaforo()" class="btn btn-sq btn-<?php echo $color; ?>" data-toggle="tooltip" title="Semaforo">
                <i class="fa fa-lightbulb-o fa-5x"></i>
            </button>
			<?php }
				if($bmultiple) { ?>
			<button class="btn btn-sq btn-info" data-toggle="tooltip" title="Planteo con preguntas Multiopciones">
                <i class="fa fa-cube fa-5x"></i>
            </button>
			<?php } ?>
      <form action="PDFs/planteos.php" style="float:right;" target="_blank" method="POST">
          <button type="submit" class="btn btn-sq btn-info" data-toggle="tooltip" title="Listar Planteo">
            <i class="fa fa-file-pdf-o fa-5x"></i>
          </button>
          <input type="hidden" name="idplanteo" value="<?php echo $idplanteo; ?>" >
      </form>
        </div>
	</div>
	<?php } else { ?>
		<h2 style="color:red;">El profesor no ha habilitado ningún planteo aun.</h2>
	<?php } ?>
  <!-- Final Page Content -->
<div id="modalpreguntas" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="titulopregunta"></h4>
      </div>
      <div class="modal-body">
	        <form id="cargarpregunta" class="form-horizontal" role="form">
			<div class="row">
	        <div class="col-md-12" id="contenidopregunta"></div>
			</div>
			<div class="row">
	            <div class="col-md-12 col-xs-12 text-right" id="showlink" style="display:none"><br>
					<a id="linkexterno" class="btn btn-info" target="_blank"><span class="fa fa-external-link"></span>&nbsp;&nbsp;Link</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12" id="esimagen" style="display:none"><br>
					<img id="imagenpregunta" alt="Imagen" class="img-rounded tooltipster img-responsive">
				</div>
				<div class="col-md-12 col-xs-12" id="noesimagen" style="display:none"><br>
					<a id="archivopregunta" target="_blank" class="btn btn-lg btn-info"><i class="fa fa-file fa-3x"></i></a>
				</div>
			</div><br>
			<div class="quiz" id="quiz" data-toggle="buttons" style="display:none">
			   <label class="element-animation1 btn btn-lg btn-primary btn-block" id="op1" style="display:none">
				<span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
				<input type="radio" name="q_answer" value="1"><div id="opcion1"></div>
			   </label>
			   <label class="element-animation2 btn btn-lg btn-primary btn-block" id="op2" style="display:none">
				<span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
				<input type="radio" name="q_answer" value="2"><div id="opcion2"></div>
				</label>
			   <label class="element-animation3 btn btn-lg btn-primary btn-block" id="op3" style="display:none">
			   <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
			   <input type="radio" name="q_answer" value="3"><div id="opcion3"></div>
			   </label>
			   <label class="element-animation4 btn btn-lg btn-primary btn-block" id="op4" style="display:none">
			   <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
			   <input type="radio" name="q_answer" value="4"><div id="opcion4"></div>
			   </label>
			   <label class="element-animation4 btn btn-lg btn-primary btn-block" id="op5" style="display:none">
			   <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
			   <input type="radio" name="q_answer" value="5"><div id="opcion5"></div>
			   </label>
			   <label class="element-animation4 btn btn-lg btn-primary btn-block" id="op6" style="display:none">
			   <span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span>
			   <input type="radio" name="q_answer" value="6"><div id="opcion6"></div>
			   </label>
		   </div>
       <div class="col-md-12 col-xs-12" id="divrespuesta"><br>
         <textarea class="form-control" rows="3" name="respuesta" id="respuesta" placeholder="Respuesta"></textarea>
       </div>
       <input type="hidden" name="idpregunta" id="idpregunta">
       <input type="hidden" name="idplanteo" value="<?php echo $idplanteo; ?>" >
		   <div class="modal-footer text-muted">
				<span id="answer"></span>
			 </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="botoncancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		    <button type="submit" id="botonaceptar" class="btn btn-primary">Aceptar</button>
        <button type="button" id="botonok" class="btn btn-default" data-dismiss="modal" style="display:none">Aceptar</button>
			</form>
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
  <!-- ============================================================= -->
<script type="text/javascript">
  var incc = 0;
  var idp = 0;
  <?php echo "var idplanteo = ".$idplanteo.";"; ?>
	function tiempo(){
		var dialog = new BootstrapDialog({
			message: function(){
				var $message = $("<div>La fecha limite para la entrega del planteo es: <strong><?php echo $limite; ?></strong></div>");
				return $message;
			}
		});
		dialog.realize();
		dialog.getModalHeader().hide();
		dialog.getModalFooter().hide();
		dialog.getModalBody().css('background-color', "#CCCCCC");
		dialog.getModalBody().css('color', '#000');
		dialog.open();
	};
	function semaforo(){
		var dialog = new BootstrapDialog({
			message: function(){
				var $message = $("<div><strong><?php echo $semaforo; ?></strong></div>");
				return $message;
			}
		});
		dialog.realize();
		dialog.getModalHeader().hide();
		dialog.getModalFooter().hide();
		dialog.getModalBody().css('background-color', "<?php echo $alert; ?>");
		dialog.getModalBody().css('color', '#fff');
		dialog.open();
	};
	function traerDatos(id,indice){
    $("#idpregunta").val(id);
		$("#titulopregunta").html("Pregunta " + indice);
		$('#showlink').hide();
    $('#answer').hide();
		$('#esimagen').hide();
		$('#noesimagen').hide();
		$('#quiz').hide();
    $('#divrespuesta').show();
    $("#botonaceptar").show();
    $("#botoncancelar").show();
    $("#botonok").hide();
    $('#op1').hide();$('#op2').hide();$('#op3').hide();$('#op4').hide();$('#op5').hide();$('#op6').hide();
    $("#respuesta").val("");
    idp = id;
		$.ajax(
        {
            type: "GET",
            url: "pregunta_datos.php",
            data: {id:id},
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
				$("#contenidopregunta").html(data['pregunta']);
        $("#respuesta").val(data['respuesta']);
				if(data['link']){
					$('#showlink').show();
					$("#linkexterno").attr('href', data['link']);
				}
				if(data['ruta']){
					if(data['esimagen']){
						$('#esimagen').show();
						$("#imagenpregunta").attr('src', data['ruta']);
					} else {
						$('#noesimagen').show();
						$("#archivopregunta").attr('href', data['ruta']);
					}
				}
				if(data['multiple'] == 1){
          $('#divrespuesta').hide();
          // Si ya cargo una respuesta no muestro las preguntas
          if(!data['opcion']){
            $('#quiz').show();
          } else {
            $( "#answer" ).show();
            if(data['opcion'] != data['incc']){
              $( "#answer" ).html('<strong style="color:red;">INCORRECTA</strong>');
            } else {
              $( "#answer" ).html('<strong style="color:green;">CORRECTA</strong>');
            }
          }

					if(data['opcion1']){
						$('#op1').show();
						$("#opcion1").html(data['opcion1']);
					}
					if(data['opcion2']){
						$('#op2').show();
						$("#opcion2").html(data['opcion2']);
					}
					if(data['opcion3']){
						$('#op3').show();
						$("#opcion3").html(data['opcion3']);
					}
					if(data['opcion4']){
						$('#op4').show();
						$("#opcion4").html(data['opcion4']);
					}
					if(data['opcion5']){
						$('#op5').show();
						$("#opcion5").html(data['opcion5']);
					}
					if(data['opcion6']){
						$('#op6').show();
						$("#opcion6").html(data['opcion6']);
					}
          $("#botonaceptar").hide();
          $("#botoncancelar").hide();
          $("#botonok").show();
          fn(data['incc']);
				}
            },
			error: function (xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");
				alert(err.Message);
			}
        });
	};
  	function fn(incc2){
      incc = incc2;
    }
	$(document).ready(function() {

    $("#cargarpregunta").on('submit',function(event){
      event.preventDefault();
      console.log($(this));
      data = $(this).serialize();
      $.ajax({
        type: "POST",
        url: "planteos_respuesta.php",
        data: data
        }).done(function( msg ) {
          window.location.reload();
        });
    });

	});

$(function(){
    $("label.btn").on('click',function () {
    	var choice = $(this).find('input:radio').val();
    	$('#quiz').fadeOut();
    	setTimeout(function(){
           $( "#answer" ).html(  $(this).checking(choice) );
         //  $('#quiz').show();
    	}, 1500);
    });
    $.fn.checking = function(ck) {
        $.ajax({
        type: "POST",
        url: "planteos_respuesta.php",
        data: {idpregunta:idp, incc:ck, idplanteo:idplanteo}
        }).done(function() {
        });
        if (ck != incc)
            return 'INCORRECTA';
        else
            return 'CORRECTA';
    };

});

</script>
</body>
</html>
