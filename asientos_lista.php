<!DOCTYPE html>
<html lang="es">
<head>
    <?php include_once('head.php'); ?>
    <title>Simedu | Lista Asientos</title>
</head>

<body class="no-skin">
    <?php include_once('header.php'); ?>
	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		}
	?>
    <!-- Page Content -->
    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Simulador Contable</a>
            </li>
            <li class="active">Asientos Contables</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

    <div id="divPeriodos">
         <h1 class="page-header">Asientos Contables</h1>
		 <a href="asientos.php" class="btn btn-lg btn-primary">Crear Asiento</a>
		 <a href="asientos_modelo.php" style="float:right;" class="btn btn-lg btn-info">Crear Modelo Asiento</a>

        <div class="space50"></div>

        <div class="row">
            <div class="col-xs-12">
	<table id="lista" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>N°</th>
                <th>Denominacion</th>
                <th>Fecha</th>
                <th>Debe</th>
				<th>Haber</th>
				<th></th>
				<th></th>
            </tr>
        </thead>
        <tbody>
			<?php
				$con_listas=consulta("SELECT * FROM listaasientos WHERE idempresa='$idempresa' AND idperiodo='$idperiodo'");
				$sumadebe = 0;
				$sumahaber = 0;
				while ($l = mysqli_fetch_array($con_listas, MYSQLI_ASSOC)) {
					$id = $l['idlista'];
					$numero = $l['nlista'];
					$denominacion = $l['denominacion'];
					$fecha = fecha($l['fecha'],"/");
					$debe = $l['debe'];
					$haber = $l['haber'];
					$sumadebe += $debe; $sumahaber += $haber;
			?>
            <tr>
				<td><?php echo $numero; ?></td>
                <td><?php echo $denominacion; ?></td>
                <td><?php echo $fecha; ?></td>
                <td><?php echo number_format($debe, 2, ',', '.'); ?></td>
				<td><?php echo number_format($haber, 2, ',', '.'); ?></td>
				<td class="text-center">
					<form action="asientos.php" method="POST">
                        <button type="submit" class="btn btn-primary" title="Editar">
							<span class="fa fa-pencil" aria-hidden="true"></span>
						</button>
                        <input type="hidden" name="idlista" id="idlista" value="<?php echo $id; ?>" >
                    </form>
				</td>
				<td class="text-center">
					<form class="delform">
                        <button type="submit" class="btn btn-danger" title="Eliminar">
							<span class="fa fa-trash-o" aria-hidden="true"></span>
						</button>
						<input type="hidden" id="dellis" name="dellis" value="<?php echo $id; ?>" >
                    </form>
				</td>
            </tr>
		<?php } ?>
        </tbody>
		<tfoot>
			<tr>
				<th></th>
                <th></th>
                <th></th>
                <th>Total: <?php echo number_format($sumadebe, 2, ',', '.'); ?></th>
				<th>Total: <?php echo number_format($sumahaber, 2, ',', '.'); ?></th>
				<th></th>
				<th></th>
            </tr>
		</tfoot>
    </table>
            </div>
        </div>
    </div>

    <div class="space50"></div>

    <!-- Final Page Content -->
  </div>
</div>
</div><!-- /.main-content -->
<?php require_once('footer.php'); ?>
<script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function () {
		var table = $('#lista').DataTable({
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});

		$('#lista').on('click', '.delform', function () {
			event.preventDefault();
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Listado',
				message: '¿Esta seguro que desea eliminar este listado?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});
	});
    </script>
</body>
</html>
