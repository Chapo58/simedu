<?php	
	$con_empresa=consulta("SELECT empresas.*, usuario.idusuario 
				FROM usuario INNER JOIN empresas ON empresas.idusuario = usuario.idusuario 
				WHERE usuario.tipousuario=85");
	while ($em = mysqli_fetch_array($con_empresa, MYSQLI_ASSOC)) {
		$idtempempresa = $em['idempresa'];
		$rsoc = str_replace(' ', '', $em['rsocial']);
		$cont = 1; $cont2 = 1;
?>
<div id="<?php echo $rsoc; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo $em['rsocial']; ?></h4>
	  	</div>
	  	<div class="modal-body"> <!-- Inicio del body -->
	    	<ul class="nav nav-pills" role="tablist">
			<?php	
				$con_fichas=consulta("SELECT * FROM fichas WHERE idempresa='$idtempempresa'");
				while ($fic = mysqli_fetch_array($con_fichas, MYSQLI_ASSOC)) {
			?>
			    <li <?php if($cont == 1) { echo 'class="active"'; } ?>><a href="<?php echo "#" . $fic['idficha']; ?>" role="tab" data-toggle="tab"><?php echo $fic['titulo']; ?></a></li>
			<?php $cont = 0; } ?>	
		  	</ul>

			<div class="tab-content">
			<?php	
				$con_fichas=consulta("SELECT * FROM fichas WHERE idempresa='$idtempempresa'");
				while ($fic = mysqli_fetch_array($con_fichas, MYSQLI_ASSOC)) {
			?>
			    <div id="<?php echo $fic['idficha']; ?>" class="tab-pane fade <?php if($cont2 == 1) { echo 'in active'; } ?>">
			        <div class="row">
			            <div class="col-xs-12" style="padding-top: 50px">
						<?php if ($fic['imagen'] != "") { ?>
			            	<div class="text-center">
			                	<img src="<?php echo $fic['imagen']; ?>" class="img-circle img-user tooltipster" title="Imagen de ficha">
		                	</div>
		                	<div class="space50"></div>
						<?php } ?>
		                	<p>
								<?php echo $fic['texto']; ?>
							</p>
			            </div>
			        </div>
			    </div>
			<?php $cont2 = 0; } ?>
			</div>
	  	</div> <!-- Final del body -->
		<div class="modal-footer">
			<div class="text-right">
				<!--<a href="#" target="_blank">Página oficial</a> | 
				<a href="#" target="_blank">Tienda virtual</a> -->
			</div>
			<?php if(!empty($em['telefonocontacto']) || !empty($em['emailcontacto']) || !empty($em['nombrecontacto'])){ ?>
			<p><strong>Contacto:</strong> <?php echo $em['telefonocontacto']; ?> | <?php echo $em['emailcontacto']; ?> | <?php echo $em['nombrecontacto']; ?>.</p>
			<?php } ?>
		</div>
	</div>
</div>
</div>
<?php } ?>