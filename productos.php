<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Productos</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="simempresarial.php">Simulador Empresarial</a>
            </li>
            <li class="active">Productos</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		}
        if(isset($_POST['edart']) && !empty($_POST['edart'])){
            $ideditar = $_POST['edart'];
            $con_editar=consulta("SELECT * FROM productos WHERE idproducto='$ideditar'");
            $editar=mysqli_fetch_array($con_editar);
			$titulo = "Modificar Producto " . $editar['nombre'];
			$accion = "producto_modificar.php";
			$txtboton = "Guardar Cambios";

			$idproducto = $editar['idproducto'];
			$modnombre = $editar['nombre'];
			$moddescripcion = $editar['descripcion'];
			$modpreciocosto = $editar['preciocosto'];
			$modpreciovta = $editar['preciovta'];
			$modiva = $editar['iva'];
			$modstock = $editar['stock'];
			$modstockmin = $editar['stockmin'];
			$imagen = $editar['imagen'];
			$modcategoria = $editar['idcategoria'];
			$modmarca = $editar['idmarca'];
			$modproveedor = $editar['idproveedor'];
			$modlista = $editar['idlistap'];
		//	$modcuenta = $editar['ncuenta'];
			$habilitado = $editar['habilitado'];

		} else {
			$titulo = "Agregar Producto";
			$accion = "producto_cargar.php";
			$txtboton = "Aceptar";
			$imagen = "images/logo.png";
			$habilitado = 1;
		}
	?>


    <!-- Page Content -->
    <div id="divProductos">
        <h1 class="page-header">Productos</h1>
		<input type="button" id="btnNuevo" onclick="switchDivs();" class="btn btn-lg btn-primary" value="Agregar Producto">
		<a href="PDFs/productos.php" target="_blank" class="btn btn-lg btn-info" style="float:right;"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir Listado de Articulos</a>
		<div class="space20"></div><br /><br />
        <div class="row">
            <div class="col-xs-12">
                <table id="tabla" class="table table-striped table-bordered" cellspacing="0" style="width:80%;margin:auto;">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Precio Vta.</th>
                            <th>Stock</th>
                            <th>Categoria</th>
              							<th>Marca</th>
              							<th>Proveedor</th>
              							<th>N° Articulo</th>
              							<th>Habilitado</th>
              							<th>Editar</th>
              							<th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
							$con_art=consulta("SELECT productos.*, categorias.descripcion as descripcioncat, marcas.descripcion as descripcionmar, proveedores.rsocial
							FROM productos LEFT JOIN categorias ON productos.idcategoria = categorias.idcategoria
							LEFT JOIN marcas ON productos.idmarca = marcas.idmarca
							LEFT JOIN proveedores ON productos.idproveedor = proveedores.idproveedor
							WHERE productos.idempresa='$idempresa'");
							while ($art = mysqli_fetch_array($con_art, MYSQLI_ASSOC)) {
								$idproducto2 = $art['idproducto'];
								$pimagen = $art['imagen'];
								$pnombre = $art['nombre'];
								$pdescripcion = $art['descripcion'];
								$pprecio = $art['preciovta'];
								$pstock = $art['stock'];
								$pcategoria = $art['descripcioncat'];
								$pmarca = $art['descripcionmar'];
								$pproveedor = $art['rsocial'];
								$phabilitado = $art['habilitado'];
						?>
                        <tr>
                            <td class="text-center"><img src=<?php echo $pimagen; ?> class="img-circle img-empresa-min"></td>
                            <td><?php echo $pnombre; ?></td>
                            <td><?php echo substr($pdescripcion, 0, 10)."..."; ?></td>
                            <td><?php echo number_format($pprecio, 2, ',', '.'); ?></td>
                            <td><?php
								if($pstock == 0) echo "<strong style='color:red;'>".$pstock."</strong>"; else echo $pstock;
							?></td>
							<td><?php echo $pcategoria; ?></td>
							<td><?php echo $pmarca; ?></td>
							<td><?php echo $pproveedor; ?></td>
							<td><?php echo $idproducto2; ?></td>
							<td align="center">
							<?php if($phabilitado == 1){ ?>
								<span class="glyphicon glyphicon-ok"></span>
							<?php } else { ?>
								<span class="glyphicon glyphicon-remove"></span>
							<?php } ?>
							</td>
							<td class="text-center">
                                <form action="productos.php" method="POST">
                                    <button type="submit" class="btn btn-primary" title="Editar">
										<span class="fa fa-pencil" aria-hidden="true"></span>
									</button>
                                <input type="hidden" name="edart" id="edart" value="<?php echo $idproducto2; ?>" >
                                </form>
							</td>
							<td class="text-center">
                                <form class="delform">
                                    <button type="submit" class="btn btn-danger" title="Eliminar">
										<span class="fa fa-trash-o" aria-hidden="true"></span>
									</button>
									<input type="hidden" id="delart" name="delart" value="<?php echo $idproducto2; ?>" >
                                </form>
                            </td>
                        </tr>
						<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="space50"></div>

    </div>
    <div id="divNuevaEditar" class="sr-only">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
		<?php if(!isset($_POST['edart'])) { ?>
        <div class="bs-callout bs-callout-warning">
            <h4>Categorias, Marcas y Proveedores</h4>
			<p>Para cargar un producto es recomendable cargar previamente algunas <a href="categoriasymarcas.php">categorias, marcas</a> y <a href="proveedores.php">proveedores</a>.</p>
        </div>
		<?php } ?>
        <form role="form" data-toggle="validator" action="<?php echo $accion; ?>" method="POST" enctype="multipart/form-data">
		<div class="row">
		<div class="col-md-8 col-md-offset-2">
        <div class="form-group">
			<label for="nombre">Nombre del Producto</label>
            <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre del producto" required>
            <div class="help-block with-errors"></div>
        </div>
		</div>
		<div class="col-md-2">
		<div class="form-group" align="center" style="margin-top:27px;">
			<div class="input-group">
        <span class="button-checkbox">
    			<button type="button" class="btn" data-color="warning"><strong>Habilitado</strong></button>
    			<input type="checkbox" id="habilitado" name="habilitado" class="hidden" />
    		</span>
			</div>
		</div>
		</div>
		<div class="col-md-12">
        <div class="form-group">
			<label for="descripcion">Descripcion</label>
            <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Descripcion del producto">
				<?php 
					if(isset($_POST['edart']) && !empty($_POST['edart'])){
						echo $moddescripcion;
					}
				?>
			</textarea>
        </div>
		</div>
		<div class="col-md-4">
        <div class="form-group">
			<label for="precio">Precio de Costo</label>
            <input type="number" id="precio" step="any" name="precio" class="form-control" placeholder="Precio de Costo">
        </div>
		</div>
		<div class="col-md-4">
		<div class="form-group">
			<label for="iva">IVA</label>
            <select class="form-control" name="iva" id="iva">
				<option selected disabled>IVA</option>
                <?php
				$con_per=consulta("select * from iva where idempresa='$idempresa'");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idiva = $p['idiva'];
					$deno = $p['denominacion'];
				?>
				<option value = '<?php echo $idiva;?>'><?php echo $deno; ?></option>
				<?php } ?>
            </select>
        </div>
		</div>
		<div class="col-md-4">
		<div class="form-group">
			<label for="preciovta">Precio de Venta (Neto)</label>
            <input type="number" step="any" id="preciovta" name="preciovta" class="form-control" placeholder="Precio de Venta" required>
        </div>
		</div>
		<input type="hidden" name="idart" id="idart" value="<?php echo $idproducto; ?>" >
        <div class="col-md-2">
		<div class="form-group">
			<label for="stock">Stock</label>
            <input type="number" id="stock" name="stock" class="form-control" value=1 placeholder="Stock Disponible" required>
            <div class="help-block with-errors"></div>
        </div>
		</div>
		<div class="col-md-2">
		<div class="form-group">
			<label for="stock">Stock Minimo</label>
            <input type="number" id="stockmin" name="stockmin" class="form-control" value=1 placeholder="Stock Minimo">
            <div class="help-block with-errors"></div>
        </div>
		</div>
		</div>
		<div class="row">
		<h4></h4>
		<div class="col-md-6">
        <div class="form-group">
            <select class="form-control" name="lista" id="lista" onchange="calcularPrecio(this.value);">
				<option selected disabled>Lista de Precios</option>
                <?php
				$con_per=consulta("SELECT * FROM listaprecios WHERE idempresa='$idempresa'");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idlista = $p['idlista'];
					$descr = $p['descripcion'];
				?>
				<option value = '<?php echo $idlista;?>'><?php echo $descr; ?></option>
				<?php } ?>
            </select>
        </div>
		</div>
		<div class="col-md-6">
        <div class="form-group">
            <select class="form-control" name="categoria" id="categoria">
				<option selected disabled>Seleccionar Categoria</option>
                <?php
				$con_per=consulta("select * from categorias where idempresa='$idempresa'");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idcategoria = $p['idcategoria'];
					$descr = $p['descripcion'];
				?>
				<option value = '<?php echo $idcategoria;?>'><?php echo $descr; ?></option>
				<?php } ?>
            </select>
        </div>
		</div>
		<div class="col-md-6">
		<div class="form-group">
            <select class="form-control" name="marca" id="marca">
				<option selected disabled>Seleccionar Marca</option>
                <?php
				$con_per=consulta("select * from marcas where idempresa='$idempresa'");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idmarca = $p['idmarca'];
					$descr = $p['descripcion'];
				?>
				<option value = '<?php echo $idmarca;?>'><?php echo $descr; ?></option>
				<?php } ?>
            </select>
        </div>
		</div>
		<div class="col-md-6">
        <div class="form-group">
            <select class="form-control" name="proveedor" id="proveedor">
				<option selected disabled>Seleccionar Proveedor</option>
				<?php
				$con_per=consulta("select * from proveedores where idempresa='$idempresa'");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idprov = $p['idproveedor'];
					$descr = $p['rsocial'];
				?>
				<option value = '<?php echo $idprov;?>'><?php echo $descr; ?></option>
				<?php } ?>
            </select>
        </div>
		</div>
		</div>
		<div class="col-md-6">
		<div class="form-group text-center">
            <img src=<?php echo $imagen; ?>  id="imagenmuestra" class="img-circle img-user tooltipster" title="Imagen del Producto" alt="Imagen del Producto">
            <div class="fileUpload btn btn-default">
        		<span>Seleccionar imagen</span>
        		<input type="file" name="imagen" id="imgInp" class="upload" />
			</div>
        </div>
		</div>

		<div class="col-md-12">
        <div class="form-group">
            <div class="pull-right">
                <button type="button" id="btnAtras" class="btn btn-default btn-lg" onclick="window.location.href='productos.php';">Atrás</button>
				<button type="submit" id="btnAceptar" class="btn btn-primary btn-lg"><?php echo $txtboton; ?></button>
            </div>
        </div>
		</div>
		</div>
        </form>
    </div>
    <!-- Final Page Content -->
  </div>
 </div>
</div><!-- /.main-content -->
<br><br><br><br><br>
<?php require_once('footer.php'); ?>

    <script type="text/javascript">
	var ids = [];
	var por1 = [];
	var por2 = [];
	var por3 = [];
	var por4 = [];
	<?php
		$con_por=consulta("SELECT * FROM listaprecios WHERE idempresa = $idempresa");
		while ($p = mysqli_fetch_array($con_por, MYSQLI_ASSOC)) {
			$id = $p['idlista'];
			if($p['por1'] < 10){ $p1 = 0 . str_replace('.','',$p['por1']); }else{ $p1 = str_replace('.','',$p['por1']); }
			if($p['por2'] < 10){ $p2 = 0 . str_replace('.','',$p['por2']); }else{ $p2 = str_replace('.','',$p['por2']); }
			if($p['por3'] < 10){ $p3 = 0 . str_replace('.','',$p['por3']); }else{ $p3 = str_replace('.','',$p['por3']); }
			if($p['por4'] < 10){ $p4 = 0 . str_replace('.','',$p['por4']); }else{ $p4 = str_replace('.','',$p['por4']); }
			echo "ids.push(\"$id\");";
			echo "por1.push(\"1.$p1\");";
			echo "por2.push(\"1.$p2\");";
			echo "por3.push(\"1.$p3\");";
			echo "por4.push(\"1.$p4\");";
		}
	?>
	function calcularPrecio(id){
		var precio = $('#precio').val();
		if(precio > 0){
			for(var x = 0 ; x < ids.length ; x++) {
				if(ids[x] == id) {
					if(por1[x]>0) precio = precio * parseFloat(por1[x]);
					if(por2[x]>0) precio = precio * parseFloat(por2[x]);
					if(por3[x]>0) precio = precio * parseFloat(por3[x]);
					if(por4[x]>0) precio = precio * parseFloat(por4[x]);
				}
			}
		//	precio = precio.replace(",", ".");
			precio = parseFloat(precio);
			$('#preciovta').val(precio);
		} else {
			// dialogo y setear de nuevo en nada el select
		}
	}

/*	function getCuenta(valor) {
		$("#ncuenta").val(valor);
	};

	function getDenominacion(valor) {
		$("#scuenta").val(valor);
	};*/

    $(document).ready(function() {

        var table = $('#tabla').dataTable({
			"order": [[ 1, "asc" ]],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ productos",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Seleccione "Agregar Producto" para cargar su primer producto.',
				"sInfo":           "Mostrando productos del _START_ al _END_ de un total de _TOTAL_ productos",
				"sInfoEmpty":      "Mostrando productos del 0 al 0 de un total de 0 productos",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ productos)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
        });

		//Cambiar imagen de perfil
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#imagenmuestra').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#imgInp").change(function(){
			readURL(this);
		});

		$('#tabla').on('click', '.delform', function () {
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Producto',
				message: '¿Esta seguro que desea eliminar este producto?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

		<?php
			if(isset($_POST['edart']) && !empty($_POST['edart'])){
				echo "switchDivs();";
				echo "$( '#nombre' ).val( '$modnombre' );";
				echo "$( '#precio' ).val( '$modpreciocosto' );";
				echo "$( '#preciovta' ).val( '$modpreciovta' );";
				echo "$( '#iva' ).val( '$modiva' );";
				echo "$( '#stock' ).val( '$modstock' );";
				echo "$( '#stockmin' ).val( '$modstockmin' );";
				echo "$( '#categoria' ).val( '$modcategoria' );";
				echo "$( '#marca' ).val( '$modmarca' );";
				echo "$( '#proveedor' ).val( '$modproveedor' );";
				echo "$( '#lista' ).val( '$modlista' );";
			//	echo "$( '#ncuenta' ).val( '$modcuenta' );";echo "$( '#scuenta' ).val( '$modcuenta' );";
			}
		?>
		<?php if($habilitado == 1){ ?>
		$('#habilitado').prop('checked', true);
		<?php } ?>
	});


    function switchDivs(){
        $("#divProductos").toggleClass('sr-only');
        $("#divNuevaEditar").toggleClass('sr-only');
    }
    </script>
    <!-- ============================================================= -->
</body>
</html>
