<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Centro de Ayuda</title>
    <?php
    	session_start();
    	require_once('funciones.php');
    		if(isset($_SESSION['email']) && isset($_SESSION['idusuario'])){
    			$email = $_SESSION['email'];
    			$idusuario = $_SESSION['idusuario'];
    			$con_usr=consulta("SELECT * FROM usuario WHERE idusuario='$idusuario'");
    			$u=mysqli_fetch_array($con_usr);
    			$idprofe = $u['idprofesor'];
    			if($u['habilitado'] == 1){
    			if($u['imagen']){ $imagenperfil = $u['imagen'];} else { $imagenperfil = "images/logo.png";}
    			if($u['idempresa'] != ""){
    				$idempresa = $_SESSION['idempresa'] = $u['idempresa'];
    				$con_emp=consulta("SELECT empresas.*, periodos.periodo FROM empresas LEFT JOIN periodos ON empresas.idperiodo = periodos.idperiodo
    				WHERE idempresa='$idempresa'");
    				$emp=mysqli_fetch_array($con_emp);
    				$_SESSION['rsocial'] = $emp['rsocial'];
    				$emodcontable = $emp['modcontable'];
    				$emodempresarial = $emp['modempresarial'];
    				$eimpexp = $emp['impexp'];
    			} else {
    				$emodcontable = 1;
    				$emodempresarial = 1;
    				$emodludico = 1;
    				$eimpexp = 1;
    			}
    			if($u['idperiodo'] != ""){
    				$idperiodo = $_SESSION['idperiodo'] = $u['idperiodo'];
    				$con_p=consulta("select periodo, descripcion from periodos where idperiodo='$idperiodo'");
    				$peri=mysqli_fetch_array($con_p);
    				$_SESSION['periodo'] = $peri['periodo'];
    				$_SESSION['descripcionperiodo'] = $peri['descripcion'];
    			}
    			} else {
    				mensaje("Su usuario se encuentra deshabilitado para la utilizacion del sistema.");
    				session_unset();
    				session_destroy();
    				setcookie ("id_usuario_dw", "", 1);
    				setcookie ("marca_aleatoria_usuario_dw", "", 1);
    				ir_a('index.php');
    			}
    		} else {
    			session_unset();
    			session_destroy();
    			setcookie ("id_usuario_dw", "", 1);
    			setcookie ("marca_aleatoria_usuario_dw", "", 1);
    			header('Location: index.php');
    		};
    		// Cambio de Periodo
    		if(isset($_POST['idperiodo']) && !empty($_POST['idperiodo'])) {
    			$idp = $_SESSION['idperiodo'] = $_POST['idperiodo'];
    			$con_p=consulta("select * from periodos where idperiodo='$idp'");
                $peri=mysqli_fetch_array($con_p);
    			$_SESSION['descripcionperiodo'] = $peri['descripcion'];
    			consulta("UPDATE usuario SET idperiodo = '$idp' WHERE idusuario = '$idusuario'");
    			consulta("UPDATE empresas SET idperiodo = '$idp' WHERE idempresa = '$idempresa'");
    		}
    		// Cambio Empresa
    		if(isset($_POST['empr']) && !empty($_POST['empr'])){
                $empr = $_POST['empr'];
                $con_emp=consulta("select * from empresas where idempresa='$empr'");
                $emp=mysqli_fetch_array($con_emp);
    			$_SESSION['rsocial'] = $emp['rsocial'];
    			$periodo = $emp['idperiodo'];
                $_SESSION['idempresa'] = $empr;
    			$con_p=consulta("select * from periodos where idperiodo='$periodo'");
                $peri=mysqli_fetch_array($con_p);
    			$_SESSION['descripcionperiodo'] = $peri['descripcion'];
    			consulta("UPDATE usuario SET idempresa = '$empr' WHERE idusuario = '$idusuario'");
    			consulta("UPDATE usuario SET idperiodo = '$periodo' WHERE idusuario = '$idusuario'");
            }
    ?>
    <meta charset="iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="description" content="Simedu Simulador Empresarial Contable Educativo">
    <meta name="author" content="Luciano Ciattaglia">

    <!-- favicon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

    <!-- estilos propios -->
    <link href="css/style.css" rel="stylesheet">

    <!-- text fonts -->
    <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />
</head>

<body data-spy="scroll" data-target="#myScrollspy">
     <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="jumbotron">
                  <div class="container">
                    <h1>Centro de Ayuda</h1>
                    <p>Bienvenido al centro de ayuda de Simedu. Desplázate por el menú para ver las diferentes categorías.</p>
                    <p><a class="btn btn-primary btn-lg" href="inicio.php" role="button">Ir al inicio »</a></p>
                  </div>
                </div>

                <div class="col-xs-3" id="myScrollspy">
                    <ul class="nav nav-tabs nav-stacked" data-spy="affix" data-offset-top="360">
          <li><a href="#misempresas">Mis Empresas</a></li>
						<ul>
						  <li><a href="#empresas">Empresas</a></li>
						  <li><a href="#institucional">Institucional</a></li>
              <li><a href="#shopping">Shopping</a></li>
						</ul>
          <li><a href="#contable">Simulador Contable</a></li>
						<ul>
  						<li><a href="#cuentas">Plan de Cuentas</a></li>
  						<li><a href="#periodos">Periodos</a></li>
  						<li><a href="#asientos">Carga de Asientos</a></li>
  						<li><a href="#listados">Listados</a></li>
						</ul>
          <li><a href="#empresarial">Simulador Empresarial</a></li>
          <li><a href="#otros">Otros</a></li>
            <ul>
              <li><a href="#imagenes">Imagenes Soportadas</a></li>
            </ul>
                    </ul>
                </div>

                <div class="col-xs-9" id="col-principal">
                    <h2 id="misempresas" class="page-header">Mis Empresas</h2>
					<h3 id="empresas">Empresas</h3>
					<p>Lo fundamental del proyecto es que desarrollamos un software educativo de simulación de gestión de una empresa,  que permita a los estudiantes un primer contacto con el fascinante mundo de los negocios y del empresario.</p>
					<p>El fin último, es que los alumnos aprendan divirtiéndose y viviendo una experiencia, a pesar de que todo tenga como base una simulación. los estudiantes podrán formar sus propias empresas virtuales y tener a cargo sus decisiones</p>
					<p>A través de la opción <a href="empresas.php">Empresas</a> podemos definir nuestras empresas con datos básicos como son Nombre de Fantasia, Cuit, Domicilio, Periodo fiscal, Plan de cuenta modelo a utilizar. Recordemos que lo primero en definir es el Periodo Fiscal, ver <a href="inicio.php">Inicio</a> opción <a href="comienzo.php">Que debo hacer</a></p>

					<h3 id="institucional">Institucional</h3>
                    <p>A través de esta opción podemos  cumplir con los siguientes módulos:</p>
					<ul>
					<li><strong>Módulo de generación de la empresa virtual:</strong> Se realiza una definición conceptual y a nivel informativo (razón social, nombre de fantasía, cuit,  dirección, etc) de cada empresa virtual.</li>
					<li><strong>Módulo de generación de vista operativa de la empresa virtual:</strong> permite completar la información de la empresa, definir la misión, visión, mercados y valores de la misma. </li>
					<li><strong>Módulo de definición de vista comercial de la empresa virtual:</strong> permite definir objetivos comerciales, productos y/o servicios a comercializar, categorizarlos. Permite armar y manejar conceptos de catálogos de los productos que se van a ofrecer, definición de lineamientos de imagen.</li>
					</ul>
					<p>
					Una vez que cargamos esta información podemos visualizarla accediendo a nuestro <a href="perfil.php">Perfil</a> en la opcion "Ver Perfil".
					Desde el ítem "Institucional" de cada empresa podemos ver información categorizada por solapas según fuimos incorporando para relacionarla con la empresa en cuestión, como Quienes Somos, Misión, Mercados, Valores, etc.
					El máximo de solapas es cuatro (4).
					</p>

                    <h3 id="shopping">Shopping</h3>
                    <p>Permite definir objetivos comerciales, productos y/o servicios a comercializar, categorizarlos. Permite armar y manejar conceptos de catálogos de los productos que se van a ofrecer, definición de lineamientos de imagen.</p>
					<p>Los productos o servicios ofrecidos con su denominación, foto, precio de venta son algunos de ítem que podemos incorporar aquí.</p>
					<p>Otros alumnos y profesores pueden ver nuestras empresas en su totalidad con una definición detallada y ordenada, también podemos imprimir en un listado con todo lo relacionado con nuestras empresas.</p>

                    <h2 id="contable" class="page-header">Simulador Contable</h2>

					<h3 id="cuentas">Plan de Cuentas</h3>
                    <p>Al definir una empresa seleccionamos un modelo por defecto, el mismo nos sirve para tener una idea de cómo empezar a trabajar.</p>
					<p>Cada alumno tendrá la tarea a acondicionar el mismo a sus necesidades según la empresa creada, pudiendo modificar, eliminar y dar de alta otras cuentas.</p>
					<p>Sugerimos cargar las cuentas imputables en minúsculas y la cuentas integradoras en mayúsculas, contamos con una opción de <a href="cuentas.php">Imprimir Plan de Cuentas</a></p>
					<p><strong>Modelo de Plan de cuentas default:</strong> Este modelo está cargado en forma genérica respetando las cuentas integradoras y de movimiento llamadas imputables.</p>

					<h3 id="periodos">Periodos</h3>
                    <p>Esta opción es la primera que tenemos que definir antes que nada ya que a través de la misma activamos  en al definir una Empresa un Periodo Fiscal.</p>
					<p>Primero: Crear un nuevo periodo, los datos solicitados son:</p>
					<ul>
					<li><strong>Periodo:</strong> Cuatro dígitos numérico. Ejemplo: <i>2015</i>.</li>
					<li><strong>Denominación:</strong>  Descripción, Ejemplo: <i>Periodo Fiscal 2015</i>, es muy importante incorporar el año ya que luego tendemos que activarlo. En la pantalla en la parte superior nos mostrara el periodo Fiscal Activo.</li>
					</ul>
					<p>
					Una vez creado el periodo ir a la opción <strong><i>Seleccionar Periodo</i></strong>, elegir el periodo recientemente cargado y <strong>Activarlo</strong>, eso quiere decir, seleccionarlo  con el mouse.
					En la pantalla aparecerá Periodo Activo: Ej <i>Periodo Fiscal 2015</i>.
					</p>

					<h3 id="asientos">Carga de Asientos</h3>
                    <p>A través de esta opción podemos definir asientos contables, recordar que un asiento tiene un número, una denominación, Fecha de imputación y movimientos llamadas imputaciones.</p>
					<p>El sistema te habilita dos imputaciones como mínimo pero a través de la opción <strong><i>Nueva Imputación</i></strong> podemos incorporar tantas imputaciones como necesitemos para definir nuestro asiento.</p>

					<h3 id="listados">Listados</h3>
                    <p>Una vez definidos los asientos contables podemos emitir los siguientes Listados:</p>
					<ul>
					<li><a href="librodiario.php">Libro Diario</a></li>
					<li><a href="libromayor.php">Libro Mayor</a></li>
					<li><a href="balancesumasysaldos.php">Balance de Sumas y Saldos</a></li>
					<li><a href="balancegeneral.php">Balance General</a></li>
					<li><a href="flujodefondos.php">Flujo de Fondos</a></li>
					</ul>

          <h2 id="otros" class="page-header">Otros</h2>

          <h3 id="imagenes">Imagenes Soportadas</h3>
          <p>Los formatos de imagenes soportados por el sistema son: <strong>JPG / JPEG / PNG / GIF</strong>.</p>

                </div>
            </div>
        </div>
    </div>
     <!-- Final Page Content -->
	<br /><br /><br />

    <?php include "footer.php" ?>
    <p class="copyright" align="right">
        Programación: <a href="https://ar.linkedin.com/in/lucianociattaglia">Luciano Ciattaglia</a>
	</p>
    <!-- Scripts al final del DOM para que la pagina cargue mas rapido -->
    <!-- ============================================================= -->
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/utilidades.js"></script>
    <!-- ============================================================= -->
</body>
</html>
