<?php
// Total Mensajes
$con_cant_mensajes=consulta("SELECT COUNT(idmensaje) AS cant_mensajes FROM mensajes WHERE destino = '$idusuario'");
$cm=mysqli_fetch_array($con_cant_mensajes);
$cantmensajes = $cm['cant_mensajes'];

// Mensajes sin leer
$con_cant_mensajes=consulta("SELECT COUNT(idmensaje) AS noleidos FROM mensajes WHERE destino = '$idusuario' AND visto_des = 0");
$cm=mysqli_fetch_array($con_cant_mensajes);
$sinleer = $cm['noleidos'];
?>
<div id="navbar" class="navbar navbar-default ace-save-state">
  <div class="navbar-container ace-save-state" id="navbar-container">
    <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
      <span class="sr-only">Toggle sidebar</span>

      <span class="icon-bar"></span>

      <span class="icon-bar"></span>

      <span class="icon-bar"></span>
    </button>

    <div class="navbar-header pull-left">
      <a href="inicio.php" class="navbar-brand">
        <small>
          <i class="glyphicon glyphicon-link"></i>
          Simedu
        </small>
      </a>
    </div>

    <div class="navbar-buttons navbar-header pull-right" role="navigation">
      <ul class="nav ace-nav">
        <?php
          if (isset($_SESSION['rsocial']) && !empty($_SESSION['rsocial'])){
        ?>
        <li class="grey dropdown-modal">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="ace-icon fa fa-industry"></i>
          </a>

          <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
              <i class="ace-icon fa fa-industry"></i>
              <?php echo $_SESSION['rsocial']; ?>
            </li>

            <li class="dropdown-content">
              <ul class="dropdown-menu dropdown-navbar">

                <li>
                  <a href="periodos.php">
                    <div class="clearfix">
                      <span class="pull-left">Periodo:</span>
                      <span class="pull-right"><?php echo $emp['periodo']; ?></span>
                    </div>
                  </a>
                </li>
                <?php
                  $con_cant_carrito=consulta("SELECT count(*) as 'cantidad' FROM carritos WHERE idcomprador='$idempresa'");
            			$cant_carrito=mysqli_fetch_array($con_cant_carrito);
                  if($cant_carrito['cantidad'] > 0) {
                    $carrito = $cant_carrito['cantidad']." items";
                  } else {
                    $carrito = "Vacio";
                  }
                ?>
				<?php if($idprofe){ ?>
                <li>
                  <a href="carrito.php">
                    <div class="clearfix">
                      <span class="pull-left">Carrito:</span>
                      <span class="pull-right"><?php echo $carrito; ?></span>
                    </div>
                  </a>
                </li>
				<?php } ?>
              </ul>
            </li>

            <li class="dropdown-footer">
              <a href="empresas.php">
                Ver detalles de las empresas
                <i class="ace-icon fa fa-arrow-right"></i>
              </a>
            </li>
          </ul>
        </li>
        <?php } ?>

        <li class="green dropdown-modal">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
            <span class="badge badge-success"><?php echo $sinleer; ?></span>
          </a>

          <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
            <li class="dropdown-header">
              <i class="ace-icon fa fa-envelope-o"></i>
              <?php echo $sinleer; ?> Mensajes Nuevos
            </li>

            <li class="dropdown-content">
              <ul class="dropdown-menu dropdown-navbar">
                <?php
                  $con_mensajes=consulta("SELECT mensajes.*, usuario.nombre, usuario.apellido, usuario.email, usuario.imagen
                    FROM mensajes LEFT JOIN usuario ON mensajes.origen = usuario.idusuario
                    WHERE mensajes.destino='$idusuario' AND mensajes.visto_des = 0 LIMIT 5");
                  while ($m = mysqli_fetch_array($con_mensajes, MYSQLI_ASSOC)) {
                    if(!empty($m['nombre']) || !empty($m['apellido'])) {
                      $remitente = $m['nombre'] . " " . $m['apellido'];
                    } else {
                      $remitente = $m['email'];
                    }
                    if($m['imagen']){ $imagenavatar = $m['imagen'];} else { $imagenavatar = "images/logo.png";}
                    $fechahead = fecha($m['fecha'],"/").", ".date("H:i",strtotime($m['hora']));
                ?>
                <li>
                  <a href="mensajes.php" class="clearfix">
                    <img src="<?php echo $imagenavatar; ?>" class="msg-photo" alt="Alex's Avatar" />
                    <span class="msg-body">
                      <span class="msg-title">
                        <span class="blue"><?php echo $remitente; ?>:</span>
                        <?php echo $m['asunto']; ?>
                      </span>

                      <span class="msg-time">
                        <i class="ace-icon fa fa-clock-o"></i>
                        <span><?php echo $fechahead; ?></span>
                      </span>
                    </span>
                  </a>
                </li>
                <?php } ?>
              </ul>
            </li>

            <li class="dropdown-footer">
              <a href="mensajes.php">
                VER TODOS LOS MENSAJES
                <i class="ace-icon fa fa-arrow-right"></i>
              </a>
            </li>
          </ul>
        </li>

        <li class="light-blue dropdown-modal">
          <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="<?php echo $imagenperfil; ?>" alt="Foto de Usuario" />
            <span class="user-info">
              <?php
                if(!empty($u['nombre']) || !empty($u['apellido'])) {
                  echo "<small>".$u['apellido']."</small>". $u['nombre'];
                } else {
                  echo $email;
                }
              ?>
            </span>

            <i class="ace-icon fa fa-caret-down"></i>
          </a>

          <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <li>
              <a href="perfil.php">
                <i class="ace-icon fa fa-user fa-fw"></i>
                Perfil
              </a>
            </li>
			<?php if($idprofe){ ?>
            <li>
              <a href="grupo.php">
                <i class="ace-icon fa fa-users fa-fw"></i>
                Grupo
              </a>
            </li>
			<?php } ?>
            <li>
              <a href="historial.php">
                <i class="ace-icon fa fa-history fa-fw"></i>
                Historial
              </a>
            </li>
            <li>
              <a href="http://ayuda.simedu.com.ar">
                <i class="ace-icon fa fa-question fa-fw"></i>
                Ayuda
              </a>
            </li>

            <li class="divider"></li>

            <li>
              <a href="#" id="cerrarsesion">
                <i class="ace-icon fa fa-power-off"></i>
                Cerrar Sesion
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div><!-- /.navbar-container -->
</div>

<div class="main-container ace-save-state" id="main-container">
  <script type="text/javascript">
    try{ace.settings.loadState('main-container')}catch(e){}
  </script>

  <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
    <script type="text/javascript">
      try{ace.settings.loadState('sidebar')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
      <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
        <a href="simcontable.php" class="btn btn-success" data-toggle="tooltip" title="Simulador Contable">
          <i class="ace-icon fa fa-book"></i>
        </a>

        <a href="comprobantes.php" class="btn btn-info" data-toggle="tooltip" title="Comprobantes">
          <i class="ace-icon fa fa-file-pdf-o"></i>
        </a>

        <a href="http://ayuda.simedu.com.ar" class="btn btn-warning" data-toggle="tooltip" title="Ayuda">
          <i class="ace-icon fa fa-question"></i>
        </a>

        <a href="mensajes.php" class="btn btn-danger" data-toggle="tooltip" title="Mensajes">
          <i class="ace-icon fa fa-envelope"></i>
        </a>
      </div>

      <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
        <span class="btn btn-success"></span>

        <span class="btn btn-info"></span>

        <span class="btn btn-warning"></span>

        <span class="btn btn-danger"></span>
      </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
      <li class="active">
        <a href="inicio.php">
          <i class="menu-icon fa fa-home"></i>
          <span class="menu-text"> Inicio </span>
        </a>

        <b class="arrow"></b>
      </li>

      <!-- Mis Empresas -->
      <li class="">
        <a href="#" class="dropdown-toggle">
          <i class="menu-icon fa fa-suitcase"></i>
          <span class="menu-text"> Mis Empresas </span>
          <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
          <li class="">
            <a href="empresas.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Empresas
            </a>
            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="institucional.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Institucional
            </a>
            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="organigramas.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Organigramas
            </a>
            <b class="arrow"></b>
          </li>
        </ul>
      </li>

      <!-- Simulador Empresarial -->
      <?php if($u['modempresarial'] == 1 && $emodempresarial == 1){ ?>
      <li class="">
        <a href="#" class="dropdown-toggle">
          <i class="menu-icon fa fa-industry"></i>
          <span class="menu-text"> Sim. Empresarial </span>
          <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
          <li class="">
            <a href="categoriasymarcas.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Categorias y Marcas
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="listaivas.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Tasas de IVA
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="listaprecios.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Listas de Precios
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="productos.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Productos
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="proveedores.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Proveedores
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="clientes.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Clientes
            </a>
            <b class="arrow"></b>
          </li>
		  <?php if($idprofe){ ?>
          <li class="">
            <a href="mercado.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Mercado
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="carrito.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Carrito
            </a>
            <b class="arrow"></b>
          </li>
		  <?php } ?>
          <li class="">
            <a href="comprobantes.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Comprobantes
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="ivacompra.php">
              <i class="menu-icon fa fa-caret-right"></i>
              IVA Compra
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="ivaventa.php">
              <i class="menu-icon fa fa-caret-right"></i>
              IVA Venta
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="mayorstock.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Mayor de Stock
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="minimostock.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Minimo de Stock
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="resumen_ventas.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Resumen de Ventas
            </a>
            <b class="arrow"></b>
          </li>
        </ul>
      </li>
      <?php } ?>

      <!-- Simulador Contable -->
      <?php if($u['modcontable'] == 1 && $emodcontable == 1){ ?>
      <li class="">
        <a href="#" class="dropdown-toggle">
          <i class="menu-icon fa fa-usd"></i>
          <span class="menu-text"> Sim. Contable </span>
          <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
          <li class="">
            <a href="cuentas.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Plan de Cuentas
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="periodos.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Periodos
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="asientos_lista.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Asientos Contables
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="librodiario.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Libro diario
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="libromayor.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Libro mayor
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="balancesumasysaldos.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Balance sumas y saldos
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="balancegeneral.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Balance general
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="flujodefondos.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Flujo de fondos
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="resumen.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Prebalance
            </a>
            <b class="arrow"></b>
          </li>
        </ul>
      </li>
      <?php } ?>

      <!-- Planteos -->
      <?php if($u['modludico'] == 1){ ?>
      <li class="">
        <a href="planteo.php">
          <i class="menu-icon fa fa-cubes"></i>
          <span class="menu-text"> Planteos </span>
        </a>

        <b class="arrow"></b>
      </li>
      <?php } ?>

      <!-- Liquidacion de Sueldos -->
      <?php if($u['tipousuario'] == 58) { ?>
        <li class="">
          <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-inbox"></i>
            <span class="menu-text"> Bienes de Uso </span>
            <b class="arrow fa fa-angle-down"></b>
          </a>
          <b class="arrow"></b>
          <ul class="submenu">
            <li class="">
              <a href="bienes_clientes.php">
                <i class="menu-icon fa fa-caret-right"></i>
                Clientes
              </a>
              <b class="arrow"></b>
            </li>
            <li class="">
              <a href="bienes_actividades_clientes.php">
                <i class="menu-icon fa fa-caret-right"></i>
                Actividades por Cliente
              </a>
              <b class="arrow"></b>
            </li>
            <li class="">
              <a href="bienes_grupos.php">
                <i class="menu-icon fa fa-caret-right"></i>
                Grupos
              </a>
              <b class="arrow"></b>
            </li>
            <li class="">
              <a href="bienes_rubros.php">
                <i class="menu-icon fa fa-caret-right"></i>
                Rubros
              </a>
              <b class="arrow"></b>
            </li>
            <li class="">
              <a href="bienes_de_uso.php">
                <i class="menu-icon fa fa-caret-right"></i>
                Bienes de Uso
              </a>
              <b class="arrow"></b>
            </li>
          </ul>
        </li>
      <?php } ?>

      <!-- Herramientas -->
      <?php if($u['impexp'] == 1 && $eimpexp == 1){ ?>
      <li class="">
        <a href="#" class="dropdown-toggle">
          <i class="menu-icon fa fa-wrench"></i>
          <span class="menu-text"> Herramientas </span>
          <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
          <li class="">
            <a href="impexp.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Exportar Datos
            </a>
            <b class="arrow"></b>
          </li>
          <?php if($u['tipousuario'] == 58) {?>
          <li class="">
            <a href="planteos_docentes.php">
              <i class="menu-icon fa fa-caret-right"></i>
              Definicion de planteos
            </a>
            <b class="arrow"></b>
          </li>
          <li class="">
            <a href="admin">
              <i class="menu-icon fa fa-caret-right"></i>
              Panel de Administracion
            </a>
            <b class="arrow"></b>
          </li>
          <?php } ?>
          <?php if($u['tipousuario'] == 2) {?>
            <li class="">
              <a href="planteos_docentes.php">
                <i class="menu-icon fa fa-caret-right"></i>
                Definicion de planteos
              </a>
              <b class="arrow"></b>
            </li>
            <li class="">
              <a href="profesores">
                <i class="menu-icon fa fa-caret-right"></i>
                Panel Docente
              </a>
              <b class="arrow"></b>
            </li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>

    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
      <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
  </div>
