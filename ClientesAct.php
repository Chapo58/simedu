<?php
session_start();
include "funciones.php";
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM clientes WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM clientes WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$rsocial = $_POST["rsocial"];
		$cuit = $_POST["cuit"];
		$domicilio = $_POST["domicilio"];
		$localidad = $_POST["localidad"];
		$provincia = $_POST["provincia"];
		$pais = $_POST["pais"];
		$email = $_POST["emailcontacto"];
		$telefono = $_POST["telefono"];
		$condiva = $_POST["condiva"];
		$ingbrutos = $_POST["ingbrutos"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO clientes(idempresa, rsocial, domicilio, localidad, provincia, pais, condiva, emailcontacto, telefono, cuit, ingbrutos) 
		VALUES('$idempresa','$rsocial','$domicilio','$localidad','$provincia','$pais','$condiva','$email','$telefono','$cuit','$ingbrutos');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM clientes WHERE idcliente = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		acthistempresa($rsocialemp, "Se cargo un nuevo Cliente");
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idcliente"];
		$rsocial = $_POST["rsocial"];
		$cuit = $_POST["cuit"];
		$domicilio = $_POST["domicilio"];
		$localidad = $_POST["localidad"];
		$provincia = $_POST["provincia"];
		$pais = $_POST["pais"];
		$email = $_POST["emailcontacto"];
		$telefono = $_POST["telefono"];
		$condiva = $_POST["condiva"];
		$ingbrutos = $_POST["ingbrutos"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE clientes SET rsocial = '$rsocial', domicilio = '$domicilio', localidad = '$localidad', provincia = '$provincia', pais = '$pais', cuit = '$cuit', emailcontacto = '$email', telefono = '$telefono', condiva = '$condiva', ingbrutos = '$ingbrutos' WHERE idcliente = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idcliente"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM clientes WHERE idcliente = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>