<?php
session_start();
include "funciones.php";



try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM proveedores WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM proveedores WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");

		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$rsocial = $_POST["rsocial"];
		$cuit = $_POST["cuit"];
		$domicilio = $_POST["domicilio"];
		$localidad = $_POST["localidad"];
		$provincia = $_POST["provincia"];
		$pais = $_POST["pais"];
		$email = $_POST["emailcontacto"];
		$telefono = $_POST["telefono"];
		$condiva = $_POST["condiva"];
		$ingbrutos = $_POST["ingbrutos"];
		$logo = $_POST["logo"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO proveedores(idempresa, rsocial, domicilio, localidad, provincia, pais, condiva, emailcontacto, telefono, cuit, ingbrutos, logo)
		VALUES('$idempresa','$rsocial','$domicilio','$localidad','$provincia','$pais','$condiva','$email','$telefono','$cuit','$ingbrutos', '$logo');");

		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM proveedores WHERE idproveedor = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		acthistempresa($rsocialemp, "Se cargo un nuevo proveedor");
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idproveedor"];
		$rsocial = $_POST["rsocial"];
		$cuit = $_POST["cuit"];
		$domicilio = $_POST["domicilio"];
		$localidad = $_POST["localidad"];
		$provincia = $_POST["provincia"];
		$pais = $_POST["pais"];
		$email = $_POST["emailcontacto"];
		$telefono = $_POST["telefono"];
		$condiva = $_POST["condiva"];
		$ingbrutos = $_POST["ingbrutos"];
		$logo = $_POST["logo"];
		//Update record in database
		if($logo != ""){
			$result = mysqli_query($con,"UPDATE proveedores SET rsocial = '$rsocial', domicilio = '$domicilio', localidad = '$localidad', provincia = '$provincia', pais = '$pais', cuit = '$cuit', emailcontacto = '$email', telefono = '$telefono', condiva = '$condiva', ingbrutos = '$ingbrutos', logo = '$logo' WHERE idproveedor = $id;");
		}else{
			$result = mysqli_query($con,"UPDATE proveedores SET rsocial = '$rsocial', domicilio = '$domicilio', localidad = '$localidad', provincia = '$provincia', pais = '$pais', cuit = '$cuit', emailcontacto = '$email', telefono = '$telefono', condiva = '$condiva', ingbrutos = '$ingbrutos' WHERE idproveedor = $id;");
		}
		

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idproveedor"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM proveedores WHERE idproveedor = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}

?>
