<?php
	include "funciones.php";
	session_start();

	if (isset($_POST['idusu']) && !empty($_POST['idusu'])) {

		$idusuario = trim($_POST['idusu']);
		if (isset($_SESSION['idusuarioprofesor']) && !empty($_SESSION['idusuarioprofesor'])) {
			$_SESSION['idusuarioprofesor'] = "";
		} else {
			$_SESSION['idusuarioprofesor'] = $_SESSION['idusuario'];
		}

		$con_u=consulta("SELECT * FROM usuario WHERE idusuario = $idusuario");
		$u=mysqli_fetch_array($con_u);
		$_SESSION['email'] = $u['email'];
		$_SESSION['idusuario'] = $u['idusuario'];

		ir_a("inicio.php");

	} else {
		ir_a("index.php");
	}

?>
