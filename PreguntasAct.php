<?php
session_start();
include "funciones.php";
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM iva WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM iva WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$ntasa = $_POST["ntasa"];
		$denominacion = $_POST["denominacion"];
		$vigencia = date("Y-m-d", strtotime($_POST["vigencia"]));
	/*	$vigencia = str_replace("/","",$vigencia);
		$ano = substr($vigencia, -4);
		$mes = substr($vigencia, -6, 2);
		$dia = substr($vigencia, -8, 2);
		$vigencia = $ano."-".$mes."-".$dia;*/
		$por = $_POST["porc"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO iva(idempresa, ntasa, denominacion, vigencia, porc) VALUES('$idempresa','$ntasa','$denominacion','$vigencia','$por');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM iva WHERE idiva = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idiva"];
		$ntasa = $_POST["ntasa"];
		$denominacion = $_POST["denominacion"];
		$vigencia = date("Y-m-d", strtotime($_POST["vigencia"]));
		$por = $_POST["porc"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE iva SET ntasa = '$ntasa', denominacion = '$denominacion', vigencia = '$vigencia', porc = '$por' WHERE idiva = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idiva"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM iva WHERE idiva = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>