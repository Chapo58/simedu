-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-08-2018 a las 08:48:35
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `principal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bienes_de_uso`
--

CREATE TABLE `bienes_de_uso` (
  `idbien` int(11) NOT NULL,
  `numero` int(10) NOT NULL,
  `detalle` varchar(191) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `vida_util` int(2) NOT NULL,
  `fecha_compra` date NOT NULL,
  `coeficiente` decimal(10,2) NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `comentario` varchar(500) NOT NULL,
  `ubicacion` varchar(20) NOT NULL,
  `tipo_bien` varchar(20) NOT NULL,
  `mueble` varchar(50) NOT NULL,
  `inmueble` varchar(50) NOT NULL,
  `inmueble_valor` varchar(50) NOT NULL,
  `cuenta_inmobiliaria` varchar(191) NOT NULL,
  `bien_de_reemplazo` int(1) NOT NULL,
  `utilidad` decimal(10,2) NOT NULL,
  `valor_cnas` decimal(10,2) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `idrubro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bienes_de_uso`
--
ALTER TABLE `bienes_de_uso`
  ADD PRIMARY KEY (`idbien`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bienes_de_uso`
--
ALTER TABLE `bienes_de_uso`
  MODIFY `idbien` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
