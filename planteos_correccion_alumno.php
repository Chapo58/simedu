<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Inicio</title>
		<?php require_once('head.php'); ?>
		<?php if(isset($_POST['idalumno']) && !empty($_POST['idalumno']) &&
						 isset($_POST['idplanteo']) && !empty($_POST['idplanteo'])) {
			$idalumno = $_POST['idalumno'];
			$con_perfil=consulta("SELECT * FROM usuario WHERE idusuario='$idalumno'");
			$p=mysqli_fetch_array($con_perfil);
			$idplanteo = $_POST['idplanteo'];
			$con_planteo=consulta("SELECT * FROM planteos WHERE idplanteo='$idplanteo'");
			$pl=mysqli_fetch_array($con_planteo);
			if($p['imagen']){ $imagenusu = $p['imagen'];} else { $imagenusu = "images/logo.png";}
			if($p['nombre']){
				$usuario = $p['nombre'] . " " . $p['apellido'];
			} else {
				$usuario = $p['email'];
			}
		} else {
			mensaje("Debe seleccionar un alumno");
			ir_a("planteos_docentes.php");
		}  ?>
		<style>
			.list-group-item {
	    height:auto;
	    min-height:220px;
			}
			.list-group-item.active small {
			    color:#fff;
			}
		</style>
	</head>

	<body class="no-skin">

		<?php require_once('header.php'); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
	            <li>
	              <i class="ace-icon fa fa-cubes home-icon"></i>
	              <a href="planteo.php">Planteos</a>
	            </li>
	            <li><a href="planteos_docentes.php">Definicion de Planteos</a></li>
	            <li>
								<form id="correccion" action="planteos_correccion.php" method="POST" class="pull-right">
	                  <a href="javascript:{}" onclick="document.getElementById('correccion').submit();"><?php echo $pl['titulo']; ?></a>
	                  <input type="hidden" name="idplanteo" id="idplanteo" value="<?php echo $idplanteo; ?>" >
	              </form>
							</li>
							<li class="active"><?php echo $usuario; ?></li>
	          </ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
				<div class="page-content">
					<h1 class="page-header">Respuestas de <?php echo $usuario; ?></h1>

	<div class="row">
		<div class="well">
			<div class="list-group">
				<?php
				$con_respuestas=consulta("SELECT respuestas.*, preguntas.* FROM respuestas
					LEFT JOIN preguntas ON respuestas.idpregunta = preguntas.idpregunta
					WHERE respuestas.idalumno = $idalumno AND respuestas.idplanteo = $idplanteo");
				while ($r = mysqli_fetch_array($con_respuestas, MYSQLI_ASSOC)) {
					$idpregunta = $r['idpregunta'];
					$ruta = $r['ruta'];
					$pregunta = $r['descripcion'];
					$respuesta = $r['respuesta'];
					$correccion = $r['correccion'];
					$nota = true;
					if($r['multiple']){
						$opcion = 'opcion'.$r['opcion'];
						$respuesta = $r[$opcion];
						if($r['opcion'] == $r['correcta']){
							$correccion = 1;
						} else {
							$correccion = 2;
						}
						$nota = false;
					}
				?>
				<div class="list-group-item">
							<div class="media col-md-3">
									<figure class="pull-left">
										<?php if($ruta){
													if(esimagen($ruta)){	?>
											<img class="media-object img-rounded img-responsive"  src="<?php echo $ruta; ?>" >
										<?php } else { ?>
											<a href="<?php echo $ruta; ?>" target="_blank" class="btn btn-lg btn-info"><i class="fa fa-file fa-3x"></i></a>
										<?php } } ?>
									</figure>
							</div>
							<div class="col-md-6">
									<h4 class="list-group-item-heading"><strong> <?php echo $pregunta; ?> </strong></h4>
									<p class="list-group-item-text">
										<?php echo $respuesta; ?>
									</p>
							</div>
							<div class="col-md-3 text-center">
								<?php if($correccion == 0){ ?>
									<button type="button" onclick="correccion(1, <?php echo $idpregunta;  ?>)" class="btn btn-success btn-lg btn-block"><i class="fa fa-check" style="float:left;margin-top:4px;"></i> Correcto </button>
									<button type="button" onclick="correccion(2, <?php echo $idpregunta;  ?>)" class="btn btn-danger btn-lg btn-block"><i class="fa fa-close" style="float:left;margin-top:4px;"></i> Incorrecto </button>
								<?php } else if($correccion == 1){ ?>
									<h1 style="color:green;">
										CORRECTO
										<?php if($nota){ ?>
										<button type="button" style="float:right;margin-top:5px;" onclick="cambiarcorreccion(<?php echo $correccion.', '.$idpregunta;  ?>)" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
										<?php } ?>
									</h1>
								<?php } else { ?>
									<h1 style="color:red;">
										INCORRECTO
										<?php if($nota){ ?>
										<button type="button" style="float:right;margin-top:5px;" onclick="cambiarcorreccion(<?php echo $correccion.', '.$idpregunta;  ?>)" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
										<?php } ?>
									</h1>
								<?php } ?>
							</div>
				</div>
				<?php } ?>
			</div>
			</div>
</div>

					<!-- Final Page Content -->
				</div>
			</div>
		</div><!-- /.main-content -->

			<?php require_once('footer.php'); ?>
			<script>
				<?php echo "var idalumno = $idalumno;"; ?>

				function correccion(valor,pregunta) {
					$.ajax({
						type: "POST",
						url: "planteos_correccion_proceso.php",
						data: { valor: valor, pregunta: pregunta, idalumno: idalumno },
						success: function(msg) {
							location.reload();
						},
						error: function (xhr, status, error) {
							var err = eval("(" + xhr.responseText + ")");
							alert(err.Message);
						}
					});
				}

				function cambiarcorreccion(valor,pregunta) {
					if(valor == 1){
						valor = 2;
						mensaje = "¿Desea marcar esta pregunta como INCORRECTA?"
					} else {
						valor = 1;
						mensaje = "¿Desea marcar esta pregunta como CORRECTA?"
					}
					BootstrapDialog.show({
						title: 'Cambiar Nota',
						message: mensaje,
						buttons: [{
							label: ' Aceptar',
							cssClass: 'btn-primary',
							action: function(){
								$.ajax({
									type: "POST",
									url: "planteos_correccion_proceso.php",
									data: { valor: valor, pregunta: pregunta, idalumno: idalumno },
									success: function(msg) {
										location.reload();
									},
									error: function (xhr, status, error) {
										var err = eval("(" + xhr.responseText + ")");
										alert(err.Message);
									}
								});
							}
						}, {
							label: ' Cancelar',
							cssClass: 'btn-default',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				}

			</script>
	</body>
</html>
