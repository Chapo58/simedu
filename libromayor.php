<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Libro Mayor</title>
  	<?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Simulador Contable</a>
            </li>
            <li class="active">Libro Mayor</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		} else {

		}
	?>
  <!-- Page Content -->
  <h1 class="page-header">Libro Mayor</h1>
  <div class="space50"></div>
  <form role="form" data-toggle="validator" target="_blank" action="PDFs/libromayor_printable.php" method="POST">
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Desde</strong></div>
                <input type="text" data-mask="99/99/9999" id="desde" name="desde" class="form-control" value="<?php echo date("d/m/Y",strtotime($desde)); ?>" placeholder="Fecha Desde" maxlength="10">
          </div>
   		</div>
	</div>
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Hasta</strong></div>
                <input type="text" data-mask="99/99/9999" id="hasta" name="hasta" class="form-control" value="<?php echo date("d/m/Y",strtotime($hasta)); ?>" placeholder="Fecha Hasta" maxlength="10">
          </div>
   		</div>
	</div>
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span><strong>&nbsp;&nbsp;Desde</strong></div>
                <input type="number" id="cdesde" onkeyup="getDenominacion($(this).val(),1);" name="cdesde" class="form-control" placeholder="Cuenta Desde">
				<select class='form-control' id='detalledesde' onchange="getCuenta($(this).val(),1);">
					<option selected disabled>Seleccionar Cuenta</option>
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
				</select>
          </div>
   		</div>
	</div>
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span><strong>&nbsp;&nbsp;Hasta</strong></div>
                <input type="number" id="chasta" name="chasta" onkeyup="getDenominacion($(this).val(),2);" class="form-control" placeholder="Cuenta Hasta">
				<select class='form-control' id='detallehasta' onchange="getCuenta($(this).val(),2);">
					<option selected disabled>Seleccionar Cuenta</option>
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
				</select>
          </div>
   		</div>
	</div>
	<div class="col-md-6 col-md-offset-5">
		<div class="form-group">
			<div class="input-group">
        <span class="button-checkbox">
    			<button type="button" class="btn" data-color="info"><strong>No mostrar detalle</strong></button>
    			<input type="checkbox" id="detalle" name="detalle" class="hidden" />
    		</span>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<button type="submit" id="btnAceptar" style="width:100%;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Generar Impresion</button>
	</div>
  </form>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

  <script type="text/javascript">
  	$(document).ready(function() {
  		//Calendarios
  		$("#desde").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  		$("#hasta").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  	});
  	function getCuenta(valor, ninp) {
  		if(ninp == 1)
  		$("#cdesde").val(valor);
  		else
  		$("#chasta").val(valor);
  	};

  	function getDenominacion(valor, ninp) {
  		if(ninp == 1)
  		$("#detalledesde").val(valor);
  		else
  		$("#detallehasta").val(valor);
  	};
  </script>
  <!-- ============================================================= -->
</body>
</html>
