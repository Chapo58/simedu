<?php

session_start();
include "funciones.php";

$destin_dir = "assets/uploads/logo/";
$file = md5(microtime()) . basename($_FILES["userfile"]["name"]);
$devo['status'] = "success";
$imageFileType = pathinfo($file, PATHINFO_EXTENSION);

// Check already exists
if (file_exists($file)) {
    $devo['status']     = "error";
    $devo['message']    = "Lo siento, el archivo ya existe.";
}
// Check size
if ($_FILES["userfile"]["size"] > 500000) {
    $devo['status']     = "error";
    $devo['message']    = "Lo siento, el archivo es demasiado grande.";
}
// Check formats
if (
    $imageFileType != "jpg" &&
    $imageFileType != "png" &&
    $imageFileType != "jpeg" &&
    $imageFileType != "gif"
) {
    $devo['status']     = "error";
    $devo['message']    = "Lo siento, JPG, JPEG, PNG & GIF están permitidos.";
}
if ($devo['status'] == "error") {
    echo json_encode($devo);
} else {
    if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $destin_dir . $file)) {
        $devo['status']     = "success";
        $devo['message']    = "El archivo fue subido correctamente";
        $devo['file']       = $file;
        echo json_encode($devo);
    } else {
        $devo['status']     = "error";
        $devo['message']    = "Lo siento, no pudo subirse el archivo.";
        echo json_encode($devo);
    }
}
