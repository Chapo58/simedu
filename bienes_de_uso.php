<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Bienes de Uso</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-inbox home-icon"></i>
              <a href="bienes_de_uso.php">Bienes de Uso</a>
            </li>
            <li class="active">Bienes de Uso</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		}
        if(isset($_POST['idbien']) && !empty($_POST['idbien'])){
            $ideditar = $_POST['idbien'];
            $con_editar=consulta("SELECT * FROM bienes_de_uso WHERE idbien='$ideditar'");
            $editar=mysqli_fetch_array($con_editar);
			$titulo = "Modificar Bien de Uso " . $editar['numero'];
			$accion = "bienes_de_uso_modificar.php";
			$txtboton = "Guardar Cambios";

			$idbien = $editar['idbien'];
			$modnumero = $editar['numero'];
			$moddetalle = $editar['detalle'];
			$modcliente = $editar['idcliente'];
			$modactividad = $editar['idactividad'];
			$modvidautil = $editar['vida_util'];
			$modcomentario = $editar['comentario'];
			$modfecha = date("Y-m", strtotime($editar['fecha_compra']));;
			$modcoeficiente = $editar['coeficiente'];
			$modcosto = $editar['costo'];
			$modubicacion = $editar['ubicacion'];
			$modtipo = $editar['tipo_bien'];
			$modmueble = $editar['mueble'];
			$modinmueble = $editar['inmueble'];
			$modinmueblevalor = $editar['inmueble_valor'];
			$modcuenta = $editar['cuenta_inmobiliaria'];
			$modreemplazo = $editar['bien_de_reemplazo'];
			$modutilidad = $editar['utilidad'];
			$modvalor = $editar['valor_cnas'];
			$modgrupo = $editar['idgrupo'];
			$modrubro = $editar['idrubro'];
		} else {
			$idbien = 0;
			$titulo = "Agregar Bien de Uso";
			$accion = "bienes_de_uso_cargar.php";
			$txtboton = "Aceptar";
			$modfecha = '';
		}
	?>


    <!-- Page Content -->
    <div id="divEmpleados">
        <h1 class="page-header">Bienes de Uso</h1>
		<input type="button" id="btnNuevo" onclick="switchDivs();" class="btn btn-lg btn-primary" value="Agregar Bien de Uso">
		<div class="space20"></div><br /><br />
        <div class="row">
            <div class="col-xs-12">
                <table id="tabla" class="table table-striped table-bordered" cellspacing="0" style="width:80%;margin:auto;">
                    <thead>
                        <tr>
                            <th>Numero</th>
                            <th>Detalle</th>
                            <th>Cliente</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
							$con_bien=consulta("SELECT bienes_de_uso.*, bienes_clientes.denominacion as cliente
							FROM bienes_de_uso 
							LEFT JOIN bienes_clientes ON bienes_de_uso.idcliente = bienes_clientes.idcliente
							WHERE bienes_de_uso.idempresa='$idempresa'");
							while ($bien = mysqli_fetch_array($con_bien, MYSQLI_ASSOC)) {
								$idbien2 = $bien['idbien'];
								$numero = $bien['numero'];
								$detalle = $bien['detalle'];
								$cliente = $bien['cliente'];
						?>
                        <tr>
                            <td><?php echo $numero; ?></td>
							<td><?php echo $detalle; ?></td>
							<td><?php echo $cliente; ?></td>
							<td class="text-center">
                                <form action="bienes_de_uso.php" method="POST">
                                    <button type="submit" class="btn btn-primary" title="Editar">
										<span class="fa fa-pencil" aria-hidden="true"></span>
									</button>
                                <input type="hidden" name="idbien" id="idbien" value="<?php echo $idbien2; ?>" >
                                </form>
							</td>
							<td class="text-center">
                                <form class="delform">
                                    <button type="submit" class="btn btn-danger" title="Eliminar">
										<span class="fa fa-trash-o" aria-hidden="true"></span>
									</button>
									<input type="hidden" id="delbien" name="delbien" value="<?php echo $idbien2; ?>" >
                                </form>
                            </td>
                        </tr>
						<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="space50"></div>

    </div>
    <div id="divNuevaEditar" class="sr-only">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
        <form role="form" data-toggle="validator" action="<?php echo $accion; ?>" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-4">
		        <div class="form-group">
					<label for="numero">Numero</label>
		            <input type="number" id="numero" name="numero" class="form-control" placeholder="Numero" required>
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="detalle">Detalle</label>
		            <input type="text" id="detalle" name="detalle" class="form-control" placeholder="Detalle" required>
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="idcliente">Cliente</label>
		            <select class="form-control" name="idcliente" id="idcliente">
						<option selected disabled>Seleccionar Cliente</option>
		                <?php
						$con_per=consulta("SELECT * FROM bienes_clientes WHERE idempresa='$idempresa'");
						while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
							$idcliente = $p['idcliente'];
							$deno = $p['denominacion'];
						?>
						<option value = '<?php echo $idcliente;?>'><?php echo $deno; ?></option>
						<?php } ?>
		            </select>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="idactividad">Actividad</label>
		            <select class="form-control" name="idactividad" id="idactividad">
						<option selected disabled>Seleccionar Actividad</option>
		                <?php
						$con_act=consulta("SELECT * FROM bienes_actividades_clientes WHERE idempresa='$idempresa'");
						while ($ac = mysqli_fetch_array($con_act, MYSQLI_ASSOC)) {
							$idactividad = $ac['idactividad'];
							$descripcion = $ac['descripcion'];
						?>
						<option value = '<?php echo $idactividad;?>'><?php echo $descripcion; ?></option>
						<?php } ?>
		            </select>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="vida_util">Vida Util</label>
		            <input type="number" id="vida_util" name="vida_util" class="form-control" placeholder="Vida Util">
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="fecha_compra">Fecha de Compra</label>
		            <input type="month" id="fecha_compra" value="<?php echo $modfecha; ?>" name="fecha_compra" class="form-control">
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="coeficiente">Coeficiente</label>
		            <input type="number" step="0.01" id="coeficiente" name="coeficiente" class="form-control" placeholder="Coeficiente">
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="costo">Costo Original</label>
		            <input type="number" step="0.01" id="costo" name="costo" class="form-control" placeholder="Costo Original">
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="ubicacion">Ubicación</label>
					<select class="form-control" name="ubicacion" id="ubicacion">
						<option value="0">Ubicación</option>
                        <option value="Pais">Pais</option>
                        <option value="Exterior">Exterior</option>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="tipo_bien">Tipo de Bien</label>
					<select class="form-control" name="tipo_bien" id="tipo_bien">
						<option value="0">Tipo de Bien</option>
                        <option value="Mueble">Mueble</option>
                        <option value="Inmueble">Inmueble</option>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="mueble">Es Rodado</label>
					<select class="form-control" name="mueble" id="mueble">
                        <option value="Si">Si</option>
                        <option value="No">No</option>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="inmueble">Inmueble</label>
					<select class="form-control" name="inmueble" id="inmueble">
                        <option value="Urbano">Urbano</option>
                        <option value="Rural">Rural</option>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="inmueble_valor">Inmueble Valor</label>
					<select class="form-control" name="inmueble_valor" id="inmueble_valor">
                        <option value="Tierra">Tierra</option>
                        <option value="Mejora">Mejora</option>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="cuenta_inmobiliaria">Cuenta Inmobiliaria</label>
		            <input type="number" id="cuenta_inmobiliaria" name="cuenta_inmobiliaria" class="form-control" placeholder="Cta Inmobiliaria">
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="bien_de_reemplazo">Es Bien de Reemplazo</label>
					<select class="form-control" name="bien_de_reemplazo" id="bien_de_reemplazo">
                        <option value="Si">Si</option>
                        <option value="No">No</option>
                    </select>
				</div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="utilidad">Utilidad</label>
		            <input type="number" step="0.01" id="utilidad" name="utilidad" class="form-control" placeholder="Utilidad">
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="valor_cnas">Valor C.N.A.S</label>
		            <input type="number" step="0.01" id="valor_cnas" name="valor_cnas" class="form-control" placeholder="Valor">
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="idgrupo">Grupo</label>
		            <select class="form-control" name="idgrupo" id="idgrupo">
						<option selected disabled>Seleccionar Grupo</option>
		                <?php
						$con_gru=consulta("SELECT * FROM bienes_grupos WHERE idempresa='$idempresa'");
						while ($gr = mysqli_fetch_array($con_gru, MYSQLI_ASSOC)) {
							$idgrupo = $gr['idgrupo'];
							$descripcion = $gr['numero'].' - '.$gr['detalle'];
						?>
						<option value = '<?php echo $idgrupo;?>'><?php echo $descripcion; ?></option>
						<?php } ?>
		            </select>
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="idrubro">Rubro</label>
		            <select class="form-control" name="idrubro" id="idrubro">
						<option selected disabled>Seleccionar Rubro</option>
		                <?php
						$con_rub=consulta("SELECT * FROM bienes_rubros WHERE idempresa='$idempresa'");
						while ($ru = mysqli_fetch_array($con_rub, MYSQLI_ASSOC)) {
							$idrubro = $ru['idrubro'];
							$descripcion = $ru['numero'].' - '.$ru['detalle'];
						?>
						<option value = '<?php echo $idrubro;?>'><?php echo $descripcion; ?></option>
						<?php } ?>
		            </select>
		        </div>
			</div>
			<div class="col-md-12">
		        <div class="form-group">
					<label for="comentario">Comentario</label>
		            <textarea class="form-control" rows="3" id="comentario" name="comentario" placeholder="Comentario...">
						<?php 
							if(isset($_POST['idbien']) && !empty($_POST['idbien'])){
								echo $modcomentario;
							}
						?>
					</textarea>
		        </div>
			</div>

			<input type="hidden" name="edbien" id="edbien" value="<?php echo $idbien; ?>" >
			<div class="col-md-12">
		        <div class="form-group">
		            <div class="pull-right">
		                <button type="button" id="btnAtras" class="btn btn-default btn-lg" onclick="window.location.href='bienes_de_uso.php';">Atrás</button>
						<button type="submit" id="btnAceptar" class="btn btn-primary btn-lg"><?php echo $txtboton; ?></button>
		            </div>
		        </div>
			</div>
		</div>
        </form>
    </div>
    <!-- Final Page Content -->
  </div>
 </div>
</div><!-- /.main-content -->
<br><br><br><br><br>
<?php require_once('footer.php'); ?>

    <script type="text/javascript">

    $(document).ready(function() {

        var table = $('#tabla').dataTable({
			"order": [[ 1, "asc" ]],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ bienes",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Seleccione "Agregar Bien de Uso" para cargar un nuevo bien.',
				"sInfo":           "Mostrando bienes del _START_ al _END_ de un total de _TOTAL_ bienes",
				"sInfoEmpty":      "Mostrando bienes del 0 al 0 de un total de 0 bienes",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ bienes)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
        });

		$('#tabla').on('click', '.delform', function () {
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Bien de Uso',
				message: '¿Esta seguro que desea eliminar este bien?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

		<?php
			if(isset($_POST['idbien']) && !empty($_POST['idbien'])){
				echo "switchDivs();";
				echo "$( '#numero' ).val( '$modnumero' );";
				echo "$( '#detalle' ).val( '$moddetalle' );";
				echo "$( '#idcliente' ).val( '$modcliente' );";
				echo "$( '#idactividad' ).val( '$modactividad' );";
				echo "$( '#vida_util' ).val( '$modvidautil' );";
				echo "$( '#fecha_compra' ).val( '$modfecha' );";
				echo "$( '#coeficiente' ).val( '$modcoeficiente' );";
				echo "$( '#costo' ).val( '$modcosto' );";
				echo "$( '#ubicacion' ).val( '$modubicacion' );";
				echo "$( '#tipo_bien' ).val( '$modtipo' );";
				echo "$( '#mueble' ).val( '$modmueble' );";
				echo "$( '#inmueble' ).val( '$modinmueble' );";
				echo "$( '#inmueble_valor' ).val( '$modinmueblevalor' );";
				echo "$( '#cuenta_inmobiliaria' ).val( '$modcuenta' );";
				echo "$( '#bien_de_reemplazo' ).val( '$modreemplazo' );";
				echo "$( '#utilidad' ).val( '$modutilidad' );";
				echo "$( '#valor_cnas' ).val( '$modvalor' );";
				echo "$( '#idgrupo' ).val( '$modgrupo' );";
				echo "$( '#idrubro' ).val( '$modrubro' );";
			}
		?>
	});


    function switchDivs(){
        $("#divEmpleados").toggleClass('sr-only');
        $("#divNuevaEditar").toggleClass('sr-only');
    }
    </script>
    <!-- ============================================================= -->
</body>
</html>
