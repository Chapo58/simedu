<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Institucional</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-suitcase home-icon"></i>
              <a href="empresas.php">Mis Empresas</a>
            </li>
            <li class="active">Institucional</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

	<?php
		if(isset($_POST['cboEmpresa']) && !empty($_POST['cboEmpresa'])) {
			$editarempresa = $_POST['cboEmpresa'];
			$con_editar=consulta("select * from empresas where idempresa='$editarempresa'");
			$editar=mysqli_fetch_array($con_editar);
			if ($editar['imagenempresa'] == "") {
				$imagen = "images/logo.png";
			} else {
				$imagen = $editar['imagenempresa'];
			}
			$web = $editar['web'];
			$nombrec = $editar['nombrecontacto'];
			$telefonoc = $editar['telefonocontacto'];
			$emailc = $editar['emailcontacto'];
		} else {
			$imagen = "images/logo.png";
			$editarempresa = false;
			$imagenficha = "images/logo.png";
		}
		if(isset($_POST['cboFichas']) && !empty($_POST['cboFichas'])) {
			$editarficha = $_POST['cboFichas'];
			$con_editarficha = consulta("select * from fichas where idficha='$editarficha'");
			$editarf = mysqli_fetch_array($con_editarficha);
			if ($editarf['imagen'] == "") {
				$imagenficha = "images/logo.png";
			} else {
				$imagenficha = $editarf['imagen'];
			}
			$tituloficha = $editarf['titulo'];
			$textoficha = $editarf['texto'];
		} else {
			$imagenficha = "images/logo.png";
			$editarficha = false;
		}
	?>

    <!-- Page Content -->
    <h1 class="page-header">Institucional</h1>
    <div class="bs-callout bs-callout-primary">
        Selecciona una empresa y comienza a crear su sección institucional. Si aún no tienes ninguna empresa creada, haz click <a href="empresas.php">aquí</a>.
    </div>

    <div class="space20"></div>

    <div class="form-group">
        <label for="cboEmpresa" class="">Empresa</label>
		<form action="institucional.php" method="POST">
			<select class="form-control" name="cboEmpresa" onchange="this.form.submit()" id="cboEmpresa">
			<?php if (!isset($_POST['cboEmpresa'])) echo "<option disabled selected>Selecciona una empresa</option>";
				  $con_emp=consulta("select * from empresas where idusuario='$idusuario'");
				  while ($emp = mysqli_fetch_array($con_emp, MYSQLI_ASSOC)) {
						$idempresa = $emp['idempresa'];
						$rsocial = $emp['rsocial'];
			?>
				<option
				value="<?php echo $idempresa; ?>"
				><?php echo $rsocial; ?></option>
			<?php } ?>
			</select>
		</form>
    </div>
<!--
    <div class="form-group">
        <a href="vista-previa.php" target="_blank">Haz click aquí para ver la vista previa de la empresa</a>
    </div>
-->
    <div class="space50"></div>
	<?php if(isset($_POST['cboEmpresa']) && !empty($_POST['cboEmpresa'])){ ?>
    <ul class="nav nav-pills" role="tablist">
        <li class="active"><a href="#general" role="tab" data-toggle="tab">General</a></li>
        <li><a href="#fichas" role="tab" data-toggle="tab">Fichas</a></li>
        <li><a href="#contacto" role="tab" data-toggle="tab">Contacto</a></li>
    </ul>

    <div class="tab-content">
      <form action="PDFs/institucional.php" style="float:right;" target="_blank" method="POST">
          <button type="submit" class="btn btn-info">
            <span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir Institucional
          </button>
          <input type="hidden" name="idempresa" value="<?php echo $editarempresa; ?>" >
      </form>
			<div id="general" class="tab-pane fade in active">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<form id="generalinst" method="POST" role="form" enctype="multipart/form-data">
							<input type="hidden" name="idemp" id="idemp" value="<?php echo $editarempresa; ?>" >
							<div class="form-group text-center">
								<img src="<?php echo $imagen; ?>" id="imagenempresa" class="img-circle img-user tooltipster" title="Logo o imagen de la empresa" alt="Logo o imagen de la empresa">
								<p>
									<div class="fileUpload btn btn-default">
										<span>Seleccionar imagen</span>
										<input type="file" name="imagen" id="imgInp" class="upload" />
									</div>
								</p>
							</div>

							<div class="form-group">
								<label for="txtSitioOficial" class="">Sitio oficial</label>
								<input type="text" id="txtSitioOficial" name="txtSitioOficial" class="form-control" placeholder="www.mi-empresa.com">
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								<div class="pull-right">
									<button type="submit" id="btnGuardarGeneral" class="btn btn-primary">Guardar cambios</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>


        <div id="fichas" class="tab-pane fade">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="bs-callout bs-callout-primary">
                        Las fichas son pestañas personalizables con información de la empresa que se mostrarán en la vista previa de la misma. Puedes crear hasta cuatro fichas por empresa. Elige una ficha en la lista desplegable para modificarla o crea una nueva.
                    </div>

                    <div class="space20"></div>
					<form action="institucional.php" method="POST" class="form-horizontal" role="form">
                        <div class="form-group">
                            <div class="form-inline">
                                <label for="cboFichas" class="control-label">Ficha: </label>

									<input type="hidden" name="cboEmpresa" id="cboEmpresa" value="<?php echo $editarempresa; ?>" >
									<select class="form-control" name="cboFichas" onchange="this.form.submit()" id="cboFichas" style="min-width: 70%; max-width: 100%;">
										<?php if (!isset($_POST['cboFichas'])) echo "<option disabled selected>Selecciona una ficha</option>";
											$con_fichas=consulta("select * from fichas where idempresa='$editarempresa'");
											while ($ficha = mysqli_fetch_array($con_fichas, MYSQLI_ASSOC)) {
												$idficha = $ficha['idficha'];
												$titulo = $ficha['titulo'];
										?>
										<option value="<?php echo $idficha; ?>"><?php echo $titulo; ?></option>
									<?php } ?>
									</select>
					</form>
                                <input type="button" class="btn btn-success btn-sm" id="btnNuevaFicha" value="Nueva" />
								<?php if(isset($editarficha) && !empty($editarficha)) { ?>
									<form id="eliminarficha" style="display: inline-block">
										<input type="submit" class="btn btn-danger btn-sm" id="btnEliminarFicha" value="Eliminar" />
										<input type="hidden" name="idemp" id="idemp" value="<?php echo $editarempresa; ?>" >
										<input type="hidden" id="delficha" name="delficha" value="<?php echo $editarficha; ?>" >
									</form>
								<?php } ?>
                            </div>
                        </div>

					<form id="frmFicha" method="POST" class="form-horizontal sr-only" role="form" enctype="multipart/form-data">
						<input type="hidden" name="idemp" id="idemp" value="<?php echo $editarempresa; ?>" >
						<input type="hidden" name="idficha" id="idficha" value="<?php echo $editarficha; ?>" >
                        <div class="form-group text-center">
							<img src="<?php echo $imagenficha; ?>" id="imagenficha" class="img-user tooltipster" title="Logo o imagen de la empresa" alt="Logo o imagen de la empresa">
							<p>
								<div class="fileUpload btn btn-default">
									<span>Seleccionar imagen</span>
									<input type="file" name="imagenficha" id="imgFic" class="upload" />
								</div>
							</p>
						</div>

                        <div class="form-group">
                            <label class="sr-only" for="txtTitulo">Título</label>
                            <input type="text" name="txtTitulo" id="txtTitulo" placeholder="Título" value="<?php echo $tituloficha; ?>" class="form-control" required>
                        </div>

                        <div class="wysiwyg-editor" id="editor">asdasdasdasd asdasdasd</div>
                        <br>
                        <div class="form-group">
                            <div class="pull-right">
                                <button type="submit" id="btnGuardarFicha" class="btn btn-primary">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="contacto" class="tab-pane fade">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form id="infocontacto" class="form-horizontal" role="form">
						<input type="hidden" name="idemp" id="idemp" value="<?php echo $editarempresa; ?>" >
                        <div class="space20"></div>
                        <div class="form-group">
                            <label class="sr-only" for="txtNombre">Nombre</label>
                            <input type="text" id="txtNombre" name="txtNombre" placeholder="Nombre del contacto" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="sr-only" for="txtTelefono">Teléfono</label>
                            <input type="text" id="txtTelefono" name="txtTelefono" placeholder="Teléfono" class="form-control">
                        </div>

                        <div class="form-group">
                            <label class="sr-only" for="txtEmail">Email</label>
                            <input type="text" id="txtEmail" name="txtEmail" placeholder="Email" class="form-control">
                        </div>

                        <div class="form-group">
                            <div class="pull-right">
                                <button type="submit" id="btnGuardarContacto" class="btn btn-primary">Guardar cambios</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	<?php } ?>
    <!-- Final Page Content -->

    <!-- Final Page Content -->
  </div>
 </div>
</div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

  	<script src="assets/js/jquery-ui.custom.min.js"></script>
	<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
	<script src="assets/js/jquery.hotkeys.index.min.js"></script>
	<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
	<script src="assets/js/bootbox.js"></script>

	<script type="text/javascript">
jQuery(function($){
		
	
	function showErrorAlert (reason, detail) {
		var msg='';
		if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
		else {
			//console.log("error uploading file", reason, detail);
		}
		$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
		 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
	}


	//but we want to change a few buttons colors for the third style
	$('#editor').ace_wysiwyg({
		toolbar:
		[
			'font',
			null,
			'fontSize',
			null,
			{name:'bold', className:'btn-info'},
			{name:'italic', className:'btn-info'},
			{name:'strikethrough', className:'btn-info'},
			{name:'underline', className:'btn-info'},
			null,
			{name:'insertunorderedlist', className:'btn-success'},
			{name:'insertorderedlist', className:'btn-success'},
			{name:'outdent', className:'btn-purple'},
			{name:'indent', className:'btn-purple'},
			null,
			{name:'justifyleft', className:'btn-primary'},
			{name:'justifycenter', className:'btn-primary'},
			{name:'justifyright', className:'btn-primary'},
			{name:'justifyfull', className:'btn-inverse'},
			null,
			{name:'createLink', className:'btn-pink'},
			{name:'unlink', className:'btn-pink'},
			null,
			{name:'insertImage', className:'btn-success'},
			null,
			'foreColor',
			null,
			{name:'undo', className:'btn-grey'},
			{name:'redo', className:'btn-grey'}
		],
		'wysiwyg': {
			fileUploadError: showErrorAlert
		}
	}).prev().addClass('wysiwyg-style1');

	
	/**
	//make the editor have all the available height
	$(window).on('resize.editor', function() {
		var offset = $('#editor1').parent().offset();
		var winHeight =  $(this).height();
		
		$('#editor1').css({'height':winHeight - offset.top - 10, 'max-height': 'none'});
	}).triggerHandler('resize.editor');
	*/
	

	//RESIZE IMAGE
	
	//Add Image Resize Functionality to Chrome and Safari
	//webkit browsers don't have image resize functionality when content is editable
	//so let's add something using jQuery UI resizable
	//another option would be opening a dialog for user to enter dimensions.
	if ( typeof jQuery.ui !== 'undefined' && ace.vars['webkit'] ) {
		
		var lastResizableImg = null;
		function destroyResizable() {
			if(lastResizableImg == null) return;
			lastResizableImg.resizable( "destroy" );
			lastResizableImg.removeData('resizable');
			lastResizableImg = null;
		}

		var enableImageResize = function() {
			$('.wysiwyg-editor')
			.on('mousedown', function(e) {
				var target = $(e.target);
				if( e.target instanceof HTMLImageElement ) {
					if( !target.data('resizable') ) {
						target.resizable({
							aspectRatio: e.target.width / e.target.height,
						});
						target.data('resizable', true);
						
						if( lastResizableImg != null ) {
							//disable previous resizable image
							lastResizableImg.resizable( "destroy" );
							lastResizableImg.removeData('resizable');
						}
						lastResizableImg = target;
					}
				}
			})
			.on('click', function(e) {
				if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
					destroyResizable();
				}
			})
			.on('keydown', function() {
				destroyResizable();
			});
	    }

		enableImageResize();

		/**
		//or we can load the jQuery UI dynamically only if needed
		if (typeof jQuery.ui !== 'undefined') enableImageResize();
		else {//load jQuery UI if not loaded
			//in Ace demo ./components will be replaced by correct components path
			$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
				enableImageResize()
			});
		}
		*/
	}


});
		$(document).ready(function() {
			//Cambiar imagen de empresa
				function readURL(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('#imagenempresa').attr('src', e.target.result);
						}

						reader.readAsDataURL(input.files[0]);
					}
				}
				$("#imgInp").change(function(){
					readURL(this);
				});
			//Cambiar imagen de ficha
				function readURL2(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('#imagenficha').attr('src', e.target.result);
						}

						reader.readAsDataURL(input.files[0]);
					}
				}
				$("#imgFic").change(function(){
					readURL2(this);
				});
			<?php
			if(isset($_POST['cboEmpresa']) && !empty($_POST['cboEmpresa'])){
				echo "$( '#cboEmpresa' ).val( '$editarempresa' );";
				echo "$( '#txtSitioOficial' ).val( '$web' );";
				echo "$( '#txtNombre' ).val( '$nombrec' );";
				echo "$( '#txtTelefono' ).val( '$telefonoc' );";
				echo "$( '#txtEmail' ).val( '$emailc' );";
			}
			if(isset($editarficha) && !empty($editarficha)) {
				echo "$( '#cboFichas' ).val( '$editarficha' );";
			}
			?>
			// Eliminar ficha
			$("#eliminarficha").on('submit',function(event){
				event.preventDefault();
				console.log($(this));
				data = $(this).serialize();

				BootstrapDialog.show({
					title: 'Eliminar Ficha',
					message: '¿Esta seguro que desea eliminar esta ficha?',
					buttons: [{
						label: ' Aceptar',
						cssClass: 'btn-primary',
						action: function(){
							$.ajax({
								type: "POST",
								url: "empresa_institucional.php",
								data: data
								}).done(function( msg ) {
									BootstrapDialog.show({
										message: msg,
										buttons: [{
											label: 'Aceptar',
											action: function() {
												window.location.reload();
											}
										}]
									});
								});
							dialogItself.close();
						}
					}, {
						label: ' Cancelar',
						cssClass: 'btn-default',
						action: function(dialogItself){
							dialogItself.close();
						}
					}]
				});
			});
			// General
			$("#generalinst").on('submit',function(event){
				event.preventDefault();
				var formData = new FormData($(this)[0]);

				$.ajax({
					type: "POST",
					url: "empresa_institucional.php",
					data: formData,
					contentType: false,
					cache: false,
						processData: false
				}).done(function( msg ) {
					BootstrapDialog.show({
					message: msg,
						buttons: [{
							label: 'Aceptar',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				});
			});
			// Ficha
			$("#frmFicha").on('submit',function(event){
				event.preventDefault();
				var formData = new FormData($(this)[0]);

				var hidden_input =
					$('<input type="hidden" name="txtDescripcion" />')
					.appendTo('#frmFicha');

					  var html_content = $('#editor').html();
					  hidden_input.val( html_content );

					  var desc = hidden_input.val();
				formData.append('txtDescripcion', desc);

				$.ajax({
					type: "POST",
					url: "empresa_institucional.php",
					data: formData,
					contentType: false,
					cache: false,
						processData: false
				}).done(function( msg ) {
					BootstrapDialog.show({
					message: msg,
						buttons: [{
							label: 'Aceptar',
							action: function(dialogItself){
								window.location.reload();
							}
						}]
					});
				});

			});
			// Informarion Contacto
			$("#infocontacto").on('submit',function(event){
				event.preventDefault();
				data = $(this).serialize();

				$.ajax({
				type: "POST",
				url: "empresa_institucional.php",
				data: data
				}).done(function( msg ) {
					BootstrapDialog.show({
						message: msg,
						buttons: [{
							label: 'Aceptar',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				});
			});

			// Ocultar mostrar ficha
            $("#btnNuevaFicha").click(function() {
                $("#frmFicha").removeClass("sr-only");
				$("#imagenficha").attr("src", "images/logo.png");
				$("#txtTitulo").val("");
				$("#idficha").val("");
				$('#editor').html("");
            });

            $( ".target" ).change(function() {
                 $("#frmFicha").toggleClass("sr-only");
            });

			<?php if(isset($_POST['cboFichas']) && !empty($_POST['cboFichas'])) {
					echo "$('.nav-pills a[href=\"#fichas\"]').tab('show');";
					echo "$(\"#frmFicha\").toggleClass(\"sr-only\");";
					echo "$('#editor').html('".$textoficha."');";
				}
			?>
		});
    </script>

    <style type="text/css">
        @media(max-width:768px) {
        #eliminarficha {
            margin-top: 10px;
        }
    </style>
</body>
</html>
