<?php
	session_start();
	include "funciones.php";
	
		$idperiodo = $_SESSION['idperiodo'];
		$idempresa = $_SESSION['idempresa'];
		$idlista = $_POST['idlista'];
	
	if (isset($_POST['ncuenta']) && !empty($_POST['ncuenta']) &&
		isset($_POST['detalles']) && !empty($_POST['detalles']) &&
		isset($_POST['debe']) && !empty($_POST['debe']) &&
		isset($_POST['haber']) && !empty($_POST['haber']) &&
		isset($_POST['nlista']) && !empty($_POST['nlista']) &&
		isset($_POST['denominacion']) && !empty($_POST['denominacion']) &&
		isset($_POST['fecha']) && !empty($_POST['fecha'])) {

		$ncuenta = $_POST['ncuenta'];
		$detalle = $_POST['detalles'];
		$debe = $_POST['debe'];
		$haber = $_POST['haber'];
		$nota = trim($_POST['nota']);
		$nlista = trim($_POST['nlista']);
		$denominacion = trim($_POST['denominacion']);
		$fechalista = trim($_POST['fecha']);
		$sumadebe = 0;
		$sumahaber = 0;
		
		$con_p=consulta("SELECT YEAR(desde) as 'desde',YEAR(hasta) as 'hasta' FROM periodos WHERE idperiodo='$idperiodo'");
		$peri=mysqli_fetch_array($con_p);
		$desde = $peri['desde'];
		$hasta = $peri['hasta'];
		$año = substr($fechalista, -4);
		$fechaalmacenable = fecha($fechalista,"-");
			
		if(testfecha($fechalista)===false) {
			echo "La fecha ingresada no es correcta.";
		} else if($año < $desde || $año > $hasta) {
			echo "La fecha ingresada no corresponde al periodo actual seleccionado.";
		} else {
			$buscarnlista = consulta("SELECT * FROM listaasientos WHERE idempresa = '$idempresa' AND idperiodo = '$idperiodo' AND nlista = '$nlista'");
			if(mysqli_num_rows($buscarnlista) > 0) { // El numero de lista ya existe
				consulta("UPDATE listaasientos SET nlista = nlista + 1 WHERE idempresa = '$idempresa' AND idperiodo = '$idperiodo' AND nlista >= '$nlista'");
			}
			if($idlista != 0){
				consulta("DELETE FROM asientos WHERE idlista='$idlista'");
				for ($x=0;$x<count($ncuenta); $x++) { 
					if($ncuenta[$x] != 0){
					$con_cuenta=consulta("SELECT * FROM cuentas WHERE codigo='$ncuenta[$x]' AND denominacion='$detalle[$x]' AND idempresa = '$idempresa'");
					$c=mysqli_fetch_array($con_cuenta);
					$idcuenta = $c['idcuenta'];
					$carga = consulta("INSERT INTO asientos (idlista, idcuenta, debe, haber) 
										VALUES ('$idlista', '$idcuenta', '$debe[$x]', '$haber[$x]')");
					$sumadebe += $debe[$x];
					$sumahaber += $haber[$x];
					}
				}
				$cargaresto = consulta("UPDATE listaasientos SET fecha = '$fechaalmacenable', denominacion = '$denominacion', nlista = '$nlista', debe = '$sumadebe', haber = '$sumahaber', nota = '$nota' WHERE idlista = '$idlista'");
				if(!$carga || !$cargaresto){
					echo "Error: ".mysqli_error();
				} else {
					echo "El asiento se ha cargado con exito.";
				}
			} else {
				$new = mysqli_query($link, "INSERT INTO listaasientos 
				(idperiodo,idempresa,fecha,denominacion,nlista,nota) values
				('$idperiodo','$idempresa','$fechaalmacenable','$denominacion','$nlista','$nota')");
				$idlistacreada = mysqli_insert_id($link);
				
				for ($x=0;$x<count($ncuenta); $x++) { 
					$con_cuenta=consulta("SELECT * FROM cuentas WHERE codigo='$ncuenta[$x]' AND denominacion='$detalle[$x]' AND idempresa = '$idempresa'");
					$c=mysqli_fetch_array($con_cuenta);
					$idcuenta = $c['idcuenta'];
					if($ncuenta[$x] != 0){
					$carga = consulta("INSERT INTO asientos (idlista, idcuenta, debe, haber) 
										VALUES ('$idlistacreada', '$idcuenta', '$debe[$x]', '$haber[$x]')");
					$sumadebe += $debe[$x];
					$sumahaber += $haber[$x];
					}
				}
				$cargaresto = consulta("UPDATE listaasientos SET debe = '$sumadebe', haber = '$sumahaber' WHERE idlista = '$idlistacreada'");
				if(!$carga || !$cargaresto){
					echo "Error: ".mysqli_error();
				} else {
					acthistempresa($_SESSION['rsocial'], "Se creo un nuevo asiento");
					echo "El asiento se ha cargado con exito.";
				}
			}
		}
	} else {
		echo "Faltan datos para cargar el asiento.";
	}
?>