<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Simedu Simulador Empresarial Contable Educativo">
	<meta name="author" content="Nery Brugnoni">
	<meta name="author" content="Luciano Ciattaglia">
	<meta name="author" content="Nicolás Cáceres">

	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />

	<!-- jQuery -->
	<script src="js/jquery-2.1.4.min.js"></script>

	<!-- CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
    <title>Simedu | Términos y Condiciones</title>
</head>

<body>
     <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="jumbotron">
              <div class="container">
                <h1>Términos y condiciones</h1>
                <p>Todo lo que necesitas saber, en un solo sitio.</p>
                <p><a class="btn btn-primary btn-lg" href="/simedu/" role="button">Ir al inicio »</a></p>
              </div>
            </div>

            <div class="container">
                <h1 class="page-header">Te damos la bienvenida a Simedu</h1>
                <span>
                    Te agradecemos el hecho de que hayas elegido Simedu. El uso de nuestro producto implica la aceptación de estas condiciones. Te recomendamos que las leas detenidamente.
                    <p>Última modificación: 30 de abril de 2014</p>
                </span>

                <h1 class="page-header">Condiciones de uso</h1>
                <span>
                    Estas condiciones de uso son el <strong>contrato</strong> bajo el cual se rigen las relaciones entre <strong>«simedu.com»</strong> y cualquier usuario directo o indirecto (en adelante, <strong>«Usuario»</strong>) de los servicios de registro en el servidor de <strong>Simedu</strong> (en adelante, <strong>«Servicio»</strong>) para hogares o para empresas. Este <strong>contrato</strong> es válido con respecto a los servicios prestados por <strong>Simedu</strong>. Al suscribirse al <strong>«Servicio»</strong>, el <strong>usuario</strong> reconoce haber leído y comprendido este <strong>contrato</strong>. El <strong>usuario</strong> se compromete a utilizar el <strong>«Servicio»</strong> solo para fines legales. Si <strong>Simedu</strong> considera a su solo juicio que un <strong>usuario</strong> esta utilizando el <strong>«Servicio»</strong> para fines no legales, <strong>Simedu</strong> se reserva el derecho de finalizar inmediatamente y sin anuncio previo el <strong>«Servicio»</strong> de dicho <strong>usuario</strong>. 
                    <strong>Simedu</strong>  se reserva el derecho de suspender o terminar el <strong>«Servicio»</strong> de cualquier <strong>usuario</strong> en cualquier momento por decisión propia.
                    <strong>Simedu</strong> puede modificar los términos y condiciones de este <strong>contrato</strong>. Los cambios se consideraran comunicados y efectivos el día que sean publicados en la página de internet de <strong>Simedu (www.simedu.com)</strong>.
                    <strong>Simedu</strong>  puede asimismo, realizar modificaciones al sitio, materiales, o servicios en cualquier momento y sin aviso previo.
                </span>
                     
                <h1 class="page-header">Servicio</h1>
                <span>
                    El <strong>«Servicio»</strong> ofrecido es el registro en el servidor TY de  <strong>Simedu</strong>  y permite a los <strong>usuarios</strong> registrados realizar Altas, Bajas, Modificaciones de datos,  generar informes e importar información. Por lo cúal es responsabilidad total del <strong>usuario</strong> la información que allí se encuentra. <strong>Simedu</strong> no se hace responsable por la pérdida de información.
                </span>

                <h1 class="page-header">Requisitos de funcionamiento</h1>
                <span>
                    El <strong>usuario</strong> comprende que el <strong>«Servicio»</strong> requiere una conexión de internet 
                    El requerimiento de ancho de banda es de 56 kbps de downstream y 56  kbps de upstream.
                    El <strong>«Servicio»</strong> por medio del software sólo puede ser utilizado desde una PC compatible que corra un sistema operativo Windows en las versiones posteriores a Windows 98. Se requiere que la máquina tenga mínimamente un procesador Pentium II y 128 MB de memoria.
                </span>
                

                <p>Tecnología:</p>
                <p>Fallas en el servicio: </p>
                <p>Utilización del servicio: </p>
                <p>Inactividad: </p>

                <p>SIMEDU  se reserva el derecho de terminar, finalizar, cerrar, o anular, cualquier cuenta que no se haya utilizado por 90 días o más.</p>

                <h1 class="page-header">Jurisdicción</h1>
                <span>
                    Las presentes Condiciones de <strong>«Servicio»</strong> se regirán por las leyes de la República Argentina. Cualquier controversia será sometida a los Tribunales Ordinarios en lo Comercial de la Ciudad de Córdoba, Argentina. 
                </span>
            </div>
        </div>
    </div>
     <!-- Final Page Content -->

    <?php include "footer.php" ?>
</body>
</html>