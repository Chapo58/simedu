<?php

session_start();
include "funciones.php";
$idusuario = $_SESSION['idusuario'];
$idempresa = $_SESSION['idempresa'];
$idperiodo = $_SESSION['idperiodo'];
$fecha =  date("Y-m-d"); 
$hora =	date("H:i:s");

$comment = isset($_REQUEST['comment']) ? $_REQUEST['comment'] : ''; 
$screenshot =  isset($_REQUEST['screenshot']) ? $_REQUEST['screenshot'] : false;

/* convert screen shot to tmp file in current folder */
if($screenshot) $screenshot = base64_to_jpg($screenshot, time().'_'.rand(0,30).'.jpg');

$rutaimagen = "";

if($screenshot){
	$rutaimagen = "images/errores/" . $idusuario . rand() . '.jpg';
		if (file_exists($rutaimagen)) {
			unlink($rutaimagen);
			move_uploaded_file($_FILES["screenshot"]["tmp_name"], $rutaimagen);
		} else {
			// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
			move_uploaded_file($_FILES["screenshot"]["tmp_name"], $rutaimagen);
		}
}

/*
*
* Since you have a lot of options for mail, and you're better off not using mail() 
* it will be up to you on how you want to send this e-mail, I use laravel, if not I use phpmailer directly - so attaching files is easy.
*
* $comment = what was typed
* $screenshot = the tmp file created for the screen shot.
* 
* Be sure to unlink the screenshot after you send it.
*
*/

consulta("INSERT INTO errores(idusuario,idempresa,idperiodo,comentario,ruta,fecha,hora) VALUES('$idusuario','$idempresa','$idperiodo','$comment','$rutaimagen','$fecha','$hora');");


$con_usr=consulta("SELECT * from usuario WHERE idusuario='$idusuario'");
$usuario=mysqli_fetch_array($con_usr);

$correoremitente = $usuario['email'];

$remitente = $usuario['nombre']." ".$usuario['apellido']." (".$usuario['email'].") [".$idusuario."]";
$mensaje = $comment;
$destinatario = "luciano@ciattaglia.com"; 
$asunto = "Consulta de $remitente"; 

//dirección del remitente 
// $headers = "From: $remitente <$correoremitente>\r\n"; 
		
mail($destinatario,$asunto,$mensaje);

echo json_encode(array('result' => 'success'));

/* comment out if you want to see the file */
//if($screenshot) unlink($screenshot);


/* function to conver base64 to jpg image */
function base64_to_jpg($string, $file) {
  $fp = fopen($file, "wb"); 
  $data = explode(',', $string);
  fwrite($fp, base64_decode($data[1])); 
  fclose($fp); 
  return $file; 
}