<?php
session_start();
include "funciones.php";
try
{
	$idempresa = $_SESSION['idempresa'];
	
	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM sueldos_codigos_empleados WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscardescripcion']) && !empty($_POST['buscardescripcion'])) {
			$buscardescripcion = $_POST['buscardescripcion'];
		} else {
			$buscardescripcion = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM sueldos_codigos_empleados WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$idempleado = $_POST["idempleado"];
		$idcodigo = $_POST["idcodigo"];
		$multiplicador = $_POST["multiplicador"];
		$divisor = $_POST["divisor"];
		$cantidad = $_POST["cantidad"];
		
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO sueldos_codigos_empleados(idempresa, idempleado, idcodigo, multiplicador, divisor, cantidad) VALUES('$idempresa','$idempleado','$idcodigo','$multiplicador','$divisor','$cantidad');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM sueldos_codigos_empleados WHERE idcodemp = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se creo un nuevo codigo");
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idcodemp"];
		$idempleado = $_POST["idempleado"];
		$idcodigo = $_POST["idcodigo"];
		$multiplicador = $_POST["multiplicador"];
		$divisor = $_POST["divisor"];
		$cantidad = $_POST["cantidad"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE sueldos_codigos_empleados SET idempleado = '$idempleado', idcodigo = '$idcodigo', multiplicador = '$multiplicador', divisor = '$divisor', cantidad = '$cantidad' WHERE idcodemp = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se modifico un codigo");
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idcodemp"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM sueldos_codigos_empleados WHERE idcodemp = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se elimino un codigo");
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>