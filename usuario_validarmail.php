<?php
	include "funciones.php";
	
	if (isset($_POST['email']) && !empty($_POST['email'])) {
	
		$email = trim($_POST['email']);
		
		$con_u=consulta("SELECT * FROM usuario WHERE email='$email'");
		$u=mysqli_fetch_array($con_u);
		if(mysqli_num_rows($con_u)==0){
			echo '<div class="alert alert-warning"> No existe una cuenta asociada a ese correo. </div>';
		} else {
			$idusuario = $u['idusuario'];
			// Se genera una cadena para validar el cambio de contraseña
		   $cadena = $idusuario.$email.rand(1,9999999).date('Y-m-d');
		   $token = sha1($cadena);
		 
			$con_existe=consulta("SELECT * FROM tblreseteopass WHERE idusuario='$idusuario'");
			$ex=mysqli_fetch_array($con_existe);
			if(mysqli_num_rows($con_ex)>0){ 
				// Se actualiza el registro en la tabla tblreseteopass
				consulta("UPDATE tblreseteopass SET token = '$token', creado = NOW() WHERE idusuario = '$idusuario';");
			} else {
				// Se inserta el registro en la tabla tblreseteopass
				consulta("INSERT INTO tblreseteopass (idusuario, email, token, creado) VALUES($idusuario,'$email','$token',NOW());");
			}
		   
		   // Se devuelve el link que se enviara al usuario
			$enlace = $_SERVER["SERVER_NAME"].'/usuario_restablecer.php?idusuario='.sha1($idusuario).'&token='.$token;
			
			$mensaje = '<html>
			 <head>
				<title>Restablece tu contraseña</title>
			 </head>
			 <body>
			   <p>Hemos recibido una petición para restablecer la contraseña de tu cuenta en Simedu.</p>
			   <p>Si hiciste esta petición, haz clic en el siguiente enlace, si no hiciste esta petición puedes ignorar este correo.</p>
			   <p>
				 <strong>Enlace para restablecer tu contraseña</strong><br>
				 <a href="'.$enlace.'"> Restablecer contraseña </a>
			   </p>
			 </body>
			</html>';
		 
		   $cabeceras = 'MIME-Version: 1.0' . "\r\n";
		   $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		   $cabeceras .= 'From: Simedu <soporte@simedu.com.ar>' . "\r\n";
		   // Se envia el correo al usuario
		   mail($email, "Recuperar contraseña", $mensaje, $cabeceras);
		   
		   echo '<div class="alert alert-info"> Un correo ha sido enviado a su cuenta de email con las instrucciones para restablecer la contraseña </div>';
		}
	} else {
		echo "Debe ingresar un correo electronico.";
	}

?>