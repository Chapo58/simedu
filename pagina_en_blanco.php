<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Inicio</title>
		<?php require_once('head.php'); ?>
	</head>

	<body class="no-skin">

		<?php require_once('header.php'); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Inicio</a>
							</li>
							<li class="active">Pagina de Inicio</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
				<div class="page-content">



					<!-- Final Page Content -->
				</div>
			</div>
		</div><!-- /.main-content -->

			<?php require_once('footer.php'); ?>
	</body>
</html>
