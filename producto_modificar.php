<?php
	include "funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['nombre']) && !empty($_POST['nombre']) &&
		isset($_POST['preciovta']) && !empty($_POST['preciovta']) &&
		isset($_POST['idart']) && !empty($_POST['idart'])) {
		
		// Quito espacios en blanco
		$id = trim($_POST['idart']);
		$nombre = trim($_POST['nombre']);
		$descripcion = trim($_POST['descripcion']);
		if(!empty($_POST['precio'])) {$preciocosto = $_POST['precio'];} else {$preciocosto = 0;}
		$preciovta = $_POST['preciovta'];
	@	$iva = $_POST['iva'];
		$stock = $_POST['stock'];
		$stockmin = $_POST['stockmin'];
		if(!empty($_POST['categoria'])) {$categoria = $_POST['categoria'];} else {$categoria = 0;}
		if(!empty($_POST['marca'])) {$marca = $_POST['marca'];} else {$marca = 0;}
		if(!empty($_POST['proveedor'])) {$proveedor = $_POST['proveedor'];} else {$proveedor = 0;}
		if(!empty($_POST['lista'])) {$lista = $_POST['lista'];} else {$lista = 0;}
	//	if(!empty($_POST['ncuenta'])) {$ncuenta = $_POST['ncuenta'];} else {$ncuenta = 0;}
		if(isset($_POST['habilitado'])) {$habilitado = 1;}else{$habilitado = 2;};
		
		// Paso a mayusculas
		$nombre = ucfirst($nombre);
		$descripcion = ucfirst($descripcion);
		
		$editar = "UPDATE productos
									SET
									nombre = '$nombre',
									descripcion = '$descripcion',
									preciocosto = '$preciocosto',
									preciovta = '$preciovta',
									iva = '$iva',
									stock = '$stock',
									stockmin = '$stockmin',
									idcategoria = '$categoria',
									idmarca = '$marca',
									idproveedor = '$proveedor',
									idlistap = '$lista',
									habilitado = '$habilitado'";
		
									
			if(!empty($_FILES["imagen"]["name"])){
				if ((($_FILES["imagen"]["type"] == "image/gif")
					|| ($_FILES["imagen"]["type"] == "image/jpeg")
					|| ($_FILES["imagen"]["type"] == "image/jpg")
					|| ($_FILES["imagen"]["type"] == "image/png"))) {
					if ($_FILES["imagen"]["error"] > 0) {
						echo "Return Code: " . $_FILES["imagen"]["error"] . "<br>";
					} else {
						$extension = substr($_FILES["imagen"]["type"], 6);
						$nombreimg = $idempresa . rand();
						$rutaimagen = "images/productos/" . $nombreimg . '.' . $extension;
						if (file_exists($rutaimagen)) {
							unlink($rutaimagen);
							move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
							$editar .= ", imagen = '$rutaimagen'";
						} else {
							// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
							move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
							$editar .= ", imagen = '$rutaimagen'";
						}
					}
				} else {
					echo "Archivo Inválido";
				}
			}
		
			$editar .= "WHERE idproducto = '$id'";
									
				$cargar = consulta($editar);
				
				if(!$cargar){
						echo "Mensaje: ".mysqli_error();
						mensaje("Error");
				}
				mensaje("El producto se actualizo con exito.");
				acthistempresa($rsocial, "Se modifico un producto");
				ir_a("productos.php");
		
	} else {
		mensaje("No se cargaron todos los datos");
		ir_a("productos.php");
	}
?>
