<?php	
	include("funciones.php");
?>
<!DOCTYPE html>
<html class="full" lang="es">

<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Simedu simular empresarial educativo">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="Nery Brugnoni Luciano Ciattaglia">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
	
	<!-- jQuery -->
    <script src="js/jquery-2.1.4.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/utilidades.js"></script>

   <title>Simedu | Crear cuenta</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">

    <!-- Custom CSS -->
    <style type="text/css">
        body {
            margin-top: 50px;
            margin-bottom: 50px;
            background: none;
        }

        .full {
          background-color: #fff;
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }

        .navbar-inverse {
          background-color: #3071a9;
          border: none !important;
        }

        .navbar-inverse .navbar-brand {
          color: #000;
        }

        .navbar-inverse .navbar-brand:hover,
        .navbar-inverse .navbar-brand:focus,
        .navbar-inverse .navbar-brand:active {
          color: #000;
        }

        .navbar-inverse a {
          color: #fff;
        }

        .navbar-inverse a:hover,
        .navbar-inverse a:focus,
        .navbar-inverse a:active {
          text-decoration: none;
        }
    </style>
	
	<script type="text/javascript">
		$(document).ready(function() {    
			$('#email').blur(function(){

				var email = $(this).val();        
				var dataString = 'email='+email;
				$.ajax({
					type: "POST",
					url: "validaciones.php",
					data: dataString,
					}).done(function( msg ) {
						document.getElementById('validar').innerHTML = msg;
					});
				});
			});                
	</script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <span class="navbar-brand"><a href="index.php">Ya tienes una cuenta? Inicia sesión!</a></span>
            </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <h1>Regístrate. Sorpréndete.</h1>
                <p>El simulador educativo SIMEDU te permite crear una cuenta usando tu correo electrónico.
					Hazlo ahora mismo.</p>
                <form role="form" data-toggle="validator" action="usuario_registro.php" method="POST">
				  <div class="form-group">
                    <label for="txtEmail" class="sr-only">Email</label>
                    <input type="email" id="email" name="email" maxlength="50" class="form-control" placeholder="Email" data-error="Ingresa un correo válido" required>
                    <div class="help-block with-errors" id="validar"></div>
                  </div>
				  <div class="form-group">
				    <label for="txtPassword" class="sr-only">Contraseña</label>
				    <input type="password" name="pass" id="txtPassword" class="form-control" placeholder="Contraseña" data-minlength="6" data-error="6 caracteres como mínimo" required>
                    <div class="help-block with-errors"></div>
				  </div>
                  <div class="form-group">
                    <label for="txtRepitePassword" class="sr-only">Repite contraseña</label>
                    <input type="password" name="pass2" id="txtRepitePassword" class="form-control" placeholder="Repite la contraseña" data-match="#txtPassword" data-error="Las contraseñas no coinciden" required>
                    <div class="help-block with-errors"></div>
                  </div>
				  <div class="form-group">
				  <label for="cboTipoUsuario" class="sr-only">Tipo de usuario</label>
			      <select class="form-control" name="tipousr" id="cboTipoUsuario">
			        <option value="1">Alumno</option>
			        <option value="2">Profesor</option>
			      </select>
				  </div>
				  <p>Si es Alumno espere que el Docente lo agregue a su curso. Esta opcion de alumnos se utiliza para instituciones ó fundaciones solamente. Si Usted es Docente elija la opción correspondiente</p>
				  <p>Al hacer clic en Aceptar, indicas que has leído y aceptado los <a id="term" href="terminos-y-condiciones.php">Términos y Condiciones</a> del servicio.</p>
				  <button type="submit" class="btn btn-primary pull-right">Aceptar</button>
				</form>
            </div>
        </div>
    </div>

</body>
</html>