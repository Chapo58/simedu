<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Planetos</title>
    <?php require_once('head.php'); ?>
    <?php
      if(isset($_POST['idplanteo']) && !empty($_POST['idplanteo'])) {
        $idplanteo = $_SESSION['idplanteo'] = $_POST['idplanteo'];
        $con_p=consulta("SELECT * FROM planteos WHERE idplanteo='$idplanteo'");
              $p=mysqli_fetch_array($con_p);
        $titulo = $p['titulo'];
        $semaforo = $p['semaforo'];
        if($p['limite'] == "0000-00-00"){
          $limite = date("d/m/Y");
          $blimite = false;
        } else {
          $limite = date("d/m/Y",strtotime($p['limite']));
          $blimite = true;
        }
      } else {
        mensaje("Debe seleccionar un planteo");
        ir_a('planteos.php');
      }
    ?>
	<style type="text/css">
		.mensage{
			border:dashed 1px red;
			background-color:#FFC6C7;
			color: #000000;
			padding: 10px;
			text-align: left;
			margin: 10px auto;
			display: none;
		}
	</style>
</head>
<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-cubes home-icon"></i>
              <a href="planteo.php">Planteos</a>
            </li>
            <li><a href="planteos_docentes.php">Definicion de Planteos</a></li>
            <li class="active"><?php echo $titulo; ?></li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Definicion Planteo</h1>
  <form id="planteoform">
  <div class="bs-callout bs-callout-info">
	<div class="row">
		<div class="col-md-12">
            <input type="text" id="titulo" name="titulo" placeholder="Titulo" value="<?php echo $titulo; ?>" class="form-control">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-2">
			<span class="button-checkbox" onclick="togglefecha();">
				<button type="button" class="btn" data-color="info"><strong>&nbsp;&nbsp;<span class="fa fa-clock-o"></span> Reloj</strong></button>
				<input type="checkbox" name="limite" id="limite" class="hidden" />
			</span>
		</div>
		<div class="col-md-4">
			<div class="form-group" style="display:none" id="displayfecha">
			  <div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
					<input type="text" data-mask="99/99/9999" name="fechalimite" id="fechalimite" class="form-control" value="<?php echo $limite; ?>" placeholder="Fecha Limite" maxlength="10">
			  </div>
			</div>
		</div>
		<div class="col-md-6">
            <div class="form-group">
                <label class="sr-only" for="cboSexo">Semaforo</label>
				<div class="input-group">
				<div class="input-group-addon"><i class="fa fa-lightbulb-o"></i>&nbsp;&nbsp;<strong>Semaforo</strong></div>
					<select class="form-control" name="semaforo" id="semaforo">
						<option value="0">Sin Color</option>
                        <option value="verde">Verde</option>
                        <option value="amarillo">Amarillo</option>
						<option value="rojo">Rojo</option>
                    </select>
				</div>
            </div>
		</div>
	</div>
  </div>
  <button type="button" data-toggle="modal" onclick="nuevo()" data-target="#modalpreguntas" style="width:50%;" class="btn btn-primary btn-lg" title="Pregunta">
		<span class="fa fa-plus-square"></span>&nbsp;&nbsp;Agregar una pregunta
  </button>
  <div class="space20"></div>
	<table id="preguntas" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Preguntas</th>
				<th></th>
				<th></th>
            </tr>
        </thead>
		<tbody>
		<?php
			$con_preg=consulta("SELECT * FROM preguntas WHERE idplanteo='$idplanteo'");
			while ($pr = mysqli_fetch_array($con_preg, MYSQLI_ASSOC)) {
				$idpregunta = $pr['idpregunta'];
				$descripcion = $pr['descripcion'];

		?>
			<tr>
                <td><?php echo $descripcion; ?></td>
				<td class="text-center">
					<button type="button" onclick="traerDatos(<?php echo $idpregunta; ?>)" data-toggle="modal" data-target="#modalpreguntas" class="btn btn-primary btn-sm" title="Editar">
						<span class="glyphicon glyphicon-pencil"></span>
					</button>
				</td>
				<td class="text-center">
					<button type="button" onclick="eliminarPregunta($(this),<?php echo $idpregunta; ?>)" class="btn btn-danger btn-sm" title="Eliminar">
						<span class="glyphicon glyphicon-remove"></span>
					</button>
				</td>
            </tr>
		<?php } ?>
		</tbody>
    </table>
	<table id="tarchivos" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Archivos</th>
				<th></th>
				<th></th>
            </tr>
        </thead>
		<tbody>
		<?php

				$con_arch=consulta("SELECT * FROM archivos WHERE idplanteo='$idplanteo'");
				while ($ar = mysqli_fetch_array($con_arch, MYSQLI_ASSOC)) {
					$idarchivo = $ar['idarchivo'];
					$ruta = $ar['ruta'];
					$nombre = $ar['nombre'];
		?>
			<tr>
                <td width="80%"><?php echo $nombre; ?></td>
				<td class="text-center">
                    <button type="button" onclick="eliminarArchivo($(this), <?php echo $idarchivo; ?>)" class="btn btn-danger" title="Eliminar">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>&nbsp;&nbsp;Eliminar
					</button>
				</td>
				<td class="text-center">
					<button class="btn btn-primary" onclick="window.open('<?php echo $ruta ?>','_blank');">
						<i class="glyphicon glyphicon-eye-open"></i>&nbsp;&nbsp;Ver
					</button>
				</td>
            </tr>
		<?php } ?>
		</tbody>
    </table>
  <!-- Final Page Content -->
	<br />
			<div class="mensage"> Archivos Subidos Correctamente </div>
            <div class="row">
				<div class="col-md-9 col-xs-12">
					<div class="input-group">
						<span class="input-group-btn">
							<span class="btn btn-primary btn-file">
								<span class="glyphicon glyphicon-file"></span><strong>&nbsp;&nbsp;Seleccionar Archivos</strong><input type="file"  multiple="multiple" id="archivos">
							</span>
						</span>
						<input type="text" value="Seleccionar archivos a adjuntar al planteo" class="form-control" readonly>
					</div>
				</div>
				<div class="col-md-3 col-xs-12">
					<input type="button" id="subir" class="btn btn-info" style="float:right;width:100%;" value="Subir Archivos Seleccionados">
				</div>
			</div>
			<br />
			<input type="hidden" name="idplanteo" value="<?php echo $idplanteo; ?>">
			<div class="row">
				<div class="col-md-12">
					<button type="submit" style="float:right;" class="btn btn-success btn-lg">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Aceptar
					</button>
				</div>
			</div>
  </form>

  <!-- Final Page Content -->
<div id="modalpreguntas" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pregunta</h4>
      </div>
      <div class="modal-body">
	        <form id="cargarpregunta" class="form-horizontal" role="form" enctype="multipart/form-data">
			<div class="row">
	            <div class="col-md-12">
					<textarea class="form-control" rows="3" name="pregunta" id="pregunta" placeholder="Pregunta" required></textarea>
				</div>
			</div><br>
			<div class="row">
	            <div class="col-md-12 col-xs-12">
					<div class="input-group image-preview">
						<input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
						<span class="input-group-btn">
							<!-- image-preview-clear button -->
							<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
								<span class="glyphicon glyphicon-remove"></span> Limpiar
							</button>
							<!-- image-preview-input -->
							<div class="btn btn-default image-preview-input">
								<span class="glyphicon glyphicon-folder-open"></span>
								<span class="image-preview-input-title">Buscar</span>
								<input type="file" name="archivo"/> <!-- rename it -->
							</div>
						</span>
					</div>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="input-group">
					<div class="input-group-addon"><span class="glyphicon glyphicon-link"></span>&nbsp;<strong>Link</strong></div>
					<input type="text" class="form-control" name="link" id="link" placeholder="Enlace Externo">
					</div>
				</div>
			</div><br>
			<div class="row funkyradio" style="display:none" id="opcionesmultiples">
				<div class="col-md-12 col-xs-12 funkyradio-success">
					<input type="radio" value=1 name="radio" id="radio1" />
					<label for="radio1">
						<input type="text" class="form-control" style="text-align: center;" name="opcion1" id="opcion1" placeholder="Opcion 1">
					</label>
				</div>
				<div class="col-md-12 col-xs-12 funkyradio-success">
					<input type="radio" value=2 name="radio" id="radio2" />
					<label for="radio2">
						<input type="text" class="form-control" style="text-align: center;" name="opcion2" id="opcion2" placeholder="Opcion 2">
					</label>
				</div>
				<div class="col-md-12 col-xs-12 funkyradio-success">
					<input type="radio" value=3 name="radio" id="radio3" />
					<label for="radio3">
						<input type="text" class="form-control" style="text-align: center;" name="opcion3" id="opcion3" placeholder="Opcion 3">
					</label>
				</div>
				<div class="col-md-12 col-xs-12 funkyradio-success">
					<input type="radio" value=4 name="radio" id="radio4" />
					<label for="radio4">
						<input type="text" class="form-control" style="text-align: center;" name="opcion4" id="opcion4" placeholder="Opcion 4">
					</label>
				</div>
				<div class="col-md-12 col-xs-12 funkyradio-success">
					<input type="radio" value=5 name="radio" id="radio5" />
					<label for="radio5">
						<input type="text" class="form-control" style="text-align: center;" name="opcion5" id="opcion5" placeholder="Opcion 5">
					</label>
				</div>
				<div class="col-md-12 col-xs-12 funkyradio-success">
					<input type="radio" value=6 name="radio" id="radio6" />
					<label for="radio6">
						<input type="text" class="form-control" style="text-align: center;" name="opcion6" id="opcion6" placeholder="Opcion 6">
					</label>
				</div>
			</div>
			<input type="hidden" id="idpregunta" name="idpregunta">
			<input type="hidden" name="idplanteo" value="<?php echo $idplanteo; ?>">
      </div>
      <div class="modal-footer">
		<span class="button-checkbox" style="float:left;" onclick="toggleopciones();">
			<button type="button" class="btn" data-color="warning"><strong>Dado</strong></button>
			<input type="checkbox" id="multiple" class="hidden" />
		</span>
        <button type="button" class="btn btn-default" onclick="nuevo()" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Guardar</button>
			</form>
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

<script type="text/javascript">
	var tabla_preguntas;
	var tabla_archivos;
	<?php echo "var idplanteo = $idplanteo;"; ?>

	function togglefecha() {
		$('#displayfecha').toggle();
	}

	function toggleopciones() {
		$('#opcionesmultiples').toggle();
	}

	function remover(tr,tabla) {
		if(tabla == 1){
			tabla_preguntas.row(tr).remove().draw();
		} else {
			tabla_archivos.row(tr).remove().draw();
		}
	}

	function nuevo(){
		$("#pregunta").val("");
		$("#link").val("");
		$("#opcion1").val("");
		$("#opcion2").val("");
		$("#opcion3").val("");
		$("#opcion4").val("");
		$("#opcion5").val("");
		$("#opcion6").val("");
		$('#multiple').prop('checked', false);
    $('#radio1').prop('checked', false);
    $('#radio2').prop('checked', false);
    $('#radio3').prop('checked', false);
    $('#radio4').prop('checked', false);
    $('#radio5').prop('checked', false);
    $('#radio6').prop('checked', false);
		$('#opcionesmultiples').hide();
	}

	function traerDatos(id){
		$("#idpregunta").val(id);
		$.ajax(
        {
            type: "GET",
            url: "pregunta_datos.php",
            data: {id:id},
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
				$("#pregunta").val(data['pregunta']);
				$("#link").val(data['link']);
				if(data['multiple'] == 1){
					$('#multiple').prop('checked', true);
					$('#opcionesmultiples').show();
					$("#opcion1").val(data['opcion1']);
					$("#opcion2").val(data['opcion2']);
					$("#opcion3").val(data['opcion3']);
					$("#opcion4").val(data['opcion4']);
					$("#opcion5").val(data['opcion5']);
					$("#opcion6").val(data['opcion6']);
          $('#radio'+data['incc']).prop('checked', true);
				}
            },
			error: function (xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");
				alert(err.Message);
			}
        });
	};
	
	function eliminarPregunta(elemento,id) {
		var tr = elemento.parents('tr');

		BootstrapDialog.show({
			title: 'Eliminar Pregunta',
			message: '¿Esta seguro que desea eliminar esta pregunta?',
			buttons: [{
				label: ' Aceptar',
				cssClass: 'btn-primary',
				action: function(dialogItself){
					dialogItself.close();
					$.ajax({
						type: "POST",
						url: "eliminaciones.php",
						data: {delpregunta : id}
					}).done(function( msg ) {
						BootstrapDialog.show({
							message: msg,
							buttons: [{
								label: 'Aceptar',
								action: function(dialogItself) {
									remover(tr,1);
									dialogItself.close();
								}
							}]
						});
					});
				}
			}, {
				label: ' Cancelar',
				cssClass: 'btn-default',
				action: function(dialogItself){
					dialogItself.close();
				}
			}]
		});
	};
	
	function eliminarArchivo(elemento,id) {
		var tr = elemento.parents('tr');

		BootstrapDialog.show({
			title: 'Eliminar Archivo',
			message: '¿Esta seguro que desea eliminar este archivo?',
			buttons: [{
				label: ' Aceptar',
				cssClass: 'btn-primary',
				action: function(dialogItself){
					dialogItself.close();
					$.ajax({
						type: "POST",
						url: "eliminaciones.php",
						data: {delarch : id}
					}).done(function( msg ) {
						BootstrapDialog.show({
							message: msg,
							buttons: [{
								label: 'Aceptar',
								action: function(dialogItself) {
									remover(tr,2);
									dialogItself.close();
								}
							}]
						});
					});
				}
			}, {
				label: ' Cancelar',
				cssClass: 'btn-default',
				action: function(dialogItself){
					dialogItself.close();
				}
			}]
		});
	};

$(function(){
	$('#subir').click(SubirArchivos);
});

function SubirArchivos(){
		var archivos = document.getElementById("archivos");//Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
		var archivo = archivos.files; //Obtenemos los archivos seleccionados en el imput
		//Creamos una instancia del Objeto FormDara.
		var archivos = new FormData();
		/* Como son multiples archivos creamos un ciclo for que recorra la el arreglo de los archivos seleccionados en el input
		Este y añadimos cada elemento al formulario FormData en forma de arreglo, utilizando la variable i (autoincremental) como
		indice para cada archivo, si no hacemos esto, los valores del arreglo se sobre escriben*/
		for(i=0; i<archivo.length; i++){
		archivos.append('archivo'+i,archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
		}

		/*Ejecutamos la función ajax de jQuery*/
		$.ajax({
			url:'subir_archivos.php', //Url a donde la enviaremos
			type:'POST', //Metodo que usaremos
			contentType:false, //Debe estar en false para que pase el objeto sin procesar
			data:archivos, //Le pasamos el objeto que creamos con los archivos
			processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
			cache:false, //Para que el formulario no guarde cache
			beforeSend: function(){
				$("#subir").addClass("m-progress");
				$("#subir").prop('disabled', true);
			},
			success: function(msg) {
				$("#subir").removeClass("m-progress");
				$("#subir").prop('disabled', false);
				MensajeFinal(msg)
			}
		});
	}

function MensajeFinal(msg){
	$('.mensage').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('.mensage').show('slow');//Mostramos el div.
}

	$(document).ready(function() {
		tabla_preguntas = $('#preguntas').DataTable({
			"paging":   false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Presione "Nueva Pregunta" para comenzar a cargar preguntas al planteo.',
				"sInfo":           "",
				"sInfoEmpty":      "",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"bFilter": false,
			"bSort" : false
		});

		tabla_archivos = $('#tarchivos').DataTable({
			"paging":   false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Este planteo no posee archivos.',
				"sInfo":           "",
				"sInfoEmpty":      "",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"bFilter": false,
			"bSort" : false
		});

	$("#fechalimite").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});

	$("#cargarpregunta").on('submit',function(event){
		event.preventDefault();
		var formData = new FormData($(this)[0]);
    if($('#multiple').is(":checked")){
      var radio = $('input[name="radio"]:checked').val();
      if(!radio){
        alert("Debe seleccionar una opcion correcta");
        return false;
      }
    }
		var subiendo = new BootstrapDialog({
			message: function(dialogRef){
				var $message = $('<div><span class="glyphicon glyphicon-refresh glyphicon-spin"></span>&nbsp; Cargando Pregunta...</div>');
				return $message;
			},
			closable: true
		});
		$.ajax({
			type: "POST",
			url: "planteos_cargarpregunta.php",
			data: formData,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function(){
				$('#modalpreguntas').modal('toggle');
				subiendo.realize();
				subiendo.getModalHeader().hide();
				subiendo.getModalFooter().hide();
				subiendo.getModalBody().css('background-color', '#00BFFF');
				subiendo.getModalBody().css('color', '#fff');
				subiendo.open();
			},
			success: function(msg){
				BootstrapDialog.show({
					title: 'Pregunta Cargada',
					message: msg,
					type: BootstrapDialog.TYPE_SUCCESS,
					buttons: [{
						label: 'Aceptar',
						action: function(dialogItself){
							location.reload();
						}
					}]
				});
			},
			error: function (xhr, status, error) {
				var err = eval("(" + xhr.responseText + ")");
				alert(err.Message);
			}
		});
	});

	$("#planteoform").on('submit',function(event){
		event.preventDefault();
		data = $(this).serialize();
		if ($('#limite').is(":checked")) {
			  var limite = 1;
			} else {
			  var limite = 0;
			}
		$.ajax({
			type: "POST",
			url: "planteos_modificar.php",
			data: data + "&limite=" + limite
		}).done(function( msg ) {
			BootstrapDialog.show({
				message: msg,
				buttons: [{
					label: 'Aceptar',
					action: function(dialogItself){
						window.location.replace("planteos_docentes.php");
					}
				}]
			});
		});
	});

	<?php if($blimite){ ?>
		$('#limite').prop('checked', true);
		$('#displayfecha').show();
	<?php } ?>

	<?php echo "$( '#semaforo' ).val( '$semaforo' );"; ?>

	});
</script>
  <!-- ============================================================= -->
</body>
</html>
