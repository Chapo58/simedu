<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Codigos por Empleado</title>
    <?php require_once('head.php'); ?>
    <?php
      if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
        mensaje("Debe seleccionar una empresa.");
        ir_a("empresas.php");
      }
    ?>
	<link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
</head>

<body class="no-skin">
    <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Liquidación de Sueldos</a>
            </li>
            <li class="active">Codigos por Empleado</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
    <!-- Page Content -->
    <h1 class="page-header">Codigos por Empleado</h1>
    <input type="button" id="btnNueva" onclick="$('#Codigos').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Agregar codigo empleado">
	<div class="bs-callout bs-callout-info">
		<form>
			<div class="row">
				<div class="col-md-4">
					<input type="text" class="form-control" name="buscardescripcion" placeholder="Descripción" id="buscardescripcion" />
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" id="LoadRecordsButton"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Buscar</button>
			</div>
		</form>
	</div>
        <div class="row">
            <div class="col-xs-12">
				<div id="Codigos" style="width: 100%;"></div>
            </div>
        </div>

    <!-- Final Page Content -->

    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

    <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>
<script type="text/javascript">
	var multiplica = [];
	var divide = [];
	var cantidad = [];
	<?php
		$con_sum=consulta("SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa ORDER BY numero ASC");
		while ($sum = mysqli_fetch_array($con_sum, MYSQLI_ASSOC)) {
			$mul = addslashes($sum['multiplica']);
			$div = addslashes($sum['divide']);
			$can = addslashes($sum['cantidad']);
			echo "multiplica.push(\"$mul\");";
			echo "divide.push(\"$div\");";
			echo "cantidad.push(\"$can\");";
		};
	?>
	$(document).ready(function () {

			$('#Codigos').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Codigos',
				paging: true,
				sorting: true,
				defaultSorting: 'idempleado ASC',
				actions: {
					listAction: 'CodigosEmpleadoAct.php?action=list',
					createAction: 'CodigosEmpleadoAct.php?action=create',
					updateAction: 'CodigosEmpleadoAct.php?action=update',
					deleteAction: 'CodigosEmpleadoAct.php?action=delete'
				},
				fields: {
					idcodemp: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					idempleado: {
						title: 'Empleado',
						width: '20%',
						options: { 
							<?php
							$con_emp=consulta("SELECT * FROM sueldos_empleados WHERE idempresa = $idempresa ORDER BY nombre ASC");
							while ($emp = mysqli_fetch_array($con_emp, MYSQLI_ASSOC)) {
						?>
							"<?php echo $emp['idempleado'] ?>": "<?php echo $emp['nombre'] ?>",
						<?php } ?>
						}
					},
					idcodigo: {
						title: 'Codigo',
						width: '20%',
						options: { 
							<?php
							$con_cod=consulta("SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa ORDER BY numero ASC");
							while ($cod = mysqli_fetch_array($con_cod, MYSQLI_ASSOC)) {
						?>
							"<?php echo $cod['idcodigo'] ?>": "<?php echo $cod['numero'].' - '.$cod['descripcion'] ?>",
						<?php } ?>
						},
						input: function (data) {
							if (data.record) {
								return '<select onchange="prueba()" class="" id="idcodigo" name="idcodigo">'+
						<?php
							$con_cod=consulta("SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa ORDER BY numero ASC");
							while ($cod = mysqli_fetch_array($con_cod, MYSQLI_ASSOC)) {
						?>
								'<option value="<?php echo $cod['idcodigo'] ?>"><?php echo $cod['numero'].' - '.$cod['descripcion'] ?></option>'+
						<?php } ?>
								 '</select>';
							} else {
								 return '<select onchange="prueba()" class="" id="idcodigo" name="idcodigo">'+
						<?php
							$con_cod=consulta("SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa ORDER BY numero ASC");
							while ($cod = mysqli_fetch_array($con_cod, MYSQLI_ASSOC)) {
						?>
								'<option value="<?php echo $cod['idcodigo'] ?>"><?php echo $cod['numero'].' - '.$cod['descripcion'] ?></option>'+
						<?php } ?>
								 '</select>';
							}
						}
					},
					multiplicador: {
						title: 'Multiplicador',
						width: '20%'
					},
					divisor: {
						title: 'Divisor',
						width: '20%',
					},
					cantidad: {
						title: 'Cantidad',
						width: '20%',
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay registros cargados!',
					addNewRecord: 'Agregar Codigo',
					editRecord: 'Editar Codigo',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El registro será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});

			 $('#Codigos').jtable('load');

			$('#LoadRecordsButton').click(function (e) {
				e.preventDefault();
				$('#Codigos').jtable('load', {
					buscardescripcion: $('#buscardescripcion').val()
				});
			});
	});

		function prueba(){
			alert("asd");
		}
</script>
</body>
</html>
