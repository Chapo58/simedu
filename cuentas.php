<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Plan de cuentas</title>
    <?php require_once('head.php'); ?>
    <?php
      if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
        mensaje("Debe seleccionar una empresa.");
        ir_a("empresas.php");
      }
    ?>
	<link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
</head>

<body class="no-skin">
    <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Simulador Contable</a>
            </li>
            <li class="active">Plan de Cuentas</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
    <!-- Page Content -->
    <h1 class="page-header">Plan de Cuentas</h1>
    <input type="button" id="btnNueva" onclick="$('#Cuentas').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Agregar cuenta">
	<a href="PDFs/plan_de_cuentas.php" target="_blank" class="btn btn-lg btn-info" style="float:right;"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir Plan de Cuentas</a>
	<div class="bs-callout bs-callout-info">
		<form>
			<div class="row">
				<div class="col-md-4">
					<input type="text" class="form-control" name="buscardenominacion" placeholder="Denominacion" id="buscardenominacion" />
				</div>
				<div class="col-md-2">
					<select class="form-control" name="buscarimputable" id="buscarimputable">
						<option value="" selected>Imputable</option>
						<option value="1">Si</option>
						<option value="2">No</option>
					</select>
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" id="LoadRecordsButton"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Buscar</button>
			</div>
		</form>
	</div>
        <div class="row">
            <div class="col-xs-12">
				<div id="Cuentas" style="width: 100%;"></div>
            </div>
        </div>

    <!-- Final Page Content -->

    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

    <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>
<script type="text/javascript">
	var denominacion = [];
	var codigo = [];
	var codigovalidar = [];
	var valorprevio = 0;
	<?php
		$con_sum=consulta("SELECT * FROM cuentas WHERE imputable = 2 AND idempresa = $idempresa ORDER BY codigo ASC");
		while ($sum = mysqli_fetch_array($con_sum, MYSQLI_ASSOC)) {
			$den = addslashes($sum['denominacion']);
			$cod = addslashes($sum['codigo']);
			echo "denominacion.push(\"$den\");";
			echo "codigo.push(\"$cod\");";
		};
		$con_sum=consulta("SELECT * FROM cuentas WHERE idempresa = $idempresa");
		while ($sum = mysqli_fetch_array($con_sum, MYSQLI_ASSOC)) {
			$cod = $sum['codigo'];
			echo "codigovalidar.push(\"$cod\");";
		};
	?>
	$(document).ready(function () {

			$('#Cuentas').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Plan de Cuentas',
				paging: true,
				sorting: true,
				defaultSorting: 'codigo ASC',
				actions: {
					listAction: 'CuentasAct.php?action=list',
					createAction: 'CuentasAct.php?action=create',
					updateAction: 'CuentasAct.php?action=update',
					deleteAction: 'CuentasAct.php?action=delete'
				},
				fields: {
					idcuenta: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					codigo: {
						title: 'Codigo',
						width: '10%',
						visibility: 'fixed',
						input: function (data) {
							if (data.record) {
								valorprevio = parseInt(data.record.codigo);
								return '<input type="number" name="codigo" id="codigo" onblur="validarcodigo($(this));" OnKeyUp="sumar(this.value);" value="' + data.record.codigo + '" />';
							} else {
								return '<input type="number" name="codigo" onblur="validarcodigo($(this));" OnKeyUp="sumar(this.value);" id="codigo" />';
							}
						}
					},
					denominacion: {
						title: 'Denominacion',
						width: '40%'
					},
					rubro: {
						title: 'Rubro',
						width: '20%',
						options: { '1': 'Activo', '2': 'Pasivo', '3': 'Patrimonio Neto', '4': 'Ingresos y Ganancias', '5': 'Gastos y Perdidas', '6': 'Costo' }
					},
					imputable: {
						title: 'Imputable',
						width: '10%',
						options: { '1': 'Si', '2': 'No' }
					},
					sumariza: {
						title: 'Sumariza',
						width: '20%',
						input: function (data) {
							if (data.record) {
								return '<select id="sumariza" name="sumariza"></select>';
							//	document.getElementById("sumariza").value = data.record.codigo;
							} else {
								return '<select id="sumariza" name="sumariza"></select>';
							}
						}
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay registros cargados!',
					addNewRecord: 'Agregar Cuenta',
					editRecord: 'Editar Cuenta',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El registro será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				},
				formCreated(event, data) {
					for (var x = 0; x < codigo.length; x++) {
						if (data.record.codigo > parseInt(codigo[x])) {
							$('#sumariza').append($('<option>', {
								value: codigo[x],
								text: denominacion[x]
							}));
						};
					};
					$('#sumariza').val(data.record.sumariza);
				},
				recordAdded(event, data){
					valorprevio = 0;
				}
			});

			 $('#Cuentas').jtable('load');

			$('#LoadRecordsButton').click(function (e) {
				e.preventDefault();
				$('#Cuentas').jtable('load', {
					buscardenominacion: $('#buscardenominacion').val(),
					buscarimputable: $('#buscarimputable').val()
				});
			});
	});
	function sumar(valor) {
		var val = parseInt(valor);
		$('#sumariza').empty();
    $('#sumariza').append($('<option>', {
      value: 0,
      text: 'Ninguno'
    }));
		for (var x = 0; x < codigo.length; x++) {
			if (val > parseInt(codigo[x])) {
				$('#sumariza').append($('<option>', {
					value: codigo[x],
					text: denominacion[x]
				}));
			};
		};
	};
	function validarcodigo(inp) {
		var val = parseInt(inp.val());
		for (var x = 0; x < codigovalidar.length; x++) {
			if(valorprevio != parseInt(codigovalidar[x])){
				if (val == parseInt(codigovalidar[x])) {
					var dialog = new BootstrapDialog({
						message: function(){
									var $message = $('<div>Ya existe una cuenta con este codigo, por favor ingrese uno diferente.</div>');
									return $message;
								}
							});
					dialog.realize();
					dialog.getModalHeader().hide();
					dialog.getModalFooter().hide();
					dialog.getModalBody().css('background-color', '#FF0000');
					dialog.getModalBody().css('color', '#fff');
					dialog.open();
					inp.val('');
				};
			};
		};
	};
</script>
</body>
</html>
