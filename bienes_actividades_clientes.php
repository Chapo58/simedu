<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Actividades Clientes</title>
    <?php require_once('head.php'); ?>
	  <link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	  <link href="jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-inbox home-icon"></i>
              <a href="#">Bienes de Uso</a>
            </li>
            <li class="active">Actividades Clientes</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

    <!-- Page Content -->
    <div>
		 <input type="button" id="btnNueva" onclick="$('#Tabla').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Agregar Actividad">

        <div class="space20"></div>

        <div class="row">
            <div class="col-xs-12">
				<div id="Tabla" style="width: 100%;"></div>
            </div>
        </div>
    </div>

    <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

    <?php require_once('footer.php'); ?>
    <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>

    <script type="text/javascript">
	$(document).ready(function () {

		    //Prepare jTable
			$('#Tabla').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Clientes',
				paging: true,
				sorting: true,
				defaultSorting: 'idcliente ASC',
				actions: {
					listAction: 'BienesActividadesClientesAct.php?action=list',
					createAction: 'BienesActividadesClientesAct.php?action=create',
					updateAction: 'BienesActividadesClientesAct.php?action=update',
					deleteAction: 'BienesActividadesClientesAct.php?action=delete'
				},
				fields: {
					idactividad: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					idcliente: {
						title: 'Cliente',
						width: '20%',
						options: { 
						<?php
							$con_cli=consulta("SELECT * FROM bienes_clientes WHERE idempresa = $idempresa ORDER BY denominacion ASC");
							while ($cli = mysqli_fetch_array($con_cli, MYSQLI_ASSOC)) {
						?>
							"<?php echo $cli['idcliente'] ?>": "<?php echo $cli['numero'].' - '.$cli['denominacion'] ?>",
						<?php } ?>
						}
					},
					orden: {
						title: 'Orden',
						width: '10%',
						options: { '1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7', '8': '8'}
					},
					codigo: {
						title: 'Codigo Actividad',
						width: '20%'
					},
					descripcion: {
						title: 'Descripcion',
						width: '30%'
					},
					formulario: {
						title: 'Formulario',
						width: '10%',
						options: { '1': '1', '2': '2', '3': '3', '4': '4', '5': 'Bienes Personales'}
					},
					agropecuario: {
						title: 'Es Agropecuario',
						width: '10%',
						options: { '1': 'Si', '2': 'No' }
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay listas cargadas!',
					addNewRecord: 'Agregar Actividad',
					editRecord: 'Editar Actividad',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'La actividad será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});

			 $('#Tabla').jtable('load');

	});
    </script>
</body>
</html>
