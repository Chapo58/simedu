<?php
session_start();
include "funciones.php";
try
{
	$idempresa = $_SESSION['idempresa'];
/*	$con_idpcuentas=consulta("SELECT idpcuentas FROM empresas WHERE idempresa='$idempresa'");
	$pc=mysqli_fetch_array($con_idpcuentas);
	$idpcuentas = $pc['idpcuentas'];*/
	
	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM cuentas WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscardenominacion']) && !empty($_POST['buscardenominacion'])) {
			$buscardenominacion = $_POST['buscardenominacion'];
		} else {
			$buscardenominacion = "";
		}
		if (isset($_POST['buscarimputable']) && !empty($_POST['buscarimputable'])) {
			$buscarimputable = $_POST['buscarimputable'];
		} else {
			$buscarimputable = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM cuentas WHERE idempresa = $idempresa AND denominacion LIKE '%".$buscardenominacion."%' AND imputable LIKE '%".$buscarimputable."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$codigo = $_POST["codigo"];
		$denominacion = $_POST["denominacion"];
		$rubro = $_POST["rubro"];
		$imputable = $_POST["imputable"];
		if(isset($_POST['sumariza']) && !empty($_POST['sumariza'])) { $sumariza = $_POST["sumariza"]; } else { $sumariza = 0;}
		
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO cuentas(idempresa, codigo, denominacion, rubro, imputable, sumariza) VALUES('$idempresa','$codigo','$denominacion','$rubro','$imputable','$sumariza');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM cuentas WHERE idcuenta = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se modifico el plan de cuentas");
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idcuenta"];
		$codigo = $_POST["codigo"];
		$denominacion = $_POST["denominacion"];
		$rubro = $_POST["rubro"];
		$imputable = $_POST["imputable"];
		if(isset($_POST['sumariza']) && !empty($_POST['sumariza'])) { $sumariza = $_POST["sumariza"]; } else { $sumariza = 0;}
		//Update record in database
		$result = mysqli_query($con,"UPDATE cuentas SET codigo = '$codigo', denominacion = '$denominacion', rubro = '$rubro', imputable = '$imputable', sumariza = '$sumariza' WHERE idcuenta = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se modifico el plan de cuentas");
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idcuenta"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM cuentas WHERE idcuenta = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se modifico el plan de cuentas");
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>