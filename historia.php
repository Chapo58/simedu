<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Historia</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="inicio.php">Inicio</a>
            </li>
            <li class="active">Historia</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Historia del Simulador</h1>

<p>El <strong>Estudio Pastor</strong> desarrolla sistemas desde el año 1992, atendiendo principalmente servicios a Estudios Contables, Empresas y Organizaciones.</p>
<p>Los sistemas desarrollados siempre tuvieron una cuota de simplicidad para el operador acompañados de un profundo profesionalismo a la hora de cubrir las necesidades de los usuarios.</p>
<p>Entre algunos podemos nombrar Sistemas de Comprobantes de Compras y Ventas, Sistemas de Impuestos Nacionales y Bienes Personales, Sistema Contables,  Liquidación de Sueldos entre otros, que al dia de hoy siguen funcionando.</p>
<p>El <strong>Simulador Educativo</strong> nace como una necesidad de llevar a las Escuelas  y también a  Institutos de formación profesional en Administración para alumnos secundarios y terciaros una herramienta didáctica para que los alumnos y profesores integren un ambiente de trabajo único de formación donde se puede definir diferentes elementos para administrar una ó varias empresas por usuario registrado.</p>
<p>El sistema se encuentra inscripto en registro de propiedad intelectual y fue desarrollado por un equipo interdisciplinario formado por profesionales en diferentes áreas a saber contadores público, Administradores de Empresas, Organizaciones sin fines de Lucro, Analistas de Sistemas y un programador.</p>
<p>Desde el año 2006 comenzamos con una idea que fue mejorando hasta que en el año 2011  se presento esta idea al Ministerio de Ciencia y tecnología a través de Uvitec  y comenzamos a desarrollar un conjunto de herramientas que sirvan como base para aprender a administrar una empresa de una forma didáctica.</p>
<p>Se incorporó la idea que pueda ser utilizado por teléfonos y Tablet como medio para operar el mismo.</p>
<p>Pensamos incorporar la factura electrónica como modelo practico.Su habilitación será solo a empresas pero los alumnos tendrán la técnica practica de cómo se hace.</p>
<p>Hasta el momento se definió la creación de un periodo contable, mis empresas, mi perfil  de usuario y el simulador contable como base de todo el sistema administrativo.</p>
<p>Falta integrarlo con los movimientos de stock, circuito de compras y ventas que será el próximo modulo a desarrollar.</p>
<p>Agregaremos también un módulo básico de Sueldos para generar los recibos de Sueldos de mis empleados.</p>
<p>Además contará con un shopping virtual que los alumnos y docentes podrán definir sus productos y servicios ofrecidos.</p>
<p>Todo esto bajo un ambiente de docentes y alumnos para que el docente a cargo pueda ver , corregir y orientar al alumno.</p>
<p>El sistema cuenta con un modulo de administración de usuarios y empresas que son vistas desde el menú principal que aun no esta desarrollado.</p>
<p>De este modo completaremos una idea y un producto que funcione en internet los 365 días del año pudiendo los alumnos y docentes trabajar desde sus casas sin ningún tipo límites más que una conexión a internet.</p>
<p>Pensados que para finales del 2015 estará desarrollado en su totalidad.</p>
<strong><u>Los módulos de la solución que desarrollamos se detallan a continuación</u>:</strong>
<br /><br />
<p>•	Modulo de generación de la empresa virtual: Se realiza una definición conceptual y a nivel informativo (razón social, nombre de fantasía, cuit, dirección, etc) de cada empresa, pudiendo definirse fichas conceptuales informativas de hasta 5 como máximo.</p>
<p>•	Modulo de generación de vista operativa de la empresa virtual: Permite completar la información de la empresa, definir misión, visión, mercados y valores de la misma.</p>
<p>•	Módulo de Contabilidad General: Permite administrar un plan de cuentas, periodos fiscales, asientos contables y emitir el Libro Diario, Mayor, Balance General, Balance de Sumas y Saldos y Flujo de Fondos.</p>
<p>•	Módulo de generación de puntos de ventas: Permite capacitar y definir información como puntos de ventas, tipos de comprobantes que se van a usar para registrar las compras y ventas, modos de cargas y configuración técnica de la actividad de la empresa virtual.</p>
<p>•	Módulo de gestión de nóminas de clientes y proveedores: Permite definir y categorizar empresas según su fin, definiendo los proveedores necesarios para la generación de los productos y servicios y posibles clientes finales para la comercialización.</p>
<p>•	Módulos de stock, artículos lista de precios y un shopping virtual por cada empresa virtual.</p>
<p>•	Módulo de simulación circuito de compras: Permite recorrer el circuito de compras y manejo de ingresos de materiales e insumos, conceptos como facturas de compras, remitos de ingresos de mercadería, orden de compra y presupuestos.</p>
<p>•	Módulo de simulación circuito de ventas: Permite recorrer los circuitos de ventas, manejo de remitos de egresos (entrega) conceptos como factura de ventas, créditos, débitos presupuestos y notas de ventas.</p>
<p>•	Módulo de simulación de circuitos administrativos: Permite capacitar en circuitos de cobranzas (uso de recibos) pagos a proveedores (conceptos como orden de pago, imputación administrativa, cuenta corrientes, etc.</p>
<p>•	Módulo de simulación de circuitos financieros: Permite recorrer y aprender conceptos como manejo de cajas, etc todo centralizado bajo una contabilidad general.</p>
<p>•	Módulos de análisis de indicadores y reportes de gestión: Permite a los alumnos emitir reportes de gestión y poder analizarlos con los profesores. A partir de los resultados plantear nuevos ejercicios y estrategias. Permite capacitar en conceptos como Libro de IVA Balance General, Sumas y saldos, flujo de Fondos, Etc.</p>


<p>A continuación nombraremos algunos integrantes de este equipo interdisciplinario independiente y primeros usuarios que nos acompañaron en este camino. </p>
<ul>
<li>Iglesia Catedral, usuario administración contable.</li>
<li>Fundación Trinitarios, usuario Administración Contable.</li>
<li>Estudio Contable Testa,  usuario Liquidación de Sueldos.</li>
<li>Estudio Bario, usuario Sistema Contable.</li>
<li>Nestor Pastor , Usuario y coperador de ideas, Administrador de Empresas.</li>
<li>Sergio Pastor, Analista de Sistema.</li>
<li>Victor Vera, relevamiento integral del sistema, programador 2011 – 2014.</li>
<li>Luciano Ciattaglia, seguridad informática y programador 2015.</li>
<li>Nery brugnoni, diseñador 2015.</li>
<li>Sergio Mino, Ventas y Comercializacion.</li>
</ul>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
  <!-- ============================================================= -->
</body>
</html>
