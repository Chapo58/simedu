<?php
	include "funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['numero']) && !empty($_POST['numero']) &&
		isset($_POST['detalle']) && !empty($_POST['detalle']) &&
		isset($_POST['edbien']) && !empty($_POST['edbien'])) {
		
		// Quito espacios en blanco
		$id = trim($_POST['edbien']);
		$numero = trim($_POST['numero']);
		$detalle = trim($_POST['detalle']);
	@	$idcliente = $_POST['idcliente'];
	@	$idactividad = $_POST['idactividad'];
	@	$vida_util = trim($_POST['vida_util']);
	@	$fecha_compra = $_POST['fecha_compra'].'-01';
	@	$coeficiente = $_POST['coeficiente'];
	@	$costo = $_POST['costo'];
	@	$comentario = $_POST['comentario'];
	@	$ubicacion = $_POST['ubicacion'];
	@	$tipo_bien = $_POST['tipo_bien'];
	@	$mueble = $_POST['mueble'];
	@	$inmueble = $_POST['inmueble'];
	@	$inmueble_valor = $_POST['inmueble_valor'];
	@	$cuenta_inmobiliaria = $_POST['cuenta_inmobiliaria'];
	@	$bien_de_reemplazo = $_POST['bien_de_reemplazo'];
	@	$utilidad = $_POST['utilidad'];
	@	$valor_cnas = $_POST['valor_cnas'];
	@	$idgrupo = $_POST['idgrupo'];
	@	$idrubro = $_POST['idrubro'];
		
		// Paso a mayusculas
		$detalle = ucfirst($detalle);
		
		$editar = "UPDATE bienes_de_uso
									SET
									numero = '$numero',
									detalle = '$detalle',
									idcliente = '$idcliente',
									idactividad = '$idactividad',
									vida_util = '$vida_util',
									fecha_compra = '$fecha_compra',
									coeficiente = '$coeficiente',
									costo = '$costo',
									comentario = '$comentario',
									ubicacion = '$ubicacion',
									tipo_bien = '$tipo_bien',
									mueble = '$mueble',
									inmueble = '$inmueble',
									inmueble_valor = '$inmueble_valor',
									cuenta_inmobiliaria = '$cuenta_inmobiliaria',
									bien_de_reemplazo = '$bien_de_reemplazo' ,
									utilidad = '$utilidad',
									valor_cnas = '$valor_cnas',
									idgrupo = '$idgrupo',
									idrubro = '$idrubro'
									WHERE idbien = '$id'";
									
		$cargar = consulta($editar);
				
		if(!$cargar){
				echo "Mensaje: ".mysqli_error();
				mensaje("Error");
		}
		mensaje($fecha_compra);
		acthistempresa($rsocial, "Se modifico un bien de uso");
		ir_a("bienes_de_uso.php");
		
	} else {
		mensaje("No se cargaron todos los datos");
		ir_a("bienes_de_uso.php");
	}
?>
