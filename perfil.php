<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Perfil</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="inicio.php">Inicio</a>
            </li>
            <li class="active">Perfil</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Perfil</h1>
  <a style="float:right;" href="perfil_usuarios.php"><span class="glyphicon glyphicon-eye-open"></span> Ver Perfil</a>
  <ul class="nav nav-pills" role="tablist">
    <li class="active"><a href="#datos-personales" role="tab" data-toggle="tab">Datos personales</a></li>
    <li><a href="#cambiar-password" role="tab" data-toggle="tab">Cambiar contraseña</a></li>
  </ul>
  <div class="tab-content">
    <div id="datos-personales" class="tab-pane fade in active">
        <div class="row">
            <div class="col-md-4 col-md-offset-4" style="padding-top: 50px">
              <form id="perfilform" class="form-horizontal" role="form" enctype="multipart/form-data">

              <div class="form-group text-center">
                  <img src=<?php echo $imagenperfil; ?>  id="imagenmuestra" class="img-circle img-user tooltipster" title="Imagen de perfil" alt="Imagen de Perfil">
                  <div class="fileUpload btn btn-default">
        			<span>Seleccionar imagen</span>
        			<input type="file" name="imagen" id="imgInp" class="upload" />
				  </div>
              </div>
			</div>
			<div class="col-md-8 col-md-offset-2">
                <div class="form-group">
                    <label class="sr-only" for="txtEmail">Email</label>
                    <div class="input-group">
                      <div class="input-group-addon">@</div>
                      <input type="text" class="form-control" name="email" id="email" placeholder="Email" maxlength="30" value="<?php echo $email; ?>" required><div id="validar"></div>
                    </div>
                </div>
			</div>
			<div class="col-md-4 col-md-offset-2">
                  <div class="form-group">
                    <label class="sr-only" for="txtNombre">Nombre</label>
                      <input type="text" id="txtNombre" name="nombre" placeholder="Nombre" class="form-control" maxlength="30" value="<?php echo $u['nombre']; ?>" onkeypress="return permite(event, 'car')">
                  </div>
			</div>
			<div class="col-md-4">
                  <div class="form-group">
                    <label class="sr-only" for="txtApellido">Apellido</label>
                      <input type="text" id="txtApellido" name="apellido" placeholder="Apellido" maxlength="30" value="<?php echo $u['apellido']; ?>" class="form-control" onkeypress="return permite(event, 'car')">
                  </div>
			</div>
			<div class="col-md-6 col-md-offset-3">
                 <div class="form-group">
                 <label class="sr-only" for="txtFechaNacimiento">Fecha de nacimiento</label>
				 <div class="input-group">
					<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                    <input type="text" id="txtFechaNacimiento" data-mask="99/99/9999" name="fechanac" class="form-control" value="<?php if(isset($u['fechanacimiento']) && !empty($u['fechanacimiento']))echo date("d/m/Y",strtotime($u['fechanacimiento'])); ?>" placeholder="Fecha de nacimiento" maxlength="10">
                 </div>
				 </div>
			</div>
			<div class="col-md-6 col-md-offset-3">
                  <div class="form-group">
                    <label class="sr-only" for="cboSexo">Sexo</label>
					<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-transgender"></i></div>
                    <select class="form-control" name="sexo" id="cbpSexo">
						<option value="0">Sexo</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select>
					</div>
                  </div>
			</div>
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group">
					<label class="sr-only" for="txtTelefono">Telefono</label>
					<div class="input-group">
					<div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
					<input type="text" name="telefono" maxlength="30" id="txtTelefono" placeholder="Telefono" value="<?php echo $u['telefono']; ?>" class="form-control">
					</div>
				</div>
			</div>
			<div class="col-md-6 col-md-offset-3">
				<div class="form-group">
					<label class="sr-only" for="txtTelefono">Facebook</label>
					<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-facebook"></i></div>
					<input type="text" name="facebook" id="txtFacebook" value="<?php echo $u['facebook']; ?>" placeholder="Facebook" class="form-control">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
                  <div class="form-group">
                    <label class="sr-only" for="txtDireccion">Dirección</label>
                      <input type="text" name="direccion" id="txtDireccion" maxlength="30" placeholder="Dirección" value="<?php echo $u['direccion']; ?>" class="form-control" onkeypress="return permite(event, 'num_car')">
                  </div>
			</div>
			<div class="col-md-3">
                  <div class="form-group">
                    <label class="sr-only" for="txtLocalidad">Localidad</label>
                      <input type="text" name="localidad" id="txtLocalidad" placeholder="Localidad" maxlength="30" value="<?php echo $u['ciudad']; ?>" class="form-control" onkeypress="return permite(event, 'num_car')">
                  </div>
			</div>
			<div class="col-md-3">
                  <div class="form-group">
                    <label class="sr-only" for="txtProvincia">Provincia/Estado</label>
                      <input type="text" name="provincia" id="txtProvincia" placeholder="Provincia/Estado" maxlength="30" value="<?php echo $u['provincia']; ?>" class="form-control" onkeypress="return permite(event, 'num_car')">
                  </div>
			</div>
			<div class="col-md-3">
                   <div class="form-group">
                    <label class="sr-only" for="cboPais">País</label>
                    <select class="form-control" name="pais" id="cboPais">
                        <option value="Argentina">Argentina</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Brasil">Brasil</option>
                        <option value="Traidores">Chile</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Perú</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Venezuela">Venezuela</option>
                      </select>
                  </div>
			</div>
			<div class="col-md-12">
                  <div class="form-group">
                      <div class="pull-right">
                        <button type="submit" id="btnGuardarDatosPersonales" class="btn btn-primary btn-lg">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Guardar cambios
						</button>
                      </div>
                  </div>
			</div>
              </form>
        </div>
    </div>

    <div id="cambiar-password" class="tab-pane fade">
      <div class="row">
          <div class="col-md-4 col-md-offset-4" style="padding-top: 50px">
              <form class="form-horizontal" id="cambiarpass" role="form" data-toggle="validator">
                <div class="form-group">
                  <label class="sr-only" for="txtPasswordActual">Contraseña actual</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                    <input type="password" class="form-control" name="passactual" id="txtPasswordActual" placeholder="Contraseña actual" required>
                  </div>
				  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label class="sr-only" for="txtPasswordNueva">Contraseña nueva</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                    <input type="password" class="form-control" name="passnuevo" id="txtPasswordNueva" placeholder="Contraseña nueva" data-minlength="6" data-error="6 caracteres como mínimo" required>
                  </div>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <label class="sr-only" for="txtPasswordConfirmar">Repite la contraseña</label>
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                    <input type="password" class="form-control" name="passnuevo2" id="txtPasswordConfirmar" placeholder="Repite la contraseña" data-match="#txtPasswordNueva" data-error="Las contraseñas no coinciden" required>
                  </div>
                  <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                  <div class="pull-right">
                    <button type="submit" id="btnGuardarPassword" class="btn btn-primary">
					<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Guardar cambios
					</button>
                  </div>
                </div>
              </form>

          </div>
       </div>
     </div>
  </div>
  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

  <script type="text/javascript">
		$(document).ready(function() {

			// Validar Email
			$('#email').blur(function(){

				var email = $(this).val();
				var dataString = 'email='+email;
				if(email != "<?php echo $email; ?>"){
					$.ajax({
						type: "POST",
						url: "validaciones.php",
						data: dataString,
						}).done(function( msg ) {
							document.getElementById('validar').innerHTML = msg;
						});
				} else {
					document.getElementById('validar').innerHTML = '';
				};
			});
			//Cambiar imagen de perfil
				function readURL(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();

						reader.onload = function (e) {
							$('#imagenmuestra').attr('src', e.target.result);
						}

						reader.readAsDataURL(input.files[0]);
					}
				}
				$("#imgInp").change(function(){
					readURL(this);
				});

				//Calendario
				$("#txtFechaNacimiento").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y',maxDate:'0'});

				// Cambiar pass
				$("#cambiarpass").on('submit',function(event){
					event.preventDefault();
					data = $(this).serialize();

					$.ajax({
					type: "POST",
					url: "usuario_cambiarpass.php",
					data: data
					}).done(function( msg ) {
						BootstrapDialog.show({
							message: msg,
							buttons: [{
								label: 'Aceptar',
								action: function(dialogItself){
									$('#txtPasswordActual').val('');
									$('#txtPasswordNueva').val('');
									$('#txtPasswordConfirmar').val('');
									dialogItself.close();
								}
							}]
						});
					});
				});

				// Formulario Perfil
				$("#perfilform").on('submit',function(event){
					event.preventDefault();
					var formData = new FormData($(this)[0]);

					$.ajax({
						type: "POST",
						url: "usuario_actualizar.php",
						data: formData,
						contentType: false,
						cache: false,
						processData: false
					}).done(function( msg ) {
						BootstrapDialog.show({
							message: msg,
							buttons: [{
								label: 'Aceptar',
								action: function(dialogItself){
									dialogItself.close();
								}
							}]
						});
					});
				});

		});
    $('#cbpSexo').val( '<?php if(!empty($u["sexo"])) echo $u["sexo"]; else echo "0"; ?>' );
		$('#cboPais').val( '<?php if(!empty($u["pais"])) echo $u["pais"]; else echo "Argentina"; ?>' );
		$('.tooltipster').tooltipster({ position: 'top' });
	</script>
  <!-- ============================================================= -->
</body>
</html>
