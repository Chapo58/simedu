<?php
session_start();
include "funciones.php";
try
{
	$idempresa = $_SESSION['idempresa'];
/*	$con_idpcuentas=consulta("SELECT idpcuentas FROM empresas WHERE idempresa='$idempresa'");
	$pc=mysqli_fetch_array($con_idpcuentas);
	$idpcuentas = $pc['idpcuentas'];*/
	
	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM sueldos_codigos WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscardescripcion']) && !empty($_POST['buscardescripcion'])) {
			$buscardescripcion = $_POST['buscardescripcion'];
		} else {
			$buscardescripcion = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa AND descripcion LIKE '%".$buscardescripcion."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$numero = $_POST["numero"];
		$descripcion = $_POST["descripcion"];
		$cantidad = $_POST["cantidad"];
		$haberes = $_POST["tipo_haberes"];
		$obtencion = $_POST["obtencion"];
		$multiplicar = $_POST["multiplica"];
		$dividir = $_POST["divide"];
		$aguinaldo = $_POST["aguinaldo"];
		$sueldo = $_POST["sueldo"];
		
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO sueldos_codigos(idempresa, numero, descripcion, obtencion, multiplica, divide, cantidad, aguinaldo, tipo_haberes, sueldo) VALUES('$idempresa','$numero','$descripcion','$obtencion','$multiplicar','$dividir','$cantidad','$aguinaldo','$haberes','$sueldo');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM sueldos_codigos WHERE idcodigo = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se creo un nuevo codigo");
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idcodigo"];
		$numero = $_POST["numero"];
		$descripcion = $_POST["descripcion"];
		$cantidad = $_POST["cantidad"];
		$haberes = $_POST["tipo_haberes"];
		$obtencion = $_POST["obtencion"];
		$multiplicar = $_POST["multiplica"];
		$dividir = $_POST["divide"];
		$aguinaldo = $_POST["aguinaldo"];
		$sueldo = $_POST["sueldo"];

		//Update record in database
		$result = mysqli_query($con,"UPDATE sueldos_codigos SET numero = '$numero', descripcion = '$descripcion', obtencion = '$obtencion', multiplica = '$multiplicar', divide = '$dividir', cantidad = '$cantidad', aguinaldo = '$aguinaldo', tipo_haberes = '$haberes', sueldo =  '$sueldo' WHERE idcodigo = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se modifico un codigo");
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idcodigo"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM sueldos_codigos WHERE idcodigo = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		acthistempresa($_SESSION['rsocial'], "Se elimino un codigo");
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>