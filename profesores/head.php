<?php
	session_start();
	require_once('../funciones.php');
		if(isset($_SESSION['email']) && isset($_SESSION['idusuario'])){
			$email = $_SESSION['email'];
			$idusuario = $_SESSION['idusuario'];
			$con_usr=consulta("SELECT * FROM usuario WHERE idusuario='$idusuario'");
			$u=mysqli_fetch_array($con_usr);
			if($u['tipousuario'] != 2){
				ir_a('../index.php');
			}
		} else {
			ir_a('../index.php');
		};
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Simedu Simulador Empresarial Contable Educativo">
<meta name="author" content="Luciano Ciattaglia">

<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />

<!-- jQuery -->
<script src="../js/jquery-2.1.4.min.js"></script>

<!-- CSS -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../fonts/font-awesome.min.css" rel="stylesheet">

<!-- Javascript -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/utilidades.js"></script>
<script src="../js/jasny-bootstrap.min.js"></script>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="../css/datatable.css">

<!-- Jasny -->
<link rel="stylesheet" type="text/css" href="../css/jasny-bootstrap.min.css">
