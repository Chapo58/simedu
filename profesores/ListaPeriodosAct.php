<?php
session_start();
$idusuario = $_SESSION['idusuario'];
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM periodos LEFT JOIN usuario ON periodos.idusuario = usuario.idusuario WHERE usuario.idprofesor = '$idusuario';");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT periodos.* FROM periodos LEFT JOIN usuario ON periodos.idusuario = usuario.idusuario WHERE usuario.idprofesor = '$idusuario' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$idusuario = $_POST["idusuario"];
		$periodo = $_POST["periodo"];
		$descripcion = $_POST["descripcion"];
		$desde = date("Y-m-d", strtotime($_POST["desde"]));
		$hasta = date("Y-m-d", strtotime($_POST["hasta"]));
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO periodos(idusuario, periodo, descripcion, desde, hasta) VALUES('$idusuario','$periodo','$descripcion','$desde','$hasta');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM periodos WHERE idperiodo = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idperiodo"];
		$idusuario = $_POST["idusuario"];
		$periodo = $_POST["periodo"];
		$descripcion = $_POST["descripcion"];
		$desde = date("Y-m-d", strtotime($_POST["desde"]));
		$hasta = date("Y-m-d", strtotime($_POST["hasta"]));
		//Update record in database
		$result = mysqli_query($con,"UPDATE periodos SET periodo = '$periodo', idusuario = '$idusuario', descripcion = '$descripcion', desde = '$desde', hasta = '$hasta' WHERE idperiodo = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idperiodo"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM periodos WHERE idperiodo = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>