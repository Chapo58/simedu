<?php
session_start();
$idusuario = $_SESSION['idusuario'];
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM productos LEFT JOIN empresas ON productos.idempresa = empresas.idempresa  LEFT JOIN usuario ON empresas.idusuario = usuario.idusuario WHERE usuario.idprofesor = '$idusuario';");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT productos.* FROM productos LEFT JOIN empresas ON productos.idempresa = empresas.idempresa  LEFT JOIN usuario ON empresas.idusuario = usuario.idusuario WHERE usuario.idprofesor = '$idusuario' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$a1 = $_POST["idempresa"];
		$a2 = $_POST["nombre"];
		$a3 = $_POST["descripcion"];
		$a4 = $_POST["preciocosto"];
		$a5 = $_POST["preciovta"];
		$a6 = $_POST["iva"];
		$a7 = $_POST["stock"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO productos(idempresa, nombre, descripcion, preciocosto, preciovta, iva, stock) VALUES('$a1','$a2','$a3','$a4','$a5','$a6','$a7');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM productos WHERE idproducto = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idproducto"];
		$a1 = $_POST["idempresa"];
		$a2 = $_POST["nombre"];
		$a3 = $_POST["descripcion"];
		$a4 = $_POST["preciocosto"];
		$a5 = $_POST["preciovta"];
		$a6 = $_POST["iva"];
		$a7 = $_POST["stock"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE productos SET idempresa = '$a1', nombre = '$a2', descripcion = '$a3', preciocosto = '$a4', preciovta = '$a5', iva = '$a6', stock = '$a7' WHERE idproducto = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idproducto"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM productos WHERE idproducto = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>