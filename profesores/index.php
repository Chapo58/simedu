<!DOCTYPE html>
<html lang="en">
<head>
    <title>Simedu | Panel Docente</title>
	<?php include "head.php"; ?>
</head>

<body>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row text-center">
                <h1 class="page-header">Panel de Administración Docente</h1>
                <p>Bienvenido a su panel de administración de Simedu. Debajo se listan las diferentes categorías disponibles.</p>
            </div>

            <div class="row text-center">
				<div class="col-md-6 col-xs-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-users fa-fw"></i>&nbsp;&nbsp;Configuracion Alumnos</h3>
						</div>
						<ul class="list-group">
							<a href="modelo_cuentas.php" class="list-group-item">Modelo Plan de Cuentas</a>
							<a href="lista_alumnos.php" class="list-group-item">Listado de Alumnos</a>
							<a href="historial.php" class="list-group-item">Historial de Alumnos</a>
							<a href="lista_periodos.php" class="list-group-item">Periodos de Alumnos</a>
						</ul>
					</div>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-industry"></i>&nbsp;&nbsp;Configuracion de Empresas de Alumnos</h3>
						</div>
						<ul class="list-group">
							<a href="lista_empresas.php" class="list-group-item">Empresas de Alumnos</a>
							<a href="lista_fichas.php" class="list-group-item">Fichas de Empresas</a>
							<a href="lista_productos.php" class="list-group-item">Productos Cargados</a>
						</ul>
					</div>
				</div>
            </div>


        </div>
    </div>
	<div class="navbar navbar-default navbar-fixed-bottom">
	  <div class="container">
		<span class="navbar-text">
			<a href="../inicio.php" class="btn btn-lg btn-info">
			<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver al simulador
			</a>
		</span>
	  </div>
	</div>
    <!-- Final Page Content -->

    <!-- Scripts al final del DOM para que la pagina cargue mas rapido -->
    <!-- ============================================================= -->
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- ============================================================= -->
</body>
</html>
