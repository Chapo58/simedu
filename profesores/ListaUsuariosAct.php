<?php
session_start();
$idusuario = $_SESSION['idusuario'];
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM usuario WHERE idprofesor = '$idusuario';");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscar']) && !empty($_POST['buscar'])) {
			$buscar = $_POST['buscar'];
		} else {
			$buscar = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM usuario WHERE idprofesor = '$idusuario' AND email LIKE '%".$buscar."%' OR idprofesor = '$idusuario' AND nombre LIKE '%".$buscar."%' OR idprofesor = '$idusuario' AND apellido LIKE '%".$buscar."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$a2 = $_POST["email"];
		$a3 = $_POST["nombre"];
		$a4 = $_POST["apellido"];
		
		$con_u=consulta("SELECT email FROM usuario WHERE email='$a2'");
		$u=mysqli_fetch_array($con_u);
		if(mysqli_num_rows($con_u)==0){
			
			//Insert record into database
			$result = mysqli_query($con,"INSERT INTO usuario(idprofesor, email, nombre, apellido, pass, habilitado, tipousuario) VALUES('$idusuario','$a2','$a3','$a4','simedu',1,1);");
			
			//Get last inserted record (to return to jTable)
			$result = mysqli_query($con,"SELECT * FROM usuario WHERE idusuario = LAST_INSERT_ID();");
			$row = mysqli_fetch_array($result);
			
			// INSERTAR PERIODO
			$idcreada = $row['idusuario'];
			$año =  date("Y"); 
			$periodo = "Periodo " . $año;
			$desde = "$año-01-01";
			$hasta = "$año-12-31";
			
			mysqli_query($link,"INSERT INTO periodos (idusuario,periodo,descripcion,desde,hasta)
					VALUES ('$idcreada','$año','$periodo','$desde','$hasta')");
			$idperiodo = mysqli_insert_id($link);
			consulta("UPDATE usuario SET idperiodo = '$idperiodo' WHERE idusuario = '$idcreada'");

			//Return result to jTable
			$jTableResult = array();
			$jTableResult['Result'] = "OK";
			$jTableResult['Record'] = $row;
			print json_encode($jTableResult);
		} else {
			//Return result to jTable
			$jTableResult = array();
			$jTableResult['Result'] = "ERROR";
			$jTableResult['Message'] = "La direccion de email ya existe, por favor ingrese una direccion diferente.";
			print json_encode($jTableResult);
		}
		
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idusuario"];
		$a2 = $_POST["email"];
		$a3 = $_POST["nombre"];
		$a4 = $_POST["apellido"];
		$a5 = $_POST["sexo"];
		$a6 = date("Y-m-d", strtotime($_POST["fechanacimiento"]));
		$a7 = $_POST["pass"];
		$a8 = $_POST["direccion"];
		$a9 = $_POST["ciudad"];
		$a10 = $_POST["provincia"];
		$a11 = $_POST["pais"];
		$a12 = $_POST["telefono"];
		$a13 = $_POST["habilitado"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE usuario SET email = '$a2', nombre = '$a3', apellido = '$a4', sexo = '$a5'
			, fechanacimiento = '$a6', pass = '$a7', direccion = '$a8', ciudad = '$a9', provincia = '$a10', pais = '$a11', telefono = '$a12', habilitado = '$a13' WHERE idusuario = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idusuario"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM usuario WHERE idusuario = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>