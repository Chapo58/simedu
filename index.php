<?php
include "funciones.php";

	//Tiene una cookie?
	if (isset($_COOKIE["id_usuario_dw"]) && isset($_COOKIE["marca_aleatoria_usuario_dw"])){
	   //Tengo cookies memorizadas
	   //Estan vacias?
	   if ($_COOKIE["id_usuario_dw"]!="" && $_COOKIE["marca_aleatoria_usuario_dw"]!=""){
		  //Voy a ver si corresponden con algún usuario
		  $con_cookie=consulta("SELECT * FROM usuario where idusuario=" . $_COOKIE["id_usuario_dw"] . " and cookie='" . $_COOKIE["marca_aleatoria_usuario_dw"] . "' and cookie<>''");
		  if (mysqli_num_rows($con_cookie)==1){
			$u=mysqli_fetch_array($con_cookie);
			session_start();
			$_SESSION['email'] = $u['email'];
			$_SESSION['idusuario'] = $u['idusuario'];
			ir_a("inicio.php");
		  }
	   }
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Software educativo de simulación de gestión multiempresa que permite a los estudiantes un primer contacto con el fascinante mundo de los negocios y del empresario proveyendo al sector un instrumento de aprendizaje vivencial.">
	<meta name="author" content="Luciano Ciattaglia">

	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <title>Simedu</title>

	<!-- CSS -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="fonts/font-awesome.min.css" rel="stylesheet">
	<link href="css/login-style.css" rel="stylesheet" media="screen">

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53444193-3"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-53444193-3');
	</script>

</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>

    <div id="signup">
		<span id="text">Aún no tienes una cuenta? </span><a href="signup.php">Regístrate!</a>
    </div>


	<!-- Home -->
	<section id="home" class="pfblock-image screen-height" style="margin-top: -50px;">
        <div class="home-overlay"></div>

		<div class="intro">
			<div class="start">Simulador Educativo</div>
			<h1>Simedu</h1>
			<div class="start">Iniciar sesión</div>

			<div style="width: 220px; margin: auto;">
				<form accept-charset="UTF-8" role="form" style="font-weight: bold" method="POST" action="usuario_verificar.php">
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="Email" name="email" type="email" <?php if(isset($_GET['email'])){ echo 'value='.$_GET['email']; }?> required>
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Contraseña" name="pass" type="password" value="" required>
						</div>
						<div class="checkbox">
							<label style="font-weight: bold">
								<input name="recordar" type="checkbox" value="Recordar"> No cerrar sesión
							</label>
						</div>
						<input class="btn btn-primary btn-block" style="font-weight: bold" type="submit" value="Ingresar" >
						<br />
						<a href="#" data-toggle="modal" data-target="#modalpass" style="color: #fff; font-weight: bold">¿Has olvidado tu contraseña?</a>
					</fieldset>
				</form>
			</div>
		</div>

        <a href="#services">
		<div class="scroll-down">
            <span>
                <i class="fa fa-angle-double-down fa-2x"></i>
            </span>
		</div>
        </a>
	</section>

	<!-- Navbar -->
	<header class="header">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#home">Simedu</a>
				</div>
				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#home">Iniciar sesión</a></li>
						<li><a href="#services">Herramientas</a></li>
						<li><a href="#skills">Características</a></li>
                        <li><a href="#portfolio">Empresas</a></li>
                        <li><a href="#info">Informacion</a></li>
						<li><a href="#contact">Contacto</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

    <!-- Herramientas -->
	<section id="services" class="pfblock pfblock-gray">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pfblock-header wow fadeInUp">
						<h2 class="pfblock-title">Acerca del Simulador Educativo</h2>
						<div class="pfblock-line"></div>
<div align="justify">
<p>Lo fundamental del proyecto es que desarrollamos un <strong>Recurso Educativo</strong> didáctico pedagógico para las carreras con orientación en <strong>Economía y Administración</strong> a través de un software web de simulación de gestión que permite a los estudiantes un primer contacto con el fascinante mundo de los negocios y del empresario proveyendo al sector un instrumento de aprendizaje vivencial. </p>
<p>El fin último es que los alumnos aprendan divirtiéndose y viviendo una experiencia a pesar de que todo tenga como base una simulación. Guiados por ejercicios planteados por los profesores, los estudiantes podrán formar sus propias empresas virtuales y tener a su cargo sus decisiones.</p>
<p>Generamos una interfaz simple de uso, muy intuitiva, que permita a cualquier alumnos o profesor operarla sin inconvenientes a nivel de navegación, concentrándose en el fin del programa y no en su operación .
Generamos una plataforma que pueda ser aplicada fácilmente a distintos entornos de negocios, agro, industrias, comerciales, de servicio, etc.
Generamos un producto multiusuario que permita gestionar fácilmente los accesos a grupos de alumnos.</p>
<p>El resultado tecnológico esperado del proyecto es desarrollar una solución web que permite complementar los procesos educativos teóricos sobre una aplicación práctica (simulada) de la gestión y administración y saber de sus ventajas.
Se desarrolló una solución en un entorno web que permite que sea accesible desde cualquier PC con conexión a internet (utilizando un navegador).
El desarrollo está basado en MySQL como motor de base de datos y diversos lenguajes como PHP, Javascript, Ajax y CSS para dejar como resultado un entorno intuitivo y visualmente agradable y sencillo.</p>
<p><strong><u>Los módulos de la solución que desarrollamos se detallan a continuación</u>:</strong></p>
<ul>
<li><strong>Modulo de Generación de la Empresa Virtual:</strong>
Se realiza una definición conceptual y a nivel informativo (razón social, nombre de fantasía, cuit, dirección, etc) de cada empresa, pudiendo definirse fichas conceptuales informativas de hasta 5 como máximo.</li>
<li><strong>Modulo de Generación de Vista Operativa de la Empresa Virtual:</strong>
Permite completar la información de la empresa, definir misión, visión, mercados y valores de la misma.</li>
<li><strong>Módulo de Contabilidad General:</strong>
Permite administrar un plan de cuentas, periodos fiscales, asientos contables y emitir el libro diario, mayor, balance general, balance de sumas y saldos y flujo de fondos.</li>
<li><strong>Módulo Ludico:</strong>
Permite definir planteos por docente, los cuales los alumnos a través de un juego van resolviendo. Los docentes pueden subir sus archivos (fotos, planillas, documentos, links, etc.). Los alumnos responden a las preguntas contenidas por los planteos y el docente evalúa los resultados de cada alumno. Preparado para todas las asignaturas, interactuando con herramientas como pueden ser un reloj, dados (multiopciones), un semáforo.
Teoría + Practica + Juego + Administración son los módulos educativos que permite a docentes y alumnos interactuar en forma dinámica con sus ideas, proyectos, pruebas, presentaciones, además de los módulos administrativos. Preparado para dispositivos móviles, tablets, y PCs.
</li>
<li><strong>Módulo de Gestión de nóminas de Clientes y Proveedores:</strong>
Permite definir y categorizar empresas según su fin, definiendo los proveedores necesarios para la generación de los productos y servicios y posibles clientes finales para la comercialización.</li>
<!--<li><strong>Módulo de generación de puntos de ventas:</strong> Permite capacitar y definir información como puntos de ventas, tipos de comprobantes que se van a usar para registrar las compras y ventas, modos de cargas y configuración técnica de la actividad de la empresa virtual.</li>-->
<li><strong>Módulos de Stock:</strong>
artículos lista de precios y un shopping virtual por cada empresa virtual.</li>
<li><strong>Módulo de Simulación Circuito de Compras:</strong>
Permite recorrer el circuito de compras y manejo de ingresos de materiales e insumos, conceptos como facturas de compras, remitos de ingresos de mercadería y presupuestos.</li>
<li><strong>Módulo de Simulación Circuito de Ventas:</strong>
Permite recorrer los circuitos de ventas,conceptos como factura de ventas, créditos, débitos presupuestos y notas de ventas.</li>
<!--<li><strong>Módulo de simulación de circuitos administrativos:</strong> Permite capacitar en circuitos de cobranzas (uso de recibos) pagos a proveedores (conceptos como orden de pago, imputación administrativa, cuenta corrientes, etc.</li>-->
<!--<li><strong>Módulo de simulación de circuitos financieros:</strong> Permite recorrer y aprender conceptos como manejo de cajas, etc todo centralizado bajo una contabilidad general.</li>-->
<li><strong>Módulos de Análisis de Indicadores y Reportes de Gestión:</strong>
Permite a los alumnos emitir reportes de gestión y poder analizarlos con los profesores. A partir de los resultados plantear nuevos ejercicios y estrategias. Permite capacitar en conceptos como Balance General, Sumas y saldos, flujo de Fondos, Etc.</li>
</ul>
</div>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<div class="iconbox wow slideInLeft">
						<div class="iconbox-text">
						<i class="fa fa-flag fa-4x"></i>
							<h3 class="iconbox-title">Institucional</h3>
							<div class="iconbox-desc">
								Puedes crear un sitio web para tu empresa o una tienda virtual y un enlace al mismo
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="iconbox wow slideInLeft">
						<div class="iconbox-text">
						<i class="fa fa-usd fa-4x"></i>
							<h3 class="iconbox-title">Contabilidad</h3>
							<div class="iconbox-desc">
								Utiliza el simulador de contabilidad, empresarial o trabaja con artículos.
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="iconbox wow slideInRight">
						<div class="iconbox-text">
						<i class="fa fa-book fa-4x"></i>
							<h3 class="iconbox-title">Empresarial</h3>
							<div class="iconbox-desc">
								Crea y moldea tus propias empresas sin limite con Simedu.
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="iconbox wow slideInRight">
						<div class="iconbox-text">
						<i class="fa fa-plus fa-4x"></i>
							<h3 class="iconbox-title">Y más</h3>
							<div class="iconbox-desc">
								Crea una cuenta y conoce todas las herramientas que Simedu te brinda!
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <!-- Caracteristicas -->
    <section class="pfblock pfblock-gray" id="skills">
			<div class="container">
				<div class="row skills">
					<div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="pfblock-header wow fadeInUp">
                                <h2 class="pfblock-title">Características</h2>
                                <div class="pfblock-line"></div>
                                <div class="pfblock-subtitle">
                                    En Simedu creemos que siempre se puede mejorar y que nunca se alcaza la perfección.
                                </div>
                            </div>
                        </div>
                     </div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="99" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="percent">99</span>
                        </span>
						<h3 class="text-center">Didáctica</h3>
					</div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="99" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="percent">99</span>
                        </span>
						<h3 class="text-center">Precisa</h3>
					</div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="99" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="percent">99</span>
                        </span>
						<h3 class="text-center">Sencilla</h3>
					</div>
					<div class="col-sm-6 col-md-3 text-center">
						<span data-percent="99" class="chart easyPieChart" style="width: 140px; height: 140px; line-height: 140px;">
                            <span class="percent">99</span>
                        </span>
						<h3 class="text-center">Amigable</h3>
					</div>
				</div>

			</div>
    </section>


	<!-- Empresas -->
	<section id="portfolio" class="pfblock">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="pfblock-header wow fadeInUp">
						<h2 class="pfblock-title">¿Quieres ver a Simedu en acción?</h2>
						<div class="pfblock-line"></div>
						<div class="pfblock-subtitle">
							Mira algunas de las empresas creadas actualmente!
						</div>
					</div>
				</div>
			</div>

            <div class="row">
			<?php
				$con_empresa=consulta("SELECT empresas.*, usuario.idusuario
				FROM usuario INNER JOIN empresas ON empresas.idusuario = usuario.idusuario
				WHERE usuario.tipousuario=85");
				while ($em = mysqli_fetch_array($con_empresa, MYSQLI_ASSOC)) {
					if ($em['imagenempresa'] == "") {
						$imagen = "images/empresa-default.png";
					} else {
						$imagen = $em['imagenempresa'];
					}
				$rsoc = str_replace(' ', '', $em['rsocial']);
			?>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <div class="grid wow zoomIn">
                    	<a href="#" data-toggle="modal" data-target="<?php echo "#" . $rsoc ?>">
	                        <figure class="effect-bubba">
	                            <img src="<?php echo $imagen; ?>" alt="img"/>
	                            <figcaption>
	                                <h2><?php echo $em['rsocial']; ?></h2>
	                                <!--<p>Slogan</p>-->
	                            </figcaption>
	                        </figure>
                        </a>
                    </div>
                </div>
			<?php } ?>
            </div>
		</div>
	</section>

	<!-- Informacion -->
	<section id="info" class="pfblock">
	<center><a href="images/Proyecto.pdf" target="_blank" class="btn btn-primary btn-lg" style="width:90%;">
		<span class="glyphicon glyphicon-save-file"></span>&nbsp;&nbsp;Descargar Informacion sobre el proyecto
	</a></center>
	</section>
	<!-- Contacto -->
	<section id="contact" class="pfblock">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="pfblock-header">
						<h2 class="pfblock-title">Contáctanos</h2>
						<div class="pfblock-line"></div>
						<div class="pfblock-subtitle">
							¿Tienes alguna duda? Qué esperas, haznos saber que opinas. Tu consulta es bienvenida!
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<form id="contact-form" role="form">
						<div class="ajax-hidden">
							<div class="form-group wow fadeInUp">
								<label class="sr-only" for="c_name">Nombre</label>
								<input type="text" id="c_name" class="form-control" name="c_name" placeholder="Nombre" required>
							</div>
							<div class="form-group wow fadeInUp" data-wow-delay=".1s">
								<label class="sr-only" for="c_email">Email</label>
								<input type="email" id="c_email" class="form-control" name="c_email" placeholder="Email" required>
							</div>
							<div class="form-group wow fadeInUp" data-wow-delay=".2s">
								<textarea class="form-control" id="c_message" name="c_message" rows="7" placeholder="Mensaje" required></textarea>
							</div>
							<button type="submit" class="btn btn-primary btn-block wow fadeInUp" data-wow-delay=".3s">Enviar</button>
						</div>
						<div class="ajax-response"></div>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php
$con_pie=consulta("SELECT * FROM piepagina");
$pie=mysqli_fetch_array($con_pie);
?>
	<!-- Footer -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<ul class="social-links">
						<li><a href="#" class="wow fadeInUp"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" class="wow fadeInUp" data-wow-delay=".5s"><i class="fa fa-envelope"></i></a></li>
					</ul>
					<p>
						<div><i><?php echo $pie['linea1']; ?></i></div>
						<div><?php echo $pie['linea2']; ?></div>
						<div><?php echo $pie['linea3']; ?></div>
						<div><?php echo $pie['linea4']; ?></div>
						<div><u>Soporte técnico</u>: <?php echo $pie['stecnico']; ?></div>
					</p>
					<img src="images/ministerio.jpg" width="250" />
					<div style="color:#222222">Diseño: Luciano Ciattaglia</div>
				</div>
			</div>
		</div>
	</footer>

	<!-- Scroll Top -->
	<div class="scroll-up">
		<a href="#home"><i class="fa fa-angle-double-up"></i></a>
	</div>


	<!-- Modal de las empresas -->
	  <?php include "modal-index.php" ?>

<div id="modalpass" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Recuperar contraseña</h4>
      </div>
      <div class="modal-body">
	        <form id="recuperarpass" class="form-horizontal" role="form">
			<div class="row">
	            <div class="col-md-12 col-xs-12">
					<div class="input-group">
					  <div class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></div>
					  <input type="text" class="form-control" name="email" id="emailrecuperarpass" placeholder="Direccion de Correo Electronico">
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		<button type="submit" class="btn btn-primary">Enviar</button>
			</form>
      </div>
    </div>

  </div>
</div>

<div id="modalenviado" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Recuperar contraseña</h4>
      </div>
      <div class="modal-body">
			<div class="row">
	            <div class="col-md-12 col-xs-12">
					<div class="input-group" id="divpass"></div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>


	<!-- Scripts al final del DOM para que la pagina cargue mas rapido -->
	<!-- ============================================================= -->
	<script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/login-custom.js"></script>
	<!-- ============================================================= -->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59975a751b1bed47ceb0568f/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
<script>
$("#recuperarpass").on('submit',function(event){
		event.preventDefault();
		data = $(this).serialize();
		$.ajax({
		type: "POST",
		url: "usuario_validarmail.php",
		data: data
		}).done(function( msg ) {
			$('#modalpass').modal('toggle');
			$('#modalenviado').modal('toggle');
			$("#divpass").html(msg);
		});
});
</script>
</html>
