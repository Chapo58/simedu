<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Empleados</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-inbox home-icon"></i>
              <a href="sueldos_empleados.php">Liquidación de Sueldos</a>
            </li>
            <li class="active">Empleados</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		}
        if(isset($_POST['idempleado']) && !empty($_POST['idempleado'])){
            $ideditar = $_POST['idempleado'];
            $con_editar=consulta("SELECT * FROM sueldos_empleados WHERE idempleado='$ideditar'");
            $editar=mysqli_fetch_array($con_editar);
			$titulo = "Modificar Empleado " . $editar['nombre'];
			$accion = "sueldos_empleados_modificar.php";
			$txtboton = "Guardar Cambios";

			$idempleado = $editar['idempleado'];
			$modnombre = $editar['nombre'];
			$modsexo = $editar['sexo'];
			$modfechaingreso = $editar['fecha_ingreso'];
			$modestadocivil = $editar['estado_civil'];
			$modcuit = $editar['cuit'];
			$modtipodocumento = $editar['tipo_documento'];
			$modfechanacimiento = $editar['fecha_nacimiento'];
			$modpais = $editar['pais'];
			$modprovincia = $editar['provincia'];
			$modciudad = $editar['ciudad'];
			$moddireccion = $editar['direccion'];
			$modcodigopostal = $editar['codigo_postal'];
			$modcategoria = $editar['idcategoria'];
			$modobrasocial = $editar['idobrasocial'];
			$modtelefono = $editar['telefono'];
			$modemail = $editar['email'];
			$modlegajo = $editar['legajo'];
			$modactivo = $editar['activo'];
			$modobservaciones = $editar['observaciones'];

		} else {
			$idempleado = 0;
			$titulo = "Agregar Empleado";
			$accion = "sueldos_empleados_cargar.php";
			$txtboton = "Aceptar";
		}
	?>


    <!-- Page Content -->
    <div id="divEmpleados">
        <h1 class="page-header">Empleados</h1>
		<input type="button" id="btnNuevo" onclick="switchDivs();" class="btn btn-lg btn-primary" value="Agregar Empleado">
		<div class="space20"></div><br /><br />
        <div class="row">
            <div class="col-xs-12">
                <table id="tabla" class="table table-striped table-bordered" cellspacing="0" style="width:80%;margin:auto;">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Cuit</th>
                            <th>Direccion</th>
                            <th>Antiguedad</th>
                            <th>Categoria</th>
                            <th>Obra Social</th>
                            <th>Observaciones</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
							$con_emp=consulta("SELECT sueldos_empleados.*, sueldos_categorias.descripcion as categoria, sueldos_obras_sociales.descripcion as obra_social
							FROM sueldos_empleados 
							LEFT JOIN sueldos_categorias ON sueldos_empleados.idcategoria = sueldos_categorias.idcategoria
							LEFT JOIN sueldos_obras_sociales ON sueldos_empleados.idobrasocial = sueldos_obras_sociales.idobrasocial
							WHERE sueldos_empleados.idempresa='$idempresa'");
							while ($emp = mysqli_fetch_array($con_emp, MYSQLI_ASSOC)) {
								$idempleado2 = $emp['idempleado'];
								$nombre = $emp['nombre'];
								$cuit = $emp['cuit'];
								$direccion = $emp['direccion'];

								$hoy = date("Y");
								$fecha_ingreso = date('Y',strtotime($emp['fecha_ingreso']));
								$antiguedad = $hoy - $fecha_ingreso;

								$categoria = $emp['categoria'];
								$obrasocial = $emp['obra_social'];
								$observaciones = $emp['observaciones'];
						?>
                        <tr>
                            <td><?php echo $nombre; ?></td>
							<td><?php echo $cuit; ?></td>
							<td><?php echo $direccion; ?></td>
							<td><?php echo $antiguedad; ?> Años</td>
							<td><?php echo $categoria; ?></td>
							<td><?php echo $obrasocial; ?></td>
							<td><?php echo substr($observaciones, 0, 40)."..."; ?></td>
							<td class="text-center">
                                <form action="sueldos_empleados.php" method="POST">
                                    <button type="submit" class="btn btn-primary" title="Editar">
										<span class="fa fa-pencil" aria-hidden="true"></span>
									</button>
                                <input type="hidden" name="idempleado" id="idempleado" value="<?php echo $idempleado2; ?>" >
                                </form>
							</td>
							<td class="text-center">
                                <form class="delform">
                                    <button type="submit" class="btn btn-danger" title="Eliminar">
										<span class="fa fa-trash-o" aria-hidden="true"></span>
									</button>
									<input type="hidden" id="delemp" name="delemp" value="<?php echo $idempleado2; ?>" >
                                </form>
                            </td>
                        </tr>
						<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="space50"></div>

    </div>
    <div id="divNuevaEditar" class="sr-only">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
        <form role="form" data-toggle="validator" action="<?php echo $accion; ?>" method="POST" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-6">
		        <div class="form-group">
					<label for="nombre">Nombre y Apellido</label>
		            <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre y Apellido" required>
		            <div class="help-block with-errors"></div>
		        </div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="sexo">Sexo</label>
					<select class="form-control" name="sexo" id="sexo">
						<option value="0">Sexo</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select>
				</div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="fecha_ingreso">Fecha Ingreso</label>
		            <input type="date" id="fecha_ingreso" name="fecha_ingreso" class="form-control" required>
		        </div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="estado_civil">Estado Civil</label>
					<select class="form-control" name="estado_civil" id="estado_civil">
						<option value="0">Estado Civil</option>
                        <option value="Soltero">Soltero</option>
                        <option value="Casado">Casado</option>
                        <option value="Divorsiado">Divorsiado</option>
                        <option value="Viudo">Viudo</option>
                    </select>
				</div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="cuit">Cuit</label>
		            <input type="text" id="cuit" name="cuit" class="form-control" required>
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="fecha_nacimiento">Fecha Nacimiento</label>
		            <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" class="form-control">
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="direccion">Dirección</label>
		            <input type="text" id="direccion" name="direccion" class="form-control">
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="codigo_postal">Codigo Postal</label>
		            <input type="text" id="codigo_postal" name="codigo_postal" class="form-control">
		        </div>
			</div>
			<div class="col-md-4">
		        <div class="form-group">
					<label for="ciudad">Ciudad</label>
		            <input type="text" id="ciudad" name="ciudad" class="form-control">
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="provincia">Provincia</label>
		            <input type="text" id="provincia" name="provincia" class="form-control">
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="pais">Pais</label>
		            <input type="text" id="pais" name="pais" class="form-control">
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="categoria">Categoria</label>
		            <select class="form-control" name="categoria" id="categoria">
						<option selected disabled>Seleccionar Categoria</option>
		                <?php
						$con_per=consulta("select * from sueldos_categorias where idempresa='$idempresa'");
						while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
							$idcategoria = $p['idcategoria'];
							$descr = $p['descripcion'];
						?>
						<option value = '<?php echo $idcategoria;?>'><?php echo $descr; ?></option>
						<?php } ?>
		            </select>
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="obra_social">Obra Social</label>
		            <select class="form-control" name="obra_social" id="obra_social">
						<option selected disabled>Seleccionar Obra Social</option>
		                <?php
						$con_per=consulta("select * from sueldos_obras_sociales");
						while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
							$idobrasocial = $p['idobrasocial'];
							$descr = $p['codigo'].' - '.$p['descripcion'];
						?>
						<option value = '<?php echo $idobrasocial;?>'><?php echo $descr; ?></option>
						<?php } ?>
		            </select>
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="telefono">Telefono</label>
		            <input type="text" id="telefono" name="telefono" class="form-control">
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="email">Email</label>
		            <input type="email" id="email" name="email" class="form-control">
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group">
					<label for="legajo">Legajo</label>
		            <input type="text" id="legajo" name="legajo" class="form-control">
		        </div>
			</div>
			<div class="col-md-6">
		        <div class="form-group" align="center">
			<div class="input-group">
		        <span class="button-checkbox">
	    			<button type="button" class="btn btn-info active" data-color="activo"><strong>Activo</strong></button>
	    			<input type="checkbox" id="activo" name="activo" class="hidden" checked />
	    		</span>
			</div>
		</div>
			</div>
			<div class="col-md-12">
		        <div class="form-group">
					<label for="observaciones">Observaciones</label>
		            <textarea class="form-control" rows="3" id="observaciones" name="observaciones" placeholder="Observaciones...">
						<?php 
							if(isset($_POST['idempleado']) && !empty($_POST['idempleado'])){
								echo $modobservaciones;
							}
						?>
					</textarea>
		        </div>
			</div>

			<input type="hidden" name="edempleado" id="edempleado" value="<?php echo $idempleado; ?>" >
			<div class="col-md-12">
		        <div class="form-group">
		            <div class="pull-right">
		                <button type="button" id="btnAtras" class="btn btn-default btn-lg" onclick="window.location.href='sueldos_empleados.php';">Atrás</button>
						<button type="submit" id="btnAceptar" class="btn btn-primary btn-lg"><?php echo $txtboton; ?></button>
		            </div>
		        </div>
			</div>
		</div>
        </form>
    </div>
    <!-- Final Page Content -->
  </div>
 </div>
</div><!-- /.main-content -->
<br><br><br><br><br>
<?php require_once('footer.php'); ?>

    <script type="text/javascript">

    $(document).ready(function() {

        var table = $('#tabla').dataTable({
			"order": [[ 1, "asc" ]],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ empleados",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Seleccione "Agregar Empleado" para cargar un nuevo empleado.',
				"sInfo":           "Mostrando empleados del _START_ al _END_ de un total de _TOTAL_ empleados",
				"sInfoEmpty":      "Mostrando empleados del 0 al 0 de un total de 0 empleados",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ empleados)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
        });

		$('#tabla').on('click', '.delform', function () {
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Empleado',
				message: '¿Esta seguro que desea eliminar este empleado?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

		<?php
			if(isset($_POST['idempleado']) && !empty($_POST['idempleado'])){
				echo "switchDivs();";
				echo "$( '#nombre' ).val( '$modnombre' );";
				echo "$( '#sexo' ).val( '$modsexo' );";
				echo "$( '#fecha_ingreso' ).val( '$modfechaingreso' );";
				echo "$( '#estado_civil' ).val( '$modestadocivil' );";
				echo "$( '#cuit' ).val( '$modcuit' );";
				echo "$( '#fecha_nacimiento' ).val( '$modfechanacimiento' );";
				echo "$( '#pais' ).val( '$modpais' );";
				echo "$( '#provincia' ).val( '$modprovincia' );";
				echo "$( '#ciudad' ).val( '$modciudad' );";
				echo "$( '#direccion' ).val( '$moddireccion' );";
				echo "$( '#codigo_postal' ).val( '$modcodigopostal' );";
				echo "$( '#categoria' ).val( '$modcategoria' );";
				echo "$( '#obra_social' ).val( '$modobrasocial' );";
				echo "$( '#telefono' ).val( '$modtelefono' );";
				echo "$( '#email' ).val( '$modemail' );";
				echo "$( '#legajo' ).val( '$modlegajo' );";
			}
		?>
	});


    function switchDivs(){
        $("#divEmpleados").toggleClass('sr-only');
        $("#divNuevaEditar").toggleClass('sr-only');
    }
    </script>
    <!-- ============================================================= -->
</body>
</html>
