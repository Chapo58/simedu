<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Importar Exportar</title>
    <?php require_once('head.php'); ?>
    <?php
  		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
  			mensaje("Debe seleccionar una empresa.");
  			ir_a("empresas.php");
  		}
  	?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-wrench home-icon"></i>
              <a href="#">Herramientas</a>
            </li>
            <li class="active">Exportar Datos</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <!--
  <h1 class="page-header">Importar</h1>
	<div class="col-lg-6 col-sm-6 col-6">
        <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
                        <span class="glyphicon glyphicon-file"></span><strong>&nbsp;&nbsp;Seleccionar Archivo</strong><input type="file" multiple>
                </span>
            </span>
            <input type="text" class="form-control" readonly>
        </div>
	</div>
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span><strong>&nbsp;&nbsp;Seleccionar Tabla</strong></div>
				<select class='form-control' id='detallehasta' onchange="getCuenta($(this).val(),2);">
					<option selected disabled>Seleccionar...</option>
					<option value="1">Plan de Cuentas</option>
					<option value="2">Listado de Asientos</option>
				</select>
          </div>
   		</div>
	</div>
	<br /><br /><br />
	<div class="col-md-offset-6 col-md-6">
        <div class="form-group">
            <button style="width:100%;" type="submit" id="btnGuardarDatosPersonales" class="btn btn-primary btn-lg">
				<span class="glyphicon glyphicon-open-file"></span>&nbsp;&nbsp;Importar
			</button>
        </div>
	</div>
	<br />
	-->
  <h1 class="page-header">Exportar</h1>
  <form role="form" data-toggle="validator" action="impexp_form.php" method="POST">
    <div class="col-md-12">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-list-alt"></span><strong>&nbsp;&nbsp;Seleccionar Tabla</strong></div>
				<select class='form-control' id='detallehasta' name="exptabla" onchange="getCuenta($(this).val(),2);" required>
					<option selected disabled>Seleccionar...</option>
					<option value="1">Plan de Cuentas</option>
					<option value="2">Listado de Asientos</option>
				</select>
          </div>
   		</div>
	</div>
	<br /><br /><br />
	<div class="col-md-offset-6 col-md-6">
        <div class="form-group">
            <button style="width:100%;" type="submit" id="btnGuardarDatosPersonales" class="btn btn-primary btn-lg">
				<span class="glyphicon glyphicon-save-file"></span>&nbsp;&nbsp;Exportar
			</button>
        </div>
	</div>
   </form>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>
  <!-- ============================================================= -->
</body>
</html>
