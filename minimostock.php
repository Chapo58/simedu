<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Libro de Stock Minimo</title>
  	<?php require_once('head.php'); ?>
    <?php
  		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
  			mensaje("Debe seleccionar una empresa.");
  			ir_a("empresas.php");
  		} else {

  		}
  	?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="simempresarial.php">Simulador Empresarial</a>
            </li>
            <li class="active">Libro de Stock Minimo</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Libro de Stock Minimo</h1>
  <div class="space50"></div>
  <form role="form" data-toggle="validator" target="_blank" action="PDFs/c_minimostock.php" method="POST">
	<div class="col-md-12">
		<button type="submit" id="btnAceptar" style="width:100%;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Generar Impresion</button>
	</div>
  </form>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
  <!-- ============================================================= -->
</body>
</html>
