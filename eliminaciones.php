<?php
session_start();
include "funciones.php";
$idusuario = $_SESSION['idusuario'];

if (isset($_POST['delemp']) && !empty($_POST['delemp'])) {

	$idemp = (int)$_POST['delemp'];

	$eliminar = "DELETE FROM `empresas` WHERE `empresas`.`idempresa` = $idemp LIMIT 1";
	$con_e=consulta("SELECT * FROM empresas WHERE `empresas`.`idempresa` = $idemp");
	$e=mysqli_fetch_array($con_e);
	$rsocial = $e['rsocial'];

	$con_usr=consulta("SELECT idempresa FROM usuario WHERE idusuario='$idusuario'");
	$u=mysqli_fetch_array($con_usr);
	if($u['idempresa'] == $idemp){
		consulta("UPDATE usuario SET idempresa = '' WHERE idusuario = '$idusuario'");
	}

	consulta($eliminar);

	acthistempresa($rsocial, "Se ha eliminado una empresa");
	echo "La empresa $rsocial se ha eliminado con exito";

} elseif (isset($_POST['dellis']) && !empty($_POST['dellis'])) {

	$idlis = (int)$_POST['dellis'];

	consulta("DELETE FROM listaasientos WHERE idlista = $idlis");
	consulta("DELETE FROM asientos WHERE idlista = $idlis");

	echo "El Listado se ha eliminado con exito";
} elseif (isset($_POST['delmod']) && !empty($_POST['delmod'])) {

	$idlis = (int)$_POST['delmod'];

	consulta("DELETE FROM modelolistas WHERE idmodelolista = $idlis");
	consulta("DELETE FROM modeloasientos WHERE idmodelolista = $idlis");

	actualizarhistorial("Se ha eliminado un modelo de asiento");

	echo "El Modelo se ha eliminado con exito";
} elseif (isset($_POST['delart']) && !empty($_POST['delart'])) {

	$idart = (int)$_POST['delart'];

	$con_cat=consulta("SELECT idcategoria FROM productos WHERE idproducto=$idart");
	$c=mysqli_fetch_array($con_cat);
	$idcategoria = $c['idcategoria'];
	consulta("UPDATE categorias SET cantproductos = cantproductos - 1 WHERE idcategoria = '$idcategoria'");

	consulta("DELETE FROM productos WHERE idproducto = $idart");

	actualizarhistorial("Se ha eliminado un producto");

	echo "El Producto se elimino con exito";

} elseif (isset($_POST['delfac']) && !empty($_POST['delfac'])) {

	$idfac = (int)$_POST['delfac'];
	$tipof = $_POST['tipof'];

	$con_pro=consulta("SELECT idproducto, cantidad FROM movstock WHERE idfactura=$idfac");
	while ($p = mysqli_fetch_array($con_pro, MYSQLI_ASSOC)) {
		$idproducto = $p['idproducto'];
		$cantidad = $p['cantidad'];
		if($tipof == 1){ // Si es 1 significa que es un comprobante de compra, por lo que hay que restar el stock
			consulta("UPDATE productos SET stock = stock - $cantidad WHERE idproducto = '$idproducto'");
		} else {
			consulta("UPDATE productos SET stock = stock + $cantidad WHERE idproducto = '$idproducto'");
		}
	}
	
	consulta("DELETE FROM movstock WHERE idfactura = $idfac");
	
	$con_fac=consulta("SELECT ruta FROM facturas WHERE idfactura=$idfac");
	$f=mysqli_fetch_array($con_fac);
	unlink ( "PDFs/".$f['ruta'] );

	consulta("DELETE FROM facturas WHERE idfactura = $idfac");

	actualizarhistorial("Se ha eliminado un comprobante");

	echo "El Comprobante se elimino con exito";

} elseif (isset($_POST['delplanteo']) && !empty($_POST['delplanteo'])) {

	$idplanteo = (int)$_POST['delplanteo'];

	consulta("DELETE FROM planteos WHERE idplanteo = $idplanteo");
	consulta("DELETE FROM preguntas WHERE idplanteo = $idplanteo");

	$con_arch=consulta("SELECT ruta FROM archivos WHERE idplanteo=$idplanteo");
	$a=mysqli_fetch_array($con_arch);
	@unlink ( $a['ruta'] );

	consulta("DELETE FROM archivos WHERE idplanteo = $idplanteo");

	actualizarhistorial("Se ha eliminado un planteo");

	echo "Se ha borrado un planteo.";

} elseif (isset($_POST['delarch']) && !empty($_POST['delarch'])) {

	$idarchivo = (int)$_POST['delarch'];

	$con_arch=consulta("SELECT ruta FROM archivos WHERE idarchivo=$idarchivo");
	$a=mysqli_fetch_array($con_arch);
	unlink ( $a['ruta'] );

	consulta("DELETE FROM archivos WHERE idarchivo = $idarchivo");

	actualizarhistorial("Se ha eliminado un archivo");

	echo "Se ha borrado un archivo.";

} elseif (isset($_POST['delpregunta']) && !empty($_POST['delpregunta'])) {

	$idpregunta = (int)$_POST['delpregunta'];

	consulta("DELETE FROM preguntas WHERE idpregunta = $idpregunta");

	actualizarhistorial("Se ha eliminado una pregunta");

	echo "Se ha borrado una pregunta.";

} elseif (isset($_POST['idmensaje']) && !empty($_POST['idmensaje'])) {

	$idmensaje = (int)$_POST['idmensaje'];

	consulta("DELETE FROM mensajes WHERE idmensaje = $idmensaje");

	actualizarhistorial("Se ha eliminado un mensaje");

	echo "El mensaje ha sido borrado.";

} elseif (isset($_POST['idempleado']) && !empty($_POST['idempleado'])) {

	$idempleado = (int)$_POST['idempleado'];

	consulta("DELETE FROM sueldos_empleados WHERE idempleado = $idempleado");

	actualizarhistorial("Se ha eliminado un empleado");

	echo "El empleado ha sido borrado.";

} elseif (isset($_POST['idbien']) && !empty($_POST['idbien'])) {

	$idbien = (int)$_POST['idbien'];

	consulta("DELETE FROM bienes_de_uso WHERE idbien = $idbien");

	actualizarhistorial("Se ha eliminado un bien de uso");

	echo "El bien de uso ha sido borrado.";

}
?>
