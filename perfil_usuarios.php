<!DOCTYPE html>
<html lang="es">
<head>
  <title>Simedu | Perfil de Usuario</title>
  <?php require_once('head.php'); ?>
	<?php
		if(isset($_POST['idusu']) && !empty($_POST['idusu'])) {
			$idusuario = $_POST['idusu'];
			$con_perfil=consulta("SELECT * FROM usuario WHERE idusuario='$idusuario'");
			$p=mysqli_fetch_array($con_perfil);
			if($p['imagen']){ $imagenusu = $p['imagen'];} else { $imagenusu = "images/logo.png";}
			if($p['tipousuario'] == 1){ $tipousu = "Alumno";} elseif($p['tipousuario'] == 2) { $tipousu = "Profesor";} elseif($p['tipousuario'] == 58){ $tipousu = "Administrador";} else { $tipousu = "Modelo"; }
			if($p['sexo'] == "Masculino"){ $imgsexo = "fa-mars";} else { $imgsexo = "fa-venus ";}
		} elseif(isset($_SESSION['idusuario']) && !empty($_SESSION['idusuario'])){
			$con_perfil=consulta("SELECT * FROM usuario WHERE idusuario='$idusuario'");
			$p=mysqli_fetch_array($con_perfil);
			if($p['imagen']){ $imagenusu = $p['imagen'];} else { $imagenusu = "images/logo.png";}
			if($p['tipousuario'] == 1){ $tipousu = "Alumno";} elseif($p['tipousuario'] == 2) { $tipousu = "Profesor";} elseif($p['tipousuario'] == 58){ $tipousu = "Administrador";} else { $tipousu = "Modelo"; }
			if($p['sexo'] == "Masculino"){ $imgsexo = "fa-mars";} else { $imgsexo = "fa-venus ";}
		} else {
			ir_a('index.php');
		};
	?>
	<style type="text/css">
		.glyphicon2 {  margin-bottom: 10px;margin-right: 10px;}

		.small2 {
		display: block;
		line-height: 1.428571429;
		color: #999;
		}
	</style>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="inicio.php">Inicio</a>
            </li>
            <li><a href="perfil.php">Perfil</a></li>
            <li><a href="grupo.php">Grupo</a></li>
            <li class="active">Perfil de <?php echo $p['nombre'] . " " . $p['apellido']; ?></li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

    <!-- Page Content -->
        <h1 class="page-header">Informacion de Usuario</h1>

      <div class="row">
        <div class="col-md-12 col-lg-12" >

          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo $p['nombre'] . " " . $p['apellido']; ?>
                <form action="mensajes.php" method="POST" class="pull-right">
                      <button type="submit" style="float:right;margin-top: -4px;" class="btn btn-xs btn-white btn-primary" data-toggle="tooltip" title="Enviar Mensaje">
                        <span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;&nbsp;Enviar Mensaje
                      </button>
                    <input type="hidden" name="idusu" value="<?php echo $idusuario; ?>" >
                </form>
              </h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="Imagen de Usuario" src="<?php echo $imagenusu; ?>" class="img-rounded img-user tooltipster img-responsive"> </div>

                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td><span class="glyphicon glyphicon-user"></span> Tipo de Usuario:</td>
                        <td><?php echo $tipousu; ?></td>
                      </tr>
                      <tr>
                        <td><span class="glyphicon glyphicon-envelope"></span> Correo Electronico:</td>
                        <td><?php echo $p['email']; ?></td>
                      </tr>
                      <tr>
                        <td><span class="glyphicon glyphicon-calendar"></span> Fecha de Nacimiento:</td>
                        <td><?php echo date("d/m/Y",strtotime($p['fechanacimiento'])); ?></td>
                      </tr>
					  <!--
                      <tr>
                        <td><i class="fa <?php echo $imgsexo; ?>"></i> Sexo:</td>
                        <td><?php echo $p['sexo']; ?></td>
                      </tr>
					  -->
                        <tr>
                        <td><i class="fa fa-facebook-square"></i> Redes Sociales:</td>
						<?php if($p['facebook'] != NULL ){ ?>
                        <td><a href="<?php echo $p['facebook']; ?>" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-facebook">&nbsp;&nbsp;Facebook</a></td>
						<?php } ?>
                      </tr>
                      <!--<tr>
                        <td><span class="glyphicon glyphicon-user"></span> Profesor:</td>
						<td><a href="#">Nombre Apellido</a></td>
                      </tr>-->
					  <tr>
                        <td><span class="glyphicon glyphicon-map-marker"></span> Ubicacion:</td>
                        <td><?php echo $p['ciudad'] . ", " . $p['provincia']; ?></td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

	<h1 class="page-header">Empresas</h1>

    <div class="row">
	<?php
		$con_empresa=consulta("SELECT * FROM empresas WHERE idusuario='$idusuario'");
		while ($em = mysqli_fetch_array($con_empresa, MYSQLI_ASSOC)) {
			if ($em['imagenempresa'] == "") {
				$imagen = "images/empresa-default.png";
			} else {
				$imagen = $em['imagenempresa'];
			}
	?>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="<?php echo $imagen; ?>" alt="Imagen" class="img-rounded img-responsive" style="min-height:180px;" />
                    </div>
                    <div class="col-sm-6">
                        <h4><?php echo $em['rsocial']; ?></h4>
                        <small class="small2"><cite title="<?php echo $em['domicilio']; ?>"><?php echo $em['domicilio']; ?> <i class="glyphicon2 glyphicon glyphicon-map-marker">
                        </i></cite></small>
                        <p>
                            <i class="glyphicon2 glyphicon glyphicon-envelope"></i><?php echo $em['emailcontacto']; ?>
                            <br />
                            <i class="glyphicon2 glyphicon glyphicon-globe"></i><a href="<?php echo $em['web']; ?>" target="_blank"><?php echo $em['web']; ?></a>
                            <br />
                            <i class="glyphicon2 glyphicon glyphicon-phone-alt"></i><?php echo $em['telefonocontacto']; ?>
						</p>

                    </div>
					<?php
						$idparainst = $em['idempresa'];
						$rsoc = str_replace(' ', '', $em['rsocial']);
						$con_fichas=consulta("SELECT * FROM fichas WHERE idempresa='$idparainst'");
						if(mysqli_num_rows($con_fichas)>0){ $disabledemp = "";} else {$disabledemp = "disabled";}
						$con_productos=consulta("SELECT * FROM productos WHERE idempresa='$idparainst'");
						if(mysqli_num_rows($con_productos)>0){ $disabledpro = "";} else {$disabledpro = "disabled";}
					?>
					<a href="#" data-toggle="modal" class="btn btn-primary" <?php echo $disabledemp; ?> style="float:right;margin-right:3%;" data-target="<?php echo "#" . $rsoc ?>">
						<span class="glyphicon glyphicon-flag"></span>&nbsp;&nbsp;Institucional
					</a>
					<form role="form" action="productos_empresas.php" method="POST">
					<input type="hidden" name="idemp" id="idemp" value="<?php echo $idparainst; ?>" >
					<button type="submit" class="btn btn-primary" style="float:right;margin-right:3%;" <?php echo $disabledpro; ?> >
						<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;Productos
					</button>
					</form>
                </div>
            </div>
        </div>
		<?php } ?>
    </div>

<?php include "modal-empresas.php" ?>

    <!-- Final Page Content -->

    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

    <!-- ============================================================= -->
</body>
</html>
