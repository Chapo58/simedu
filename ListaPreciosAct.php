<?php
session_start();
include "funciones.php";
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM listaprecios WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM listaprecios WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$descripcion = $_POST["descripcion"];
		$por1 = $_POST["por1"];
		$por2 = $_POST["por2"];
		$por3 = $_POST["por3"];
		$por4 = $_POST["por4"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO listaprecios(idempresa, descripcion, por1, por2, por3, por4) VALUES('$idempresa','$descripcion','$por1','$por2','$por3','$por4');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM listaprecios WHERE idlista = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idlista"];
		$descripcion = $_POST["descripcion"];
		$por1 = $_POST["por1"];
		$por2 = $_POST["por2"];
		$por3 = $_POST["por3"];
		$por4 = $_POST["por4"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE listaprecios SET descripcion = '$descripcion', por1 = '$por1', por2 = '$por2', por3 = '$por3', por4 = '$por4' WHERE idlista = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idlista"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM listaprecios WHERE idlista = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>