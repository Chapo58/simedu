<?php
	include "funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['nombre']) && !empty($_POST['nombre']) &&
		isset($_POST['cuit']) && !empty($_POST['cuit']) &&
		isset($_POST['edempleado']) && !empty($_POST['edempleado'])) {
		
		// Quito espacios en blanco
		$id = trim($_POST['edempleado']);
		$nombre = trim($_POST['nombre']);
	@	$sexo = trim($_POST['sexo']);
	@	$fecha_ingreso = $_POST['fecha_ingreso'];
	@	$estado_civil = $_POST['estado_civil'];
		$cuit = trim($_POST['cuit']);
	@	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	@	$direccion = $_POST['direccion'];
	@	$codigo_postal = $_POST['codigo_postal'];
	@	$ciudad = $_POST['ciudad'];
	@	$provincia = $_POST['provincia'];
	@	$pais = $_POST['pais'];
	@	$categoria = $_POST['categoria'];
	@	$obra_social = $_POST['obra_social'];
	@	$telefono = $_POST['telefono'];
	@	$email = $_POST['email'];
	@	$observaciones = $_POST['observaciones'];
	@	$legajo = $_POST['legajo'];
	@	$activo = $_POST['activo'];
		
		// Paso a mayusculas
		$nombre = ucfirst($nombre);
		
		$editar = "UPDATE sueldos_empleados
									SET
									nombre = '$nombre',
									sexo = '$sexo',
									fecha_ingreso = '$fecha_ingreso',
									estado_civil = '$estado_civil',
									cuit = '$cuit',
									fecha_nacimiento = '$fecha_nacimiento',
									direccion = '$direccion',
									codigo_postal = '$codigo_postal',
									ciudad = '$ciudad',
									provincia = '$provincia',
									pais = '$pais',
									idcategoria = '$categoria',
									idobrasocial = '$obra_social',
									telefono = '$telefono',
									email = '$email',
									observaciones = '$observaciones' ,
									legajo = '$legajo',
									activo = '$activo'
									WHERE idempleado = '$id'";
									
		$cargar = consulta($editar);
				
		if(!$cargar){
				echo "Mensaje: ".mysqli_error();
				mensaje("Error");
		}
		mensaje("El empleado se actualizo con exito.");
		acthistempresa($rsocial, "Se modifico un empleado");
		ir_a("sueldos_empleados.php");
		
	} else {
		mensaje("No se cargaron todos los datos");
		ir_a("sueldos_empleados.php");
	}
?>
