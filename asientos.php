<!DOCTYPE html>
<html lang="es">
<head>
    <?php include_once('head.php'); ?>
	<?php
		if(isset($_POST['idlista']) && !empty($_POST['idlista'])) {
			$idlista = $_POST['idlista'];
			$con_l=consulta("SELECT * FROM listaasientos WHERE idlista='$idlista'");
      $lista=mysqli_fetch_array($con_l);
			$nlista = $lista['nlista'];
			$denominacion = $lista['denominacion'];
			$fecha = fecha($lista['fecha'],"/");
			$bandera = true;
			$modelo = false;
			$sumadebe = 0;
			$sumahaber = 0;
			$con_nmax=consulta("SELECT * FROM asientos WHERE idlista = '$idlista' AND idasiento = (SELECT MAX(idasiento) FROM asientos WHERE idlista = '$idlista')");
			$cmax=mysqli_fetch_array($con_nmax);
			$ultimoasiento = $cmax['idasiento'] + 1;
		} elseif(isset($_POST['modelo']) && !empty($_POST['modelo'])) {
			$idmodelolista = $_POST['modelo'];
			$idlista = 0;
			$bandera = false;
			$modelo = true;
			$sumadebe = 0;
			$sumahaber = 0;
			$con_nmax=consulta("SELECT * FROM modeloasientos WHERE idmodelolista = '$idmodelolista' AND idmodelo = (SELECT MAX(idmodelo) FROM modeloasientos WHERE idmodelolista = '$idmodelolista')");
			$cmax=mysqli_fetch_array($con_nmax);
			$ultimoasiento = $cmax['idmodelo'] + 1;
			$con_l=consulta("SELECT * FROM modelolistas WHERE idmodelolista='$idmodelolista'");
      $lista=mysqli_fetch_array($con_l);
			$denominacion = $lista['denominacion'];
			$con_nmax=consulta("SELECT * FROM listaasientos WHERE idempresa = '$idempresa' AND nlista = (SELECT MAX(nlista) FROM listaasientos WHERE idempresa = '$idempresa' AND idperiodo = '$idperiodo')");
			$cmax=mysqli_fetch_array($con_nmax);
			$nlista = $cmax['nlista'] + 1;
			$fecha = date("d/m/Y");
		} else {
			$idlista = 0;
			$bandera = false;
			$modelo = false;
			$sumadebe = 0;
			$sumahaber = 0;
			$ultimoasiento = 0;
			$con_nmax=consulta("SELECT * FROM listaasientos WHERE idempresa = '$idempresa' AND nlista = (SELECT MAX(nlista) FROM listaasientos WHERE idempresa = '$idempresa' AND idperiodo = '$idperiodo')");
			$cmax=mysqli_fetch_array($con_nmax);
			$nlista = $cmax['nlista'] + 1;
			$denominacion = "";
			$fecha = date("d/m/Y");
		}
	?>
    <title>Simedu | Asientos</title>
</head>

<body class="no-skin">
  <?php include_once('header.php'); ?>

  <!-- Page Content -->
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-usd home-icon"></i>
            <a href="#">Simulador Contable</a>
          </li>
          <li><a href="asientos_lista.php">Asientos Contables</a></li>
          <li class="active"><?php if($denominacion){echo $denominacion;}else{echo "Nuevo Asiento";}; ?></li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">
  <!-- Page Content -->
  <div class="bs-callout bs-callout-info">
	<h4>Cabecera</h4>
	<div class="row">
		<div class="col-md-2">
            <input type="number" id="nlista" name="nlista" placeholder="N° de Asiento" value="<?php echo $nlista; ?>" class="form-control">
		</div>
		<div class="col-md-6">
            <input type="text" id="denominacion" maxlength=30 name="denominacion" placeholder="Denominacion" value="<?php echo $denominacion; ?>" class="form-control">
		</div>
		<div class="col-md-4">
      <input type="text" id="fecha" name="fecha" placeholder="Fecha" value="<?php echo $fecha; ?>" class="form-control">
		</div>
	</div>
  </div>
  <div class="bs-callout bs-callout-default col-md-8">
  <form action="asientos.php" method="POST" class="form-horizontal" role="form">
    <select class="form-control" style="float:left;width:70%;" onchange="this.form.submit()" name="modelo">
		<?php if (!isset($_POST['modelo'])) echo "<option disabled selected>Selecciona un Modelo</option>";
			$con_modelos=consulta("SELECT * FROM modelolistas WHERE idusuario='$idusuario'");
			while ($m = mysqli_fetch_array($con_modelos, MYSQLI_ASSOC)) {
				$idmodelo = $m['idmodelolista'];
				$mdenominacion = $m['denominacion'];
			?>
			<option value="<?php echo $idmodelo; ?>"><?php echo $mdenominacion; ?></option>
		<?php } ?>
	</select>
  </form>
  <a class="btn btn-info" style="float:right;" href="asientos_modelo.php"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Nuevo Modelo</a>
  </div>
  <input type="button" id="nuevo" class="btn btn-lg btn-primary" style="width:100%;" value="Nueva Imputacion">
  <div class="space20"></div>
  <table id="asientos" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>id</th>
				<th>Cuenta</th>
                <th>Detalle</th>
                <th>Debe</th>
                <th>Haber</th>
				<th></th>
            </tr>
        </thead>
		<tbody>
			<?php
				if($bandera){
				$con_asi=consulta("SELECT * FROM asientos WHERE idlista='$idlista'");
				while ($a = mysqli_fetch_array($con_asi, MYSQLI_ASSOC)) {
					$idasiento = $a['idasiento'];
					$idcuenta = $a['idcuenta'];
					$debe = $a['debe'];
					$haber = $a['haber'];
					$sumadebe += $debe;
					$sumahaber += $haber;
					$con_cuenta=consulta("SELECT * FROM cuentas WHERE idcuenta='$idcuenta'");
					$c=mysqli_fetch_array($con_cuenta);
					$codigo = $c['codigo'];

			?>
            <tr>
				<td><?php echo $idasiento; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" value="<?php echo $codigo; ?>" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
                <td><input type="number" placeholder="Debe" <?php if($debe == 0){echo "disabled";}else{echo 'value="'.$debe.'"';} ?> name="debe" onblur="debe($(this).closest('td').next().find('input'),$(this).val());" id="debe" class="form-control" /></td>
                <td><input type="number" placeholder="Haber" <?php if($haber == 0){echo "disabled";}else{echo 'value="'.$haber.'"';} ?> name="haber" onblur="haber($(this).closest('td').prev().find('input'),$(this).val());" id="haber" class="form-control" /></td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
				<?php }} ?>
			<?php
				if($modelo){
				$con_asi=consulta("SELECT * FROM modeloasientos WHERE idmodelolista='$idmodelolista'");
				while ($a = mysqli_fetch_array($con_asi, MYSQLI_ASSOC)) {
					$idasiento = $a['idmodelo'];
					$idcuenta = $a['idcuenta'];
					$con_cuenta=consulta("SELECT * FROM cuentas WHERE idcuenta='$idcuenta'");
					$c=mysqli_fetch_array($con_cuenta);
					$codigo = $c['codigo'];
			?>
            <tr>
				<td><?php echo $idasiento; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" value="<?php echo $codigo; ?>" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
                <td><input type="number" placeholder="Debe" name="debe" onblur="debe($(this).closest('td').next().find('input'),$(this).val());" id="debe" class="form-control" /></td>
                <td><input type="number" placeholder="Haber" name="haber" onblur="haber($(this).closest('td').prev().find('input'),$(this).val());" id="haber" class="form-control" /></td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
				<?php }} ?>
			<?php if(!$bandera && !$modelo){ ?>
			<tr>
				<td><?php echo $ultimoasiento; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<option selected disabled>Seleccionar Denominacion</option>
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
                <td><input type="number" placeholder="Debe" name="debe" onblur="debe($(this).closest('td').next().find('input'),$(this).val());" id="debe" class="form-control" /></td>
                <td><input type="number" placeholder="Haber" name="haber" onblur="haber($(this).closest('td').prev().find('input'),$(this).val());" id="haber" class="form-control" /></td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
			<tr>
				<td><?php echo $ultimoasiento += 1; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<option selected disabled>Seleccionar Denominacion</option>
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
                <td><input type="number" placeholder="Debe" name="debe" onblur="debe($(this).closest('td').next().find('input'),$(this).val());" id="debe" class="form-control" /></td>
                <td><input type="number" placeholder="Haber" name="haber" onblur="haber($(this).closest('td').prev().find('input'),$(this).val());" id="haber" class="form-control" /></td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
			<?php } ?>
		</tbody>
		<tfoot>
            <tr>
				<th></th>
				<th><div id="diferencia"></div></th>
                <th></th>
                <th><div id="totaldebe">Total: <?php echo $sumadebe; ?></div></th>
                <th><div id="totalhaber">Total: <?php echo $sumahaber; ?></div></th>
            </tr>
        </tfoot>
    </table>
	<textarea class="form-control" rows="3" id="nota" placeholder="Nota Asociativa"></textarea>
  <!-- Final Page Content -->
  <br/>
	<input type="button" id="guardar" class="btn btn-lg btn-success" style="float:right;" value="Guardar Asiento">
  <br/><br/><br/><br/><br/>
</div>
</div>
</div><!-- /.main-content -->
  <?php require_once('footer.php'); ?>

<script type="text/javascript">
	var sumadebe = 0;
	var sumahaber = 0;
	var diferencia = 0;
	var tasiento;
	var select = "<select class='form-control' id='detalle' onchange=" + '"' +"getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());"+ '"' +">" +
	<?php
	$con_den2=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
	while ($den2 = mysqli_fetch_array($con_den2, MYSQLI_ASSOC)) {
			$denominacion2 = $den2['denominacion'];
			$codigo2 = $den2['codigo'];
	?>
		"<option value='<?php echo $codigo2; ?>'><?php echo $denominacion2; ?></option>" +
	<?php } ?>
	"<option selected disabled>Seleccionar Denominacion</option>" +
	"</select>";
	<?php echo "var ultimoasiento = $ultimoasiento;"; ?>
	function getCuenta(input, valor) {
		input.val(valor);
	};

	function getDenominacion(select, valor) {
		select.val(valor);
	};

	function remover(tr) {
		tasiento.row(tr).remove().draw();
		sumadebe = 0;
		sumahaber = 0;
		$("input[id=debe]").each(function() {
			var number = parseFloat(this.value) || 0;
			sumadebe += parseFloat(number);
		});
		$('#totaldebe').html("Total: " + sumadebe.toFixed(2));
		$("input[id=haber]").each(function() {
			var number = parseFloat(this.value) || 0;
			sumahaber += parseFloat(number);
		});
		$('#totalhaber').html("Total: " + sumahaber.toFixed(2));
		diferencia = sumahaber - sumadebe;
		$('#diferencia').html("Diferencia: <div style='color:#C00;display:inline;'>" + Math.abs(diferencia.toFixed(2)) + "</div>");
	}

	function debe(inp, val) {
		if(val == "" || val == 0) {
			inp.prop('disabled', false);
		} else {
			inp.prop('disabled', true);
		}
		sumadebe = 0;
		$("input[id=debe]").each(function() {
			var number = parseFloat(this.value) || 0;
			sumadebe += parseFloat(number);
		});
		$('#totaldebe').html("Total: " + sumadebe.toFixed(2));
		diferencia = sumahaber - sumadebe;
		$('#diferencia').html("Diferencia: <div style='color:#C00;display:inline;'>" + Math.abs(diferencia.toFixed(2)) + "</div>");
	};

	function haber(inp, val) {
		if(val == "" || val == 0) {
			inp.prop('disabled', false);
		} else {
			inp.prop('disabled', true);
		}
		sumahaber = 0;
		$("input[id=haber]").each(function() {
			var number = parseFloat(this.value) || 0;
			sumahaber += parseFloat(number);
		});
		$('#totalhaber').html("Total: " + sumahaber.toFixed(2));
		diferencia = sumahaber - sumadebe;
		$('#diferencia').html("Diferencia: <div style='color:#C00;display:inline;'>" + Math.abs(diferencia.toFixed(2)) + "</div>");
	};

	$(document).ready(function() {
	    tasiento = $('#asientos').DataTable({
			"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
			],
			"paging":   false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Presione "Nuevo Asiento" para comenzar a cargar asientos.',
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"bFilter": false,
			"bSort" : false
		});

		$('#nuevo').on( 'click', function () {
			ultimoasiento += 1;
			tasiento.row.add( [
				ultimoasiento,
				'<input type="number" placeholder="Nº Cuenta" id="ncuenta" name="ncuenta" onkeyup=' + '"' +"getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());"+ '"' +' class="form-control" />',
				select,
				'<input type="number" placeholder="Debe" name="debe" onblur=' + '"' + "debe($(this).closest('td').next().find('input'),$(this).val());" + '"' + ' id="debe" class="form-control" />',
				'<input type="number" placeholder="Haber" name="haber" onblur=' + '"' + "haber($(this).closest('td').prev().find('input'),$(this).val());" + '"' + ' id="haber" class="form-control" />',
				'<button type="button" class="btn btn-danger btn-md" onclick=' + '"' + "remover($(this).parents('tr'))" + '"' + '><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'
			] ).draw();
		} );

	/*	$("input[id=debe]").keypress(function(evt){
			if(evt.keyCode == 44){
				evt.preventDefault();
			}
		});

		$("input[id=haber]").keypress(function(evt){
			if(evt.keyCode == 44){
				evt.preventDefault();
			}
		});*/

		//Calendario
		$("#fecha").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
		<?php
			if($bandera){
			$con_nota=consulta("SELECT * FROM listaasientos where idlista='$idlista'");
			$not=mysqli_fetch_array($con_nota);
			echo "var nota = '" . $not['nota'] . "';";
		?>
		$('#nota').val(nota);
		$("input[id=ncuenta]").each(function() {
			$(this).closest('td').next('td').find('select').val(this.value);
		});
		<?php } elseif($modelo) { ?>
		$("input[id=ncuenta]").each(function() {
			$(this).closest('td').next('td').find('select').val(this.value);
		});
		<?php } ?>
	});

	$('#guardar').click( function() {
		sumadebe = parseFloat(sumadebe).toFixed(2);
		sumahaber = parseFloat(sumahaber).toFixed(2);
		if(sumadebe != sumahaber) {
			var mensaje = "Los totales no coinciden. Total Debe: " + sumadebe + " | Total Haber: " + sumahaber;
			BootstrapDialog.show({
				message: mensaje,
				buttons: [{
					label: 'Aceptar',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}]
			});
		} else {
			var errores = true;
			var vhaber = [];
			var vdebe = [];
			var vcodigos = [];
			var vdetalles = [];
			var nota = $('#nota').val();
			var nlista = $('#nlista').val();
			var denominacion = $('#denominacion').val();
			var fecha = $('#fecha').val();
			<?php echo "var idlista = $idlista;"; ?>
			$("input[id=haber]").each(function() {
				var number = parseFloat(this.value) || 0;
				vhaber.push(parseFloat(number));
			});
			$("input[id=debe]").each(function() {
				var number2 = parseFloat(this.value) || 0;
				vdebe.push(parseFloat(number2));
			});
			$("input[id=ncuenta]").each(function() {
				var number3 = parseFloat(this.value) || 0;
				if(number3 == 0){
					if(errores){
						balert("Hay cuentas vacias en el asiento!");
					}
					errores = false;
				} else {
					vcodigos.push(parseFloat(number3));
				}

			});
			$("select[id=detalle]").each(function() {
				var detalle = $(this).find('option:selected').text() || 0;
				vdetalles.push(detalle);
			});
			if(errores){
				$.ajax({
					type: "POST",
					url: "cargar_asientos.php",
					data: { ncuenta: vcodigos, detalles: vdetalles, debe: vdebe, haber: vhaber, nota : nota, nlista : nlista, denominacion : denominacion, fecha : fecha, idlista : idlista },
					beforeSend: function(){
						$("#guardar").addClass("m-progress");
						$("#guardar").prop('disabled', true);
					},
					success: function(msg) {
						$("#guardar").removeClass("m-progress");
						$("#guardar").prop('disabled', false);
						BootstrapDialog.show({
							message: msg,
							buttons: [{
								label: 'Aceptar',
								action: function() {
									location.href= "asientos_lista.php";
								}
							}]
						});
					}
				});
			}
		}
    } );

</script>

</body>
</html>
