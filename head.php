<?php
	session_start();
	require_once('funciones.php');
		if(isset($_SESSION['email']) && isset($_SESSION['idusuario'])){
			$email = $_SESSION['email'];
			$idusuario = $_SESSION['idusuario'];
			$con_usr=consulta("SELECT * FROM usuario WHERE idusuario='$idusuario'");
			$u=mysqli_fetch_array($con_usr);
			$idprofe = $u['idprofesor'];
			if($u['habilitado'] == 1){
			if($u['imagen']){ $imagenperfil = $u['imagen'];} else { $imagenperfil = "images/logo.png";}
			if($u['idempresa'] != ""){
				$sinempresa = false;
				$idempresa = $_SESSION['idempresa'] = $u['idempresa'];
				$con_emp=consulta("SELECT empresas.*, periodos.periodo FROM empresas LEFT JOIN periodos ON empresas.idperiodo = periodos.idperiodo
				WHERE idempresa='$idempresa'");
				$emp=mysqli_fetch_array($con_emp);
				$_SESSION['rsocial'] = $emp['rsocial'];
				$emodcontable = $emp['modcontable'];
				$emodempresarial = $emp['modempresarial'];
				$eimpexp = $emp['impexp'];
			} else {
				$sinempresa = true;
				$emodcontable = 1;
				$emodempresarial = 1;
				$emodludico = 1;
				$eimpexp = 1;
			}
			if($u['idperiodo'] != ""){
				$idperiodo = $_SESSION['idperiodo'] = $u['idperiodo'];
				$con_p=consulta("select * from periodos where idperiodo='$idperiodo'");
				$peri=mysqli_fetch_array($con_p);
				$_SESSION['periodo'] = $peri['periodo'];
				$_SESSION['descripcionperiodo'] = $peri['descripcion'];
				$desde = $peri['desde'];
        $hasta = $peri['hasta'];
        $condesde = date("d/m/Y",strtotime($desde));
        $conhasta = date("d/m/Y",strtotime($hasta));
			}
			} else {
				mensaje("Su usuario se encuentra deshabilitado para la utilizacion del sistema.");
				session_unset();
				session_destroy();
				setcookie ("id_usuario_dw", "", 1);
				setcookie ("marca_aleatoria_usuario_dw", "", 1);
				ir_a('index.php');
			}
		} else {
			session_unset();
			session_destroy();
			setcookie ("id_usuario_dw", "", 1);
			setcookie ("marca_aleatoria_usuario_dw", "", 1);
			header('Location: index.php');
		};
		// Cambio de Periodo
		if(isset($_POST['idperiodo']) && !empty($_POST['idperiodo'])) {
			$idp = $_SESSION['idperiodo'] = $_POST['idperiodo'];
			$con_p=consulta("select * from periodos where idperiodo='$idp'");
            $peri=mysqli_fetch_array($con_p);
			$_SESSION['descripcionperiodo'] = $peri['descripcion'];
			consulta("UPDATE usuario SET idperiodo = '$idp' WHERE idusuario = '$idusuario'");
			consulta("UPDATE empresas SET idperiodo = '$idp' WHERE idempresa = '$idempresa'");
		}
		// Cambio Empresa
		if(isset($_POST['empr']) && !empty($_POST['empr'])){
            $empr = $_POST['empr'];
            $con_emp=consulta("select * from empresas where idempresa='$empr'");
            $emp=mysqli_fetch_array($con_emp);
			$_SESSION['rsocial'] = $emp['rsocial'];
			$periodo = $emp['idperiodo'];
            $_SESSION['idempresa'] = $empr;
			$con_p=consulta("select * from periodos where idperiodo='$periodo'");
            $peri=mysqli_fetch_array($con_p);
			$_SESSION['descripcionperiodo'] = $peri['descripcion'];
			consulta("UPDATE usuario SET idempresa = '$empr' WHERE idusuario = '$idusuario'");
			consulta("UPDATE usuario SET idperiodo = '$periodo' WHERE idusuario = '$idusuario'");
        }
?>
<meta charset="iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<meta name="description" content="Simedu Simulador Empresarial Contable Educativo">
<meta name="author" content="Luciano Ciattaglia">

<!-- favicon -->
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

<!-- estilos propios -->
<link href="css/style.css" rel="stylesheet">

<!-- text fonts -->
<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

<!-- ace styles -->
<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />

<!-- ace settings handler -->
<script src="assets/js/ace-extra.min.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53444193-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-53444193-3');
</script>
