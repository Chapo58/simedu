<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Comprobantes</title>
    <?php require_once('head.php'); ?>
    <link href="css/select2.css" rel="stylesheet">
</head>

<body class="no-skin">
	<?php require_once('header.php'); ?>
  <?php
    if(isset($_POST['idcomprobante']) && !empty($_POST['idcomprobante'])) {
      $idcomprobante = $_POST['idcomprobante'];
      $con_comp=consulta("SELECT * FROM facturas WHERE idfactura='$idcomprobante'");
      $comp=mysqli_fetch_array($con_comp);
      $idpersona = $comp['idproveedor'];
      $fecha = fecha($comp['fecha'],"/");
      if($comp['tipo'] == 'A' || $comp['tipo'] == 'B' || $comp['tipo'] == 'C'){
        $tcomprobante = 'F';
      } else {
        $tcomprobante = $comp['tipo'];
      }
      $pventa = $comp['puntofacturacion'];
      $nfactura = $comp['numero'];
      $numero = str_pad($pventa, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura, 8, "0", STR_PAD_LEFT);
      $conceptosng = $comp['conceptosng'];
      $retenciones = $comp['retenciones'];
      $editar = true;
    } else {
      $fecha = date("d/m/Y");
      $conceptosng;
      $retenciones;
      $editar = false;
      $numero = "0001-00000001";
    }
   ?>


  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-industry home-icon"></i>
            <a href="#">Simulador Empresarial</a>
          </li>
          <li><a href="comprobantes.php">Comprobantes</a></li>
          <li class="active">Comprobante de Compra</li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Generar Comprobante de Compra</h1>
   <div class="row">
  	<div class="col-md-7">
	<select id="proveedores" class="form-control">
	<option selected disabled>Seleccionar Proveedor</option>
		<?php
			$con_com=consulta("SELECT * FROM proveedores WHERE idempresa = '$idempresa'");
			while ($c = mysqli_fetch_array($con_com, MYSQLI_ASSOC)) {
				$idpro = $c['idproveedor'];
				$rsocial = $c['rsocial'];
		?>
			<option value = '<?php echo $idpro;?>'><?php echo $rsocial; ?></option>
		<?php } ?>
	</select>
	</div>
	<div class="col-md-5">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Fecha</strong></div>
                <input type="text" data-mask="99/99/9999" id="fecha" name="fecha" class="form-control" value="<?php echo $fecha; ?>" placeholder="Fecha" maxlength="10" required>
          </div>
   		</div>
	</div>
	<!--<div class="col-md-4">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;DDJJ</strong></div>
                <input type="text" data-mask="99/9999" id="fechaddjj" name="fechaddjj" class="form-control" value="<?php echo date("m/Y"); ?>" placeholder="Mes/Año" maxlength="7" required>
          </div>
   		</div>
	</div>-->
  </div>
  <div class="row">
	<div class="col-md-4">
	<select class="form-control" id = "formadepago">
		<option selected disabled>Forma de Pago</option>
		<?php
			$con_per=consulta("SELECT * FROM formpag");
			while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
				$idfpago = $p['idpago'];
				$descr = $p['pag_descr'];
		?>
		<option value="<?php echo $idfpago;?>"><?php echo $descr;?></option>
		<?php } ?>
	</select>
	</div>
	<div class="col-md-4">
	<select class="form-control" id = "tcomprobante">
		<option selected disabled>Tipo de Comprobante</option>
		<option value="F">Factura</option>
    <option value="OC">Orden de Compra</option>
		<option value="NC">Nota de Credito</option>
		<option value="ND">Nota de Debito</option>
	</select>
	</div>
	<div class="col-md-4">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span><strong>&nbsp;&nbsp;N°</strong></div>
                <input type="text" data-mask="9999-99999999" id="num" name="num" class="form-control" value="<?php echo $numero; ?>" placeholder="N° Factura" maxlength="13">
          </div>
   		</div>
	</div>
  </div>
  <div class="row">
	<div class="col-md-5 col-md-offset-1">
		<div class="form-group">
            <input type="number" value="<?php echo $conceptosng; ?>" id="conceptos" name="conceptos" class="form-control" placeholder="Conceptos no gravados">
   		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
            <input type="number" id="retenciones" value="<?php echo $retenciones; ?>" name="retenciones" class="form-control" placeholder="Retenciones">
   		</div>
	</div>
  </div>
  <br>
  <input type="button" id="nuevo" class="btn btn-lg btn-primary" style="width:100%;" value="Agregar Producto">
  <!-- Final Page Content -->
	<table id="tproductos" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
              <th>Producto</th>
      				<th>Cantidad</th>
              <th>Precio Unitario</th>
              <th>Alicuota</th>
      				<th>Total</th>
				<th></th>
            </tr>
        </thead>
		<tbody>
      <?php
      if($editar){
        $con_com=consulta("SELECT * FROM movstock WHERE idfactura='$idcomprobante'");
        while ($c = mysqli_fetch_array($con_com, MYSQLI_ASSOC)) {
          ($c['iva'] < 10) ? $p1 = 0 . str_replace('.','',$c['iva']) : $p1 = str_replace('.','',$p['iva']);
          $caliva = 1 . '.' . $p1;
      ?>
      <tr>
        <td>
          <select class='form-control' id='productos' onchange="getPrecio($(this).closest('td').next('td').next('td').find('input'),$(this).val());">
          <option disabled>Seleccionar Producto</option>
          <?php
            $con_pro=consulta("SELECT idproducto,nombre FROM productos WHERE idempresa='$idempresa' ORDER BY nombre ASC");
            while ($p = mysqli_fetch_array($con_pro, MYSQLI_ASSOC)) {
                $idproducto = $p['idproducto'];
                $nombre = $p['nombre'];
                ($idproducto == $c['idproducto']) ? $seleccinado = 'selected' : $seleccinado = '';
          ?>
              <option value='<?php echo $idproducto; ?>' <?php echo $seleccinado; ?>><?php echo $nombre; ?></option>
          <?php } ?>
          </select>
        </td>
        <td>
          <input type="number" placeholder="Cantidad" value="<?php echo $c['cantidad']; ?>" id="cant" name="cant" onblur="calTotal();" class="form-control" />
        </td>
        <td>
          <input type="number" placeholder="Precio" value=<?php echo $c['precio']; ?> name="precios" onblur="calTotal();" id="precios" class="form-control" />
        </td>
        <td>
          <input type="number" placeholder="Alicuota" value=<?php echo $c['iva']; ?> name="ivas" onblur="calTotal();" id="ivas" class="form-control" />
        </td>
        <td>
          <input type="number" placeholder="Total" value=<?php echo $c['precio'] * $caliva * $c['cantidad']; ?> name="totalprod" onblur="calTotal();" id="totalprod" class="form-control" disabled />
        </td>
        <td>
          <button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        </td>
      </tr>
    <?php }
      } ?>
      <tr>
				<td>
					<select class='form-control' id='productos' onchange="getPrecio($(this).closest('td').next('td').next('td').find('input'),$(this).val());">
					<option selected disabled>Seleccionar Producto</option>
					<?php
						$con_pro=consulta("SELECT idproducto,nombre FROM productos WHERE idempresa='$idempresa' ORDER BY nombre ASC");
						while ($p = mysqli_fetch_array($con_pro, MYSQLI_ASSOC)) {
								$idproducto = $p['idproducto'];
								$nombre = $p['nombre'];
					?>
							<option value='<?php echo $idproducto; ?>'><?php echo $nombre; ?></option>
					<?php } ?>
					</select>
				</td>
				<td>
          <input type="number" placeholder="Cantidad" value=1 id="cant" name="cant" onblur="calTotal();" class="form-control" />
        </td>
        <td>
          <input type="number" placeholder="Precio" name="precios" onblur="calTotal();" id="precios" class="form-control" />
        </td>
        <td>
          <input type="number" placeholder="Alicuota" name="ivas" onblur="calTotal();" id="ivas" class="form-control" />
        </td>
				<td>
          <input type="number" placeholder="Total" name="totalprod" onblur="calTotal();" id="totalprod" class="form-control" disabled />
        </td>
				<td>
          <button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        </td>
      </tr>
		</tbody>
    <tfoot>
            <tr>
				<th></th>
                <th></th>
				<th></th>
        <th></th>
                <th><div id="total"></div></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    <br/>
	<button class="btn btn-success btn-lg" style="float:right;" id="generar">
		<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Generar Comprobante
	</button>
	<br/><br/><br/><br/><br/>

  </div>
 </div>
</div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>
  <script src="js/select2.js"></script>

<script type="text/javascript">
var ids = [];
var precios = [];
var ivas = [];

  <?php
  $con_pro=consulta("SELECT productos.idproducto, productos.preciocosto, iva.porc
            FROM productos
            LEFT JOIN iva ON productos.iva = iva.idiva
            WHERE productos.idempresa='$idempresa'");
  while ($p = mysqli_fetch_array($con_pro, MYSQLI_ASSOC)) {
    $id = $p['idproducto'];
    $precio = number_format($p['preciocosto'], 2, '.', '');
    $iva = number_format($p['porc'], 2, '.', '');
    echo "ids.push(\"$id\");";
    echo "precios.push(\"$precio\");";
    echo "ivas.push(\"$iva\");";
  }
?>

	var select = "<select class='form-control' id='productos' onchange=" + '"' +"getPrecio($(this).closest('td').next('td').next('td').find('input'),$(this).val());"+ '"' +">" +
	<?php
	$con_pro2=consulta("SELECT idproducto,nombre FROM productos WHERE idempresa='$idempresa' ORDER BY nombre ASC");
		while ($p2 = mysqli_fetch_array($con_pro2, MYSQLI_ASSOC)) {
			$idproducto2 = $p2['idproducto'];
			$nombre2 = $p2['nombre'];
	?>
		"<option value='<?php echo $idproducto2; ?>'><?php echo str_replace('"','\"',$nombre2); ?></option>" +
	<?php } ?>
	"<option selected disabled>Seleccionar Producto</option>" +
	"</select>";

	function remover(tr) {
		tproductos.row(tr).remove().draw();
		calTotal();
	}

  function getPrecio(input, id){
		for(var x = 0 ; x < ids.length ; x++) {
			if(ids[x] == id) {
				input.val(precios[x]);
        input.closest('td').next('td').find('input').val(ivas[x]);
			}
		};
		calTotal();
	}

  function calTotal(){
		var sumaprecios = 0;
		$("input[id=precios]").each(function() {
			var number = parseFloat(this.value) || 0;
			var cantidad = $(this).closest('td').prev('td').find('input').val();
      var iva = $(this).closest('td').next('td').find('input').val();
			if(cantidad == 0) cantidad = 1;
			sumaprecios += parseFloat(number * calculariva(iva) * cantidad);
			$(this).closest('td').next('td').next('td').find('input').val(parseFloat(number * calculariva(iva) * cantidad));
		});
		$('#total').html("Total: " + sumaprecios);
	}

	$('#generar').click( function() {
		var fpago = $('#formadepago').val();
		var tcomprobante = $('#tcomprobante').val();
		var proveedor = $("#proveedores").val();
		var fecha = $("#fecha").val();
		var num = $("#num").val();
		var conceptos = $("#conceptos").val();
		var retenciones = $("#retenciones").val();
		if(!fpago) {
			balert("Debe seleccionar una forma de pago.");
		} else if (!tcomprobante) {
			balert("Debe seleccionar un tipo de comprobante.");
		} else if (!proveedor) {
			balert("Debe seleccionar un proveedor.");
		} else if (!num) {
			balert("Debe ingresar un numero de factura.");
		} else {
			var fprecios = [];
			var fcantidades = [];
      var fivas = [];
			var productos = [];

      $("input[id=precios]").each(function() {
        var number = parseFloat(this.value) || 0;
        var cant = $(this).closest('td').prev('td').find('input').val();
        var inpcantidad = $(this).closest('td').prev('td').find('input');
        var iva = $(this).closest('td').next('td').find('input').val();
        if(cant < 1) {
          cant = 1;
          inpcantidad.val(1);
          balert("La cantidad no puede ser negativa");
        }
        fprecios.push(parseFloat(number));
        fcantidades.push(parseFloat(cant));
        fivas.push(parseFloat(iva));
      });

      <?php if($editar){ ?>
        var idcomprobante = <?php echo $idcomprobante; ?>;
      <?php } else { ?>
        var idcomprobante = 0;
      <?php } ?>

			$("select[id=productos]").each(function() {
				var p = $(this).find('option:selected').val() || 0;
				productos.push(p);
			});

			if(fprecios[0] > 0) {
				$.ajax({
					type: "POST",
					url: "PDFs/generar_comprobantecompra.php",
					data: { fpago: fpago, tcomprobante: tcomprobante, proveedor: proveedor, precios: fprecios, cantidades : fcantidades, productos : productos, ivas : fivas, numf : num, fecha : fecha, conceptos : conceptos, retenciones : retenciones, idcomprobante : idcomprobante },
					beforeSend: function(){
						$("#generar").addClass("m-progress");
						$("#generar").prop('disabled', true);
					},
					success: function(msg) {
						$("#generar").removeClass("m-progress");
						$("#generar").prop('disabled', false);
						BootstrapDialog.show({
						message: msg,
						buttons: [{
							label: 'Aceptar',
							action: function() {
								location.href= "comprobantes.php";
							}
						}]
						});
					}
				});
			} else {
				balert("Debe seleccionar al menos un producto.");
			}


		}
    } );

	$(document).ready(function() {
		$("#proveedores").select2({
		});

		tproductos = $('#tproductos').DataTable({
			"paging":   false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'No hay productos cargados!.',
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"bFilter": false,
			"bSort" : false
		});

    $('#nuevo').on( 'click', function () {
			tproductos.row.add( [
				select,
				'<input type="number" placeholder="Cantidad" value=1 id="cant" name="cant" onblur="calTotal();" class="form-control" />',
				'<input type="number" placeholder="Precio" name="precios" onblur="calTotal();" id="precios" class="form-control" />',
        '<input type="number" placeholder="Alicuota" name="ivas" onblur="calTotal();" id="ivas" class="form-control" />',
				'<input type="number" placeholder="Total" name="totalprod" onblur="calTotal();" id="totalprod" class="form-control" disabled />',
				'<button type="button" class="btn btn-danger btn-md" onclick=' + '"' + "remover($(this).parents('tr'))" + '"' + '><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'
			] ).draw();
		} );

    <?php
      if($editar){
        echo "$( '#proveedores' ).val( '$idpersona' ).trigger('change');";
        echo "$( '#formadepago' ).val( 1 );";
        echo "$( '#tcomprobante' ).val( '$tcomprobante' );";
      }
    ?>
	});
</script>
  <!-- ============================================================= -->
</body>
</html>
