<?php
	include "funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['nombre']) && !empty($_POST['nombre']) &&
		isset($_POST['preciovta']) && !empty($_POST['preciovta'])) {
		
		// Quito espacios en blanco
		$nombre = trim($_POST['nombre']);
		$descripcion = trim($_POST['descripcion']);
		if(!empty($_POST['precio'])) {$preciocosto = $_POST['precio'];} else {$preciocosto = 0;}
		$preciovta = number_format($_POST['preciovta'], 2, '.', '');
	@	$iva = $_POST['iva'];
		$stock = $_POST['stock'];
		$stockmin = $_POST['stockmin'];
		if(!empty($_POST['categoria'])) {$categoria = $_POST['categoria'];} else {$categoria = 0;}
		if(!empty($_POST['marca'])) {$marca = $_POST['marca'];} else {$marca = 0;}
		if(!empty($_POST['proveedor'])) {$proveedor = $_POST['proveedor'];} else {$proveedor = 0;}
		if(!empty($_POST['lista'])) {$lista = $_POST['lista'];} else {$lista = 0;}
	//	if(!empty($_POST['ncuenta'])) {$ncuenta = $_POST['ncuenta'];} else {$ncuenta = 0;}
		if(isset($_POST['habilitado'])) {$habilitado = 1;}else{$habilitado = 2;};
		
		// Paso a mayusculas
		$nombre = ucfirst($nombre);
		$descripcion = ucfirst($descripcion);
		
		$rutaimagenp = "images/logo.png";
		
									
			if(!empty($_FILES["imagen"]["name"])){
				if ((($_FILES["imagen"]["type"] == "image/gif")
					|| ($_FILES["imagen"]["type"] == "image/jpeg")
					|| ($_FILES["imagen"]["type"] == "image/jpg")
					|| ($_FILES["imagen"]["type"] == "image/png"))) {
					if ($_FILES["imagen"]["error"] > 0) {
						echo "Return Code: " . $_FILES["imagen"]["error"] . "<br>";
					} else {
						$extension = substr($_FILES["imagen"]["type"], 6);
						$nombreimg = $idempresa . rand();
						$rutaimagen = "images/productos/" . $nombreimg . '.' . $extension;
						if (file_exists($rutaimagen)) {
							unlink($rutaimagen);
							move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
							$rutaimagenp = $rutaimagen;
						} else {
							// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
							move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
							$rutaimagenp = $rutaimagen;
						}
					}
				} else {
					echo "Archivo Inválido";
				}
			}
			
			$cargarproducto = "INSERT INTO productos(idempresa,nombre,descripcion,preciocosto,preciovta,iva,stock,stockmin,imagen,idcategoria,idmarca,idproveedor,idlistap,habilitado)
									VALUES (
									'$idempresa',
									'$nombre',
									'$descripcion',
									'$preciocosto',
									'$preciovta',
									'$iva',
									'$stock',
									'$stockmin',
									'$rutaimagenp',
									'$categoria',
									'$marca',
									'$proveedor',
									'$lista',
									'$habilitado')";
									
				$cargar = consulta($cargarproducto);
				
				consulta("UPDATE categorias SET cantproductos = cantproductos + 1 WHERE idcategoria = '$categoria'");
				
				if(!$cargar){
						echo "Mensaje: ".mysqli_error();
						mensaje("Error");
				}
				mensaje("El producto se cargo con exito.");
				acthistempresa($rsocial, "Se agrego un producto nuevo");
				ir_a("productos.php");
		
	} else {
		mensaje("No se cargaron todos los datos");
		ir_a("productos.php");
	}
?>
