<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Mercado</title>
  	<?php require_once('head.php'); ?>
    <?php
      if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
        mensaje("Debe seleccionar una empresa.");
        ir_a("empresas.php");
      }
    ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="simempresarial.php">Simulador Empresarial</a>
            </li>
            <li class="active">Mercado</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Mercado</h1>
  <div class="bs-callout bs-callout-info">
	<h4>Filtros</h4>
	<div class="row">
		<div class="col-md-4">
            <select class="form-control" id = "filtrocategorias">
				<option selected disabled>Filtrar por Categoria</option>
				<option>Todas</option>
				<?php $con_categorias=consulta("SELECT categorias.*, count(idcategoria) as cont FROM categorias INNER JOIN empresas ON empresas.idempresa = categorias.idempresa
				INNER JOIN usuario ON empresas.idusuario = usuario.idusuario WHERE usuario.idprofesor='$idprofe' AND categorias.cantproductos > 0 GROUP BY categorias.descripcion");
					while ($c = mysqli_fetch_array($con_categorias, MYSQLI_ASSOC)) {
						$idcategoria = $c['idcategoria'];
						$descripcion = $c['descripcion'];
					?>
					<option value="<?php echo $idcategoria; ?>"><?php echo $descripcion; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-4">
            <select class="form-control" id = "filtromarcas">
				<option selected disabled>Filtrar por Marca</option>
				<option>Todas</option>
				<?php $con_marcas=consulta("SELECT marcas.* FROM marcas INNER JOIN empresas ON empresas.idempresa = marcas.idempresa
				INNER JOIN usuario ON empresas.idusuario = usuario.idusuario WHERE usuario.idprofesor='$idprofe' GROUP BY marcas.descripcion HAVING count(idmarca) > 0");
					while ($m = mysqli_fetch_array($con_marcas, MYSQLI_ASSOC)) {
						$idmarca = $m['idmarca'];
						$descripcion = $m['descripcion'];
					?>
					<option value="<?php echo $idmarca; ?>"><?php echo $descripcion; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-4">
            <select class="form-control" id = "filtroempresa">
				<option selected disabled>Filtrar por Vendedor</option>
				<option>Todos</option>
				<?php $con_empresas=consulta("SELECT empresas.rsocial, empresas.idempresa FROM empresas
				INNER JOIN usuario ON empresas.idusuario = usuario.idusuario
				LEFT JOIN productos on empresas.idempresa = productos.idempresa
				WHERE usuario.idprofesor='$idprofe' GROUP BY empresas.rsocial HAVING count(productos.idproducto) > 0");
					while ($e = mysqli_fetch_array($con_empresas, MYSQLI_ASSOC)) {
						$idempresa = $e['idempresa'];
						$rsocial = $e['rsocial'];
					?>
					<option value="<?php echo $idempresa; ?>"><?php echo $rsocial; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
  </div>
    <div class="space20"></div>
				<table id="productos" class="table table-striped table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Producto</th>
                            <th>Descripcion</th>
							<th>Marca</th>
							<th>Categoria</th>
							<th>Vendedor</th>
                            <th>Precio</th>
							<th></th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
							$con_productos=consulta("SELECT productos.*, marcas.descripcion as mdescr, categorias.descripcion as cdescr, empresas.rsocial, empresas.imagenempresa, usuario.idusuario
							FROM productos LEFT JOIN marcas ON productos.idmarca = marcas.idmarca LEFT JOIN categorias ON productos.idcategoria = categorias.idcategoria
							LEFT JOIN empresas ON productos.idempresa = empresas.idempresa
							LEFT JOIN usuario ON empresas.idusuario = usuario.idusuario
							WHERE usuario.idprofesor='$idprofe' AND productos.habilitado = 1 AND productos.stock > 0");
							while ($p = mysqli_fetch_array($con_productos, MYSQLI_ASSOC)) {
								$idproducto = $p['idproducto'];
								$imagenp = $p['imagen'];
								$nombre = $p['nombre'];
								$descripcion = $p['descripcion'];
								$empresa = $p['rsocial'];
								$imagenemp = $p['imagenempresa'];
								$precio = $p['preciovta'];
								$marca = $p['mdescr'];
								$categoria = $p['cdescr'];

						?>
                        <tr>
                            <td class="text-center"><img src=<?php echo $imagenp; ?> class="img-circle img-empresa-min"></td>
                            <td><?php echo $nombre; ?></td>
                            <td><?php echo $descripcion; ?></td>
							<td><?php echo $marca; ?></td>
							<td><?php echo $categoria; ?></td>
							<td class="text-center"><img src=<?php echo $imagenemp; ?> class="img-circle img-empresa-min">&nbsp;&nbsp;&nbsp;<?php echo $empresa; ?></td>
							<td>$<?php echo number_format($precio, 2, ',', '.'); ?></td>
							<td class="text-center">
							<button class="btn btn-success btn-product" onclick="cargarProducto(<?php echo $idproducto; ?>);" >
								<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;Comprar
							</button>
							</td>
                        </tr>
						<?php } ?>
                    </tbody>
                </table>


  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

<script type="text/javascript">
	function cargarProducto(id) {
		var botoncarrito = "<a href='carrito.php' class='btn btn-info' style='float:right;'><span class='glyphicon glyphicon-eye-open'></span>&nbsp;&nbsp;Ver Carrito</a><br/><br/>";
		$.ajax({
			type: "POST",
			url: 'carrito_cargar.php',
			data: { idproducto : id },
				success: function(mensaje) {
					BootstrapDialog.show({
					message: mensaje+botoncarrito,
						buttons: [{
							label: 'Aceptar',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				}
		});
	}

	$(document).ready(function() {

		tproductos = $('#productos').DataTable({
			"order": [[ 1, "asc" ]],
			"columnDefs": [
            {
                "targets": [ 4 ],
                "visible": false
            }
			],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ productos",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'No existen productos cargados en este momento!',
				"sInfo":           "Mostrando productos del _START_ al _END_ de un total de _TOTAL_ productos",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});

		$('#filtrocategorias').on( 'change', function () {
			var cat = $(this).find('option:selected').text();
			if (cat == "Todas") {cat = ""};
			tproductos
				.columns( 4 )
				.search( cat )
				.draw();
		} );
		$('#filtromarcas').on( 'change', function () {
			var mar = $(this).find('option:selected').text();
			if (mar == "Todas") {mar = ""};
			tproductos
				.columns( 3 )
				.search( mar )
				.draw();
		} );
		$('#filtroempresa').on( 'change', function () {
			var emp = $(this).find('option:selected').text();
			if (emp == "Todos") {emp = ""};
			tproductos
				.columns( 5 )
				.search( emp )
				.draw();
		} );

	});
</script>
  <!-- ============================================================= -->
</body>
</html>
