<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Comprobantes</title>
    <?php require_once('head.php'); ?>
    <?php
      if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
        mensaje("Debe seleccionar una empresa.");
        ir_a("empresas.php");
      } else {

      }
    ?>
</head>

<body class="no-skin">
	<?php require_once('header.php'); ?>

  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-industry home-icon"></i>
            <a href="#">Simulador Empresarial</a>
          </li>
          <li class="active">Comprobantes</li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">
  <!-- Page Content -->
  <div class="alert alert-danger">
    Solo se listan los comprobantes comprendidos entre las fechas <?php echo $condesde; ?> y <?php echo $conhasta; ?> de acuerdo a su <a href="periodos.php">periodo seleccionado</a>
  </div>
  <h1 class="page-header">
	  Comprobantes de compras
	  <a href="generar_comprobante_compra.php" style="float:right;" class="btn btn-primary">
		Generar Comprobante de Compra
	  </a>
  </h1>

    <table id="compras" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Fecha</th>
        <th>Hora</th>
				<th>Vendedor</th>
        <th>Tipo de Comprobante</th>
				<th>Nº de Comprobante</th>
        <th>Importe</th>
				<th>Ver</th>
        <th>Editar</th>
				<th>Borrar</th>
            </tr>
        </thead>
        <tbody>
			<?php
				$con_comp=consulta("SELECT facturas.*, empresas.rsocial, proveedores.rsocial as 'rsocialp' FROM facturas
				LEFT JOIN empresas ON facturas.idvendedor = empresas.idempresa
				LEFT JOIN proveedores ON facturas.idproveedor = proveedores.idproveedor
				WHERE idcomprador = '$idempresa' AND facturas.fecha BETWEEN '$desde' AND '$hasta'
				ORDER BY facturas.idfactura DESC");
				while ($c = mysqli_fetch_array($con_comp, MYSQLI_ASSOC)) {
					$idfactura = $c['idfactura'];
					if($c['rsocial'] == ""){
						$vendedor = $c['rsocialp'];
					} else {
						$vendedor = $c['rsocial'];
					}
					$fecha = $c['fecha'];
					$hora = $c['hora'];
					$total = $c['total'];
					$ruta = $c['ruta'];
					$tipo = $c['tipo'];
					if($tipo == "A" || $tipo == "B" || $tipo == "C"){
						$tipoc = "FACTURA " . $tipo;
					} elseif($tipo == "NC"){
						$tipoc = "NOTA DE CREDITO";
					} elseif($tipo == "ND"){
						$tipoc = "NOTA DE DEBITO";
					} elseif($tipo == "R"){
						$tipoc = "REMITO";
          } elseif($tipo == "OC"){
						$tipoc = "ORDEN DE COMPRA";
					} else {
						$tipoc = "PRESUPUESTO";
					}
					$pventa = $c['puntofacturacion'];
					$nfactura = $c['numero'];
					$dia = substr($fecha, -2);
					$mes   = substr($fecha, 5, 2);
					$ano = substr($fecha, 0, 4);
					$pfecha = $dia."/".$mes."/".$ano;
			?>
            <tr>
				<td><?php echo $pfecha; ?></td>
                <td><?php echo $hora; ?></td>
                <td><?php echo $vendedor; ?></td>
                <td align="center"><span class="label label-default"><?php echo $tipoc; ?></span></td>
				<td><?php echo str_pad($pventa, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura, 8, "0", STR_PAD_LEFT); ?></td>
				<td>$<?php echo number_format($total, 2, ',', '.'); ?></td>
				<td style="text-align: center;"><a class="btn btn-info" href="<?php echo 'PDFs/'.$ruta; ?>" target="_blank"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Ver</a></td>
        <td style="text-align: center;">
          <form action="generar_comprobante_compra.php" method="POST">
            <button type="submit" class="btn btn-primary" title="Editar">
							<span class="fa fa-pencil" aria-hidden="true"></span>
						</button>
            <input type="hidden" name="idcomprobante" id="idcomprobante" value="<?php echo $idfactura; ?>" >
          </form>
				</td>
        <td style="text-align: center;">
					<form class="delform">
                        <button type="submit" class="btn btn-danger" title="Eliminar">
							<span class="fa fa-trash-o" aria-hidden="true"></span>
						</button>
						<input type="hidden" id="delfac" name="delfac" value="<?php echo $idfactura; ?>" >
						<input type="hidden" name="tipof" value=1 >
                    </form>
				</td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
	<br><br>
	<h1 class="page-header">
		Comprobantes de ventas
		<a href="generar_comprobante_venta.php" style="float:right;" class="btn btn-primary">
			Generar Comprobante de Venta
		</a>
	</h1>

    <table id="ventas" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Fecha</th>
        <th>Hora</th>
				<th>Comprador</th>
        <th>Tipo de Comprobante</th>
				<th>Nº de Comprobante</th>
        <th>Importe</th>
				<th>Ver</th>
        <th>Editar</th>
				<th>Borrar</th>
            </tr>
        </thead>
        <tbody>
			<?php
				$con_comp=consulta("SELECT facturas.*, empresas.rsocial, clientes.rsocial as 'rsocialc' FROM facturas
				LEFT JOIN empresas ON facturas.idcomprador = empresas.idempresa
				LEFT JOIN clientes ON facturas.idcliente = clientes.idcliente
				WHERE idvendedor = '$idempresa' AND facturas.fecha BETWEEN '$desde' AND '$hasta'
				ORDER BY facturas.idfactura DESC");
				while ($c = mysqli_fetch_array($con_comp, MYSQLI_ASSOC)) {
					$idfactura = $c['idfactura'];
					if($c['rsocial'] == ""){
						$comprador = $c['rsocialc'];
					} else {
						$comprador = $c['rsocial'];
					}
					$fecha = $c['fecha'];
					$hora = $c['hora'];
					$total = $c['total'];
					$ruta = $c['ruta'];
					$tipo = $c['tipo'];
					if($tipo == "A" || $tipo == "B" || $tipo == "C"){
						$tipoc = "FACTURA " . $tipo;
					} elseif($tipo == "NC"){
						$tipoc = "NOTA DE CREDITO";
					} elseif($tipo == "ND"){
						$tipoc = "NOTA DE DEBITO";
					} elseif($tipo == "R"){
						$tipoc = "REMITO";
					} elseif($tipo == "RC"){
						$tipoc = "RECIBO";
					} else {
						$tipoc = "PRESUPUESTO";
					}
					$pventa = $c['puntofacturacion'];
					$nfactura = $c['numero'];
					$dia = substr($fecha, -2);
					$mes   = substr($fecha, 5, 2);
					$ano = substr($fecha, 0, 4);
					$pfecha = $dia."/".$mes."/".$ano;
			?>
            <tr>
				<td><?php echo $pfecha; ?></td>
                <td><?php echo $hora; ?></td>
                <td><?php echo $comprador; ?></td>
                <td align="center"><span class="label label-default"><?php echo $tipoc; ?></span></td>
				<td><?php echo str_pad($pventa, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura, 8, "0", STR_PAD_LEFT); ?></td>
				<td>$<?php echo number_format($total, 2, ',', '.'); ?></td>
				<td style="text-align: center;"><a class="btn btn-info" href="<?php echo 'PDFs/'.$ruta; ?>" target="_blank"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Ver</a></td>
        <td style="text-align: center;">
          <form action="generar_comprobante_venta.php" method="POST">
            <button type="submit" class="btn btn-primary" title="Editar">
							<span class="fa fa-pencil" aria-hidden="true"></span>
						</button>
            <input type="hidden" name="idcomprobante" id="idcomprobante" value="<?php echo $idfactura; ?>" >
          </form>
				</td>
        <td style="text-align: center;">
					<form class="delform">
                        <button type="submit" class="btn btn-danger" title="Eliminar">
							<span class="fa fa-trash-o" aria-hidden="true"></span>
						</button>
						<input type="hidden" id="delfac" name="delfac" value="<?php echo $idfactura; ?>" >
						<input type="hidden" name="tipof" value=2 >
                    </form>
				</td>
			</tr>
		<?php } ?>
        </tbody>
    </table>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

<script type="text/javascript">
	$(document).ready(function() {
			var compras = $('#compras').DataTable({
			"aaSorting": [],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});

		var ventas = $('#ventas').DataTable({
			"aaSorting": [],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});

		$('#compras').on('click', '.delform', function () {
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Comprobante',
				message: '¿Esta seguro que desea eliminar este comprobante?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

		$('#ventas').on('click', '.delform', function () {
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Comprobante',
				message: '¿Esta seguro que desea eliminar este comprobante?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

	});
</script>
  <!-- ============================================================= -->
</body>
</html>
