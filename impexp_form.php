<?php
	session_start();
	require_once('funciones.php');
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];
if(isset($_POST['exptabla']) && !empty($_POST['exptabla'])) {
	$tabla = $_POST['exptabla'];
	if($tabla == 1) {
		/*consulta("SELECT 'codigo', 'denominacion', 'rubro', 'imputable', 'sumariza'
				UNION ALL
				SELECT codigo, denominacion, rubro, imputable, sumariza
				FROM
						cuentas
				WHERE idempresa = '$idempresa'
				INTO OUTFILE
						'C:/xampp/htdocs/simedu/simedu.csv'
						FIELDS TERMINATED BY ';'
						OPTIONALLY ENCLOSED BY '\"'
						LINES TERMINATED BY '\r\n'");*/
		
		$result = consulta("SELECT codigo, denominacion, rubro, imputable, sumariza FROM cuentas WHERE idempresa = '$idempresa' ORDER BY codigo ASC");
		$headers = $result->fetch_fields();
		foreach($headers as $header) {
		$head[] = $header->name;
		}
		$fp = fopen('php://output', 'w');
		if ($fp && $result) {
		fputcsv($fp, array_values($head)); 
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="cuentas.csv"');
		while ($row = $result->fetch_array(MYSQLI_NUM)) {
			fputcsv($fp, array_values($row), ';', ' ');
		}
		die;
		}

	} else if($tabla == 2){
		$result = consulta("SELECT listaasientos.nlista, cuentas.codigo, cuentas.denominacion, asientos.debe, asientos.haber
				FROM asientos INNER JOIN cuentas ON asientos.idcuenta = cuentas.idcuenta
				INNER JOIN listaasientos ON asientos.idlista = listaasientos.idlista
				WHERE cuentas.idempresa = '$idempresa' ORDER BY listaasientos.nlista, cuentas.codigo ASC");
		$headers = $result->fetch_fields();
		foreach($headers as $header) {
		$head[] = $header->name;
		}
		$fp = fopen('php://output', 'w');
		if ($fp && $result) {
		fputcsv($fp, array_values($head)); 
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="asientos.csv"');
		while ($row = $result->fetch_array(MYSQLI_NUM)) {
			fputcsv($fp, array_values($row), ';', ' ');
		}
		die;
		}
	}
	
	ir_a("impexp.php");
	
}
?>