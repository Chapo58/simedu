<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Inicio</title>
		<?php require_once('head.php'); ?>
	</head>

	<body class="no-skin">

		<?php require_once('header.php'); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
				              <i class="ace-icon fa fa-suitcase home-icon"></i>
				              <a href="empresas.php">Mis Empresas</a>
				            </li>
							<li class="active">Organigramas</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
				<div class="page-content">
					<h1 class="page-header">
						Organigramas
						<!--<button class="btn btn-lg btn-info" style="float:right;" onclick="guardar(1);">
						<span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir Organigrama
						</button>-->
					</h1>
					<form action="organigrama_guardar.php" method="POST">
					<textarea class="form-control" rows="5" name="texto" id="texto" required>
							<?php
							$con_o=consulta("SELECT * FROM organigramas WHERE idusuario='$idusuario'");
							if(mysqli_num_rows($con_o)==1){
								$o=mysqli_fetch_array($con_o);

								echo trim($o['texto']);

							 } ?>
					</textarea>
					<br>
					<button type="submit" class="btn btn-primary btn-lg" style="float:right;">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Guardar
					</button>
					</form>
					<!-- Final Page Content -->
				</div>
			</div>
		</div><!-- /.main-content -->

			<?php require_once('footer.php'); ?>			
		<script>

CKEDITOR.replace( 'texto' );

/*function guardar(imprimir){

					  var texto = $('#texto').html();
						alert(texto);
						$.ajax({
							type: "POST",
							url: 'organigrama_guardar.php',
							data: { texto : texto },
							beforeSend: function(){
								
							},
							success: function(mensaje) {
								if(imprimir == 1){
									window.location = "PDFs/organigrama_alumno.php";
								} else {
									alerta("Organigrama",mensaje);
								}
							},
							error: function (xhr, status, error) {
								var err = eval("(" + xhr.responseText + ")");
								alert(err.Message);
							}
						});

};*/
		</script>
	</body>
</html>
