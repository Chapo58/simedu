<?php
	header('Content-Type: text/html; charset=UTF-8'); 
	
	date_default_timezone_set('America/Argentina/Cordoba');
	
	//Obteniendo Sistema Operativo
		function so_info($uagent) {
			// the order of this array is important
			global $uagent;
			$oses   = array(
				'Win311' => 'Win16',
				'Win95' => '(Windows 95)|(Win95)|(Windows_95)',
				'WinME' => '(Windows 98)|(Win 9x 4.90)|(Windows ME)',
				'Win98' => '(Windows 98)|(Win98)',
				'Win2000' => '(Windows NT 5.0)|(Windows 2000)',
				'WinXP' => '(Windows NT 5.1)|(Windows XP)',
				'WinServer2003' => '(Windows NT 5.2)',
				'WinVista' => '(Windows NT 6.0)',
				'Windows 7' => '(Windows NT 6.1)',
				'Windows 8' => '(Windows NT 6.2)',
				'WinNT' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
				'OpenBSD' => 'OpenBSD',
				'SunOS' => 'SunOS',
				'Ubuntu' => 'Ubuntu',
				'Android' => 'Android',
				'Linux' => '(Linux)|(X11)',
				'iPhone' => 'iPhone',
				'iPad' => 'iPad',
				'MacOS' => '(Mac_PowerPC)|(Macintosh)',
				'QNX' => 'QNX',
				'BeOS' => 'BeOS',
				'OS2' => 'OS/2',
				'SearchBot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
			);
			$uagent = strtolower($uagent ? $uagent : $_SERVER['HTTP_USER_AGENT']);
			foreach ($oses as $os => $pattern)
				if (preg_match('/' . $pattern . '/i', $uagent))
					return $os;
			return 'Desconocido';
		}
		
	
	function mensaje($m){
		echo "<script language=Javascript>alert('$m');</script>";
	}
	
	function ir_a($p){
		echo "<script language=Javascript>window.location = \"" . $p . "\"; </script>";
	}
	
	function abrir_ventana($p){
		echo "<script language=Javascript>window.open(" . $p . ",'_blank'); </script>";
	}
	
	$conex;
	
	function consulta($sql){
		global $conex;
		$conex=mysqli_connect("localhost","root","","sztwcdgf_simedusergio") or die("Error " . mysqli_error($conex)); 
		mysqli_set_charset($conex,'utf8');
		$consulta=@mysqli_query($conex,$sql);
		return $consulta;
	}
	$link = $con = mysqli_connect("localhost","root","","sztwcdgf_simedusergio");
	mysqli_set_charset($link,'utf8');
	
	function testfecha($input,$format="") {
		if(!empty($input)){
			$separator_type= array(
				"/",
				"-",
				"."
			);
			foreach ($separator_type as $separator) {
				$find= stripos($input,$separator);
				if($find<>false){
					$separator_used= $separator;
				}
			}
			$input_array= explode($separator_used,$input);
			if ($format=="mdy") {
				return checkdate($input_array[0],$input_array[1],$input_array[2]);
			} elseif ($format=="ymd") {
				return checkdate($input_array[1],$input_array[2],$input_array[0]);
			} else {
				return checkdate($input_array[1],$input_array[0],$input_array[2]);
			}
			$input_array=array();
		}
		
    }
	
	function actualizarhistorial($accion) {
		$act = $accion;
		$idusuario = $_SESSION['idusuario'];
		$fecha =  date("Y-m-d"); 
		$hora =	date("H:i");
		$ip = $ip = $_SERVER['REMOTE_ADDR'];
		$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$uagent = $_SERVER['HTTP_USER_AGENT'];
		$sistemaoperativo = so_info($uagent);
		$nc=consulta("insert into historial 
			(idusuario,fecha,hora,accion,ip,hostname,so) values
			('$idusuario','$fecha','$hora','$act','$ip','$hostname','$sistemaoperativo')");
		if(!$nc){
			echo "Mensaje: ".mysqli_error();
			mensaje("Error");
		}	
	}
	
	function acthistnuevousuario($email) {
		$act = "Creacion de cuenta nueva";
		$email = $email;
		$con_u=consulta("SELECT * FROM usuario WHERE email='$email'");
		$u=mysqli_fetch_array($con_u);
		$idusuario = $u['idusuario'];
		$fecha =  date("Y-m-d"); 
		$hora =	date("H:i");
		$ip = $ip = $_SERVER['REMOTE_ADDR'];
		$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$uagent = $_SERVER['HTTP_USER_AGENT'];
		$sistemaoperativo = so_info($uagent);
		$nc=consulta("insert into historial 
			(idusuario,fecha,hora,accion,ip,hostname,so) values
			('$idusuario','$fecha','$hora','$act','$ip','$hostname','$sistemaoperativo')");
		if(!$nc){
			echo "Mensaje: ".mysqli_error();
			mensaje("Error");
		}	
	}
	
	function acthistempresa($rsocial, $accion) {
		$act = $accion;
		$rsocial = $rsocial;
		$idusuario = $_SESSION['idusuario'];
		$fecha =  date("Y-m-d");
		$hora =	date("H:i");
		$ip = $ip = $_SERVER['REMOTE_ADDR'];
		$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$uagent = $_SERVER['HTTP_USER_AGENT'];
		$sistemaoperativo = so_info($uagent);
		$nc=consulta("insert into historial 
			(idusuario,rsocialempresa,fecha,hora,accion,ip,hostname,so) values
			('$idusuario','$rsocial','$fecha','$hora','$act','$ip','$hostname','$sistemaoperativo')");
		if(!$nc){
			echo "Mensaje: ".mysqli_error();
			mensaje("Error");
		}	
	}
	
	function fecha($fecha,$tipo){
		if($tipo == "/"){
			return date("d/m/Y",strtotime($fecha));
		} elseif($tipo == "-"){
			$fecha = str_replace("/","",$fecha);
			$año = substr($fecha, -4);
			$mes = substr($fecha, -6, 2);
			$dia = substr($fecha, -8, 2);
			$fecha = $año."-".$mes."-".$dia;
			return $fecha;
		}	
	}
	
	function sanear_string($string)
{
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
 
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":"),
        '',
        $string
    );
	// Quitar espacios en blanco
	$string = str_replace(" ", "_", $string);
	
    return $string;
}

	function esimagen($ruta){
		$ext = substr($ruta, -4);
		if($ext == ".jpg" || $ext == "jpeg" || $ext == ".png" || $ext == ".gif"){
			return true;
		}
		return false;
	}
	
	

?>