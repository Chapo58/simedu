<!DOCTYPE html>
<html lang="es">
<head>
  <?php require_once('head.php'); ?>
	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		} else {

		}
	?>
    <title>Simedu | Balance General</title>
</head>

<body class="no-skin">

<?php require_once('header.php'); ?>

<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-usd home-icon"></i>
          <a href="#">Simulador Contable</a>
        </li>
        <li class="active">Balance General</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
  <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Balance General</h1>
  <div class="space50"></div>
  <form role="form" data-toggle="validator" target="_blank" action="PDFs/balancegeneral.php" method="POST">
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Desde</strong></div>
                <input type="text" data-mask="99/99/9999" id="desde" name="desde" class="form-control" value="<?php echo date("d/m/Y",strtotime($desde)); ?>" placeholder="Fecha Desde" maxlength="10">
          </div>
   		</div>
	</div>
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Hasta</strong></div>
                <input type="text" data-mask="99/99/9999" id="hasta" name="hasta" class="form-control" value="<?php echo date("d/m/Y",strtotime($hasta)); ?>" placeholder="Fecha Hasta" maxlength="10">
          </div>
   		</div>
	</div>
	<div class="col-md-12">
		<button type="submit" id="btnAceptar" style="width:100%;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Generar Impresion</button>
	</div>
  </form>

  <!-- Final Page Content -->
  </div>
 </div>
</div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

  <script type="text/javascript">
  	$(document).ready(function() {
  		//Calendarios
  		$("#desde").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  		$("#hasta").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  	});
  </script>
  <!-- ============================================================= -->
</body>
</html>
