

			<?php
			$con_pie=consulta("SELECT * FROM piepagina");
			$pie=mysqli_fetch_array($con_pie);
			?>
			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120" style="margin-top:-1%;margin-bottom:-1%;">
							<?php echo $pie['linea1']; ?>
						</span>
						<!-- <span class="action-buttons">
							<a href="https://www.facebook.com/simedueducativo/" target="_blank">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>
						</span> -->
						 <div style="margin-top:-1.5%;">
							<?php echo $pie['linea2']; ?>
						</div>
						<div style="margin-top:-1.5%;">
							<?php echo $pie['linea3']; ?>
						</div>
						<!-- <div style="margin-top:-1.5%;">
							<?php echo $pie['linea4']; ?>
						</div>
						<div style="margin-top:-1.5%;margin-bottom:-1%;">
							<u>Soporte técnico</u>: <?php echo $pie['stecnico']; ?>
						</div> -->
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<script src="js/utilidades.js"></script>
		<script src="js/jasny-bootstrap.min.js"></script>

		<!-- DataTables CSS -->
		<link rel="stylesheet" type="text/css" href="css/datatable.css">
		<!-- Jasny -->
		<link rel="stylesheet" type="text/css" href="css/jasny-bootstrap.min.css">

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!-- ace scripts -->
		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- CKEditor -->
		<script src="js/ckeditor/ckeditor.js"></script>
		
<!--<div class="feedback left" style="position: fixed; bottom: 10px; left: 10px; z-index: 1000;">
      <div class="tooltips">
          <div class="btn-group dropup">
            <button type="button" class="btn btn-primary dropdown-toggle btn-circle btn-lg" data-toggle="dropdown" data-toggle="tooltip" data-placement="right" title="Reportar Error" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-bug fa-2x" title="Report Bug"></i>
            </button>
            <ul class="dropdown-menu dropdown-menu-right dropdown-menu-form">
              <li>
                <div class="report">
                  <h4 class="text-center">Reportar Error o Sugerencia</h4>
                  <form class="doo" method="post" action="report.php">
                    <div class="col-sm-12">
                      <textarea required name="comment" class="form-control" placeholder="En caso de error por favor sea lo mas descriptivo posible detallando cualquier mensaje del sistema visto."></textarea>
                      <input name="screenshot" type="hidden" class="screen-uri">
                     </div>
                     <div class="col-sm-12 clearfix" style="margin-top:20px;">
                      <button class="btn btn-primary btn-block">Enviar Reporte</button>
                     </div>
                 </form>
                </div>
                <div class="loading text-center hideme">
                  <h2>Por favor espere...</h2>
                  <h2><i class="fa fa-refresh fa-spin"></i></h2>
                </div>
                <div class="reported text-center hideme">
                  <h2>Muchas gracias</h2>
                  <p>Hemos recibido su reporte, lo revisaremos a la brevedad.</p>
                   <div class="col-sm-12 clearfix">
                      <button class="btn btn-success btn-block do-close">Cerrar</button>
                   </div>
                </div>
                <div class="failed text-center hideme">
                  <h2>Error!</h2>
                  <p>Hemos tenido un error cuando intentamos cargar su reporte, por favor contactenos via mail a soporte@simedu.com.ar.</p>
                   <div class="col-sm-12 clearfix">
                      <button class="btn btn-danger btn-block do-close">Cerrar</button>
                   </div>
                </div>
              </li>
            </ul>
          </div>
      </div>
</div>-->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59975a751b1bed47ceb0568f/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
Tawk_API.visitor = {
    name  : "<?php
                if(!empty($u['nombre']) || !empty($u['apellido'])) {
                  echo $u['nombre']." ".$u['apellido'];
                } else {
                  echo $email;
                }
              ?>",
    email : "<?php echo $email; ?>"
};
Tawk_API.onLoad = function(){
    Tawk_API.setAttributes({
        'Empresa'    : "<?php if(isset($_SESSION['rsocial']) && !empty($_SESSION['rsocial'])){ echo $_SESSION['rsocial']; }?>"
    }, function(error){});
};
</script>
<!--End of Tawk.to Script-->
<?php if (isset($_SESSION['idusuarioprofesor']) && !empty($_SESSION['idusuarioprofesor'])) { ?>
<div style="position: fixed; bottom: 10px; right: 10px; z-index: 1000;">
  <form action="cambiarusuario.php" method="POST" class="pull-right">
      <button type="submit" class="btn btn-danger btn-lg" data-toggle="tooltip" title="Volver a su cuenta">
        <span class="fa fa-eye-slash" aria-hidden="true"></span>
      </button>
      <input type="hidden" name="idusu" value="<?php echo $_SESSION['idusuarioprofesor']; ?>" >
  </form>
</div>
<?php } ?>

</div><!-- /.main-container -->

<!-- inline scripts related to this page -->
<script type="text/javascript">
	CKEDITOR.config.extraPlugins = 'justify';
  jQuery(function($) {

    /////////////////////////////////////
    $(document).one('ajaxloadstart.page', function(e) {
      $tooltip.remove();
    });

      //Android's default browser somehow is confused when tapping on label which will lead to dragging the task
    //so disable dragging when clicking on label
    var agent = navigator.userAgent.toLowerCase();
    if(ace.vars['touch'] && ace.vars['android']) {
      $('#tasks').on('touchstart', function(e){
      var li = $(e.target).closest('#tasks li');
      if(li.length == 0)return;
      var label = li.find('label.inline').get(0);
      if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
      });
    }

    //show the dropdowns on top or bottom depending on window height and menu position
    $('#task-tab .dropdown-hover').on('mouseenter', function(e) {
      var offset = $(this).offset();

      var $w = $(window)
      if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
        $(this).addClass('dropup');
      else $(this).removeClass('dropup');
    });

  })

  $(function () {
      setNavigation();
  });

  function setNavigation() {
      var path = window.location.pathname;
      path = path.replace("/simedu/", "");
      path = decodeURIComponent(path);
      $(".nav-pills a").each(function () {
          var href = $(this).attr('href');
          if (path.substring(0, href.length) === href) {
              $(this).parents('li:eq(1)').addClass('active');
          }
      });
  }
  $(document).ready(function() {
    $( "#cerrarsesion" ).click(function() {
      <?php if($idusuario == 107){ ?>
      var comentario = "<form id='comentariocerrarsesion' action='guardaropinion.php' method='POST'><textarea class='form-control' rows=3 name='comentario' placeholder='Describa su experiencia con nuestro simulador educativo'></textarea></form>";
      BootstrapDialog.show({
        title: 'Diganos su opinion!',
        message: comentario,
        type: BootstrapDialog.TYPE_WARNING,
        buttons: [{
          label: ' Enviar',
          cssClass: 'btn-primary',
          action: function(){
            $('form#comentariocerrarsesion').submit();
          }
        }, {
          label: ' Cancelar',
          cssClass: 'btn-default',
          action: function(dialogItself){
            dialogItself.close();
          }
        }]
      });
      <?php } else { ?>
      BootstrapDialog.show({
        title: 'Cerrar Sesión',
        message: '¿Esta seguro que desea cerrar sesión?',
        type: BootstrapDialog.TYPE_WARNING,
        buttons: [{
          label: ' Aceptar',
          cssClass: 'btn-primary',
          action: function(){
            window.location.href = "usuario_logout.php";
          }
        }, {
          label: ' Cancelar',
          cssClass: 'btn-default',
          action: function(dialogItself){
            dialogItself.close();
          }
        }]
      });
      <?php } ?>
    });
  });
</script>
