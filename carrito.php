<!DOCTYPE html>
<html lang="es">
<head>
    <?php require_once('head.php'); ?>
    <title>Simedu | Carrito</title>
	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		}
	?>
</head>

<body class="no-skin">
	<?php require_once('header.php'); ?>

  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-industry home-icon"></i>
            <a href="#">Simulador Empresarial</a>
          </li>
          <li class="active">Carrito</li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Carrito de compras de <?php echo $_SESSION['rsocial']; ?></h1>
  		<div class="space50"></div>
        <div class="row">
            <div class="col-xs-12">
                <table id="productos" class="table table-striped table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Producto</th>
                            <th>Descripcion</th>
							              <th>Vendedor</th>
                            <th>Cantidad</th>
							              <th>Stock</th>
                            <th>Precio Unitario</th>
							              <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
						<?php
							$con_carrito=consulta("SELECT carritos.*, productos.nombre, productos.descripcion, productos.idproducto, productos.preciovta, productos.imagen, productos.stock, empresas.rsocial
							FROM carritos LEFT JOIN productos ON carritos.idproducto = productos.idproducto LEFT JOIN empresas ON productos.idempresa = empresas.idempresa WHERE carritos.idcomprador='$idempresa'");
							$suma = 0;
							while ($c = mysqli_fetch_array($con_carrito, MYSQLI_ASSOC)) {
								$imagen = $c['imagen'];
								$nombre = $c['nombre'];
								$descripcion = $c['descripcion'];
								$empresa = $c['rsocial'];
								$precio = $c['preciovta'];
								$stock = $c['stock'];
								$cantidad = $c['cantidad'];
								$suma += $precio * $cantidad;

						?>
                        <tr>
                            <td class="text-center"><img src=<?php echo $imagen; ?> class="img-circle img-empresa-min"></td>
                            <td><?php echo $nombre; ?></td>
                            <td><?php echo $descripcion; ?></td>
							<td><?php echo $empresa; ?></td>
                            <td><input type="number" placeholder="Cantidad" value="<?php echo $cantidad; ?>" id="ncuenta" name="ncuenta" onblur="calcularValor($(this),<?php echo $c['idcarrito']; ?>,<?php echo $stock; ?>);" class="form-control" /></td>
                            <td><?php echo number_format($stock, 0, ',', '.'); ?></td>
							<td><?php echo number_format($precio, 2, ',', '.'); ?></td>
							<td class="text-center"><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'),<?php echo $c['idcarrito']; ?>)"><span class="glyphicon glyphicon-remove"></span></button></td>
                        </tr>
						<?php } ?>
                    </tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th><div id="total">Total: <?php echo number_format($suma, 2, ',', '.'); ?></div></th>
							<th></th>
						</tr>
					</tfoot>
                </table>
            </div>
        </div>
		<br>
		<select class="form-control" id = "formadepago">
			<option selected disabled>Seleccionar Forma de Pago</option>
			<?php
				$con_per=consulta("SELECT * FROM formpag");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idfpago = $p['idpago'];
					$descr = $p['pag_descr'];
			?>
			<option value="<?php echo $idfpago;?>"><?php echo $descr;?></option>
			<?php } ?>
		</select>
		<br>
		<button class="btn btn-default btn-lg" style="float:left;" onclick="guardarCarrito();" >
			<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Guardar Cambios
		</button>
		<button class="btn btn-success btn-lg" style="float:right;" id="confirma" onclick="realizarCompra();" >
			<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Confirmar Compra
		</button>


  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

<script type="text/javascript">
	var tproductos;
	var ids = [];
	var precios = [];
	var cantidades = [];
	<?php
		$con_pro=consulta("SELECT carritos.*, productos.idproducto, productos.preciovta
							FROM carritos LEFT JOIN productos ON carritos.idproducto = productos.idproducto WHERE carritos.idcomprador='$idempresa'");
		while ($p = mysqli_fetch_array($con_pro, MYSQLI_ASSOC)) {
			$id = $p['idcarrito'];
			$precio = number_format($p['preciovta'], 2, '.', '');
			$cantidad = $p['cantidad'];
			echo "ids.push(\"$id\");";
			echo "precios.push(\"$precio\");";
			echo "cantidades.push(\"$cantidad\");";
		}
	?>

	function remover(tr, id) {
		tproductos.row(tr).remove().draw();
		var suma = 0;
		for(var x = 0 ; x < precios.length ; x++) {
			if(ids[x] == id) {
			/*	precios.splice(x, 1);
				ids.splice(x, 1);
				cantidades.splice(x, 1);*/
				precios[x] = 0;
				cantidades[x] = 0;
			}
		};
		for(var x = 0 ; x < precios.length ; x++) {
			suma += parseFloat(precios[x] * cantidades[x]);
		};
		$('#total').html("Total: " + suma);
	};

	function calcularValor(input, id, stock) {
		cant = input.val();
		if(cant > 0){
			if(cant > stock){
				BootstrapDialog.show({
					message: "No hay stock suficiente",
					buttons: [{
						label: 'Aceptar',
						action: function(dialogItself) {
							dialogItself.close();
						}
					}]
				});
				input.val(1);
				var suma = 0;
				for(var x = 0 ; x < precios.length ; x++) {
					if(ids[x] == id) {
						cantidades[x] = 1;
					}
				};
				for(var x = 0 ; x < precios.length ; x++) {
					suma += parseFloat(precios[x] * cantidades[x]);
				};
				$('#total').html("Total: " + suma);
			} else {
				var suma = 0;
				for(var x = 0 ; x < precios.length ; x++) {
					if(ids[x] == id) {
						cantidades[x] = cant;
					}
				};
				for(var x = 0 ; x < precios.length ; x++) {
					suma += parseFloat(precios[x] * cantidades[x]);
				};
				$('#total').html("Total: " + suma);
			}
		} else {
		/*	BootstrapDialog.show({
				message: "La cantidad debe ser un valor positivo igual o mayor a 1",
				buttons: [{
					label: 'Aceptar',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}]
			});*/
			input.val(1);
			var suma = 0;
			for(var x = 0 ; x < precios.length ; x++) {
				if(ids[x] == id) {
					cantidades[x] = 1;
				}
			};
			for(var x = 0 ; x < precios.length ; x++) {
				suma += parseFloat(precios[x] * cantidades[x]);
			};
			$('#total').html("Total: " + suma);
		}
	};

	function guardarCarrito(){
		$.ajax({
			type: "POST",
			url: "carrito_modificar.php",
			data: { ids: ids, cantidades: cantidades }
		}).done(function( msg ) {
			BootstrapDialog.show({
			message: msg,
			buttons: [{
				label: 'Aceptar',
				action: function() {
					location.href= "carrito.php";
				}
			}]
			});
		});
	}

	function realizarCompra(){
		var formpago = $('#formadepago').val();
		if (!formpago){
			BootstrapDialog.show({
				message: "Debe seleccionar una forma de pago.",
				buttons: [{
					label: 'Aceptar',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}]
			});
		} else {
			$.ajax({
				type: "POST",
				url: "PDFs/carrito_comprar.php",
				data: { ids: ids, cantidades: cantidades, formpago : formpago },
				beforeSend: function(){
					$("#confirma").addClass("m-progress");
					$("#confirma").prop('disabled', true);
				},
				success: function(msg) {
					$("#confirma").removeClass("m-progress");
					$("#confirma").prop('disabled', false);
					BootstrapDialog.show({
					message: msg,
					buttons: [{
						label: 'Aceptar',
						action: function() {
							location.href= "comprobantes.php";
						}
					}]
					});
				}
			});
		}
	}

	$(document).ready(function() {

		tproductos = $('#productos').DataTable({
			"paging":   false,
			"info":     false,
            "searching":   false,
			"order": [[ 1, "asc" ]],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'El carrito se encuentra vacio!',
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});


	});
</script>
  <!-- ============================================================= -->
</body>
</html>
