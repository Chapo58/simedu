<?php

if ($_SERVER['HTTP_REFERER'] == ""){
		header("Location: ../index.php");
		exit();
}

session_start();
include_once("funciones.php");

$id = intval($_GET['id']);

$idusuario = $_SESSION['idusuario'];

$con_datos=consulta("SELECT preguntas.*, respuestas.respuesta, respuestas.opcion, respuestas.idalumno
	FROM preguntas LEFT JOIN respuestas ON preguntas.idpregunta = respuestas.idpregunta AND respuestas.idalumno = '$idusuario'
	WHERE preguntas.idpregunta='$id'");
$d=mysqli_fetch_array($con_datos);

$arr= array();
$arr['pregunta'] = $d['descripcion'];
$arr['link'] = $d['link'];
$arr['ruta'] = $d['ruta'];
$arr['esimagen'] = esimagen($d['ruta']);
$arr['multiple'] = $d['multiple'];
$arr['opcion1'] = $d['opcion1'];
$arr['opcion2'] = $d['opcion2'];
$arr['opcion3'] = $d['opcion3'];
$arr['opcion4'] = $d['opcion4'];
$arr['opcion5'] = $d['opcion5'];
$arr['opcion6'] = $d['opcion6'];
$arr['incc'] = $d['correcta'];
$arr['respuesta'] = $d['respuesta'];
$arr['opcion'] = $d['opcion'];

echo json_encode($arr);

?>
