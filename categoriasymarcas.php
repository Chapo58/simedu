<!DOCTYPE html>
<html lang="es">
<head>
  <title>Simedu | Categorias</title>
  <?php require_once('head.php'); ?>
	<link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="jtable/css/themes/lightcolor/blue/jtable.css" rel="stylesheet" type="text/css" />
  <?php
  if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
  mensaje("Debe seleccionar una empresa.");
  ir_a("empresas.php");
  }
  ?>
</head>

<body class="no-skin">

    <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="#">Simulador Empresarial</a>
            </li>
            <li class="active">Categorias y Marcas</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

    <!-- Page Content -->
         <h1 class="page-header">Categorias y Marcas</h1>

	<div class="row">
    	<div class="col-md-6 col-xs-12">
        <div class="panel panel-info">
        	<div class="panel-heading">
        		<h3 class="panel-title">
        			<span class="glyphicon glyphicon-dash"></span> Categorias
        		</h3>
        	</div>
        	<div class="panel-body">
            <input type="button" onclick="$('#Tabla').jtable('showCreateForm');" style="float:right;font-weight:bold;" class="btn btn-sm btn-primary btn-outline" value="Crear nueva Categoria">
            <br/><br/>
						<div id="Tabla" style="width: 100%;"></div>
        	</div>
        </div>
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="panel panel-info">
        	<div class="panel-heading">
        		<h3 class="panel-title">
        			<span class="glyphicon glyphicon-dash"></span> Marcas
        		</h3>
        	</div>
        	<div class="panel-body">
            <input type="button" onclick="$('#Marcas').jtable('showCreateForm');" style="float:right;font-weight:bold;" class="btn btn-sm btn-primary btn-outline" value="Crear nueva Marca">
            <br/><br/>
						<div id="Marcas" style="width: 100%;"></div>
        	</div>
        </div>
      </div>
	</div>

    <!-- Final Page Content -->

    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

  <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
  <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function () {

		    //Prepare jTable
			$('#Tabla').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Categorias',
				paging: true,
				sorting: true,
				defaultSorting: 'descripcion ASC',
				actions: {
					listAction: 'CategoriasAct.php?action=list',
					createAction: 'CategoriasAct.php?action=create',
					updateAction: 'CategoriasAct.php?action=update',
					deleteAction: 'CategoriasAct.php?action=delete'
				},
				fields: {
					idcategoria: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					descripcion: {
						title: 'Descripcion',
						width: '60%',
						visibility: 'fixed'
					}
          // ,
					// cantproductos: {
					// 	title: 'Cantidad',
					// 	width: '40%',
					// 	create: false,
					// 	edit: false
					// }
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay categorias cargadas!',
					addNewRecord: 'Agregar Categoria',
					editRecord: 'Editar Categoria',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'La categoria será eliminada. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});

			$('#Marcas').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Marcas',
				paging: true,
				sorting: true,
				defaultSorting: 'descripcion ASC',
				actions: {
					listAction: 'MarcasAct.php?action=list',
					createAction: 'MarcasAct.php?action=create',
					updateAction: 'MarcasAct.php?action=update',
					deleteAction: 'MarcasAct.php?action=delete'
				},
				fields: {
					idmarca: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					descripcion: {
						title: 'Descripcion',
						width: '60%',
						visibility: 'fixed'
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay marcas cargadas!',
					addNewRecord: 'Agregar Marca',
					editRecord: 'Editar Marca',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'La categoria será eliminada. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
          pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});

			 $('#Tabla').jtable('load');
			 $('#Marcas').jtable('load');

	});
    </script>
</body>
</html>
