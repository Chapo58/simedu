﻿<?php
$token = $_GET['token'];
$idusuario = $_GET['idusuario'];
include_once("funciones.php");
 
$sql = consulta("SELECT * FROM tblreseteopass WHERE token = '$token'");
 
if(mysqli_num_rows($sql)>0){
   $res=mysqli_fetch_array($sql);
   if( sha1($res['idusuario']) == $idusuario ){
?>
<!DOCTYPE html>
<html lang="es">
 <head>
  <meta name="author" content="Luciano Ciattaglia">
  <title> Restablecer contraseña </title>
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
 </head>
 
 <body>
  <div class="container" role="main">
   <div class="col-md-4"></div>
   <div class="col-md-4">
    <form action="usuario_cambiarpassolvidado.php" method="post">
     <div class="panel panel-default">
      <div class="panel-heading"> Restaurar contraseña </div>
      <div class="panel-body">
       <p></p>
       <div class="form-group">
        <label for="password"> Nueva contraseña </label>
        <input type="password" class="form-control" name="password1" required>
       </div>
       <div class="form-group">
        <label for="password2"> Confirmar contraseña </label>
        <input type="password" class="form-control" name="password2" required>
       </div>
       <input type="hidden" name="token" value="<?php echo $token ?>">
       <input type="hidden" name="idusuario" value="<?php echo $idusuario ?>">
       <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Recuperar contraseña" >
       </div>
      </div>
     </div>
    </form>
   </div>
  <div class="col-md-4"></div>
  </div> <!-- /container -->
 
<script src="js/bootstrap.min.js"></script>
<script src="js/utilidades.js"></script>
<script src="js/jasny-bootstrap.min.js"></script>

 </body>
</html>
<?php
   }
   else{
     header('Location:index.php');
   }
 }
 else{
     header('Location:index.php');
 }
?>