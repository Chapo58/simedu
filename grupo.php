<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Grupo</title>
    <?php require_once('head.php'); ?>
	<style>
		.glyphicon-lg
		{
			font-size:4em
		}
		.info-block
		{
			border-right:5px solid #E6E6E6;
			margin-bottom:25px
		}
		.info-block .square-box
		{
			width:100px;
			max-height:110px;
			margin-right:22px;
			text-align:center!important;
			background-color:#676767;
			padding:25px 0
		}
		.info-block.block-info
		{
			border-color:#20819e
		}
		.info-block.block-info .square-box
		{
			background-color:#fff;
			color:#FFF
		}
	</style>
</head>
<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="inicio.php">Inicio</a>
            </li>
            <li><a href="perfil.php">Perfil</a></li>
            <li class="active">Grupo</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Otros alumnos
    <?php if($u['tipousuario'] == 2 || $u['tipousuario'] == 58) { ?>
      <a href="PDFs/organigrama.php" target="_blank" class="btn btn-lg btn-info" style="float:right;"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir Organigrama</a>
    <?php } ?>
  </h1>

	<div class="row">
        <div class="col-lg-12">
            <input type="search" class="form-control" id="input-search" placeholder="Buscar..." >
        </div>
	</div><br>
	<div class="row">
        <div class="searchable-container">
		<?php
			$con_uss=consulta("SELECT * FROM usuario WHERE idprofesor='$idprofe'");
			while ($us = mysqli_fetch_array($con_uss, MYSQLI_ASSOC)) {
				if($us['imagen']){ $imagenus = $us['imagen'];} else { $imagenus = "images/logo.png";}
				$mail = $us['email'];
				$id = $us['idusuario'];
				$nombre = $us['nombre']." ".$us['apellido'];
				$ubicacion = $us['ciudad'].", ".$us['provincia'];
				if($ubicacion == ", ") $ubicacion = "";
		?>
            <div class="items col-xs-12 col-sm-6 col-md-6 col-lg-6 clearfix">
               <div class="info-block block-info clearfix">
                    <div class="square-box pull-left">
                        <img src=<?php echo $imagenus; ?> class="img-circle" style="width:100px;heigth:100px;">
                    </div>
                    <?php if($u['tipousuario'] == 2 || $u['tipousuario'] == 58) { ?>
					          <form action="cambiarusuario.php" method="POST" class="pull-right">
                        <button type="submit" class="btn btn-warning" data-toggle="tooltip" title="Ingresar en la cuenta del Alumno">
							            <span class="fa fa-eye" aria-hidden="true"></span>
						            </button>
                        <input type="hidden" name="idusu" value="<?php echo $id; ?>" >
                    </form>
                    <?php } ?>
                    <form action="perfil_usuarios.php" method="POST" class="pull-right">
                        <button type="submit" class="btn btn-info" data-toggle="tooltip" title="Ver Perfil">
							            <span class="fa fa-user" aria-hidden="true"></span>
						            </button>
                        <input type="hidden" name="idusu" id="idusu" value="<?php echo $id; ?>" >
                    </form>
                    <form action="mensajes.php" method="POST" class="pull-right">
                        <button type="submit" class="btn btn-primary" data-toggle="tooltip" title="Enviar Mensaje">
							            <span class="fa fa-envelope" aria-hidden="true"></span>
						            </button>
                        <input type="hidden" name="idusu" value="<?php echo $id; ?>" >
                    </form>
                    <h4><?php echo $nombre; ?></h4>
                    <p><?php echo $ubicacion; ?></p>
                    <span><?php echo $mail; ?></span>
                </div>
            </div>
		<?php } ?>
        </div>
	</div>
  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

<script type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$('#input-search').on('keyup', function() {
			  var rex = new RegExp($(this).val(), 'i');
				$('.searchable-container .items').hide();
				$('.searchable-container .items').filter(function() {
					return rex.test($(this).text());
				}).show();
			});
		});
	});
</script>
  <!-- ============================================================= -->
</body>
</html>
