<?php
session_start();
include "funciones.php";
//mysqli_set_charset($conex,'utf8');
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM periodos WHERE idusuario = $idusuario;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM periodos WHERE idusuario = $idusuario ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$periodo = $_POST["periodo"];
		$descripcion = $_POST["descripcion"];
		$desde = date("Y-m-d", strtotime($_POST["desde"]));
		$hasta = date("Y-m-d", strtotime($_POST["hasta"]));
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO periodos(idusuario, periodo, descripcion, desde, hasta) VALUES('$idusuario','$periodo','$descripcion','$desde','$hasta');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM periodos WHERE idperiodo = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		actualizarhistorial("Se creo un nuevo Periodo");
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idperiodo"];
		$periodo = $_POST["periodo"];
		$descripcion = $_POST["descripcion"];
		$desde = date("Y-m-d", strtotime($_POST["desde"]));
		$hasta = date("Y-m-d", strtotime($_POST["hasta"]));
		//Update record in database
		$result = mysqli_query($con,"UPDATE periodos SET periodo = '$periodo', descripcion = '$descripcion', desde = '$desde', hasta = '$hasta' WHERE idperiodo = $id;");
		
		acthistempresa($rsocialemp, "Se modifico un periodo");
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idperiodo"];
		$con_p=consulta("SELECT * FROM listaasientos WHERE idperiodo='$id'");
		$p=mysqli_fetch_array($con_p);
		if(mysqli_num_rows($con_p)==0){
			
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM periodos WHERE idperiodo = $id;");
		consulta("DELETE FROM listaasientos WHERE idperiodo = $id;");
		
		acthistempresa($rsocialemp, "Se elimino un periodo");
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
		
	} else {
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = "No puede eliminar un periodo fiscal con asientos cargados.";
		print json_encode($jTableResult);
	}
	
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>