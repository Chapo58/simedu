<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Inicio</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">
	<?php require_once('header.php'); ?>
  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="inicio.php">Inicio</a>
          </li>
          <li class="active">¿Por donde comienzo?</li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">¿Que debo hacer?</h1>

<p><strong>Introducción:</strong> La idea es crear <a href="empresas.php">empresas</a> para luego trabajar con ellas. Recordá que podes crear varias, pero antes de comenzar a trabajar con empresas tienes que crear un <a href="periodos.php">Periodo Fiscal</a> que se encuentra en el módulo contable.</p>
<p>Este es el periodo de tiempo donde vas poder imputar tus movimientos está compuesto por :</p>
<p>1) Un año es numérico de cuatro dígitos, ej 2016.</p>
<p>2) Una denominación que aparecerá en los listados ej Periodo Fiscal 2016 o Ejercicio Contable 2016.</p>
<p>3) Una fecha desde y una fecha Hasta, el rango es de 1 año. Todos tus movimientos estarán comprendidos entre estas fechas.</p>

<p>Una vez creado el periodo fiscal ó ejercicio, desde mis empresas botón Crear nueva empresa podes definir los datos básicos de la empresa.
Existen dos datos que tienes que prestar atención, 1) Plan de cuenta , 2) Periodo, el primero tiene la opción de elegir un plan de cuentas modelo el cual propone el docente a todos sus alumnos llamado Plan de Cuentas Default, y el segundo tienes que seleccionar el periodo fiscal que definiste previamente.
Luego presionar el botón Aceptar, que te informara que la empresa fue creada con éxito.
Luego tienes el botón "<span class="fa fa-hand-o-right" aria-hidden="true"></span>" que selecciona la empresa creada.</p>
<p>En el menu superior podras ver la empresa activa y periodo fiscal correspondiente.
Luego puedes trabajar con tu empresa, para ello tienes un modelo de plan de cuentas que tendrás que adaptar a tus necesidades y luego ya puedes cargar asientos ó comprobantes, emitir informes y todo lo relacionado con esa empresa.</p>


  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

  <script type="text/javascript">
  	$(document).ready(function() {

  	});
  </script>
  <!-- ============================================================= -->
</body>
</html>
