<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Correccion Planteo</title>
		<?php require_once('head.php'); ?>
		<?php
			if(isset($_POST['idplanteo']) && !empty($_POST['idplanteo'])) {
				$idplanteo = $_SESSION['idplanteo'] = $_POST['idplanteo'];
				$con_p=consulta("SELECT * FROM planteos WHERE idplanteo='$idplanteo'");
				$p=mysqli_fetch_array($con_p);
				$titulo = $p['titulo'];
			} else {
				mensaje("Debe seleccionar un planteo");
				ir_a('planteos.php');
			}
		?>
		<style>
			table.dataTable tbody td {
			  vertical-align: middle;
			}
		</style>
	</head>

	<body class="no-skin">

		<?php require_once('header.php'); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
	            <li>
	              <i class="ace-icon fa fa-cubes home-icon"></i>
	              <a href="planteo.php">Planteos</a>
	            </li>
	            <li><a href="planteos_docentes.php">Definicion de Planteos</a></li>
	            <li class="active"><?php echo $titulo; ?></li>
	          </ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
				<div class="page-content">

					<table id="tabla" style="font-size:110%;" class="table table-striped table-bordered" cellspacing="0">
							<thead>
								<tr>
									<th></th>
									<th>Alumno</th>
									<th>Respondidas</th>
									<th>Nota</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							<?php
								$con_alumnos=consulta("SELECT usuario.*, IFNULL(respondidas.cantidad, 0) as 'respondidas'
								FROM usuario LEFT JOIN (SELECT idalumno, count(*) as 'cantidad' FROM respuestas GROUP BY idalumno) respondidas ON usuario.idusuario = respondidas.idalumno
								WHERE usuario.idprofesor='$idusuario'");
								while ($a = mysqli_fetch_array($con_alumnos, MYSQLI_ASSOC)) {
									$idalumno = $a['idusuario'];
									if($a['nombre']){
										$usuario = $a['nombre'] . " " . $a['apellido'];
									} else {
										$usuario = $a['email'];
									}
									$imagen = $a['imagen'];
									$nota = 0;
							?>
								<tr>
									<td class="text-center"><img src=<?php echo $imagen; ?> class="img-circle img-empresa-min"></td>
									<td><?php echo $usuario; ?></td>
									<td align="center"><strong><?php echo $a['respondidas']; ?></strong></td>
									<td align="center"><strong><?php echo $nota; ?></strong></td>
									<td class="text-center">
										<form action="planteos_correccion_alumno.php" method="POST">
					            <button type="submit" class="btn btn-warning" title="Corregir">
												<span class="fa fa-check"></span>&nbsp;<strong>Corregir</strong>
											</button>
											<input type="hidden" name="idalumno" value="<?php echo $idalumno; ?>" >
											<input type="hidden" name="idplanteo" value="<?php echo $idplanteo; ?>" >
					          </form>
									</td>
								</tr>
							<?php } ?>
							</tbody>
					</table>


					<!-- Final Page Content -->
				</div>
			</div>
		</div><!-- /.main-content -->

		<?php require_once('footer.php'); ?>
		<script>
		$('#tabla').DataTable({
			"order": [[ 1, "asc" ]],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'No existen registros.',
				"sInfo":           "",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
		</script>
	</body>
</html>
