<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Alicuotas</title>
    <?php require_once('head.php'); ?>
	  <link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	  <link href="jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="#">Simulador Empresarial</a>
            </li>
            <li class="active">Tasas de IVA</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		}
	?>

    <!-- Page Content -->
    <div id="divListas">
         <h1 class="page-header">Tasas de IVA</h1>
		 <div class="bs-callout bs-callout-info">
            <h4>Tasas de IVA</h4>
            <p>Cuando carges o modifiques tus productos, podras seleccionar cualquiera de las tasas aqui detalladas.</p>
        </div>
		 <input type="button" id="btnNueva" onclick="$('#Tabla').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Agregar Tasa">

        <div class="space20"></div>

        <div class="row">
            <div class="col-xs-12">
				<div id="Tabla" style="width: 100%;"></div>
            </div>
        </div>
    </div>

    <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

    <?php require_once('footer.php'); ?>
    <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>

    <script type="text/javascript">
	$(document).ready(function () {

	/*	function prueba(dateObject) {
			var d = new Date(dateObject);
			var day = d.getDate();
			var month = d.getMonth() + 1;
			var year = d.getFullYear();
			if (day < 10) {
				day = "0" + day;
			}
			if (month < 10) {
				month = "0" + month;
			}
			var date = day + "/" + month + "/" + year;

			return date;
		};
*/
		    //Prepare jTable
			$('#Tabla').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Tasas de IVA',
				paging: true,
				sorting: true,
				defaultSorting: 'ntasa ASC',
				actions: {
					listAction: 'ListaIvaAct.php?action=list',
					createAction: 'ListaIvaAct.php?action=create',
					updateAction: 'ListaIvaAct.php?action=update',
					deleteAction: 'ListaIvaAct.php?action=delete'
				},
				fields: {
					idiva: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					ntasa: {
						title: 'N° de Tasa',
						width: '20%',
						visibility: 'fixed'
					},
					denominacion: {
						title: 'Denominacion',
						width: '30%'
					},
					vigencia: {
						title: 'Vigencia',
						width: '15%',
						type: 'date',
						displayFormat: 'dd/mm/yy'
					/*	input: function (data) {
							if (data.record) {
								// Dar formato, trabajo
								return '<input class="hasDatepicker" type="text" data-mask="99/99/9999" name="vigencia" id="vigencia" value="' + prueba(data.record.vigencia) + '" />';
							} else {
								return '<input class="hasDatepicker" type="text" data-mask="99/99/9999" name="vigencia" id="vigencia" />';
							}
						}*/
					},
					porc: {
						title: 'Porcentaje',
						width: '15%'
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay listas cargadas!',
					addNewRecord: 'Agregar Lista',
					editRecord: 'Editar Lista',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'La lista será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});

			 $('#Tabla').jtable('load');

			 $.datepicker.regional['es'] = {
			 closeText: 'Cerrar',
			 prevText: '<Ant',
			 nextText: 'Sig>',
			 currentText: 'Hoy',
			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			 weekHeader: 'Sm',
			 dateFormat: 'dd/mm/yy',
			 firstDay: 1,
			 isRTL: false,
			 showMonthAfterYear: false,
			 yearSuffix: ''
			 };
			 $.datepicker.setDefaults($.datepicker.regional['es']);

	});
    </script>
</body>
</html>
