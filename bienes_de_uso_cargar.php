<?php
	include "funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['numero']) && !empty($_POST['numero']) &&
		isset($_POST['detalle']) && !empty($_POST['detalle'])) {
		
		// Quito espacios en blanco
		$numero = trim($_POST['numero']);
		$detalle = trim($_POST['detalle']);
	@	$idcliente = $_POST['idcliente'];
	@	$idactividad = $_POST['idactividad'];
	@	$vida_util = trim($_POST['vida_util']);
	@	$fecha_compra = $_POST['fecha_compra'];
	@	$coeficiente = $_POST['coeficiente'];
	@	$costo = $_POST['costo'];
	@	$comentario = $_POST['comentario'];
	@	$ubicacion = $_POST['ubicacion'];
	@	$tipo_bien = $_POST['tipo_bien'];
	@	$mueble = $_POST['mueble'];
	@	$inmueble = $_POST['inmueble'];
	@	$inmueble_valor = $_POST['inmueble_valor'];
	@	$cuenta_inmobiliaria = $_POST['cuenta_inmobiliaria'];
	@	$bien_de_reemplazo = $_POST['bien_de_reemplazo'];
	@	$utilidad = $_POST['utilidad'];
	@	$valor_cnas = $_POST['valor_cnas'];
	@	$idgrupo = $_POST['idgrupo'];
	@	$idrubro = $_POST['idrubro'];
		
		// Paso a mayusculas
		$detalle = ucfirst($detalle);
			
		$cargarbien = "INSERT INTO bienes_de_uso(idempresa,numero,detalle,idcliente,idactividad,vida_util,fecha_compra,coeficiente,costo,comentario,ubicacion,tipo_bien,mueble,inmueble,inmueble_valor,cuenta_inmobiliaria,bien_de_reemplazo,utilidad,valor_cnas,idgrupo,idrubro)
								VALUES (
								'$idempresa',
								'$numero',
								'$detalle',
								'$idcliente',
								'$idactividad',
								'$vida_util',
								'$fecha_compra',
								'$coeficiente',
								'$costo',
								'$comentario',
								'$ubicacion',
								'$tipo_bien',
								'$mueble',
								'$inmueble',
								'$inmueble_valor',
								'$cuenta_inmobiliaria',
								'$bien_de_reemplazo',
								'$utilidad',
								'$valor_cnas',
								'$idgrupo',
								'$idrubro')";
									
		$cargar = consulta($cargarbien);
		
		if(!$cargar){
				echo "Mensaje: ".mysqli_error();
				mensaje("Error");
		}
		mensaje("El bien de uso se cargo con exito.");
		acthistempresa($rsocial, "Se agrego un bien de uso nuevo");
		ir_a("bienes_de_uso.php");

	} else {
		mensaje("No se cargaron todos los datos");
		ir_a("bienes_de_uso.php");
	}
?>
