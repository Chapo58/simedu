<?php
	
	session_start();
	include "funciones.php";
	
	if($_POST['idemp'] == false) {
		mensaje("Debe seleccionar una empresa.");
		ir_a("institucional.php");
	} else {
		$idempresa = trim($_POST['idemp']);
		$con_emp=consulta("select * from empresas where idempresa='$idempresa'");
		$u=mysqli_fetch_array($con_emp);
		$rsocial = $u['rsocial'];
		
		
		if (isset($_FILES["imagen"]["name"]) && !empty($_FILES["imagen"]["name"]) ||
			isset($_POST['txtSitioOficial']) && !empty($_POST['txtSitioOficial'])) {
			
				// Quito espacios en blanco
				$web = trim($_POST['txtSitioOficial']);
				
				$editar = "UPDATE empresas
									SET
									web = '$web'";
				
				if(!empty($_FILES["imagen"]["name"])){
					if ((($_FILES["imagen"]["type"] == "image/gif")
						|| ($_FILES["imagen"]["type"] == "image/jpeg")
						|| ($_FILES["imagen"]["type"] == "image/jpg")
						|| ($_FILES["imagen"]["type"] == "image/png"))) {
						if ($_FILES["imagen"]["error"] > 0) {
							echo "Return Code: " . $_FILES["imagen"]["error"] . "<br>";
						} else {
							$extension = substr($_FILES["imagen"]["type"], 6);
							$rutaimagen = "images/imagenesempresas/" . $idempresa . '.' . $extension;
							if (file_exists($rutaimagen)) {
								unlink($rutaimagen);
								move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
								$rutaimagenemp = $rutaimagen;
								$editar .= ", imagenempresa = '$rutaimagen'";
							} else {
								// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
								move_uploaded_file($_FILES["imagen"]["tmp_name"], $rutaimagen);
								$rutaimagenemp = $rutaimagen;
								$editar .= ", imagenempresa = '$rutaimagen'";
							}
						}
					} else {
						echo "Archivo Inválido";
					}
				}
				
				$editar .= "WHERE idempresa = '$idempresa';";
				
				acthistempresa($rsocial, "Se modifico la informacion institucional(General)");

		} elseif ((isset($_POST['txtNombre']) && !empty($_POST['txtNombre']) ||
				   isset($_POST['txtTelefono']) && !empty($_POST['txtTelefono']) ||
				   isset($_POST['txtEmail']) && !empty($_POST['txtEmail']))) {
			
			// Quito espacios en blanco y paso a mayusculas
			$cnombre = trim($_POST['txtNombre']);
			$ctelefono = trim($_POST['txtTelefono']);
			$cemail = trim($_POST['txtEmail']);
			
			$cnombre =  ucfirst($cnombre);
			$editar = "UPDATE empresas
									SET
									nombrecontacto = '$cnombre',
									telefonocontacto = '$ctelefono',
									emailcontacto = '$cemail'
									WHERE idempresa = '$idempresa'";
			
			acthistempresa($rsocial, "Se modifico la informacion institucional(Contacto)");
			
		} elseif (isset($_POST['txtTitulo']) || isset($_POST['txtDescripcion'])) {
			if (!empty($_POST['txtTitulo']) && !empty($_POST['txtDescripcion'])) {
				if($_POST['idficha'] == false) {
					// Quito espacios en blanco y paso a mayusculas
					$titulo = trim($_POST['txtTitulo']);
					$descripcion = trim($_POST['txtDescripcion']);
					$titulo = ucfirst($titulo);
					
					$rutaimagenficha = "";
					
					if(!empty($_FILES["imagenficha"]["name"])){
						if ((($_FILES["imagenficha"]["type"] == "image/gif")
							|| ($_FILES["imagenficha"]["type"] == "image/jpeg")
							|| ($_FILES["imagenficha"]["type"] == "image/jpg")
							|| ($_FILES["imagenficha"]["type"] == "image/png"))) {
							if ($_FILES["imagenficha"]["error"] > 0) {
								echo "Return Code: " . $_FILES["imagenficha"]["error"] . "<br>";
							} else {
								$extension = substr($_FILES["imagenficha"]["type"], 6);
								$rutaimagen = "images/imagenesfichas/" . $idempresa . rand() . '.' . $extension;
								if (file_exists($rutaimagen)) {
									unlink($rutaimagen);
									move_uploaded_file($_FILES["imagenficha"]["tmp_name"], $rutaimagen);
									$rutaimagenficha = $rutaimagen;
								} else {
									// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
									move_uploaded_file($_FILES["imagenficha"]["tmp_name"], $rutaimagen);
									$rutaimagenficha = $rutaimagen;
								}
							}
						} else {
							echo "Archivo Inválido";
						}
					}
					$editar = "INSERT INTO fichas (idempresa,imagen,titulo,texto)
											VALUES (
											'$idempresa',
											'$rutaimagenficha',
											'$titulo',
											'$descripcion')";
											
					acthistempresa($rsocial, "Se creo una nueva ficha");
				} else {
					$idficha = $_POST['idficha'];
					// Quito espacios en blanco y paso a mayusculas
					$titulo = trim($_POST['txtTitulo']);
					$descripcion = trim($_POST['txtDescripcion']);
					$titulo = ucfirst($titulo);
					
					$editar = "UPDATE fichas
									SET
									titulo = '$titulo',
									texto = '$descripcion'";

					if(!empty($_FILES["imagenficha"]["name"])){
						if ((($_FILES["imagenficha"]["type"] == "image/gif")
							|| ($_FILES["imagenficha"]["type"] == "image/jpeg")
							|| ($_FILES["imagenficha"]["type"] == "image/jpg")
							|| ($_FILES["imagenficha"]["type"] == "image/png"))) {
							if ($_FILES["imagenficha"]["error"] > 0) {
								echo "Return Code: " . $_FILES["imagenficha"]["error"] . "<br>";
							} else {
								$extension = substr($_FILES["imagenficha"]["type"], 6);
								$rutaimagen = "images/imagenesfichas/" . $idempresa . rand() . '.' . $extension;
								if (file_exists($rutaimagen)) {
									unlink($rutaimagen);
									move_uploaded_file($_FILES["imagenficha"]["tmp_name"], $rutaimagen);
									$editar .= ", imagen = '$rutaimagen'";
								} else {
									// Muevo la imagen desde su ubicacion temporal a la carpeta de imagenes
									move_uploaded_file($_FILES["imagenficha"]["tmp_name"], $rutaimagen);
									$editar .= ", imagen = '$rutaimagen'";
								}
							}
						} else {
							echo "Archivo Inválido";
						}
					}
					$editar .= "WHERE idficha = '$idficha';";
									
					acthistempresa($rsocial, "Se modifico la informacion institucional(Fichas)");
				}
			} else {
				mensaje("La ficha debe tener un titulo y una descripcion ". $_POST['txtTitulo']. $_POST['txtDescripcion']);
				ir_a("institucional.php");
			}
		} elseif (isset($_POST['delficha']) && !empty($_POST['delficha'])) {
			$idficha = $_POST['delficha'];
			
			$editar = "DELETE FROM fichas WHERE fichas.idficha = $idficha LIMIT 1";
			
			acthistempresa($rsocial, "Se elimino una ficha");
		}
		
		$actualizardatos=consulta($editar);
		if(!$actualizardatos){
			echo "Mensaje: ".mysqli_error();
			mensaje("Error");
		} else {
			echo "Los datos institucionales de $rsocial se actualizaron con exito.";
		}
	}
?>