<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | IVA Compra</title>
  	<?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="#">Simulador Empresarial</a>
            </li>
            <li class="active">Libro de IVA Compra</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Libro de IVA Compra</h1>
  <div class="space50"></div>
  <form role="form" data-toggle="validator" target="_blank" action="PDFs/c_LibroIvaCompra.php" method="POST">
	<div class="col-md-6 col-md-offset-3">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Mes/Año</strong></div>
                <input type="text" data-mask="99/9999" id="fecha" name="fecha" class="form-control" value="<?php echo date("m/Y"); ?>" placeholder="Mes/Año" maxlength="7" required>
          </div>
   		</div>
	</div>
	<div class="col-md-6 col-md-offset-3">
        <div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-flag"></span><strong>&nbsp;&nbsp;N° de Hoja</strong></div>
                    <input type="number" id="inicio" name="inicio" class="form-control" placeholder="N° de Hoja" maxlength="10">
            </div>
		</div>
	</div>
	<div class="col-md-12">
		<button type="submit" id="btnAceptar" style="width:100%;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Listar</button>
	</div>
  </form>


  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

  <!-- ============================================================= -->
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
</body>
</html>
