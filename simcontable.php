<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Simulador Contable</title>
  	<?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Simulador Contable</a>
            </li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Simulador Contable</h1>

  <div class="row">
  <div class="col-md-12">
  <a href="empresas.php">
  <div class="bs-callout bs-callout-info">
    <h4>Empresas<i style="font-size: 15px;float:right;" class="fa fa-suitcase"></i></h4>
    <p>Se necesita tener al menos una empresa creada para poder utilizar este modulo.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="cuentas.php">
  <div class="bs-callout bs-callout-default">
    <h4>Plan de Cuentas</h4>
    <p>Plan de Cuentas.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="periodos.php">
  <div class="bs-callout bs-callout-default">
    <h4>Periodos</h4>
    <p>Periodos por usuario.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="asientos_lista.php">
  <div class="bs-callout bs-callout-default">
    <h4>Asientos</h4>
    <p>Carga de asientos.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="librodiario.php">
  <div class="bs-callout bs-callout-default">
    <h4>Libro Diario</h4>
    <p>Libro Diario.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="libromayor.php">
  <div class="bs-callout bs-callout-default">
    <h4>Libro Mayor</h4>
    <p>Libro Mayor.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="balancesumasysaldos.php">
  <div class="bs-callout bs-callout-default">
    <h4>Balance Sumas y Saldos</h4>
    <p>Balance Sumas y Saldos.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="balancegeneral.php">
  <div class="bs-callout bs-callout-default">
    <h4>Balance General</h4>
    <p>Balance General.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="flujodefondos.php">
  <div class="bs-callout bs-callout-default">
    <h4>Flujo de Fondos</h4>
    <p>Flujo de Fondos.</p>
  </div>
  </a>
  </div>

  </div>


  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
  <!-- ============================================================= -->
</body>
</html>
