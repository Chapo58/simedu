<?php

  session_start();
  require_once('funciones.php');

  $idempresa = $_SESSION['idempresa'];
  $idusuario = $_SESSION['idusuario'];
  $año = $_SESSION['año'];

  $con_resumen=consulta("SELECT cuentas.rubro, SUM(IFNULL(asientos.debe,0)) AS debe, SUM(IFNULL(asientos.haber,0)) as haber
  FROM cuentas
  LEFT JOIN (SELECT asientos.*, listaasientos.fecha FROM asientos LEFT JOIN listaasientos ON asientos.idlista = listaasientos.idlista WHERE YEAR(listaasientos.fecha) = $año)
  AS asientos ON cuentas.idcuenta = asientos.idcuenta WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 1 OR cuentas.rubro = 2
  GROUP BY cuentas.rubro ORDER BY cuentas.rubro ASC");
  $arr = array();
  $sumatotal = 0;
  while ($r = mysqli_fetch_array($con_resumen, MYSQLI_ASSOC)) {
    $calcular = abs($r['debe'] - $r['haber']);
    array_push($arr, $calcular);
    $sumatotal += $calcular;
  };

 /* CAT:Pie charts */

 /* pChart library inclusions */
 include("pchart/class/pData.class.php");
 include("pchart/class/pDraw.class.php");
 include("pchart/class/pPie.class.php");
 include("pchart/class/pImage.class.php");

 /* Create and populate the pData object */
 $MyData = new pData();
 if(isset($arr[5])){
    $MyData->addPoints(array($arr[0],$arr[1]),"ScoreA");
 } else {
    $MyData->addPoints(array($arr[0],$arr[1]),"ScoreA");
 }

 $MyData->setSerieDescription("ScoreA","Application A");

 $activo = number_format(($arr[0]*100)/$sumatotal, 2, ',', '.') . "%";
 $pasivo = number_format(($arr[1]*100)/$sumatotal, 2, ',', '.') . "%";
 /*$pneto = number_format(($arr[2]*100)/$sumatotal, 2, ',', '.') . "%";
 $iyg = number_format(($arr[3]*100)/$sumatotal, 2, ',', '.') . "%";
 $gyp = number_format(($arr[4]*100)/$sumatotal, 2, ',', '.') . "%";*/
// $costo = "Costo ".number_format(($arr[5]*100)/$sumatotal, 2, ',', '.') . "%";

if($activo == 0.00){
  $activo = "Activo";
} else {
  $activo = "Activo ".$activo;
}
if($pasivo == 0.00){
  $pasivo = "Pasivo";
} else {
  $pasivo = "Pasivo ".$pasivo;
}
/*if($pneto == 0.00){
  $pneto = "Patrimonio Neto";
} else {
  $pneto = "Patrimonio Neto ".$pneto;
}
if($iyg == 0.00){
  $iyg = "Ingresos y Ganancias";
} else {
  $iyg = "Ingresos y Ganancias ".$iyg;
}
if($gyp == 0.00){
  $gyp = "Gastos y Perdidas";
} else {
  $gyp = "Gastos y Perdidas ".$gyp;
}*/

 /* Define the absissa serie */
 $MyData->addPoints(array($activo,$pasivo),"Labels");
 $MyData->setAbscissa("Labels");

 /* Create the pChart object */
 $myPicture = new pImage(500,280,$MyData,TRUE); // Ancho y Alto

 /* Set the default font properties */
 $myPicture->setFontProperties(array("FontName"=>"pchart/fonts/Forgotte.ttf","FontSize"=>30,"R"=>80,"G"=>80,"B"=>80));

 /* Create the pPie object */
 $PieChart = new pPie($myPicture,$MyData);

 /* Enable shadow computing */
 $myPicture->setShadow(TRUE,array("X"=>3,"Y"=>3,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

 /* Draw a splitted pie chart */
 $PieChart->draw3DPie(120,90,array("Radius"=>100,"DataGapAngle"=>12,"DataGapRadius"=>10,"Border"=>TRUE));

 /* Write the legend box */
 $myPicture->setFontProperties(array("FontName"=>"pchart/fonts/verdana.ttf","FontSize"=>13,"R"=>0,"G"=>0,"B"=>0));
 $PieChart->drawPieLegend(0,160,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput("pchart/examples/pictures/example.draw3DPie.transparent.png");
?>
