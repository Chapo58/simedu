<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Simulador Empresarial</title>
  	<?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="#">Simulador Empresarial</a>
            </li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Simulador Empresarial</h1>

  <div class="row">
  <div class="col-md-12">
  <a href="empresas.php">
  <div class="bs-callout bs-callout-info">
    <h4>Empresas<i style="font-size: 15px;float:right;" class="fa fa-suitcase"></i></h4>
    <p>Se necesita tener al menos una empresa creada para poder utilizar este modulo.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="productos.php">
  <div class="bs-callout bs-callout-default">
    <h4>Productos<i style="font-size: 15px;float:right;" class="fa fa-futbol-o "></i></h4>
    <p>Productos.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="categoriasymarcas.php">
  <div class="bs-callout bs-callout-default">
    <h4>Categorias y Marcas<i style="font-size: 15px;float:right;" class="fa fa-tag"></i></h4>
    <p>Categorias y Marcas.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="proveedores.php">
  <div class="bs-callout bs-callout-default">
    <h4>Proveedores<i style="font-size: 15px;float:right;" class="fa fa-truck"></i></h4>
    <p>Proveedores.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="clientes.php">
  <div class="bs-callout bs-callout-default">
    <h4>Clientes<i style="font-size: 15px;float:right;" class="fa fa-users"></i></h4>
    <p>Clientes.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="listaprecios.php">
  <div class="bs-callout bs-callout-default">
    <h4>Listas de Precios<i style="font-size: 15px;float:right;" class="fa fa-tags"></i></h4>
    <p>Listas de Precios.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="listaivas.php">
  <div class="bs-callout bs-callout-default">
    <h4>Tasas de IVA<i style="font-size: 15px;float:right;" class="fa fa-tags"></i></h4>
    <p>Tasas de IVA.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="mercado.php">
  <div class="bs-callout bs-callout-default">
    <h4>Mercado<i style="font-size: 15px;float:right;" class="fa fa-credit-card"></i></h4>
    <p>Mercado.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="carrito.php">
  <div class="bs-callout bs-callout-default">
    <h4>Carrito de Compras<i style="font-size: 15px;float:right;" class="fa fa-shopping-cart"></i></h4>
    <p>Carrito de Compras.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="comprobantes.php">
  <div class="bs-callout bs-callout-default">
    <h4>Comprobantes<i style="font-size: 15px;float:right;" class="fa fa-file-pdf-o"></i></h4>
    <p>Comprobantes.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="ivacompra.php">
  <div class="bs-callout bs-callout-default">
    <h4>IVA Compra<i style="font-size: 15px;float:right;" class="fa fa-file-text"></i></h4>
    <p>Libro de IVA Compra.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="ivaventa.php">
  <div class="bs-callout bs-callout-default">
    <h4>IVA Venta<i style="font-size: 15px;float:right;" class="fa fa-file-text"></i></h4>
    <p>Libro de IVA Venta.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="mayorstock.php">
  <div class="bs-callout bs-callout-default">
    <h4>Mayor de Stock<i style="font-size: 15px;float:right;" class="fa fa-bar-chart"></i></h4>
    <p>Libro Mayor de Stock.</p>
  </div>
  </a>
  </div>
  <div class="col-md-6">
  <a href="minimostock.php">
  <div class="bs-callout bs-callout-default">
    <h4>Libro de Stock Minimo<i style="font-size: 15px;float:right;" class="fa fa-bar-chart"></i></h4>
    <p>Libro de Stock Minimo.</p>
  </div>
  </a>
  </div>

  </div>


  <!-- Final Page Content -->
  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
  <!-- ============================================================= -->
</body>
</html>
