<table style="margin-left:18px;font-size: 80%;">
  <tr>
<td><img src="../images/logo.png" width="191" height="98" title="Logo de la Empresa" alt="Logo de la Empresa"></td>
<td style="width: 23%;font-size: 400%;font-weight: bold;" align="right"><strong>X</strong></td>
<td style="width: 45%;font-size: 130%;" align="right"><?php echo "RECIBO Nº ".str_pad($pventa, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura, 8, "0", STR_PAD_LEFT); ?></td>
  </tr>
</table>
<table style="margin-left:18px;font-size: 80%;">
<tr>
<td style="width: 60%;"><?php echo $v['rsocial']; ?></td>
<td style="width: 35%;" colspan="2"></td>
</tr>
<tr>
<td style="width: 60%;"><?php echo $v['domicilio']; ?></td>
<td style="width: 15%;font-weight: bold;">Emision:</td>
<td style="width: 20%;"><?php echo $_POST['fecha']; ?></td>
</tr>
<tr>
<td style="width: 60%;"><?php echo $v['localidad']; ?></td>
<td style="width: 15%;font-weight: bold;">CUIT:</td>
<td style="width: 20%;"><?php echo $v['cuit']; ?></td>
</tr>
<tr>
<td style="width: 60%;"><?php echo $v['provincia']; if($v['pais'] != ""){ echo ", ".$v['pais']; } ?></td>
<td style="width: 15%;font-weight: bold;">II.BB.:</td>
<td style="width: 20%;"><?php echo $v['ingbrutos']; ?></td>
</tr>
<tr>
<td style="width: 60%;"><?php echo $v['telefono']; ?></td>
<td style="width: 15%;font-weight: bold;">Inicio Actividades:</td>
<td style="width: 20%;"><?php echo date("d/m/Y",strtotime($v['inicioact'])); ?></td>
</tr>
<tr>
<td><?php echo $v['emailcontacto']; ?></td>
<td style="width: 15%;font-weight: bold;">I.V.A.:</td>
<td style="width: 20%;"><?php echo $v['iva_descr']; ?></td>
</tr>
</table>
<div style="margin-left:18px;margin-top:-5px;">
<hr style="margin-top:0;width:100%;" />
</div>
