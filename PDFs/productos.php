<?php

ini_set('memory_limit', '128M');

// define('_MPDF_URI','mpdf/');

include("mpdf/mpdf.php");

session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);

ob_start();  // start output buffering
include('productos_html.php');
$content = ob_get_clean(); // get content of the buffer and clean the buffer

ob_start();  // start output buffering
include('header.php');
$header = ob_get_clean(); // get content of the buffer and clean the buffer

$footer = '<div align="right" style="color:gray">< Pagina {PAGENO} ></div>';

$mpdf=new mPDF('c','A4-L','','',12,12,35,15,7,7);

// $mpdf->progbar_altHTML = '<html><body>
// 	<div style="margin-top: 5em; text-align: center; font-family: Verdana; font-size: 15px;">
//     <img style="vertical-align: middle" src="mpdf/examples/loading.gif" />
//     Creando libro diario. Por favor espere...
//   </div>';
// $mpdf->StartProgressBarOutput();

$mpdf->SetDisplayMode('fullpage');

$mpdf->debug = true;
$mpdf->useSubstitutions = false;
$mpdf->cacheTables = true;
$mpdf->simpleTables= true;
$mpdf->packTableData=true;

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdf/css/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
$mpdf->WriteHTML($content);

$mpdf->Output('listado_de_articulos.pdf','I');

exit;


?>
