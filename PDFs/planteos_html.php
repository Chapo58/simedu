	<h2 align="center"><?php echo $p['titulo']; ?></h2>
	<br>
	<table align="center" style="width: 100%;">
			<tr>
				<th bgcolor="#C9C9C9" height="20" style="width: 50%;">Reloj: <?php echo $limite; ?></th>
				<th bgcolor="<?php echo $colorfondo; ?>" height="20" style="width: 50%;">Semaforo: <?php echo $semaforo; ?></th>
			</tr>
    </table>
	<br /><br />
    <table border="1" align="center" style="width: 100%;">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th align="center" colspan="2" height="20">PREGUNTAS</th>
            </tr>
        </thead>
        <tbody>
			<?php	
				$indice = 0;
				$con_preguntas=consulta("SELECT * FROM preguntas WHERE idplanteo='$idplanteo' ORDER BY idpregunta ASC");
				while ($pr = mysqli_fetch_array($con_preguntas, MYSQLI_ASSOC)) {
					$descripcion = $pr['descripcion'];
					$ruta = $pr['ruta'];
					$link = $pr['link'];
					$multiple = $pr['multiple'];
					$opcion1 = $pr['opcion1'];
					$opcion2 = $pr['opcion2'];
					$opcion3 = $pr['opcion3'];
					$opcion4 = $pr['opcion4'];
					$opcion5 = $pr['opcion5'];
					$opcion6 = $pr['opcion6'];
					$indice++;
			?>
            <tr>
				<th align="center" style="width: 5%;color:red;"><?php echo $indice; ?></th>
				<td style="width: 95%;"><?php echo $descripcion; ?></td>
            </tr>
			<?php if($link){ ?>
			<tr>
				<td></td>
				<td>Link: <a href="<?php echo $link; ?>" target="_blank"><?php echo $link; ?></a></td>
            </tr>
			<?php } if($ruta){ ?>
			<tr>
				<td></td>
				<td>
					<?php if(esimagen($ruta)){ ?>
						<a href="<?php echo "../".$ruta; ?>" target="_blank"><img src="<?php echo "../".$ruta; ?>" width="200" height="200"></a>
					<?php } else { ?>
					<a href="<?php echo "../".$ruta; ?>" target="_blank">Archivo adjunto</a>
					<?php } ?>
				</td>
            </tr>
			<?php } if($multiple){ ?>
				<?php if($opcion1){ ?>
					<tr>
						<td align="center" style="width: 5%;">A</td>
						<td style="width: 95%;"><?php echo $opcion1; ?></td>
					</tr>
				<?php } ?>
				<?php if($opcion2){ ?>
					<tr>
						<td align="center" style="width: 5%;">B</td>
						<td style="width: 95%;"><?php echo $opcion2; ?></td>
					</tr>
				<?php } ?>
				<?php if($opcion3){ ?>
					<tr>
						<td align="center" style="width: 5%;">C</td>
						<td style="width: 95%;"><?php echo $opcion3; ?></td>
					</tr>
				<?php } ?>
				<?php if($opcion4){ ?>
					<tr>
						<td align="center" style="width: 5%;">D</td>
						<td style="width: 95%;"><?php echo $opcion4; ?></td>
					</tr>
				<?php } ?>
				<?php if($opcion5){ ?>
					<tr>
						<td align="center" style="width: 5%;">E</td>
						<td style="width: 95%;"><?php echo $opcion5; ?></td>
					</tr>
				<?php } ?>
				<?php if($opcion6){ ?>
					<tr>
						<td align="center" style="width: 5%;">F</td>
						<td style="width: 95%;"><?php echo $opcion6; ?></td>
					</tr>
				<?php } ?>
			<?php } ?>
			<tr>
                <td colspan="2" height="10"></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
	<br /><br />
	<?php $con_archivos=consulta("SELECT * FROM archivos WHERE idplanteo='$idplanteo' ORDER BY idarchivo ASC"); 
		if(mysqli_num_rows($con_archivos)>0){
	?>
	<table border="1" align="center">
		<tr bgcolor="#D6D6D6">
			<th height="20" style="width: 100%;">Archivos adjuntos al planteo:</th>
		</tr>
	<?php while ($arch = mysqli_fetch_array($con_archivos, MYSQLI_ASSOC)) { ?>
		<tr>
			<td style="width: 100%;">
			<?php if(esimagen($arch['ruta'])){ ?>
				<a href="<?php echo "../".$arch['ruta']; ?>" target="_blank"><img src="<?php echo "../".$arch['ruta']; ?>" width="300" height="300"></a>
			<?php } else { ?>
				<a href="simedu.com.ar/<?php echo $arch['ruta']; ?>"><?php echo $arch['nombre']; ?></a>
			<?php } ?>
			</td>
        </tr>
	<?php } ?>
	</table>
	<?php } ?>
