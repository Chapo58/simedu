<?php

ini_set('memory_limit', '128M');

// define('_MPDF_URI','mpdf/');

include("mpdf/mpdf.php");

session_start();
require_once('../funciones.php');
$idprofesor = $_SESSION['idusuario'];
$idplanteo= $_POST['idplanteo'];
$con_planteo=consulta("SELECT * FROM planteos WHERE idplanteo='$idplanteo'");
$p=mysqli_fetch_array($con_planteo);
if($p['limite'] == "0000-00-00"){
	$limite = "Sin tiempo limite";
} else {
	$limite = date("d/m/Y",strtotime($p['limite']));
}
if($p['semaforo'] == ""){
	$semaforo = "Sin color";
	$colorfondo = "#C9C9C9";
} else {
	$color = $p['semaforo'];
	$con_semaforo=consulta("SELECT $color FROM semaforos WHERE idprofesor='$idprofesor'");
	$s=mysqli_fetch_array($con_semaforo);
	$semaforo = $s["$color"];
	if($color == "verde"){
		$colorfondo = "#88FF7A";
	}elseif($color == "amarillo"){
		$colorfondo = "#FFF75A";
	} else {
		$colorfondo = "#FF5151";
	}
}

ob_start();  // start output buffering
include('planteos_html.php');
$content = ob_get_clean(); // get content of the buffer and clean the buffer

ob_start();  // start output buffering
include('header.php');
$header = ob_get_clean(); // get content of the buffer and clean the buffer

$footer = '<div align="right" style="color:gray">< Pagina {PAGENO} ></div>';

$mpdf=new mPDF('c','A4','','',12,12,35,15,7,7);

// $mpdf->progbar_altHTML = '<html><body>
// 	<div style="margin-top: 5em; text-align: center; font-family: Verdana; font-size: 15px;">
//     <img style="vertical-align: middle" src="mpdf/examples/loading.gif" />
//     Creando libro diario. Por favor espere...
//   </div>';
// $mpdf->StartProgressBarOutput();

$mpdf->SetDisplayMode('fullpage');

$mpdf->debug = true;
$mpdf->useSubstitutions = false;
$mpdf->cacheTables = true;
$mpdf->simpleTables= true;
$mpdf->packTableData=true;

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdf/css/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
$mpdf->WriteHTML($content);

$mpdf->Output('planteo.pdf','I');

exit;


?>
