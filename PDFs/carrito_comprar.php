<?php
	include "../funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['ids']) && !empty($_POST['ids']) &&
		isset($_POST['cantidades']) && !empty($_POST['cantidades']) && 
		isset($_POST['formpago']) && !empty($_POST['formpago'])) {
		
		$ids = $_POST['ids'];
		$cantidades = $_POST['cantidades'];
		$idfpago = $_POST['formpago'];
		$con_fpago=consulta("SELECT * FROM formpag WHERE idpago = '$idfpago'");
		$fp=mysqli_fetch_array($con_fpago);
		$formadepago = $fp['pag_descr'];
		
		for ($x=0;$x<count($ids); $x++) {
			if($cantidades[$x] > 0){
				consulta("UPDATE carritos SET cantidad = '$cantidades[$x]' WHERE idcarrito = $ids[$x]");
			} else {
				consulta("DELETE FROM carritos WHERE idcarrito = $ids[$x]");
			}
		}
		
		require_once('html2pdf.class.php');
		$con_car=consulta("SELECT idvendedor FROM carritos WHERE idcomprador='$idempresa' GROUP BY idvendedor");
		while ($p = mysqli_fetch_array($con_car, MYSQLI_ASSOC)) {
			$factura = "comprobantes/".$idempresa . rand() * rand().'.pdf';
			$idvendedor = $p['idvendedor'];
			$idcomprador = $idempresa;
			
			$con_vendedor=consulta("SELECT empresas.*, condiva.iva_descr, CURDATE() + INTERVAL 1 MONTH as 'vencimiento' FROM empresas LEFT JOIN condiva ON empresas.condiva = condiva.idiva WHERE idempresa = '$idvendedor'");
			$v=mysqli_fetch_array($con_vendedor);
			// echo date("d/m/Y",strtotime($v['vencimiento']));
			
			$con_comprador=consulta("SELECT empresas.*, condiva.iva_descr FROM empresas LEFT JOIN condiva ON empresas.condiva = condiva.idiva WHERE idempresa = '$idcomprador'");
			$c=mysqli_fetch_array($con_comprador);
			
			if($v['condiva'] == 1 && $c['condiva'] == 1){
				$tipo = "A";
			} elseif($v['condiva'] == 1 && $c['condiva'] == 2){
				$tipo = "B";
			} elseif($v['condiva'] == 2){
				$tipo = "C";
			}
			
			$con_nmax=consulta("SELECT numero FROM facturas WHERE idvendedor = '$idvendedor' AND numero = (SELECT MAX(numero) FROM facturas WHERE idvendedor = '$idvendedor' AND tipo = '$tipo')");
			if(mysqli_num_rows($con_nmax)==0){
				$nfactura = 1;
			} else {
				$cmax=mysqli_fetch_array($con_nmax);
				$nfactura = $cmax['numero'] + 1;
			}
			
			ob_start();
			
			include('carrito_pdf.php');
			
			$content = ob_get_clean();
			
			// convert to PDF
			try
			{
				$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 3);
				$html2pdf->setTestTdInOnePage(false);
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output("$factura", 'F');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
		}
		echo "Su compra ha sido guardada.";
		acthistempresa($rsocial, "Se realizo una compra");
		consulta("DELETE FROM carritos WHERE idcomprador = $idempresa");
	} else {
		ir_a("index.php");
	}
?>
