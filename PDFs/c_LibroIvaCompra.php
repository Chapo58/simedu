<?php
    // get the HTML
    ob_start();
	$fecha = $_POST['fecha'];
	$fecha2 = str_replace("/","",$fecha);
	$año = substr($fecha2, -4);
	$mes = substr($fecha2, -6, 2);
	$inicio = $_POST['inicio'];
    include('h_LibroIvaCompra.php');
    $content = ob_get_clean();

    // convert to PDF
    require_once('html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 3);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('ivacompra.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
