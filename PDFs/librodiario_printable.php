<?php
session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);

$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
$condesde = fecha($_POST['desde'],"-");
$conhasta = fecha($_POST['hasta'],"-");
$iniasientos = $_POST['inicio'];
if(isset($_POST['ninterno'])) {$ninterno = true;}else{$ninterno = false;};
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Libro Diario</title>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<style>
body {
	margin: 0;
	padding: 0;
	background-color: #FAFAFA;
	font: 12pt "Tahoma";
}
* {
	box-sizing: border-box;
	-moz-box-sizing: border-box;
}
.page {
	width: 21cm;
	min-height: 29.7cm;
	padding: 1cm;
	margin: 1cm auto;
	border: 1px #D3D3D3 solid;
	border-radius: 5px;
	background: white;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}
.subpage {
	padding: 1cm;
	border: 2px gray solid;
	height: 256mm;
	outline: 2cm #fff solid;
}
@page {
	size: A4;
	margin: 1.0cm;
}
thead, tfoot {
	background-color: #ABE2F5 !important;
	-webkit-print-color-adjust: exact;
}
tr.color {
	background-color: #CECECE !important;
	-webkit-print-color-adjust: exact;
}
tr.color2 {
	background-color: #A4ADB9 !important;
	-webkit-print-color-adjust: exact;
}
@media print {
	html, body {
	 width: 210mm;
	 height: 297mm;
 	}
	#header, #footer {
		 position: fixed;
		 display: block;
		 top: 0;
	}
	#footer {
			 bottom: 0;
	}
	thead, tfoot {
		background-color: #ABE2F5 !important;
    -webkit-print-color-adjust: exact;
	}
	tr.color {
		background-color: #CECECE !important;
		-webkit-print-color-adjust: exact;
	}
	tr.color2 {
		background-color: #A4ADB9 !important;
		-webkit-print-color-adjust: exact;
	}
	.page {
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
	}
}
		</style>
	</head>

	<body onload="window.print()">
			<!-- <div id="header">
				<table style="margin-left:140px;font-size: 12pt;">
							<tr>
								<td width="300"><u>Empresa</u>: <?php echo $rsocial; ?></td>
								<td width="300"><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
							</tr>
							<tr>
								<td width="300"><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
								<td width="300"><u>Fecha Impresion</u>: <?php echo date('d/m/Y'); ?></td>
							</tr>
							<tr>
								<td width="300"><u>Domicilio</u>: <?php echo $e['domicilio']; ?></td>
							</tr>
				</table>
			</div> -->
	    <div class="page">
				<h2 align="center">LIBRO DIARIO</h2>
				<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
				<br>
			    <table border="1" align="center">
			        <thead>
			            <tr>
			                <th align="center">Codigo</th>
			                <th>Detalle</th>
							<th align="center">Debe</th>
							<th align="center">Haber</th>
			            </tr>
			        </thead>
							<tfoot>
			            <tr>
			                <th align="center">Codigo</th>
			                <th>Detalle</th>
							<th align="center">Debe</th>
							<th align="center">Haber</th>
			            </tr>
			        </tfoot>
			        <tbody>
						<?php
							$con_lista=consulta("SELECT * FROM listaasientos WHERE idempresa='$idempresa' AND (fecha BETWEEN '$condesde' AND '$conhasta') ORDER BY fecha ASC, nlista ASC");
							$sumadebe = 0;
							$sumahaber = 0;
							while ($l = mysqli_fetch_array($con_lista, MYSQLI_ASSOC)) {
								if($ninterno){ // Si hay poner el numero interno y hay que reordenar
									$nlista = $iniasientos;
									$nrointerno = $l['nlista'];
								} else {
									$nrointerno = $nlista = $l['nlista'];
								}
								$idlista = $l['idlista'];
								$fecha = fecha($l['fecha'],"/");
								$ldenominacion = $l['denominacion'];
								$sumadebe += $ldebe = $l['debe'];
								$sumahaber += $lhaber = $l['haber'];
								$nota = $l['nota'];
						?>
			            <tr class="color">
							<td colspan="2">Denominacion: <?php echo $ldenominacion; ?></td>
							<td colspan="2">N° de Asiento: <?php echo $nlista; if($ninterno) echo "&nbsp;-&nbsp;N° Interno: " . $nrointerno; ?></td>
			            </tr>
						<tr class="color">
							<td colspan="2">Nota: <?php echo substr($nota, 0, 30)."..."; ?></td>
							<td colspan="2">Fecha: <?php echo $fecha; ?></td>
						</tr>
						<?php
							$con_asientos=consulta("SELECT asientos.*, cuentas.codigo, cuentas.denominacion FROM cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta WHERE asientos.idlista='$idlista' ORDER BY asientos.debe DESC");
							while ($a = mysqli_fetch_array($con_asientos, MYSQLI_ASSOC)) {
							$cuenta = $a['codigo'];
							$denominacion = $a['denominacion'];
							$debe = $a['debe'];
							$haber = $a['haber'];
						?>
						<tr>
							<td style="width: 20%;"><?php echo $cuenta; ?></td>
			                <td style="width: 40%;"><?php echo $denominacion; ?></td>
			                <td style="width: 20%;" align="right"><?php if($debe != 0 ) echo number_format($debe, 2, ',', '.'); ?></td>
							<td style="width: 20%;" align="right"><?php if($haber != 0 ) echo number_format($haber, 2, ',', '.'); ?></td>
			            </tr>
						<?php } ?>
						<tr class="color2">
							<td colspan="2" align="right">TOTALES&nbsp;&nbsp;</td>
							<td align="right"><?php echo number_format($ldebe, 2, ',', '.'); ?></td>
							<td align="right"><?php echo number_format($lhaber, 2, ',', '.'); ?></td>
						</tr>
					<?php
							if($ninterno){ $iniasientos++; };
							} ?>
						<tr bgcolor="#F79B9B">
							<td colspan="2" align="right">SALDOS&nbsp;&nbsp;</td>
							<td align="right"><?php echo number_format($sumadebe, 2, ',', '.'); ?></td>
							<td align="right"><?php echo number_format($sumahaber, 2, ',', '.'); ?></td>
						</tr>
			        </tbody>
			    </table>
	    </div>
	    <!-- <div class="page">
	        <div class="subpage">Page 2/2</div>
	    </div> -->
	</body>
</html>
