<?php

ini_set('memory_limit', '128M');

// define('_MPDF_URI','mpdf/');

include("mpdf/mpdf.php");

session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$rsocial = $_SESSION['rsocial'];

$comentario = $_POST['comentario'];
$importe = $_POST['importe'];
$idcomprador = $cliente = $_POST['cliente'];
$fecha = $_POST['fecha'];
$facturas = $_POST['facturas'];
$importes = $_POST['importes'];

if(is_numeric($idcomprador)){
  $con_comprador=consulta("SELECT empresas.*, condiva.iva_descr FROM empresas LEFT JOIN condiva ON empresas.condiva = condiva.idiva WHERE idempresa = '$idcomprador'");
  $c=mysqli_fetch_array($con_comprador);
  $idcliente = 0;
} else {
  $idcomprador = substr($idcomprador,1);
  $con_comprador=consulta("SELECT clientes.*, condiva.iva_descr FROM clientes LEFT JOIN condiva ON clientes.condiva = condiva.idiva WHERE idcliente = '$idcomprador'");
  $c=mysqli_fetch_array($con_comprador);
  $idcliente = $idcomprador;
  $idcomprador = 0;
}

$con_vendedor=consulta("SELECT * FROM empresas WHERE idempresa = '$idempresa'");
$v=mysqli_fetch_array($con_vendedor);

$factura = "comprobantes/".$idempresa . rand() * rand().'.pdf';
$con_nmax=consulta("SELECT MAX(numero) as numero, MAX(puntofacturacion) as pnumero FROM facturas WHERE idvendedor = '$idempresa' AND tipo = 'RC'");
if(mysqli_num_rows($con_nmax)==0){
  $nfactura = 1;
  $pventa = 1;
} else {
  $cmax=mysqli_fetch_array($con_nmax);
  if($cmax['numero'] == 99999999){
    $nfactura = 1;
    $pventa = $cmax['pnumero'] + 1;
  } else {
    $nfactura = $cmax['numero'] + 1;
    $pventa = 1;
  }
}

ob_start();  // start output buffering
include('generar_recibo_venta_html.php');
$content = ob_get_clean(); // get content of the buffer and clean the buffer

ob_start();  // start output buffering
include('generar_recibo_venta_header.php');
$header = ob_get_clean(); // get content of the buffer and clean the buffer

ob_start();  // start output buffering
include('footer_comprobantes.php');
$footer = ob_get_clean(); // get content of the buffer and clean the buffer

$mpdf=new mPDF('c','A4','','',12,12,35,15,7,7);

// $mpdf->progbar_altHTML = '<html><body>
// 	<div style="margin-top: 5em; text-align: center; font-family: Verdana; font-size: 15px;">
//     <img style="vertical-align: middle" src="mpdf/examples/loading.gif" />
//     Creando libro diario. Por favor espere...
//   </div>';
// $mpdf->StartProgressBarOutput();

$mpdf->SetDisplayMode('fullpage');

$mpdf->debug = true;
$mpdf->useSubstitutions = false;
$mpdf->cacheTables = true;
$mpdf->simpleTables= true;
$mpdf->packTableData=true;

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdf/css/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
$mpdf->WriteHTML($content);

$mpdf->Output($factura,'F');

echo "El comprobante ha sido generado correctamente.";
acthistempresa($rsocial, "Se genero un comprobante de venta");

exit;


?>
