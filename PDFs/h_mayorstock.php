<?php
session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);
$sqldesde = fecha($_POST['desde'],"-");
$sqlhasta = fecha($_POST['hasta'],"-");
?>
<style>
<?php 
require_once('../css/bootstrap.css');
require_once('../css/style.css');
?>
</style>
<page backtop="30mm" backbottom="10mm" backleft="20mm" backright="20mm">
    <page_header>
        <table style="margin-left:80px;">
            <tr>
                <td rowspan="4" width="100"><img src=<?php echo $imagenempresa; ?> width="75" height="75" class="img-circle img-user tooltipster" title="Imagen de perfil" alt="Imagen de Perfil"></td>
            </tr>
			<tr>
				<td height="20" width="300"><u>Empresa</u>: <?php echo $rsocial; ?></td>
				<td height="20" width="300"><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
			</tr>
			<tr>
				<td height="20" width="300"><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
				<td height="20" width="300"><u>Fecha Impresion</u>: <?php echo date('d/m/Y'); ?></td>
			</tr>
			<tr>
				<td height="20" width="300"><u>Domicilio</u>: <?php echo $e['domicilio']; ?></td>
			</tr>
        </table>
    </page_header>
    <page_footer>
        <div align="right">Pagina [[page_cu]]/[[page_nb]]</div>
    </page_footer>
	<table border="1" align="center">
		<tr bgcolor="#DDFFE6">
            <th align="center" height="20" style="width: 100%;">LIBRO MAYOR DE STOCK</th>
        </tr>
	</table>
	<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
	<br /><br />
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th align="center">Fecha</th>
                <th align="center">N° Comprobante</th>
				<th align="center">Codigo</th>
				<th align="center">Detalle</th>
				<th align="center">Entrada</th>
				<th align="center">Salida</th>
				<th align="center">Saldo</th>
            </tr>
        </thead>
        <tbody>
			<?php				
				$con_stock=consulta("SELECT movstock.*, facturas.fecha, facturas.numero, facturas.puntofacturacion, facturas.idcomprador, facturas.idvendedor, productos.nombre 
							FROM movstock LEFT JOIN (SELECT idfactura, fecha, numero, puntofacturacion, idcomprador, idvendedor FROM facturas WHERE (fecha BETWEEN '$sqldesde' AND '$sqlhasta')) AS facturas 
							ON movstock.idfactura = facturas.idfactura
							LEFT JOIN productos ON movstock.idproducto = productos.idproducto
                            WHERE facturas.idcomprador = '$idempresa' OR facturas.idvendedor = '$idempresa'
							ORDER BY movstock.idproducto ASC, facturas.fecha ASC");
				while ($s = mysqli_fetch_array($con_stock, MYSQLI_ASSOC)) {
					$nfactura = $s['numero'];
					$pventa = $s['puntofacturacion'];
					$producto = $s['nombre'];
					$cantidad = $s['cantidad'];
					$saldo = $s['restantes'];
					$idproducto = $s['idproducto'];
					$idcomprador = $s['idcomprador'];
					$fecha = date("d/m/Y",strtotime($s['fecha']));
					if($idcomprador == $idempresa){
						$comprado = $cantidad;
						$vendido = "";
					} else {
						$comprado = "";
						$vendido = $cantidad;
					}
			?>
			<tr>
				<td style="width: 15%;"><?php echo $fecha; ?></td>
                <td style="width: 15%;"><?php echo str_pad($pventa, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura, 8, "0", STR_PAD_LEFT); ?></td>
                <td style="width: 10%;"><?php echo $idproducto; ?></td>
				<td style="width: 30%;"><?php echo $producto; ?></td>
				<td style="width: 10%;"><?php echo $comprado; ?></td>
				<td style="width: 10%;"><?php echo $vendido; ?></td>
				<td style="width: 10%;"><?php echo $saldo; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
</page>