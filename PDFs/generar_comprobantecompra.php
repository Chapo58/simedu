<?php
	include "../funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['fpago']) && !empty($_POST['fpago']) &&
		isset($_POST['proveedor']) && !empty($_POST['proveedor']) &&
		isset($_POST['tcomprobante']) && !empty($_POST['tcomprobante'])) {

		$productos = $_POST['productos'];
		$cantidades = $_POST['cantidades'];
		$precios = $_POST['precios'];
		$ivas = $_POST['ivas'];
		$fpago = $_POST['fpago'];
		$tcomprobante = $_POST['tcomprobante'];
		$idproveedor = $_POST['proveedor'];
		$idcomprobante = $_POST['idcomprobante'];
		$pventa = substr($_POST['numf'], 0, 4);
		$numf = substr($_POST['numf'], 5, 8);
		$nfactura = $pventa."-".$numf;

		if($_POST['conceptos']){
			$conceptos = $_POST['conceptos'];
		} else {
			$conceptos = 0;
		}
		if($_POST['retenciones']){
			$retenciones = $_POST['retenciones'];
		} else {
			$retenciones = 0;
		}

		$con_fpago=consulta("SELECT * FROM formpag WHERE idpago = '$fpago'");
		$fp=mysqli_fetch_array($con_fpago);
		$formadepago = $fp['pag_descr'];

		require_once('html2pdf.class.php');
		$factura = "comprobantes/".$idempresa . rand() * rand().'.pdf';
		$idcomprador = $idempresa;

		$con_comprador=consulta("SELECT empresas.*, condiva.iva_descr FROM empresas LEFT JOIN condiva ON empresas.condiva = condiva.idiva WHERE idempresa = '$idcomprador'");
		$c=mysqli_fetch_array($con_comprador);
		// echo date("d/m/Y",strtotime($v['vencimiento']));

		$con_vendedor=consulta("SELECT proveedores.*, condiva.iva_descr FROM proveedores LEFT JOIN condiva ON proveedores.condiva = condiva.idiva WHERE idproveedor = '$idproveedor'");
		$v=mysqli_fetch_array($con_vendedor);

		if($v['condiva'] == 1 && $c['condiva'] == 1){
			$letra = "A";
		} elseif($v['condiva'] == 1 && $c['condiva'] == 2){
			$letra = "B";
		} else/*if($v['condiva'] == 2)*/{
			$letra = "C";
		}

		$coniva = true;

		if($tcomprobante == "F"){
			$esfactura = true;
			if($v['condiva'] == 1 && $c['condiva'] == 1){
				$tipo = "A";
			} elseif($v['condiva'] == 1 && $c['condiva'] != 1){
				$tipo = "B";
				$coniva = false;
			} else/*if($v['condiva'] == 2)*/{
				$tipo = "C";
				$coniva = false;
			}
		} elseif($tcomprobante == "OC") {
			$tipo = "OC";
			$tipoc = "ORDEN DE COMPRA";
			$esfactura = false;
			$letra = "";
		} elseif($tcomprobante == "NC") {
			$tipo = "NC";
			$tipoc = "NOTA DE CREDITO";
			$esfactura = false;
		} else {
			$tipo = "ND";
			$tipoc = "NOTA DE DEBITO";
			$esfactura = false;
		}

		if($idcomprobante){
			$con_comp=consulta("SELECT * FROM facturas WHERE idfactura = '$idcomprobante'");
			$comp=mysqli_fetch_array($con_comp);
			$ruta = $comp['ruta'];
			unlink($ruta);
			$factura = $ruta;
			consulta("DELETE FROM movstock WHERE idfactura = $idcomprobante");
		} else {
			$factura = "comprobantes/".$idempresa . rand() * rand().'.pdf';
		}

		ob_start();
		if($tipo == "OC"){
			include('orden_de_compra.php');
		} else {
			include('comprobantecompra.php');
		}

		$content = ob_get_clean();

			// convert to PDF
		try
		{
			$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 3);
			$html2pdf->setTestTdInOnePage(false);
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
			$html2pdf->Output("$factura", 'F');
		}
		catch(HTML2PDF_exception $e) {
			echo $e;
			exit;
		}

		if($idcomprobante){
			echo "El comprobante ha sido editado correctamente.";
			acthistempresa($rsocial, "Se edito un comprobante de compra");
		} else {
			echo "El comprobante ha sido generado correctamente.";
			acthistempresa($rsocial, "Se genero un comprobante de compra");
		}

	} else {
		ir_a("index.php");
	}
?>
