	<h2 align="center">LIBRO DIARIO</h2>
	<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
	<br>
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th align="center">Codigo</th>
                <th>Detalle</th>
				<th align="center">Debe</th>
				<th align="center">Haber</th>
            </tr>
        </thead>
				<tfoot>
            <tr bgcolor="#ABE2F5">
                <th align="center">Codigo</th>
                <th>Detalle</th>
				<th align="center">Debe</th>
				<th align="center">Haber</th>
            </tr>
        </tfoot>
        <tbody>
			<?php
				$con_lista=consulta("SELECT * FROM listaasientos WHERE idempresa='$idempresa' AND (fecha BETWEEN '$condesde' AND '$conhasta') ORDER BY fecha ASC, nlista ASC");
				$sumadebe = 0;
				$sumahaber = 0;
				while ($l = mysqli_fetch_array($con_lista, MYSQLI_ASSOC)) {
					if($ninterno){ // Si hay poner el numero interno y hay que reordenar
						$nlista = $iniasientos;
						$nrointerno = $l['nlista'];
					} else {
						$nrointerno = $nlista = $l['nlista'];
					}
					$idlista = $l['idlista'];
					$fecha = fecha($l['fecha'],"/");
					$ldenominacion = $l['denominacion'];
					$sumadebe += $ldebe = $l['debe'];
					$sumahaber += $lhaber = $l['haber'];
					$nota = $l['nota'];
			?>
            <tr bgcolor="#CECECE">
				<td colspan="2">Denominacion: <?php echo $ldenominacion; ?></td>
				<td colspan="2">N° de Asiento: <?php echo $nlista; if($ninterno) echo "&nbsp;-&nbsp;N° Interno: " . $nrointerno; ?></td>
            </tr>
			<tr bgcolor="#CECECE">
				<td colspan="2">Nota: <?php echo substr($nota, 0, 30)."..."; ?></td>
				<td colspan="2">Fecha: <?php echo $fecha; ?></td>
			</tr>
			<?php
				$con_asientos=consulta("SELECT asientos.*, cuentas.codigo, cuentas.denominacion FROM cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta WHERE asientos.idlista='$idlista' ORDER BY asientos.debe DESC");
				while ($a = mysqli_fetch_array($con_asientos, MYSQLI_ASSOC)) {
				$cuenta = $a['codigo'];
				$denominacion = $a['denominacion'];
				$debe = $a['debe'];
				$haber = $a['haber'];
			?>
			<tr>
				<td style="width: 20%;"><?php echo $cuenta; ?></td>
                <td style="width: 40%;"><?php echo $denominacion; ?></td>
                <td style="width: 20%;" align="right"><?php if($debe != 0 ) echo number_format($debe, 2, ',', '.'); ?></td>
				<td style="width: 20%;" align="right"><?php if($haber != 0 ) echo number_format($haber, 2, ',', '.'); ?></td>
            </tr>
			<?php } ?>
			<tr bgcolor="#A4ADB9">
				<td colspan="2" align="right">TOTALES&nbsp;&nbsp;</td>
				<td align="right"><?php echo number_format($ldebe, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($lhaber, 2, ',', '.'); ?></td>
			</tr>
		<?php
				if($ninterno){ $iniasientos++; };
				} ?>
			<tr bgcolor="#F79B9B">
				<td colspan="2" align="right">SALDOS&nbsp;&nbsp;</td>
				<td align="right"><?php echo number_format($sumadebe, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($sumahaber, 2, ',', '.'); ?></td>
			</tr>
        </tbody>
    </table>
