  <h2 align="center">PLAN DE CUENTAS</h2>
	<br />
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th>Codigo</th>
                <th>Denominacion</th>
				<th align="center">Rubro</th>
				<th>Imputable</th>
				<th align="center">Sumariza</th>
            </tr>
        </thead>
        <tfoot>
            <tr bgcolor="#ABE2F5">
                <th>Codigo</th>
                <th>Denominacion</th>
				<th align="center">Rubro</th>
				<th>Imputable</th>
				<th align="center">Sumariza</th>
            </tr>
        </tfoot>
        <tbody>
			<?php
				$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' ORDER BY codigo ASC");
				while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
					$codigo = $c['codigo'];
					$denominacion = $c['denominacion'];
					$rubro = $c['rubro'];
					if($c['imputable'] == 1) {
						$imputable = "Si";
					} else {
						$imputable = "No";
					}
					$sumariza = $c['sumariza'];
			?>
            <tr>
				<td style="width: 15%;"><?php echo $codigo; ?></td>
                <td style="width: 50%;">
					<?php if($c['imputable'] == 1) echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $denominacion; else echo $denominacion; ?>
				</td>
                <td style="width: 10%;" align="center"><?php echo $rubro; ?></td>
                <td style="width: 10%;" align="center"><?php echo $imputable; ?></td>
				<td style="width: 15%;" align="center"><?php echo $sumariza; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
