	<?php
		consulta("INSERT INTO balancegral (codigo, denominacion, sumariza, imputable, debe, haber)
			SELECT cuentas.codigo, cuentas.denominacion, cuentas.sumariza, cuentas.imputable, IFNULL(a.debe,0) as debe, IFNULL(a.haber,0) as haber
			FROM cuentas LEFT JOIN
			(SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber, listaasientos.fecha FROM asientos LEFT JOIN listaasientos ON asientos.idlista = listaasientos.idlista WHERE listaasientos.idempresa = '$idempresa' AND (listaasientos.fecha BETWEEN '$condesde' AND '$conhasta') GROUP BY asientos.idcuenta) AS a
			ON cuentas.idcuenta = a.idcuenta
			WHERE cuentas.idempresa = '$idempresa'
			GROUP BY cuentas.codigo ORDER BY cuentas.codigo ASC");
		$con_sumariza=consulta("SELECT cuentas.sumariza as codigo, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber
				FROM cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta
				INNER JOIN listaasientos ON asientos.idlista = listaasientos.idlista
				WHERE cuentas.idempresa = '$idempresa' AND listaasientos.idempresa = '$idempresa'
				AND (listaasientos.fecha BETWEEN '$condesde' AND '$conhasta')
				GROUP BY cuentas.sumariza ORDER BY cuentas.sumariza ASC");
				while ($s = mysqli_fetch_array($con_sumariza, MYSQLI_ASSOC)) {
					$sumariza = $s['codigo'];
					$debe = $s['debe'];
					$haber = $s['haber'];
					consulta("UPDATE balancegral SET debe = '$debe', haber = '$haber' WHERE codigo = '$sumariza'");
					$con_s=consulta("SELECT sumariza FROM cuentas WHERE idempresa = '$idempresa' AND codigo = '$sumariza'");
					$s2=mysqli_fetch_array($con_s);
					$sumariza = $s2['sumariza'];
					consulta("UPDATE balancegral SET debe = debe + '$debe', haber = haber + '$haber' WHERE codigo = '$sumariza'");
					$con_s=consulta("SELECT sumariza FROM cuentas WHERE idempresa = '$idempresa' AND codigo = '$sumariza'");
					$s3=mysqli_fetch_array($con_s);
					$sumariza = $s3['sumariza'];
					consulta("UPDATE balancegral SET debe = debe + '$debe', haber = haber + '$haber' WHERE codigo = '$sumariza'");
					$con_s=consulta("SELECT sumariza FROM cuentas WHERE idempresa = '$idempresa' AND codigo = '$sumariza'");
					$s4=mysqli_fetch_array($con_s);
					$sumariza = $s4['sumariza'];
					consulta("UPDATE balancegral SET debe = debe + '$debe', haber = haber + '$haber' WHERE codigo = '$sumariza'");
				}
	?>
  <h2 align="center">BALANCE GENERAL</h2>
	<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
	<br />
    <table border="1" align="center">
        <thead>
			       <tr bgcolor="#ABE2F5">
                <th colspan="4"></th>
				        <th colspan="2" align="center">Saldo</th>
            </tr>
            <tr bgcolor="#ABE2F5">
                <th align="center">Cuenta</th>
                <th>Detalle</th>
        				<th align="center">Debe</th>
        				<th align="center">Haber</th>
        				<th align="center">Debe</th>
        				<th align="center">Haber</th>
            </tr>
        </thead>
        <tfoot>
            <tr bgcolor="#ABE2F5">
                <th align="center">Cuenta</th>
                <th>Detalle</th>
        				<th align="center">Debe</th>
        				<th align="center">Haber</th>
        				<th align="center">Debe</th>
        				<th align="center">Haber</th>
            </tr>
        </tfoot>
        <tbody>
			<?php
				$sumadebe = 0;
				$sumahaber = 0;
				$diferenciahaber = 0;
				$diferenciadebe = 0;

				$con_cuentas=consulta("SELECT * FROM balancegral WHERE debe > 0 OR haber > 0 ORDER BY codigo ASC");
				while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
					$ncuenta = $c['codigo'];
					$detalle = $c['denominacion'];
					if($c['imputable'] == 1){
						$sumadebe += $c['debe'];
						$sumahaber += $c['haber'];
					}
					if($c['debe'] == 0){
						$debe = "";
					} else {
						$debe = $c['debe'];
					}
					if($c['haber'] == 0){
						$haber = "";
					} else {
						$haber = $c['haber'];
					}
					$diferencia = $c['debe'] - $c['haber'];
					if($diferencia < 0){
						$debe2 = "";
						$haber2 = abs($diferencia);
					if($c['imputable'] == 1)	$diferenciahaber += $haber2;
					} elseif($diferencia == 0){
						$debe2 = "";
						$haber2 = "";
					} else {
						$debe2 = $diferencia;
						$haber2 = "";
					if($c['imputable'] == 1)	$diferenciadebe += $debe2;
					}
			?>
			<tr>
				<td style="width: 8%;"><?php echo $ncuenta; ?></td>
        <td style="width: 40%;"><?php if($c['imputable'] == 1){echo "&nbsp;&nbsp;&nbsp;&nbsp;".$detalle;} else {echo $detalle;} ?></td>
        <td style="width: 13%;" align="right"><?php if($debe != "") echo number_format($debe, 2, ',', '.'); ?></td>
				<td style="width: 13%;" align="right"><?php if($haber != "") echo number_format($haber, 2, ',', '.'); ?></td>
				<td style="width: 13%;" align="right"><?php if($debe2 != "") echo number_format($debe2, 2, ',', '.'); ?></td>
				<td style="width: 13%;" align="right"><?php if($haber2 != "") echo number_format($haber2, 2, ',', '.'); ?></td>
            </tr>
		<?php } ?>
			<tr bgcolor="#A4ADB9">
				<td colspan="2" align="right">SALDOS&nbsp;&nbsp;</td>
				<td align="right"><?php echo number_format($sumadebe, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($sumahaber, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($diferenciadebe, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($diferenciahaber, 2, ',', '.'); ?></td>
			</tr>
        </tbody>
    </table>
	<?php consulta("TRUNCATE balancegral"); ?>
