	<h2 align="center">BALANCE SUMAS Y SALDOS</h2>
	<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
	<br />
    <table border="1" align="center">
        <thead>
			       <tr bgcolor="#ABE2F5">
                <th colspan="2"></th>
        				<th colspan="2" align="center">Suma</th>
        				<th colspan="2" align="center">Saldo</th>
            </tr>
            <tr bgcolor="#ABE2F5">
                <th align="center">Cuenta</th>
                <th>Detalle</th>
        				<th align="center">Debe</th>
        				<th align="center">Haber</th>
        				<th align="center">Deudor</th>
        				<th align="center">Acreedor</th>
            </tr>
        </thead>
        <tfoot>
            <tr bgcolor="#ABE2F5">
                <th align="center">Cuenta</th>
                <th>Detalle</th>
        				<th align="center">Debe</th>
        				<th align="center">Haber</th>
        				<th align="center">Deudor</th>
        				<th align="center">Acreedor</th>
            </tr>
        </tfoot>
        <tbody>
			<?php
				$sumadebe = 0;
				$sumahaber = 0;
				$diferenciahaber = 0;
				$diferenciadebe = 0;
				$con_cuentas=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber, cuentas.codigo, cuentas.denominacion, cuentas.sumariza, listaasientos.fecha, listaasientos.idlista
				FROM cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta
				INNER JOIN listaasientos ON asientos.idlista = listaasientos.idlista
				WHERE cuentas.idempresa = '$idempresa' AND listaasientos.idempresa = '$idempresa'
				AND cuentas.codigo >= '$cuentadesde' AND cuentas.codigo <= '$cuentahasta'
				AND (listaasientos.fecha BETWEEN '$condesde' AND '$conhasta')
				GROUP BY cuentas.codigo,  asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
				while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
					$ncuenta = $c['codigo'];
					$detalle = $c['denominacion'];
					$sumadebe += $c['debe'];
					$sumahaber += $c['haber'];
					if($c['debe'] == 0){
						$debe = "";
					} else {
						$debe = $c['debe'];
					}
					if($c['haber'] == 0){
						$haber = "";
					} else {
						$haber = $c['haber'];
					}
					$diferencia = $c['debe'] - $c['haber'];
					if($diferencia < 0){
						$debe2 = "";
						$haber2 = abs($diferencia);
						$diferenciahaber += $haber2;
					} elseif($diferencia == 0){
						$debe2 = "";
						$haber2 = "";
					} else {
						$debe2 = $diferencia;
						$haber2 = "";
						$diferenciadebe += $debe2;
					}
			?>
			<tr>
				<td style="width: 8%;"><?php echo $ncuenta; ?></td>
        <td style="width: 40%;"><?php echo $detalle; ?></td>
        <td style="width: 13%;" align="right"><?php if($debe != "") echo number_format($debe, 2, ',', '.'); ?></td>
				<td style="width: 13%;" align="right"><?php if($haber != "") echo number_format($haber, 2, ',', '.'); ?></td>
				<td style="width: 13%;" align="right"><?php if($debe2 != "") echo number_format($debe2, 2, ',', '.'); ?></td>
				<td style="width: 13%;" align="right"><?php if($haber2 != "") echo number_format($haber2, 2, ',', '.'); ?></td>
            </tr>
		<?php } ?>
			<tr bgcolor="#A4ADB9">
				<td colspan="2" align="right">TOTALES&nbsp;&nbsp;</td>
				<td align="right"><?php echo number_format($sumadebe, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($sumahaber, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($diferenciadebe, 2, ',', '.'); ?></td>
				<td align="right"><?php echo number_format($diferenciahaber, 2, ',', '.'); ?></td>
			</tr>
        </tbody>
    </table>
