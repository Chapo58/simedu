<?php
    // get the HTML
    ob_start();
    include('h_minimostock.php');
    $content = ob_get_clean();

    // convert to PDF
    require_once('html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 3);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('minimostock.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
