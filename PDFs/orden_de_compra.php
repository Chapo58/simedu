<style>
<?php
require_once('../css/bootstrap.css');
require_once('../css/style.css');
?>
</style>
<page backtop="70mm" backbottom="20mm" backleft="5mm" backright="5mm">
    <page_header>
        <table style="margin-left:18px;font-size: 80%;">
            <tr>
        <td><img src="<?php (isset($v['logo']) && $v['logo'] ? print "../assets/uploads/logo/" . $v['logo'] : print "../images/logo.png") ?>" width="191" height="98" title="Logo de la Empresa" alt="Logo de la Empresa"></td>
				<td style="width: 23%;font-size: 400%;font-weight: bold;" align="right"><strong></strong></td>
				<td style="width: 45%;font-size: 130%;" align="right">ORDEN DE COMPRA</td>
            </tr>
		</table>
		<table style="margin-left:18px;font-size: 80%;">
			<tr>
				<td style="width: 60%;"><?php echo $c['rsocial']; ?></td>
				<td style="width: 35%;" colspan="2"></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['domicilio']; ?></td>
				<td style="width: 15%;font-weight: bold;">Emision:</td>
				<td style="width: 20%;"><?php echo $_POST['fecha']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['localidad']; ?></td>
				<td style="width: 15%;font-weight: bold;">CUIT:</td>
				<td style="width: 20%;"><?php echo $c['cuit']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['provincia']; if($c['pais'] != ""){ echo ", ".$c['pais']; } ?></td>
				<td style="width: 15%;font-weight: bold;">II.BB.:</td>
				<td style="width: 20%;"><?php echo $c['ingbrutos']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['telefono']; ?></td>
				<td style="width: 15%;font-weight: bold;"><!--Inicio Actividades:--></td>
				<td style="width: 20%;"><?php // echo date("d/m/Y",strtotime($c['inicioact'])); ?></td>
			</tr>
			<tr>
				<td><?php echo $c['emailcontacto']; ?></td>
				<td style="width: 15%;font-weight: bold;">I.V.A.:</td>
				<td style="width: 20%;"><?php echo $c['iva_descr']; ?></td>
			</tr>
        </table>
		<div style="margin-left:18px;margin-top:-5px;">
		<hr style="margin-top:0;" />
		</div>
		<table style="margin-left:18px;font-size: 80%;margin-top:-5px;">
			<tr>
				<td style="width: 60%;font-size: 120%;font-weight: bold;"><?php echo $v['rsocial']; ?></td>
				<td style="width: 15%;font-weight: bold;"></td>
				<td style="width: 20%;"></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['domicilio'];; ?></td>
				<td style="width: 15%;font-weight: bold;">CUIT:</td>
				<td style="width: 20%;"><?php echo $v['cuit'];; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['localidad']; ?></td>
				<td style="width: 15%;font-weight: bold;">II.BB.:</td>
				<td style="width: 20%;"><?php echo $v['ingbrutos']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['provincia']; if($v['pais'] != ""){ echo ", ".$v['pais']; } ?></td>
				<td style="width: 15%;font-weight: bold;">I.V.A.:</td>
				<td style="width: 20%;"><?php echo $v['iva_descr']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['telefono']; ?></td>
				<td colspan=2></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['emailcontacto']; ?></td>
				<td style="width: 15%;"></td>
				<td style="width: 20%;"></td>
			</tr>
		</table>
    </page_header>
    <page_footer>
		<hr />
		<table style="margin-left:18px;font-size: 80%;">
			<tr>
				<td style="width: 20%;">Fecha: <?php echo $_POST['fecha']; ?></td>
				<td style="width: 20%;">Hora: <?php echo date('H:i:s'); ?></td>
				<td style="width: 20%;" align="center">Usuario: <?php echo $idusuario; ?></td>
				<td style="width: 35%;" align="right">Pagina [[page_cu]] de [[page_nb]]</td>
			</tr>
		</table>
    </page_footer>
    <table border="0.5" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th style="font-size: 90%;">Cantidad</th>
				        <th style="font-size: 90%;">Articulo</th>
                <th style="font-size: 90%;">Precio Unitario</th>
				      <?php if($coniva){ ?>
				        <th style="font-size: 90%;">Alicuota</th>
				      <?php } ?>
				        <th style="font-size: 90%;">Total</th>
            </tr>
        </thead>
        <tbody>
		<?php
			$sumaneto = 0;
			$sumaiva = 0;
			$sumatotal = 0;
			$v= array();
			for ($x=0;$x<count($productos); $x++) {
				if($productos[$x] != "Seleccionar Producto") {
				$con_producto=consulta("SELECT productos.*, iva.porc FROM productos LEFT JOIN iva ON productos.iva = iva.idiva WHERE productos.idproducto = '$productos[$x]'");
				$pr=mysqli_fetch_array($con_producto);
				$nombre = $pr['nombre'];
				$precio = $precios[$x];
				$cantidad = $cantidades[$x];
				$idiva = $pr['iva'];
				$iva = $pr['porc'];
				$caliva = 1 . "." . str_replace('.','',$pr['porc']);
				$resiva = 0 . "." . str_replace('.','',$pr['porc']);
				$total = $precio * $cantidad * $caliva;
				$sumatotal += $total;
				$sumaneto += $precio * $cantidad;
				$sumaiva += $precio * $cantidad * $resiva;
			@	$v[$idiva] += $precio * $cantidad * $resiva;
        if($tipo != "OC"){
  				consulta("UPDATE productos SET stock = stock + $cantidad WHERE idproducto = '$productos[$x]'");
  				$restantes = $pr['stock'] + $cantidades[$x];
  				consulta("INSERT INTO movstock(idfactura,idproducto,precio,iva,cantidad,restantes)
  				VALUES (0,'$productos[$x]','$precio','$iva','$cantidad','$restantes')");
        }
		?>
            <tr>
							<td style="width: 15%;"><?php echo $cantidad; ?></td>
            <?php if($coniva){ ?>
              <td style="width: 40%;"><?php echo $nombre; ?></td>
              <td style="width: 15%;" align="right"><?php echo number_format($precio, 2, ',', '.'); ?></td>
							<td style="width: 15%;" align="right"><?php echo number_format($iva, 2, ',', '.'); ?></td>
							<td style="width: 15%;" align="right"><?php echo number_format($total, 2, ',', '.'); ?></td>
						<?php } else { ?>
							<td style="width: 55%;"><?php echo $nombre; ?></td>
              <td style="width: 15%;" align="right"><?php echo number_format($precio, 2, ',', '.'); ?></td>
							<td style="width: 25%;" align="right"><?php echo number_format($total, 2, ',', '.'); ?></td>
						<?php } ?>
            </tr>
		<?php } }
		$sumatotal = $sumatotal + $conceptos + $retenciones;
		?>
        </tbody>
    </table>
	<div style="position: absolute;bottom: 0;left: 0;">
	<hr />
	<table style="font-size: 80%;margin-top:-5px;">
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;">2. Forma de Pago:</td>
			<td style="width: 35%;"><?php echo $formadepago; ?></td>
			<?php if($coniva){ ?>
			<td style="width: 25%;">Neto:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($sumaneto, 2, ',', '.'); ?></td>
			<?php } else { ?>
			<td style="width: 25%;"></td>
			<td style="width: 25%;" align="right"></td>
			<?php } ?>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<?php if($coniva){ ?>
			<td style="width: 25%;">IVA:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($sumaiva, 2, ',', '.'); ?></td>
			<?php } else { ?>
			<td style="width: 25%;"></td>
			<td style="width: 25%;" align="right"></td>
			<?php } ?>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;">Conceptos no gravados:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($conceptos, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;">Retenciones:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($retenciones, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td colspan=2><hr style="margin-top:0;" /></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;font-size: 130%;font-weight: bold;">Total</td>
			<td style="width: 25%;font-size: 130%;font-weight: bold;" align="right"><?php echo number_format($sumatotal, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 35%;"></td>
			<td style="width: 25%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
	</table>
	</div>
	<?php
		$fecha =  fecha($_POST['fecha'],"-");
		$hora =	date("H:i");
		consulta("INSERT INTO facturas (idcomprador, idproveedor, tipo, fecha, hora, impneto, total, ruta, numero, puntofacturacion, conceptosng, retenciones) VALUES
		('$idcomprador','$idproveedor','$tipo','$fecha','$hora','$sumaneto','$sumatotal','$factura','$numf','$pventa','$conceptos','$retenciones')");
		$idcreada = mysqli_insert_id($conex);
		foreach ($v as $clave => $valor) {
			if($clave > 0){
				consulta("INSERT INTO ivafacturado(idfactura, idiva, monto) VALUES ('$idcreada','$clave','$valor')");
			}
		}
    if($tipo != "OC"){
		    consulta("UPDATE movstock SET idfactura = '$idcreada' WHERE idfactura = 0");
    }
	?>
</page>
