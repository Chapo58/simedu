	<?php
		$desde1 = str_replace("/","",$desde);
		$hasta1 = str_replace("/","",$hasta);
		$año = substr($desde1, -4);
		$año2 = substr($hasta1, -4);
		$mes = substr($desde1, -6, 2);
		$mes2 = substr($hasta1, -6, 2);
		$dia = substr($desde1, -8, 2);
		$dia2 = substr($hasta1, -8, 2);
		$desde2 = $año."-".$mes."-".$dia;
		$hasta2 = $año2."-".$mes2."-".$dia2;
		$d1 = strtotime($desde2);
		$d2 = strtotime($hasta2);
		$min_date = min($d1, $d2);
		$max_date = max($d1, $d2);
		$i = 0;
		while (($min_date = strtotime("+1 MONTH", $min_date)) <= $max_date) {
			$i++;
		}
		$naño1 = $año;
		$naño2 = $año;
		$nmes1 = $mes;
		$nmes2 = $mes;
	?>
<?php if($activo){ ?>
	<h2 align="center">FLUJO DE FONDOS</h2>
	<br />

<table border="1" align="center">
 <thead>
    <tr bgcolor="#ABE2F5">
        <th align="center" colspan="2">ACTIVO</th>
		<?php
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumna[$x] = 0;
				$nmes1 = intval($nmes1);
				$naño1 = intval($naño1);
				if($nmes1 > 12){
					$nmes1 = 01;
					$naño1 = $naño1 + 1;
				}
				if($nmes1 < 10){
					$nmes1 = "0".$nmes1;
				}
		?>
        <th align="center"><?php echo $nmes1."/".$naño1 ?></th>
		<?php
			$nmes1 = intval($nmes1);
			$nmes1 += 1;
			}
		?>
		<th align="center">TOTAL</th>
		<th align="center">%</th>
    </tr>
 </thead>
 <tbody>
	
	<?php
		$con_total = consulta("SELECT SUM(asientos.debe - asientos.haber) as total 
		FROM cuentas 
		LEFT JOIN asientos ON asientos.idcuenta = cuentas.idcuenta 
		WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 1 AND cuentas.imputable = 1");
		$t = mysqli_fetch_array($con_total);
		$total = $t['total'];
		$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa = '$idempresa' AND rubro = 1 AND imputable = 1 ORDER BY codigo ASC");
		while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
			$idcuenta = $c['idcuenta'];
			$codigo = $c['codigo'];
			$denominacion = substr($c['denominacion'], 0, 25);
	?>
		<tr>
			<td><?php echo $codigo; ?></td>
			<td><?php echo $denominacion; ?></td>
	<?php
			$totalfila = 0;
			for ($x = 0; $x <= $i; $x++) {
			$nmes2 = intval($nmes2);
			$naño2 = intval($naño2);
			if($nmes2 > 12){
				$nmes2 = 1;
				$naño2 = $naño2 + 1;
			}
			$con_todo=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber,
								cuentas.codigo, cuentas.denominacion, listaasientos.fecha, listaasientos.idlista FROM
								cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta INNER JOIN
								listaasientos ON asientos.idlista = listaasientos.idlista WHERE
								listaasientos.idempresa = '$idempresa' AND cuentas.idcuenta = '$idcuenta' AND
								MONTH(listaasientos.fecha) = $nmes2
								AND YEAR(listaasientos.fecha) = $naño2 GROUP BY cuentas.codigo,
								asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
			if(mysqli_num_rows($con_todo)>0){
				$debe = 0;
				$haber = 0;
				while ($t = mysqli_fetch_array($con_todo, MYSQLI_ASSOC)) {
					$diferencia = $t['debe'] - $t['haber'];
				}
	?>
			<td align="right"><?php echo number_format($diferencia, 0, ',', '.'); ?></td>
	<?php
			$totalfila += $diferencia;
			$totalcolumna[$x] += $diferencia;
			} else {
	?>
			<td></td>
	<?php
			}
			$nmes2 += 1;
			} // Fin FOR
			$nmes2 = $mes;
			$naño2 = $año;
	?>
			<td><?php echo number_format($totalfila, 0, ',', '.'); ?></td>
			<td><?php echo number_format((($totalfila * 100) / $total), 2, ',', '.'); ?></td>
		</tr>
	<?php
		} // Fin While Cuentas
	?>
	<tr bgcolor="#FFEDB0">
        <td align="right" colspan="2">TOTALES&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumna[$x], 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<td align="right">100%</td>
    </tr>
		<tr bgcolor="#FFB0B0">
        <td align="right" colspan="2">TOTAL ACUMULADO&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<td align="right">100%</td>
    </tr>
 </tbody>
</table>
<pagebreak>
<?php } ?>
<?php if($pasivo){
		$naño1 = $año;
		$naño2 = $año;
		$nmes1 = $mes;
		$nmes2 = $mes;	?>
		<h2 align="center">FLUJO DE FONDOS</h2>
		<br />
<table border="1" align="center">
 <thead>
    <tr bgcolor="#ABE2F5">
        <th align="center" colspan="2">PASIVO</th>
		<?php
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumna[$x] = 0;
				$nmes1 = intval($nmes1);
				$naño1 = intval($naño1);
				if($nmes1 > 12){
					$nmes1 = 01;
					$naño1 = $naño1 + 1;
				}
				if($nmes1 < 10){
					$nmes1 = "0".$nmes1;
				}
		?>
        <th align="center"><?php echo $nmes1."/".$naño1 ?></th>
		<?php
			$nmes1 = intval($nmes1);
			$nmes1 += 1;
			}
		?>
		<th align="center">TOTAL</th>
		<th align="center">%</th>
    </tr>
 </thead>
 <tbody>

	<?php
		$con_total = consulta("SELECT SUM(asientos.debe - asientos.haber) as total 
		FROM cuentas 
		LEFT JOIN asientos ON asientos.idcuenta = cuentas.idcuenta 
		WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 2 AND cuentas.imputable = 1");
		$t = mysqli_fetch_array($con_total);
		$total = $t['total'];
		$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa = '$idempresa' AND rubro = 2 AND imputable = 1 ORDER BY codigo ASC");
		while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
			$idcuenta = $c['idcuenta'];
			$codigo = $c['codigo'];
			$denominacion = substr($c['denominacion'], 0, 25);
	?>
		<tr>
			<td><?php echo $codigo; ?></td>
			<td><?php echo $denominacion; ?></td>
	<?php
			$totalfila = 0;
			for ($x = 0; $x <= $i; $x++) {
			$nmes2 = intval($nmes2);
			$naño2 = intval($naño2);
			if($nmes2 > 12){
				$nmes2 = 1;
				$naño2 = $naño2 + 1;
			}
			$con_todo=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber,
								cuentas.codigo, cuentas.denominacion, listaasientos.fecha, listaasientos.idlista FROM
								cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta INNER JOIN
								listaasientos ON asientos.idlista = listaasientos.idlista WHERE
								listaasientos.idempresa = '$idempresa' AND cuentas.idcuenta = '$idcuenta' AND
								MONTH(listaasientos.fecha) = $nmes2
								AND YEAR(listaasientos.fecha) = $naño2 GROUP BY cuentas.codigo,
								asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
			if(mysqli_num_rows($con_todo)>0){
				$debe = 0;
				$haber = 0;
				while ($t = mysqli_fetch_array($con_todo, MYSQLI_ASSOC)) {
					$diferencia = $t['debe'] - $t['haber'];
				}
	?>
			<td align="right"><?php echo number_format($diferencia, 0, ',', '.'); ?></td>
	<?php
			$totalfila += $diferencia;
			$totalcolumna[$x] += $diferencia;
			} else {
	?>
			<td></td>
	<?php
			}
			$nmes2 += 1;
			} // Fin FOR
			$nmes2 = $mes;
			$naño2 = $año;
	?>
			<td><?php echo number_format($totalfila, 0, ',', '.'); ?></td>
			<td><?php echo number_format((($totalfila * 100) / $total), 2, ',', '.'); ?></td>
		</tr>
	<?php
		} // Fin While Cuentas
	?>
	<tr bgcolor="#FFEDB0">
        <td align="right" colspan="2">TOTALES&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumna[$x], 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
		<tr bgcolor="#FFB0B0">
        <td align="right" colspan="2">TOTAL ACUMULADO&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
 </tbody>
</table>

<pagebreak>

<?php } ?>
<?php if($pneto){
		$naño1 = $año;
		$naño2 = $año;
		$nmes1 = $mes;
		$nmes2 = $mes;?>
		<h2 align="center">FLUJO DE FONDOS</h2>
		<br />
 <table border="1" align="center">
 <thead>
    <tr bgcolor="#ABE2F5">
        <th align="center" colspan="2">PATRIMONIO NETO</th>
		<?php
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumna[$x] = 0;
				$nmes1 = intval($nmes1);
				$naño1 = intval($naño1);
				if($nmes1 > 12){
					$nmes1 = 01;
					$naño1 = $naño1 + 1;
				}
				if($nmes1 < 10){
					$nmes1 = "0".$nmes1;
				}
		?>
        <th align="center"><?php echo $nmes1."/".$naño1 ?></th>
		<?php
			$nmes1 = intval($nmes1);
			$nmes1 += 1;
			}
		?>
		<th align="center">TOTAL</th>
		<th align="center">%</th>
    </tr>
 </thead>
 <tbody>

	<?php
		$con_total = consulta("SELECT SUM(asientos.debe - asientos.haber) as total 
		FROM cuentas 
		LEFT JOIN asientos ON asientos.idcuenta = cuentas.idcuenta 
		WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 3 AND cuentas.imputable = 1");
		$t = mysqli_fetch_array($con_total);
		$total = $t['total'];
		$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa = '$idempresa' AND rubro = 3 AND imputable = 1 ORDER BY codigo ASC");
		while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
			$idcuenta = $c['idcuenta'];
			$codigo = $c['codigo'];
			$denominacion = substr($c['denominacion'], 0, 25);
	?>
		<tr>
			<td><?php echo $codigo; ?></td>
			<td><?php echo $denominacion; ?></td>
	<?php
			$totalfila = 0;
			for ($x = 0; $x <= $i; $x++) {
			$nmes2 = intval($nmes2);
			$naño2 = intval($naño2);
			if($nmes2 > 12){
				$nmes2 = 1;
				$naño2 = $naño2 + 1;
			}
			$con_todo=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber,
								cuentas.codigo, cuentas.denominacion, listaasientos.fecha, listaasientos.idlista FROM
								cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta INNER JOIN
								listaasientos ON asientos.idlista = listaasientos.idlista WHERE
								listaasientos.idempresa = '$idempresa' AND cuentas.idcuenta = '$idcuenta' AND
								MONTH(listaasientos.fecha) = $nmes2
								AND YEAR(listaasientos.fecha) = $naño2 GROUP BY cuentas.codigo,
								asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
			if(mysqli_num_rows($con_todo)>0){
				$debe = 0;
				$haber = 0;
				while ($t = mysqli_fetch_array($con_todo, MYSQLI_ASSOC)) {
					$diferencia = $t['debe'] - $t['haber'];
				}
	?>
			<td align="right"><?php echo number_format($diferencia, 0, ',', '.'); ?></td>
	<?php
			$totalfila += $diferencia;
			$totalcolumna[$x] += $diferencia;
			} else {
	?>
			<td></td>
	<?php
			}
			$nmes2 += 1;
			} // Fin FOR
			$nmes2 = $mes;
			$naño2 = $año;
	?>
			<td><?php echo number_format($totalfila, 0, ',', '.'); ?></td>
			<td><?php echo number_format((($totalfila * 100) / $total), 2, ',', '.'); ?></td>
		</tr>
	<?php
		} // Fin While Cuentas
	?>
	<tr bgcolor="#FFEDB0">
        <td align="right" colspan="2">TOTALES&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumna[$x], 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
		<tr bgcolor="#FFB0B0">
        <td align="right" colspan="2">TOTAL ACUMULADO&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
 </tbody>
</table>

<pagebreak>

<?php } ?>
<?php if($ingygan){
		$naño1 = $año;
		$naño2 = $año;
		$nmes1 = $mes;
		$nmes2 = $mes;?>
		<h2 align="center">FLUJO DE FONDOS</h2>
		<br />
<table border="1" align="center">
 <thead>
    <tr bgcolor="#ABE2F5">
        <th align="center" colspan="2">INGRESOS Y GANANCIAS</th>
		<?php
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumna[$x] = 0;
				$nmes1 = intval($nmes1);
				$naño1 = intval($naño1);
				if($nmes1 > 12){
					$nmes1 = 01;
					$naño1 = $naño1 + 1;
				}
				if($nmes1 < 10){
					$nmes1 = "0".$nmes1;
				}
		?>
        <th align="center"><?php echo $nmes1."/".$naño1 ?></th>
		<?php
			$nmes1 = intval($nmes1);
			$nmes1 += 1;
			}
		?>
		<th align="center">TOTAL</th>
		<th align="center">%</th>
    </tr>
 </thead>
 <tbody>

	<?php
		$con_total = consulta("SELECT SUM(asientos.debe - asientos.haber) as total 
		FROM cuentas 
		LEFT JOIN asientos ON asientos.idcuenta = cuentas.idcuenta 
		WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 4 AND cuentas.imputable = 1");
		$t = mysqli_fetch_array($con_total);
		$total = $t['total'];
		$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa = '$idempresa' AND rubro = 4 AND imputable = 1 ORDER BY codigo ASC");
		while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
			$idcuenta = $c['idcuenta'];
			$codigo = $c['codigo'];
			$denominacion = substr($c['denominacion'], 0, 25);
	?>
		<tr>
			<td><?php echo $codigo; ?></td>
			<td><?php echo $denominacion; ?></td>
	<?php
			$totalfila = 0;
			for ($x = 0; $x <= $i; $x++) {
			$nmes2 = intval($nmes2);
			$naño2 = intval($naño2);
			if($nmes2 > 12){
				$nmes2 = 1;
				$naño2 = $naño2 + 1;
			}
			$con_todo=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber,
								cuentas.codigo, cuentas.denominacion, listaasientos.fecha, listaasientos.idlista FROM
								cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta INNER JOIN
								listaasientos ON asientos.idlista = listaasientos.idlista WHERE
								listaasientos.idempresa = '$idempresa' AND cuentas.idcuenta = '$idcuenta' AND
								MONTH(listaasientos.fecha) = $nmes2
								AND YEAR(listaasientos.fecha) = $naño2 GROUP BY cuentas.codigo,
								asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
			if(mysqli_num_rows($con_todo)>0){
				$debe = 0;
				$haber = 0;
				while ($t = mysqli_fetch_array($con_todo, MYSQLI_ASSOC)) {
					$diferencia = $t['debe'] - $t['haber'];
				}
	?>
			<td align="right"><?php echo number_format($diferencia, 0, ',', '.'); ?></td>
	<?php
			$totalfila += $diferencia;
			$totalcolumna[$x] += $diferencia;
			} else {
	?>
			<td></td>
	<?php
			}
			$nmes2 += 1;
			} // Fin FOR
			$nmes2 = $mes;
			$naño2 = $año;
	?>
			<td><?php echo number_format($totalfila, 0, ',', '.'); ?></td>
			<td><?php echo number_format((($totalfila * 100) / $total), 2, ',', '.'); ?></td>
		</tr>
	<?php
		} // Fin While Cuentas
	?>
	<tr bgcolor="#FFEDB0">
        <td align="right" colspan="2">TOTALES&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumna[$x], 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
		<tr bgcolor="#FFB0B0">
        <td align="right" colspan="2">TOTAL ACUMULADO&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
 </tbody>
</table>

<pagebreak>

<?php } ?>
<?php if($gyp){
		$naño1 = $año;
		$naño2 = $año;
		$nmes1 = $mes;
		$nmes2 = $mes;?>
		<h2 align="center">FLUJO DE FONDOS</h2>
		<br />
<table border="1" align="center">
 <thead>
    <tr bgcolor="#ABE2F5">
        <th align="center" colspan="2">GASTOS Y PERDIDAS</th>
		<?php
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumna[$x] = 0;
				$nmes1 = intval($nmes1);
				$naño1 = intval($naño1);
				if($nmes1 > 12){
					$nmes1 = 01;
					$naño1 = $naño1 + 1;
				}
				if($nmes1 < 10){
					$nmes1 = "0".$nmes1;
				}
		?>
        <th align="center"><?php echo $nmes1."/".$naño1 ?></th>
		<?php
			$nmes1 = intval($nmes1);
			$nmes1 += 1;
			}
		?>
		<th align="center">TOTAL</th>
		<th align="center">%</th>
    </tr>
 </thead>
 <tbody>

	<?php
		$con_total = consulta("SELECT SUM(asientos.debe - asientos.haber) as total 
		FROM cuentas 
		LEFT JOIN asientos ON asientos.idcuenta = cuentas.idcuenta 
		WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 5 AND cuentas.imputable = 1");
		$t = mysqli_fetch_array($con_total);
		$total = $t['total'];
		$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa = '$idempresa' AND rubro = 5 AND imputable = 1 ORDER BY codigo ASC");
		while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
			$idcuenta = $c['idcuenta'];
			$codigo = $c['codigo'];
			$denominacion = substr($c['denominacion'], 0, 25);
	?>
		<tr>
			<td><?php echo $codigo; ?></td>
			<td><?php echo $denominacion; ?></td>
	<?php
			$totalfila = 0;
			for ($x = 0; $x <= $i; $x++) {
			$nmes2 = intval($nmes2);
			$naño2 = intval($naño2);
			if($nmes2 > 12){
				$nmes2 = 1;
				$naño2 = $naño2 + 1;
			}
			$con_todo=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber,
								cuentas.codigo, cuentas.denominacion, listaasientos.fecha, listaasientos.idlista FROM
								cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta INNER JOIN
								listaasientos ON asientos.idlista = listaasientos.idlista WHERE
								listaasientos.idempresa = '$idempresa' AND cuentas.idcuenta = '$idcuenta' AND
								MONTH(listaasientos.fecha) = $nmes2
								AND YEAR(listaasientos.fecha) = $naño2 GROUP BY cuentas.codigo,
								asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
			if(mysqli_num_rows($con_todo)>0){
				$debe = 0;
				$haber = 0;
				while ($t = mysqli_fetch_array($con_todo, MYSQLI_ASSOC)) {
					$diferencia = $t['debe'] - $t['haber'];
				}
	?>
			<td align="right"><?php echo number_format($diferencia, 0, ',', '.'); ?></td>
	<?php
			$totalfila += $diferencia;
			$totalcolumna[$x] += $diferencia;
			} else {
	?>
			<td></td>
	<?php
			}
			$nmes2 += 1;
			} // Fin FOR
			$nmes2 = $mes;
			$naño2 = $año;
	?>
			<td><?php echo number_format($totalfila, 0, ',', '.'); ?></td>
			<td><?php echo number_format((($totalfila * 100) / $total), 2, ',', '.'); ?></td>
		</tr>
	<?php
		} // Fin While Cuentas
	?>
	<tr bgcolor="#FFEDB0">
        <td align="right" colspan="2">TOTALES&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumna[$x], 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
		<tr bgcolor="#FFB0B0">
        <td align="right" colspan="2">TOTAL ACUMULADO&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
 </tbody>
</table>

<pagebreak>

<?php } ?>
<?php if($costo){
		$naño1 = $año;
		$naño2 = $año;
		$nmes1 = $mes;
		$nmes2 = $mes;?>
		<h2 align="center">FLUJO DE FONDOS</h2>
		<br />
<table border="1" align="center">
 <thead>
    <tr bgcolor="#ABE2F5">
        <th align="center" colspan="2">COSTO</th>
		<?php
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumna[$x] = 0;
				$nmes1 = intval($nmes1);
				$naño1 = intval($naño1);
				if($nmes1 > 12){
					$nmes1 = 01;
					$naño1 = $naño1 + 1;
				}
				if($nmes1 < 10){
					$nmes1 = "0".$nmes1;
				}
		?>
        <th align="center"><?php echo $nmes1."/".$naño1 ?></th>
		<?php
			$nmes1 = intval($nmes1);
			$nmes1 += 1;
			}
		?>
		<th align="center">TOTAL</th>
		<th align="center">%</th>
    </tr>
 </thead>
 <tbody>

	<?php
		$con_total = consulta("SELECT SUM(asientos.debe - asientos.haber) as total 
		FROM cuentas 
		LEFT JOIN asientos ON asientos.idcuenta = cuentas.idcuenta 
		WHERE cuentas.idempresa = '$idempresa' AND cuentas.rubro = 6 AND cuentas.imputable = 1");
		$t = mysqli_fetch_array($con_total);
		$total = $t['total'];
		$con_cuentas=consulta("SELECT * FROM cuentas WHERE idempresa = '$idempresa' AND rubro = 6 AND imputable = 1 ORDER BY codigo ASC");
		while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
			$idcuenta = $c['idcuenta'];
			$codigo = $c['codigo'];
			$denominacion = substr($c['denominacion'], 0, 25);
	?>
		<tr>
			<td><?php echo $codigo; ?></td>
			<td><?php echo $denominacion; ?></td>
	<?php
			$totalfila = 0;
			for ($x = 0; $x <= $i; $x++) {
			$nmes2 = intval($nmes2);
			$naño2 = intval($naño2);
			if($nmes2 > 12){
				$nmes2 = 1;
				$naño2 = $naño2 + 1;
			}
			$con_todo=consulta("SELECT asientos.idcuenta, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber,
								cuentas.codigo, cuentas.denominacion, listaasientos.fecha, listaasientos.idlista FROM
								cuentas INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta INNER JOIN
								listaasientos ON asientos.idlista = listaasientos.idlista WHERE
								listaasientos.idempresa = '$idempresa' AND cuentas.idcuenta = '$idcuenta' AND
								MONTH(listaasientos.fecha) = $nmes2
								AND YEAR(listaasientos.fecha) = $naño2 GROUP BY cuentas.codigo,
								asientos.idcuenta, cuentas.denominacion ORDER BY cuentas.codigo ASC");
			if(mysqli_num_rows($con_todo)>0){
				$debe = 0;
				$haber = 0;
				while ($t = mysqli_fetch_array($con_todo, MYSQLI_ASSOC)) {
					$diferencia = $t['debe'] - $t['haber'];
				}
	?>
			<td align="right"><?php echo number_format($diferencia, 0, ',', '.'); ?></td>
	<?php
			$totalfila += $diferencia;
			$totalcolumna[$x] += $diferencia;
			} else {
	?>
			<td></td>
	<?php
			}
			$nmes2 += 1;
			} // Fin FOR
			$nmes2 = $mes;
			$naño2 = $año;
	?>
			<td><?php echo number_format($totalfila, 0, ',', '.'); ?></td>
			<td><?php echo number_format((($totalfila * 100) / $total), 2, ',', '.'); ?></td>
		</tr>
	<?php
		} // Fin While Cuentas
	?>
	<tr bgcolor="#FFEDB0">
        <td align="right" colspan="2">TOTALES&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumna[$x], 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
		<tr bgcolor="#FFB0B0">
        <td align="right" colspan="2">TOTAL ACUMULADO&nbsp;&nbsp;</td>
		<?php
			$totalcolumnas = 0;
			for ($x = 0; $x <= $i; $x++) {
				$totalcolumnas += $totalcolumna[$x];

		?>
        <td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
		<?php
			}
		?>
		<td align="right"><?php  echo number_format($totalcolumnas, 0, ',', '.'); ?></td>
    </tr>
 </tbody>
</table>

<?php } ?>
