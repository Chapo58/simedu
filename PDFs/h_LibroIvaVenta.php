<?php
session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);

// Subtotal Responsables Inscriptos
$con_exen=consulta("SELECT SUM(total) as totalexento FROM facturas WHERE idcomprador='$idempresa' AND tipo = 'A'");
$exe=mysqli_fetch_array($con_exen);
$totalexentos = $exe['totalexento'];
?>
<style>
<?php 
require_once('../css/bootstrap.css');
require_once('../css/style.css');
?>
</style>
<page backtop="30mm" backbottom="10mm" backleft="5mm" backright="5mm" orientation="landscape">
    <page_header>
        <table style="margin-left:80px;">
            <tr>
                <td rowspan="4" width="100"><img src=<?php echo $imagenempresa; ?> width="75" height="75" class="img-circle img-user tooltipster" title="Imagen de perfil" alt="Imagen de Perfil"></td>
            </tr>
			<tr>
				<td height="20" width="300"><u>Empresa</u>: <?php echo $rsocial; ?></td>
				<td height="20" width="300"><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
			</tr>
			<tr>
				<td height="20" width="300"><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
				<td height="20" width="300"><u>Fecha Impresion</u>: <?php echo date('d/m/Y'); ?></td>
			</tr>
			<tr>
				<td height="20" width="300"><u>Domicilio</u>: <?php echo $e['domicilio']; ?></td>
			</tr>
        </table>
    </page_header>
    <page_footer>
        <div align="right">Pagina [[page_cu]]/[[page_nb]]</div>
    </page_footer>
	<table border="1" align="center">
		<tr bgcolor="#DDFFE6">
            <th align="center" height="20" style="width: 90%;">
			LIBRO DE IVA VENTA
			</th>
			<?php if($inicio) { ?>
			<th align="center" height="20" style="width: 10%;">HOJA Nº <?php echo $inicio; ?></th>
			<?php $inicio++; } ?>
        </tr>
	</table>
	<p align="center">Correspondiente al <?php echo $fecha; ?></p>
	<br /><br />
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th>Tipo de Comprobante</th>
                <th>Fecha</th>
				<th>N° Comprobante</th>
				<th>Cliente</th>
				<th>C.U.I.T.</th>
				<th>Importe Neto</th>
			<?php	
				$con_iva=consulta("SELECT denominacion FROM iva WHERE idempresa='$idempresa' ORDER BY ntasa ASC");
				while ($i = mysqli_fetch_array($con_iva, MYSQLI_ASSOC)) {
			?>
				<th><?php echo $i['denominacion']; ?></th>
			<?php } ?>
				<th>Total</th>
            </tr>
        </thead>
        <tbody>
			<?php	
				$totalneto = 0;
				$totaltotal = 0;
				$v= array(); // ARRAY IVAS TABLAS
				$vf= array(); // ARRAY IVAS FACTURAS
				$n= array(); // ARRAY DE NETOS FACTURAS
				$vd= array(); // ARRAY IVAS NOTAS DE DEBITO
				$nd= array(); // ARRAY DE NETOS NOTAS DE DEBITO
				$con_facturas=consulta("SELECT f.idfactura, f.tipo, f.fecha, f.impneto, f.total, f.numero, f.puntofacturacion, f.idcomprador, f.idcliente, c.rsocial as crsocial, c.cuit as ccuit, e.rsocial, e.cuit
									FROM facturas as f LEFT JOIN empresas as e ON f.idcomprador = e.idempresa
									LEFT JOIN clientes as c ON f.idcliente = c.idcliente 
									WHERE f.idvendedor = '$idempresa' AND YEAR(f.fecha) = '$año' AND MONTH(f.fecha) = '$mes' AND f.tipo <> 'X'
									ORDER BY f.fecha ASC");
				while ($f = mysqli_fetch_array($con_facturas, MYSQLI_ASSOC)) {
					$idfactura = $f['idfactura'];
					$tipo = $f['tipo'];
					if($tipo == "NC"){
						$tipocomp = "NCC";
						$esfactura = false;
						
						$impneto = number_format(-1 * $f['impneto'], 2, ',', '.');
						$total = number_format(-1 * $f['total'], 2, ',', '.');
						$totalneto += -1 * $f['impneto'];
						$totaltotal += -1 * $f['total'];
					} else {
						if($tipo == "ND") {
							$tipocomp = "NDC";
							$esfactura = false;
						} else {
							$tipocomp = "FCV";
							$esfactura = true;
						}
						$impneto = number_format($f['impneto'], 2, ',', '.');
						$total = number_format($f['total'], 2, ',', '.');
						$totalneto += $f['impneto'];
						$totaltotal += $f['total'];
					}
					$fecha = date("d/m/Y",strtotime($f['fecha']));
					$numero = str_pad($f['numero'], 8, "0", STR_PAD_LEFT);
					$pfacturacion = str_pad($f['puntofacturacion'], 4, "0", STR_PAD_LEFT);
					if($f['idcomprador'] == 0){
						$rsocial = $f['crsocial'];
						$cuit = $f['ccuit'];
					} else {
						$rsocial = $f['rsocial'];
						$cuit = $f['cuit'];
					}
			?>
			<tr>
				<td><?php echo $tipocomp; ?></td>
                <td><?php echo $fecha; ?></td>
				<td><?php if($esfactura) echo $tipo . " " . $pfacturacion . "-" . $numero; ?></td>
				<td><?php echo $rsocial; ?></td>
				<td><?php echo $cuit; ?></td>
				<td><?php echo $impneto; ?></td>
				<?php	
					$con_ivas=consulta("SELECT iva.ntasa, iva.denominacion, IFNULL(ivaf.monto, 0) as monto FROM iva
					LEFT JOIN (SELECT monto, idiva FROM ivafacturado WHERE idfactura = '$idfactura') as ivaf
                    ON ivaf.idiva = iva.idiva WHERE iva.idempresa = '$idempresa' 
					ORDER BY iva.ntasa ASC");
					while ($is = mysqli_fetch_array($con_ivas, MYSQLI_ASSOC)) {
						
					$ntasa = $is['denominacion'];
					
					if($tipo == "NC"){
					@	$v[$ntasa] += -1 * $is['monto'];
						$montoiva = number_format(-1 * $is['monto'], 2, ',', '.');
					} elseif($tipo == "A" || $tipo == "B" || $tipo == "C") {
					@	$v[$ntasa] += $is['monto'];
						$montoiva = number_format($is['monto'], 2, ',', '.');
						if($is['monto'] > 0){
							@	$n[$ntasa] += $f['impneto'];
							@	$vf[$ntasa] += $is['monto'];
						}
					} else {
					@	$v[$ntasa] += $is['monto'];
						$montoiva = number_format($is['monto'], 2, ',', '.');
						if($is['monto'] > 0){
							@	$nd[$ntasa] += $f['impneto'];
							@	$vd[$ntasa] += $is['monto'];
						}
					}
				?>
				<td><?php echo $montoiva; ?></td>
				<?php } ?>
				<td><?php echo $total; ?></td>
            </tr>
			<?php } ?>
        </tbody>
    </table>
	<br>
	<table align="center" style="font-size: 80%;">
		<tr>
			<td style="width: 15%;">Neto:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($totalneto, 2, ',', '.'); ?></td>
		</tr>
		<?php foreach ($v as $clave => $valor) { ?>
		<tr>
			<td style="width: 15%;">IVA Tasa <?php echo $clave; ?>:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($valor, 2, ',', '.'); ?></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan=2><hr style="margin-top:0;" /></td>
		</tr>
		<tr>
			<td style="width: 15%;font-size: 130%;font-weight: bold;">Total</td>
			<td style="width: 25%;font-size: 130%;font-weight: bold;" align="right"><?php echo number_format($totaltotal, 2, ',', '.'); ?></td>
		</tr>
	</table>
</page>
<page backtop="30mm" backbottom="10mm" backleft="20mm" backright="20mm">
	<table border="1" align="center">
		<tr bgcolor="#DDFFE6">
            <th align="center" height="20" style="width: 100%;">
			DETALLES DE VENTAS
			</th>
        </tr>
	</table>
	
	<table border="1" align="center">
		<tr>
			<th colspan="4" height="20">TOTALES DE OPERACIONES QUE GENERAN DEBITO FISCAL</th>
		</tr>
		<tr>
			<td height="20"></td>
			<th>NETO GRAVADO</th>
			<th>I.V.A.</th>
			<th>TOTAL</th>
		</tr>
		<?php 
			$sumanetos = 0;
			$sumaivas = 0;
			$sumatotal = 0;
			foreach ($vf as $clave => $valor) { 
			@	$sumanetos += $n[$clave];
				$sumaivas += $valor;
		?>
		<tr>
			<td style="width: 40%;" height="20"><?php echo $clave; ?></td>
			<td style="width: 20%;"><?php echo number_format(@$n[$clave], 2, ',', '.'); ?></td>
			<td style="width: 20%;"><?php echo number_format($valor, 2, ',', '.'); ?></td>
			<td style="width: 20%;"><?php echo number_format(@$n[$clave] + $valor, 2, ',', '.'); ?></td>
		</tr>
		<?php } ?>
		<tr>
			<th>SUBTOTAL Gravado</th>
			<td><?php echo number_format($sumanetos, 2, ',', '.'); ?></td>
			<td><?php echo number_format($sumaivas, 2, ',', '.'); ?></td>
			<td><?php echo number_format($sumanetos + $sumaivas, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<th height="20">SUBTOTAL No Gravado - Exento - No alcanzado</th>
			<td colspan="2"></td>
			<td><?php echo number_format($totalexentos, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<th colspan="4" height="20">TOTALES DE OPERACIONES QUE GENERAN RESTITUCION DE DEBITO FISCAL</th>
		</tr>
		<?php 
			$sumanetos = 0;
			$sumaivas = 0;
			$sumatotal = 0;
			foreach ($vd as $clave => $valor) { 
			@	$sumanetos += $nd[$clave];
				$sumaivas += $valor;
		?>
		<tr>
			<td style="width: 40%;" height="20"><?php echo $clave; ?></td>
			<td style="width: 20%;"><?php echo number_format(@$nd[$clave], 2, ',', '.'); ?></td>
			<td style="width: 20%;"><?php echo number_format($valor, 2, ',', '.'); ?></td>
			<td style="width: 20%;"><?php echo number_format(@$nd[$clave] + $valor, 2, ',', '.'); ?></td>
		</tr>
		<?php } ?>
		<tr>
			<th>SUBTOTAL Gravado N/C</th>
			<td><?php echo number_format($sumanetos, 2, ',', '.'); ?></td>
			<td><?php echo number_format($sumaivas, 2, ',', '.'); ?></td>
			<td><?php echo number_format($sumanetos + $sumaivas, 2, ',', '.'); ?></td>
		</tr>
	</table>
	
</page>