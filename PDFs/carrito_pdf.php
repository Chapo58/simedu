<style>
<?php 
require_once('../css/bootstrap.css');
require_once('../css/style.css');
?>
</style>
<page backtop="70mm" backbottom="20mm" backleft="5mm" backright="5mm">
    <page_header>
        <table style="margin-left:18px;font-size: 80%;">
            <tr>
                <td><img src="<?php echo "../".$v['imagenempresa']; ?>" width="191" height="98" title="Logo de la Empresa" alt="Logo de la Empresa"></td>
				<td style="width: 23%;font-size: 400%;font-weight: bold;" align="right"><strong><?php echo $tipo; ?></strong></td>
				<td style="width: 45%;font-size: 130%;" align="right">FACTURA N 0001-<?php echo str_pad($nfactura, 8, "0", STR_PAD_LEFT); ?></td>
            </tr>
		</table>
		<table style="margin-left:18px;font-size: 80%;">
			<tr>
				<td style="width: 60%;"><?php echo $v['rsocial']; ?></td>
				<td style="width: 35%;" colspan="2"></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['domicilio']; ?></td>
				<td style="width: 15%;font-weight: bold;">Emision:</td>
				<td style="width: 20%;"><?php echo date('d/m/Y'); ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['localidad']; ?></td>
				<td style="width: 15%;font-weight: bold;">CUIT:</td>
				<td style="width: 20%;"><?php echo $v['cuit']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['provincia']; if($v['pais'] != ""){ echo ", ".$v['pais']; } ?></td>
				<td style="width: 15%;font-weight: bold;">II.BB.:</td>
				<td style="width: 20%;"><?php echo $v['ingbrutos']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $v['telefono']; ?></td>
				<td style="width: 15%;font-weight: bold;">Inicio Actividades:</td>
				<td style="width: 20%;"><?php echo $v['inicioact']; ?></td>
			</tr>
			<tr>
				<td><?php echo $v['emailcontacto']; ?></td>
				<td style="width: 15%;font-weight: bold;">I.V.A.:</td>
				<td style="width: 20%;"><?php echo $v['iva_descr']; ?></td>
			</tr>
        </table>
		<div style="margin-left:18px;margin-top:-5px;">
		<hr style="margin-top:0;" />
		</div>
		<table style="margin-left:18px;font-size: 80%;margin-top:-5px;">
			<tr>
				<td style="width: 60%;font-size: 120%;font-weight: bold;"><?php echo $c['rsocial']; ?></td>
				<td style="width: 15%;font-weight: bold;">Cuenta</td>
				<td style="width: 20%;"><?php echo $idusuario; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['domicilio'];; ?></td>
				<td style="width: 15%;font-weight: bold;">CUIT:</td>
				<td style="width: 20%;"><?php echo $c['cuit'];; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['localidad']; ?></td>
				<td style="width: 15%;font-weight: bold;">II.BB.:</td>
				<td style="width: 20%;"><?php echo $c['ingbrutos']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['provincia']; if($c['pais'] != ""){ echo ", ".$c['pais']; } ?></td>
				<td style="width: 15%;font-weight: bold;">I.V.A.:</td>
				<td style="width: 20%;"><?php echo $c['iva_descr']; ?></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['telefono']; ?></td>
				<td colspan=2></td>
			</tr>
			<tr>
				<td style="width: 60%;"><?php echo $c['emailcontacto']; ?></td>
				<td style="width: 15%;"></td>
				<td style="width: 20%;"></td>
			</tr>
		</table>
    </page_header>
    <page_footer>
		<hr />
		<table style="margin-left:18px;font-size: 80%;">
			<tr>
				<td style="width: 20%;">Fecha: <?php echo date('d/m/Y'); ?></td> 
				<td style="width: 20%;">Hora: <?php echo date('H:i:s'); ?></td>
				<td style="width: 20%;" align="center">Usuario: <?php echo $idusuario; ?></td>
				<td style="width: 35%;" align="right">Pagina [[page_cu]] de [[page_nb]]</td>
			</tr>
		</table>
    </page_footer>
    <table border="0.5" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th style="font-size: 90%;">Cantidad</th>
				<th style="font-size: 90%;">Articulo</th>
				<th style="font-size: 90%;">Precio Unitario</th>
				<th style="font-size: 90%;">Alicuota</th>
				<th style="font-size: 90%;">Total</th>
            </tr>
        </thead>
        <tbody>
		<?php 
			$con_productos=consulta("SELECT carritos.*, productos.nombre, productos.preciovta, productos.iva
							FROM carritos LEFT JOIN productos ON carritos.idproducto = productos.idproducto 
							WHERE carritos.idcomprador='$idempresa' AND idvendedor = '$idvendedor'");
							$sumaneto = 0;
							$sumaiva = 0;
							$sumatotal = 0;
							while ($pr = mysqli_fetch_array($con_productos, MYSQLI_ASSOC)) {
								$idp = $pr['idproducto'];
								$nombre = $pr['nombre'];
								$precio = $pr['preciovta'];
								$cantidad = $pr['cantidad'];
								$iva = $pr['iva'];
								$caliva = 1 . "." . str_replace('.','',$pr['iva']);
								$resiva = 0 . "." . str_replace('.','',$pr['iva']);
								$total = $precio * $cantidad * $caliva;
								$sumatotal += $precio * $cantidad * $caliva;
								$sumaneto += $precio * $cantidad;
								$sumaiva += $precio * $cantidad * $resiva;
								
								consulta("UPDATE productos SET stock = stock-$cantidad WHERE idproducto = '$idp'");
						?>
                        <tr>
							<td style="width: 15%;"><?php echo $cantidad; ?></td>
                            <td style="width: 40%;"><?php echo $nombre; ?></td>
                            <td style="width: 15%;" align="right"><?php echo number_format($precio, 2, ',', '.'); ?></td>
							<td style="width: 15%;" align="right"><?php echo number_format($iva, 2, ',', '.'); ?></td>
							<td style="width: 15%;" align="right"><?php echo number_format($total, 2, ',', '.'); ?></td>
                        </tr>
						<?php } ?>
        </tbody>
    </table>
	
	<div style="position: absolute;bottom: 0;left: 0;">
	<hr />
	<table style="font-size: 80%;margin-top:-5px;">
		<tr>
			<td style="width: 15%;"><!--1. Validez:--></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;">1. Forma de Pago:</td>
			<td style="width: 45%;"><?php echo $formadepago; ?></td>
			<td style="width: 15%;">Neto:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($sumaneto, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;">IVA:</td>
			<td style="width: 25%;" align="right"><?php echo number_format($sumaiva, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;"></td>
			<td style="width: 25%;"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td colspan=2><hr style="margin-top:0;" /></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;font-size: 130%;font-weight: bold;">Total</td>
			<td style="width: 25%;font-size: 130%;font-weight: bold;" align="right"><?php echo number_format($sumatotal, 2, ',', '.'); ?></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
		<tr>
			<td style="width: 15%;"></td>
			<td style="width: 45%;"></td>
			<td style="width: 15%;"></td>
			<td style="width: 25%;" align="right"></td>
		</tr>
	</table>
	</div>
	<?php
		$fecha =  date("Y-m-d");
		$hora =	date("H:i");
		consulta("INSERT INTO facturas (idcomprador, idvendedor, tipo, fecha, hora, total, ruta, numero) VALUES
		('$idcomprador','$idvendedor','$tipo','$fecha','$hora','$sumatotal','$factura','$nfactura')");
	?>
</page>