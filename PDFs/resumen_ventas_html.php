	<h2 align="center">RESUMEN DE VENTAS</h2>
	<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
	<br>
	<table border="1" align="center">
			<thead>
				<tr bgcolor="#ABE2F5">
					<th>Tipo de Comprobante</th>
					<th>Fecha</th>
					<th>N° Comprobante</th>
					<th>Cliente</th>
					<th>Debe</th>
					<th>Haber</th>
					<th>Saldo</th>
				</tr>
			</thead>
			<tbody>
		<?php
			$totaldebe = 0;
			$totalhaber = 0;
			$saldo = 0;
			$con_facturas=consulta("SELECT f.idfactura, f.tipo, f.fecha, f.total, f.numero, f.puntofacturacion, f.idcomprador, f.idcliente, c.rsocial as crsocial, c.cuit as ccuit, e.rsocial, e.cuit
								FROM facturas as f LEFT JOIN empresas as e ON f.idcomprador = e.idempresa
								LEFT JOIN clientes as c ON f.idcliente = c.idcliente
								WHERE f.idvendedor = '$idempresa' AND (f.fecha BETWEEN '$condesde' AND '$conhasta') AND f.tipo <> 'X' $where
								ORDER BY f.fecha ASC");
			while ($f = mysqli_fetch_array($con_facturas, MYSQLI_ASSOC)) {
				$idfactura = $f['idfactura'];
				$tipo = $f['tipo'];
				if($tipo == "NC"){
					$tipocomp = "NCC";
					$totaldebe += $debe = 0;
					$totalhaber += $haber = $f['total'];
				} else {
					if($tipo == "ND") {
						$tipocomp = "NDC";
						$totaldebe += $debe = $f['total'];
						$totalhaber += $haber = 0;
					} elseif($tipo == "R") {
						$tipocomp = "REC";
						$totaldebe += $debe = 0;
						$totalhaber += $haber = $f['total'];
					} else {
						$tipocomp = "FCV";
						$totaldebe += $debe = $f['total'];
						$totalhaber += $haber = 0;
					}
				}
				$saldo += $haber - $debe;
				$fecha = date("d/m/Y",strtotime($f['fecha']));
				$numero = str_pad($f['numero'], 8, "0", STR_PAD_LEFT);
				$pfacturacion = str_pad($f['puntofacturacion'], 4, "0", STR_PAD_LEFT);
				if($f['idcomprador'] == 0){
					$rsocial = $f['crsocial'];
					$cuit = $f['ccuit'];
				} else {
					$rsocial = $f['rsocial'];
					$cuit = $f['cuit'];
				}
		?>
		<tr>
			<td><?php echo $tipocomp; ?></td>
			<td><?php echo $fecha; ?></td>
			<td><?php echo $tipo . " " . $pfacturacion . "-" . $numero; ?></td>
			<td><?php echo $rsocial; ?></td>
			<td><?php echo $debe; ?></td>
			<td><?php echo $haber; ?></td>
			<td><?php echo $saldo; ?></td>
		</tr>
		<?php } ?>
			</tbody>
			<tfoot>
				<tr bgcolor="#F79B9B">
					<td colspan="4" align="right">TOTALES&nbsp;&nbsp;</td>
					<td align="right"><?php echo number_format($totaldebe, 2, ',', '.'); ?></td>
					<td align="right"><?php echo number_format($totalhaber, 2, ',', '.'); ?></td>
					<td align="right"><?php echo number_format($saldo, 2, ',', '.'); ?></td>
				</tr>
			</tfoot>
	</table>
