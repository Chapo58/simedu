<?php
if($e['domicilio']){
	$ubicacion = $e['domicilio'];
}
if($e['localidad']){
	$ubicacion = $ubicacion . ", " . $e['localidad'];
}
if($e['provincia']){
	$ubicacion = $ubicacion . ", " . $e['provincia'];
}
if($e['pais']){
	$ubicacion = $ubicacion . ", " . $e['pais'];
}
if($e['inicioact'] != "0000-00-00"){
	$inicioact = str_replace ("-","/",date("d-m-Y",strtotime($e['inicioact'])));
} else {
	$inicioact = "No Especificado";
}
if($e['telefono']){
	$telefono = $e['telefono'];
} else {
	$telefono = "No Especificado";
}
if($e['ingbrutos']){
	$ingbru = number_format($e['ingbrutos'], 0, ',', '.');
} else {
	$ingbru = "No Especificado";
}
if($e['nombrecontacto']){
	$contacto = $e['nombrecontacto'];
}
if($e['telefonocontacto']){
	$contacto = $contacto . " - " . $e['telefonocontacto'];
}
if($e['emailcontacto']){
	$contacto = $contacto . " - " . $e['emailcontacto'];
}
?>
	<br>
    <table border="1" style="font-size:110%;width:100%;" align="center">
        <tbody>
					<tr>
							<td rowspan="11" width="100"><img src=<?php echo $imagenempresa; ?> width="250" height="250" class="tooltipster" title="Imagen de empresa" alt="Imagen de Empresa"></td>
					</tr>
					<tr>
						<td><u>Empresa</u>: <?php echo $rsocial; ?></td>
					</tr>
					<tr>
						<td><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
					</tr>
					<tr>
						<td><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
					</tr>
					<tr>
						<td><u>Inicio de Actividades: </u>: <?php echo $inicioact; ?></td>
					</tr>
					<tr>
						<td><u>Ubicacion</u>: <?php echo $ubicacion; ?></td>
					</tr>
					<tr>
						<td><u>Telefono</u>: <?php echo $telefono; ?></td>
					</tr>
					<tr>
						<td><u>Ingresos Brutos</u>: <?php echo $ingbru; ?></td>
					</tr>
					<tr>
						<td><u>Condicion IVA</u>: <?php echo $e['iva_descr']; ?></td>
					</tr>
					<tr>
						<td><u>Web</u>: <?php echo $e['web']; ?></td>
					</tr>
					<tr>
						<td><u>Contacto</u>: <?php echo $contacto; ?></td>
					</tr>
        </tbody>
    </table>
		<br/><br/><br/>
		<h2 align='center'>FICHAS</h2>
		<?php
		$con_fichas=consulta("SELECT * FROM fichas WHERE idempresa = $idempresa");
		while ($f = mysqli_fetch_array($con_fichas, MYSQLI_ASSOC)) {
			$imagenficha = '../' . $f['imagen'];
			$titulo = $f['titulo'];
			$descripcion = $f['texto'];
		?>
		<table border="1" style="width:100%;" align="center">
					<tr>
						<th style="font-size:150%;"><?php echo $titulo; ?></th>
					</tr>
					<tr>
							<td align="center"><img src=<?php echo $imagenficha; ?> width="200" height="200" class="tooltipster" title="Imagen de ficha" alt="Imagen de Ficha"></td>
					</tr>
					<tr>
						<td style="text-align:justify"><?php echo $descripcion; ?></td>
					</tr>
		</table>
		<?php } ?>
