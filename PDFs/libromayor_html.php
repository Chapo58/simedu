  <h2 align="center">LIBRO MAYOR</h2>
	<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
	<br />
    <table border="1" align="center">
		<?php
			$con_cuentas=consulta("SELECT cuentas.idcuenta, cuentas.codigo, cuentas.denominacion, asientos.idcuenta FROM cuentas
								   INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta
								   WHERE idempresa='$idempresa' AND imputable = 1 AND cuentas.codigo >= $cuentadesde AND cuentas.codigo <= $cuentahasta
								   GROUP BY cuentas.idcuenta ORDER BY cuentas.codigo ASC");
			$sumatotaldebe = 0;
			$sumatotalhaber = 0;
			$sumatotaldiferencia = 0;
			while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
				$idcuenta = $c['idcuenta'];
				$codigo = $c['codigo'];
				$denominacion = $c['denominacion'];
				$sumadebe = 0;
				$sumahaber = 0;
				$sumadiferencia = 0;
		?>
			<tr bgcolor="#CECECE">
				<td colspan="1">Cuenta</td>
				<td colspan="2">Descripcion</td>
				<td colspan="3"></td>
            </tr>
			<tr bgcolor="#E0E0E0">
				<td colspan="1"><?php echo $codigo; ?></td>
				<td colspan="2"><?php echo $denominacion; ?></td>
				<td colspan="3"></td>
            </tr>
			<?php if($condetalle) { ?>
			<tr>
				<td style="width: 10%;">Asiento</td>
				<td style="width: 15%;">Fecha</td>
				<td style="width: 30%;">Detalle</td>
				<td style="width: 15%;">Debe</td>
				<td style="width: 15%;">Haber</td>
				<td style="width: 15%;">Saldo</td>
			</tr>
			<?php } ?>
		<?php
			$con_asientos=consulta("SELECT listaasientos.denominacion, listaasientos.nlista, listaasientos.fecha, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber
									FROM listaasientos INNER JOIN asientos ON asientos.idlista = listaasientos.idlista
									WHERE listaasientos.idempresa= '$idempresa' AND asientos.idcuenta = '$idcuenta'
									AND (listaasientos.fecha BETWEEN '$condesde' AND '$conhasta')
                                    GROUP BY asientos.idlista ORDER BY listaasientos.fecha ASC, listaasientos.nlista ASC");
			while ($a = mysqli_fetch_array($con_asientos, MYSQLI_ASSOC)) {
				$nombrelista = substr($a['denominacion'], 0, 30);
				$nlista = $a['nlista'];
				$fecha = fecha($a['fecha'],"/");
				$debe = $a['debe'];
				$haber = $a['haber'];
				$sumadebe += $debe;
				$sumatotaldebe += $debe;
				$sumahaber += $haber;
				$sumatotalhaber += $haber;
				$diferencia = $debe - $haber;
				$sumadiferencia += $diferencia;
				$sumatotaldiferencia += $diferencia;

		?>
		<?php if($condetalle) { ?>
		<tr>
			<td><?php echo $nlista; ?></td>
            <td><?php echo $fecha; ?></td>
			<td><?php echo $nombrelista; ?></td>
            <td align="right"><?php echo number_format($debe, 2, ',', '.'); ?></td>
			<td align="right"><?php echo number_format($haber, 2, ',', '.'); ?></td>
			<td align="right"><?php echo number_format($sumadiferencia, 2, ',', '.'); ?></td>
        </tr>
		<?php } ?>
		<?php } ?>

		<tr bgcolor="#C9F8FF">
			<td colspan="3" align="right">TOTALES&nbsp;&nbsp;</td>
            <td align="right"><?php echo number_format($sumadebe, 2, ',', '.'); ?></td>
            <td align="right"><?php echo number_format($sumahaber, 2, ',', '.'); ?></td>
			<td align="right"><?php echo number_format($sumadiferencia, 2, ',', '.'); ?></td>
        </tr>
		<?php } ?>
		<tr bgcolor="#9CD6FF">
			<td colspan="3" align="right">SALDOS&nbsp;&nbsp;</td>
			<td align="right"><?php echo number_format($sumatotaldebe, 2, ',', '.'); ?></td>
			<td align="right"><?php echo number_format($sumatotalhaber, 2, ',', '.'); ?></td>
			<td align="right"><?php echo number_format($sumatotaldiferencia, 2, ',', '.'); ?></td>
		</tr>
    </table>
