  <h2 align="center">LISTADO DE ARTICULOS</h2>
	<br />
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th align="center">N° Articulo</th>
                <th align="center">Nombre</th>
        				<th align="center">Stock</th>
        				<th align="center">Stock Min.</th>
        				<th align="center">Categoria</th>
        				<th align="center">Marca</th>
        				<th align="center">Habilitado</th>
        				<th align="center">Precio Neto</th>
        				<th align="center">IVA</th>
        				<th align="center">Total</th>
            </tr>
        </thead>
        <tbody>
			<?php
				$con_art=consulta("SELECT productos.*, proveedores.rsocial, marcas.descripcion as 'marca', iva.porc, categorias.descripcion  as 'categoria'
				FROM productos LEFT JOIN proveedores ON productos.idproveedor = proveedores.idproveedor
				LEFT JOIN marcas ON productos.idmarca = marcas.idmarca
				LEFT JOIN iva ON productos.iva = iva.idiva
				LEFT JOIN categorias ON productos.idcategoria = categorias.idcategoria
				WHERE productos.idempresa = '$idempresa'");
				while ($p = mysqli_fetch_array($con_art, MYSQLI_ASSOC)) {
					$narticulo = $p['idproducto'];
					$nombre = $p['nombre'];
					$stock = $p['stock'];
					$stockmin = $p['stockmin'];
					$categoria = $p['categoria'];
					$marca = $p['marca'];
					$precio = number_format($p['preciovta'], 2, ',', '.');
					$iva = $p['porc'];
					$caliva = 1 . "." . str_replace('.','',$p['porc']);
					if($p['habilitado']){
						$habilitado = "Habilitado";
					} else {
						$habilitado = "Deshabilitado";
					}
					$total = number_format($precio * $caliva, 2, ',', '.');
			?>
			<tr>
				<td style="width: 10%;"><?php echo $narticulo; ?></td>
        <td style="width: 20%;"><?php echo $nombre; ?></td>
        <td style="width: 5%;" align="center"><?php echo $stock; ?></td>
        <td style="width: 5%;" align="center"><?php echo $stockmin; ?></td>
        <td style="width: 10%;"><?php echo $categoria; ?></td>
        <td style="width: 10%;"><?php echo $marca; ?></td>
        <td style="width: 10%;"><?php echo $habilitado; ?></td>
        <td style="width: 10%;"><?php echo $precio; ?></td>
        <td style="width: 10%;"><?php echo $iva; ?></td>
        <td style="width: 10%;"><?php echo $total; ?></td>
      </tr>
		<?php } ?>
        </tbody>
    </table>
