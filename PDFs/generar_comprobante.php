<?php
	include "../funciones.php";
	session_start();
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocial = $_SESSION['rsocial'];

	if (isset($_POST['fpago']) && !empty($_POST['fpago']) &&
		isset($_POST['cliente']) && !empty($_POST['cliente']) &&
		isset($_POST['tcomprobante']) && !empty($_POST['tcomprobante'])) {

		$productos = $_POST['productos'];
		$cantidades = $_POST['cantidades'];
		$precios = $_POST['precios'];
		$ivas = $_POST['ivas'];
		$fpago = $_POST['fpago'];
		$tcomprobante = $_POST['tcomprobante'];
		$idcomprador = $_POST['cliente'];
		$comentario = $_POST['comentario'];
		$importe = $_POST['importe'];
		$idcomprobante = $_POST['idcomprobante'];

		$idvendedor = $idempresa;

		if($_POST['conceptos']){
			$conceptos = $_POST['conceptos'];
		} else {
			$conceptos = 0;
		}
		if($_POST['retenciones']){
			$retenciones = $_POST['retenciones'];
		} else {
			$retenciones = 0;
		}

		$con_fpago=consulta("SELECT * FROM formpag WHERE idpago = '$fpago'");
		$fp=mysqli_fetch_array($con_fpago);
		$formadepago = $fp['pag_descr'];

		require_once('html2pdf.class.php');

		$con_vendedor=consulta("SELECT empresas.*, condiva.iva_descr FROM empresas LEFT JOIN condiva ON empresas.condiva = condiva.idiva WHERE idempresa = '$idvendedor'");
		$v=mysqli_fetch_array($con_vendedor);

		if(is_numeric($idcomprador)){
			$con_comprador=consulta("SELECT empresas.*, condiva.iva_descr FROM empresas LEFT JOIN condiva ON empresas.condiva = condiva.idiva WHERE idempresa = '$idcomprador'");
			$c=mysqli_fetch_array($con_comprador);
			$idcliente = 0;
		} else {
			$idcomprador = substr($idcomprador,1);
			$con_comprador=consulta("SELECT clientes.*, condiva.iva_descr FROM clientes LEFT JOIN condiva ON clientes.condiva = condiva.idiva WHERE idcliente = '$idcomprador'");
			$c=mysqli_fetch_array($con_comprador);
			$idcliente = $idcomprador;
			$idcomprador = 0;
		}

		if($v['condiva'] == 1 && $c['condiva'] == 1){
			$letra = "A";
		} elseif($v['condiva'] == 1 && $c['condiva'] == 2){
			$letra = "B";
		} else/*if($v['condiva'] == 2)*/{
			$letra = "C";
		}

		$coniva = true;

		if($tcomprobante == "F"){
			$esfactura = true;
			$tipoc = "FACTURA";
			if($v['condiva'] == 1 && $c['condiva'] == 1){
				$tipo = "A";
			} elseif($v['condiva'] == 1 && $c['condiva'] != 1){
				$tipo = "B";
				$coniva = false;
			} else/*if($v['condiva'] == 2)*/{
				$tipo = "C";
				$coniva = false;
			}
		} elseif($tcomprobante == "P") {
			$tipo = $letra = "X";
			$tipoc = "PRESUPUESTO";
			$esfactura = true;
		} elseif($tcomprobante == "R") {
			$tipo = $letra = "R";
			$tipoc = "REMITO";
			$esfactura = true;
		} elseif($tcomprobante == "NC") {
			$tipo = "NC";
			$tipoc = "NOTA DE CREDITO";
			$esfactura = false;
		} else {
			$tipo = "ND";
			$tipoc = "NOTA DE DEBITO";
			$esfactura = false;
		}

		if($idcomprobante){
			$con_comp=consulta("SELECT * FROM facturas WHERE idfactura = '$idcomprobante'");
			$comp=mysqli_fetch_array($con_comp);
			$ruta = $comp['ruta'];
			unlink($ruta);
			$factura = $ruta;
			$nfactura = $comp['numero'];
			consulta("DELETE FROM movstock WHERE idfactura = $idcomprobante");
		} else {
			$factura = "comprobantes/".$idempresa . rand() * rand().'.pdf';

			$con_nmax=consulta("SELECT MAX(numero) as numero FROM facturas WHERE idvendedor = '$idvendedor' AND tipo = '$tipo'");
			if(mysqli_num_rows($con_nmax)==0){
				$nfactura = 1;
			} else {
				$cmax=mysqli_fetch_array($con_nmax);
				$nfactura = $cmax['numero'] + 1;
			}
		}

		ob_start();

		include('comprobante.php');

		$content = ob_get_clean();

			// convert to PDF
		try
		{
			$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 3);
			$html2pdf->setTestTdInOnePage(false);
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
			$html2pdf->Output("$factura", 'F');
		}
		catch(HTML2PDF_exception $e) {
			echo $e;
			exit;
		}

		if($idcomprobante){
			echo "El comprobante ha sido editado correctamente.";
			acthistempresa($rsocial, "Se edito un comprobante de venta");
		} else {
			echo "El comprobante ha sido generado correctamente.";
			acthistempresa($rsocial, "Se genero un comprobante de venta");
		}

	} else {
		ir_a("index.php");
	}
?>
