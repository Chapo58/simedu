<?php
session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);
?>
<style>
<?php 
require_once('../css/bootstrap.css');
require_once('../css/style.css');
?>
</style>
<page backtop="30mm" backbottom="10mm" backleft="20mm" backright="20mm">
    <page_header>
        <table style="margin-left:80px;">
            <tr>
                <td rowspan="4" width="100"><img src=<?php echo $imagenempresa; ?> width="75" height="75" class="img-circle img-user tooltipster" title="Imagen de perfil" alt="Imagen de Perfil"></td>
            </tr>
			<tr>
				<td height="20" width="300"><u>Empresa</u>: <?php echo $rsocial; ?></td>
				<td height="20" width="300"><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
			</tr>
			<tr>
				<td height="20" width="300"><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
				<td height="20" width="300"><u>Fecha Impresion</u>: <?php echo date('d/m/Y'); ?></td>
			</tr>
			<tr>
				<td height="20" width="300"><u>Domicilio</u>: <?php echo $e['domicilio']; ?></td>
			</tr>
        </table>
    </page_header>
    <page_footer>
        <div align="right">Pagina [[page_cu]]/[[page_nb]]</div>
    </page_footer>
	<table border="1" align="center">
		<tr bgcolor="#DDFFE6">
            <th align="center" height="20" style="width: 100%;">LIBRO DE STOCK MINIMO</th>
        </tr>
	</table>
	<br /><br />
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th align="center">N° Articulo</th>
                <th align="center">Nombre</th>
				<th align="center">Stock Actual</th>
				<th align="center">Stock Minimo</th>
				<th align="center">Faltante</th>
            </tr>
        </thead>
        <tbody>
			<?php				
				$con_stock=consulta("SELECT * FROM productos WHERE stock < stockmin AND idempresa = '$idempresa'");
				while ($s = mysqli_fetch_array($con_stock, MYSQLI_ASSOC)) {
					$narticulo = $s['idproducto'];
					$nombre = $s['nombre'];
					$stock = $s['stock'];
					$stockmin = $s['stockmin'];
			?>
			<tr>
				<td style="width: 10%;"><?php echo $narticulo; ?></td>
                <td style="width: 50%;"><?php echo $nombre; ?></td>
				<td style="width: 15%;"><?php echo $stock; ?></td>
				<td style="width: 15%;"><?php echo $stockmin; ?></td>
				<td style="width: 10%;"><?php echo $stock - $stockmin; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
</page>