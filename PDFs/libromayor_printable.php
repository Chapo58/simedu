<?php
session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);

$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
$condesde = fecha($_POST['desde'],"-");
$conhasta = fecha($_POST['hasta'],"-");
$cuentadesde = 0;
$cuentahasta = 99999999;
if(isset($_POST['cdesde']) && !empty($_POST['cdesde']) && isset($_POST['chasta']) && !empty($_POST['chasta']) && $_POST['cdesde'] < $_POST['chasta']) {
  $cuentadesde = $_POST['cdesde'];
  $cuentahasta = $_POST['chasta'];
}
if(isset($_POST['detalle'])) {$condetalle = false;}else{$condetalle = true;};
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Libro Diario</title>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<style>
body {
	margin: 0;
	padding: 0;
	background-color: #FAFAFA;
	font: 12pt "Tahoma";
}
* {
	box-sizing: border-box;
	-moz-box-sizing: border-box;
}
.page {
	width: 21cm;
	min-height: 29.7cm;
	padding: 1cm;
	margin: 1cm auto;
	border: 1px #D3D3D3 solid;
	border-radius: 5px;
	background: white;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}
.subpage {
	padding: 1cm;
	border: 2px gray solid;
	height: 256mm;
	outline: 2cm #fff solid;
}
@page {
	size: A4;
	margin: 1.0cm;
}
tr.color {
	background-color: #CECECE !important;
	-webkit-print-color-adjust: exact;
}
tr.color2 {
	background-color: #E0E0E0 !important;
	-webkit-print-color-adjust: exact;
}
tr.color3 {
	background-color: #000000 !important;
	-webkit-print-color-adjust: exact;
}
tr.color4 {
	background-color: #C9F8FF !important;
	-webkit-print-color-adjust: exact;
}
tr.color5 {
	background-color: #9CD6FF !important;
	-webkit-print-color-adjust: exact;
}
@media print {
	html, body {
	 width: 210mm;
	 height: 297mm;
 	}
	#header, #footer {
		 position: fixed;
		 display: block;
		 top: 0;
	}
	#footer {
			 bottom: 0;
	}
	tr.color {
		background-color: #CECECE !important;
		-webkit-print-color-adjust: exact;
	}
	tr.color2 {
		background-color: #E0E0E0 !important;
		-webkit-print-color-adjust: exact;
	}
	tr.color3 {
		background-color: #000000 !important;
		-webkit-print-color-adjust: exact;
	}
	tr.color4 {
		background-color: #C9F8FF !important;
		-webkit-print-color-adjust: exact;
	}
	tr.color5 {
		background-color: #9CD6FF !important;
		-webkit-print-color-adjust: exact;
	}
	.page {
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
	}
}
		</style>
	</head>

	<body onload="window.print()">
			<!-- <div id="header">
				<table style="margin-left:140px;font-size: 12pt;">
							<tr>
								<td width="300"><u>Empresa</u>: <?php echo $rsocial; ?></td>
								<td width="300"><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
							</tr>
							<tr>
								<td width="300"><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
								<td width="300"><u>Fecha Impresion</u>: <?php echo date('d/m/Y'); ?></td>
							</tr>
							<tr>
								<td width="300"><u>Domicilio</u>: <?php echo $e['domicilio']; ?></td>
							</tr>
				</table>
			</div> -->
	    <div class="page">
				<h2 align="center">LIBRO MAYOR</h2>
				<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
				<br />
			    <table border="1" align="center">
					<?php
						$con_cuentas=consulta("SELECT cuentas.idcuenta, cuentas.codigo, cuentas.denominacion, asientos.idcuenta FROM cuentas
											   INNER JOIN asientos ON asientos.idcuenta = cuentas.idcuenta
											   WHERE idempresa='$idempresa' AND imputable = 1 AND cuentas.codigo >= $cuentadesde AND cuentas.codigo <= $cuentahasta
											   GROUP BY cuentas.idcuenta ORDER BY cuentas.codigo ASC");
						$sumatotaldebe = 0;
						$sumatotalhaber = 0;
						$sumatotaldiferencia = 0;
						while ($c = mysqli_fetch_array($con_cuentas, MYSQLI_ASSOC)) {
							$idcuenta = $c['idcuenta'];
							$codigo = $c['codigo'];
							$denominacion = $c['denominacion'];
							$sumadebe = 0;
							$sumahaber = 0;
							$sumadiferencia = 0;
					?>
						<tr class="color">
							<td colspan="1">Cuenta</td>
							<td colspan="2">Descripcion</td>
							<td colspan="3"></td>
			            </tr>
						<tr class="color2">
							<td colspan="1"><?php echo $codigo; ?></td>
							<td colspan="2"><?php echo $denominacion; ?></td>
							<td colspan="3"></td>
			            </tr>
						<tr class="color3"><td colspan="6"></td></tr>
						<?php if($condetalle) { ?>
						<tr>
							<td style="width: 10%;">Asiento</td>
							<td style="width: 15%;">Fecha</td>
							<td style="width: 30%;">Detalle</td>
							<td style="width: 15%;">Debe</td>
							<td style="width: 15%;">Haber</td>
							<td style="width: 15%;">Saldo</td>
						</tr>
						<?php } ?>
					<?php
						$con_asientos=consulta("SELECT listaasientos.denominacion, listaasientos.nlista, listaasientos.fecha, SUM(asientos.debe) as debe, SUM(asientos.haber) as haber
												FROM listaasientos INNER JOIN asientos ON asientos.idlista = listaasientos.idlista
												WHERE listaasientos.idempresa= '$idempresa' AND asientos.idcuenta = '$idcuenta'
												AND (listaasientos.fecha BETWEEN '$condesde' AND '$conhasta')
			                                    GROUP BY asientos.idlista ORDER BY listaasientos.fecha ASC, listaasientos.nlista ASC");
						while ($a = mysqli_fetch_array($con_asientos, MYSQLI_ASSOC)) {
							$nombrelista = substr($a['denominacion'], 0, 30);
							$nlista = $a['nlista'];
							$fecha = fecha($a['fecha'],"/");
							$debe = $a['debe'];
							$haber = $a['haber'];
							$sumadebe += $debe;
							$sumatotaldebe += $debe;
							$sumahaber += $haber;
							$sumatotalhaber += $haber;
							$diferencia = $debe - $haber;
							$sumadiferencia += $diferencia;
							$sumatotaldiferencia += $diferencia;

					?>
					<?php if($condetalle) { ?>
					<tr>
						<td><?php echo $nlista; ?></td>
			            <td><?php echo $fecha; ?></td>
						<td><?php echo $nombrelista; ?></td>
			            <td align="right"><?php echo number_format($debe, 2, ',', '.'); ?></td>
						<td align="right"><?php echo number_format($haber, 2, ',', '.'); ?></td>
						<td align="right"><?php echo number_format($sumadiferencia, 2, ',', '.'); ?></td>
			        </tr>
					<?php } ?>
					<?php } ?>

					<tr class="color4">
						<td colspan="3" align="right">TOTALES&nbsp;&nbsp;</td>
			            <td align="right"><?php echo number_format($sumadebe, 2, ',', '.'); ?></td>
			            <td align="right"><?php echo number_format($sumahaber, 2, ',', '.'); ?></td>
						<td align="right"><?php echo number_format($sumadiferencia, 2, ',', '.'); ?></td>
			        </tr>

					<?php } ?>
					<tr class="color5">
						<td colspan="3" align="right">SALDOS&nbsp;&nbsp;</td>
						<td align="right"><?php echo number_format($sumatotaldebe, 2, ',', '.'); ?></td>
						<td align="right"><?php echo number_format($sumatotalhaber, 2, ',', '.'); ?></td>
						<td align="right"><?php echo number_format($sumatotaldiferencia, 2, ',', '.'); ?></td>
					</tr>
			    </table>
	    </div>
	    <!-- <div class="page">
	        <div class="subpage">Page 2/2</div>
	    </div> -->
	</body>
</html>
