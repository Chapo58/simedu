  <h2 align="center">LISTADO DE PROVEEDORES</h2>
	<br />
    <table border="1" align="center">
        <thead>
            <tr bgcolor="#ABE2F5">
                <th align="center">Razon Social</th>
                <th align="center">Domicilio</th>
        				<th align="center">Cond. IVA</th>
        				<th align="center">C.U.I.T.</th>
        				<th align="center">Ing. Brutos</th>
        				<th align="center">Telefono</th>
        				<th align="center">Mail</th>
            </tr>
        </thead>
        <tbody>
			<?php
				$con_cli=consulta("SELECT proveedores.*, condiva.iva_descr
				FROM proveedores LEFT JOIN condiva ON proveedores.condiva = condiva.idiva
				WHERE proveedores.idempresa = '$idempresa'");
				while ($c = mysqli_fetch_array($con_cli, MYSQLI_ASSOC)) {
					$rsocial = $c['rsocial'];
					$ingbru = number_format($c['ingbrutos'], 0, ',', '.');
					$domicilio = $c['domicilio'].", ".$c['localidad'];
					$condiva = $c['iva_descr'];
          $cuit = $c['cuit'];
          $telefono = $c['telefono'];
          $mail = $c['emailcontacto'];
			?>
			<tr>
				<td style="width: 20%;"><?php echo $rsocial; ?></td>
        <td style="width: 20%;"><?php echo $domicilio; ?></td>
        <td style="width: 15%;"><?php echo $condiva; ?></td>
        <td style="width: 10%;"><?php echo $cuit; ?></td>
        <td style="width: 10%;"><?php echo $ingbru; ?></td>
        <td style="width: 10%;"><?php echo $telefono; ?></td>
        <td style="width: 15%;"><?php echo $mail; ?></td>
      </tr>
		<?php } ?>
        </tbody>
    </table>
