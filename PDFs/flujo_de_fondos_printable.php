<?php
session_start();
require_once('../funciones.php');
$idempresa = $_SESSION['idempresa'];
$idusuario = $_SESSION['idusuario'];
$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
$e=mysqli_fetch_array($con_emp);
$imagenempresa = '../' . $e['imagenempresa'];
$rsocial = $e['rsocial'];
$idperiodo = $e['idperiodo'];
$con_per=consulta("SELECT * FROM periodos WHERE idperiodo='$idperiodo'");
$p=mysqli_fetch_array($con_per);

$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
$desde1 = str_replace("/","",$desde);
$año = substr($desde1, -4);
$_SESSION['año'] = $año;

$con_resumen=consulta("SELECT cuentas.rubro, SUM(IFNULL(asientos.debe,0)) AS debe, SUM(IFNULL(asientos.haber,0)) as haber FROM cuentas
LEFT JOIN (SELECT asientos.*, listaasientos.fecha FROM asientos LEFT JOIN listaasientos ON asientos.idlista = listaasientos.idlista WHERE YEAR(listaasientos.fecha) = $año)
AS asientos ON cuentas.idcuenta = asientos.idcuenta
WHERE cuentas.idempresa = '$idempresa'
GROUP BY cuentas.rubro ORDER BY cuentas.rubro ASC");
$arr = array();
$sumatotal = 0;
while ($r = mysqli_fetch_array($con_resumen, MYSQLI_ASSOC)) {
	$calcular = abs($r['debe'] - $r['haber']);
	array_push($arr, $calcular);
	$sumatotal += $calcular;
};
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | ANALISIS DE FONDOS</title>
		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<style>
body {
	margin: 0;
	padding: 0;
	background-color: #FAFAFA;
	font: 12pt "Tahoma";
}
* {
	box-sizing: border-box;
	-moz-box-sizing: border-box;
}
.page {
	width: 21cm;
	min-height: 29.7cm;
	padding: 1cm;
	margin: 1cm auto;
	border: 1px #D3D3D3 solid;
	border-radius: 5px;
	background: white;
	box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}
.subpage {
	padding: 1cm;
	border: 2px gray solid;
	height: 256mm;
	outline: 2cm #fff solid;
}
@page {
	size: A4;
	margin: 1.0cm;
}
tr.color {
	background-color: #9EE7FF !important;
	-webkit-print-color-adjust: exact;
}
tr.color2 {
	background-color: #C4C4C4 !important;
	-webkit-print-color-adjust: exact;
}
@media print {
	html, body {
	 width: 210mm;
	 height: 297mm;
 	}
	#header, #footer {
		 position: fixed;
		 display: block;
		 top: 0;
	}
	#footer {
			 bottom: 0;
	}
	tr.color {
		background-color: #9EE7FF !important;
		-webkit-print-color-adjust: exact;
	}
	tr.color2 {
		background-color: #C4C4C4 !important;
		-webkit-print-color-adjust: exact;
	}
	.page {
			margin: 0;
			border: initial;
			border-radius: initial;
			width: initial;
			min-height: initial;
			box-shadow: initial;
			background: initial;
			page-break-after: always;
	}
}
		</style>
	</head>

	<body onload="window.print()">
			<!-- <div id="header">
				<table style="margin-left:140px;font-size: 12pt;">
							<tr>
								<td width="300"><u>Empresa</u>: <?php echo $rsocial; ?></td>
								<td width="300"><u>Periodo</u>: <?php echo str_replace ("-","/",date("d-m-Y",strtotime($p['desde']))) . " - " . str_replace ("-","/",date("d-m-Y",strtotime($p['hasta']))); ?></td>
							</tr>
							<tr>
								<td width="300"><u>C.U.I.T</u>: <?php echo $e['cuit']; ?></td>
								<td width="300"><u>Fecha Impresion</u>: <?php echo date('d/m/Y'); ?></td>
							</tr>
							<tr>
								<td width="300"><u>Domicilio</u>: <?php echo $e['domicilio']; ?></td>
							</tr>
				</table>
			</div> -->
	    <div class="page">
				<h2 align="center">ANALISIS DE FONDOS</h2>
				<p align="center">Desde el <?php echo $desde; ?> al <?php echo $hasta; ?></p>
				<br />
        <table border="1" align="center">
              <thead>
                  <tr class="color">
                      <th colspan="4">Resumen Comparativo</th>
                  </tr>
              </thead>
              <tbody>
            <?php
							$i = 0;
              $con_resumen=consulta("SELECT cuentas.rubro, SUM(IFNULL(asientos.debe,0)) AS debe, SUM(IFNULL(asientos.haber,0)) as haber FROM cuentas
              LEFT JOIN (SELECT asientos.*, listaasientos.fecha FROM asientos LEFT JOIN listaasientos ON asientos.idlista = listaasientos.idlista WHERE YEAR(listaasientos.fecha) = $año)
              AS asientos ON cuentas.idcuenta = asientos.idcuenta
              WHERE cuentas.idempresa = '$idempresa'
              GROUP BY cuentas.rubro ORDER BY cuentas.rubro ASC");
              $haycosto = false;
              while ($r = mysqli_fetch_array($con_resumen, MYSQLI_ASSOC)) {
                $nrubro = $r['rubro'];
                switch ($nrubro) {
                  case 1:
                    $rubro = "Activo (+)";
                    $activo = $r['debe'] - $r['haber'];
                    break;
                  case 2:
                    $rubro = "Pasivo (-)";
                    $pasivo = $r['debe'] - $r['haber'];
                    break;
                  case 3:
                    $rubro = "Patrimonio Neto (-)";
                    $pneto = $r['debe'] - $r['haber'];
                    break;
                  case 4:
                    $rubro = "Ingresos y Ganancias (+)";
                    $ingygan = $r['debe'] - $r['haber'];
                    break;
                  case 5:
                    $rubro = "Gastos y Perdidas (-)";
                    $gasyper = $r['debe'] - $r['haber'];
                    break;
                  case 6:
                    $rubro = "Costo (-)";
                    $costo = $r['haber'];
                    $haycosto = true;
                    break;
                }
            ?>
            <tr>
              <td style="width: 40%;"><?php echo $rubro; ?></td>
              <td style="width: 20%;" align="right"><?php echo "$ " . number_format($r['debe'] - $r['haber'], 0, ',', '.'); ?></td>
              <td align="right"><?php echo $arr[$i]."%"; ?></td>
            </tr>
            <?php if($nrubro == 2) { ?>
            <tr>
              <td>Diferencia&nbsp;&nbsp;</td>
              <td align="right"><?php echo "$ " . number_format(abs($activo) + $pasivo, 0, ',', '.'); ?></td>
              <td align="right">100%</td>
            </tr>
            <?php } elseif($nrubro == 3) { ?>
            <tr class="color2">
              <td align="right">Saldo&nbsp;&nbsp;</td>
              <td align="right"><?php echo "$ " . number_format(abs($activo) - abs($pasivo) - abs($pneto), 0, ',', '.'); ?></td>
              <td align="right">&nbsp;&nbsp;&nbsp;<?php echo "$ " . number_format(abs($activo) - abs($pasivo) - abs($pneto), 0, ',', '.'); ?></td>
            </tr>
            <?php } elseif($nrubro == 5) { ?>
            <tr>
              <td>Diferencia&nbsp;&nbsp;</td>
              <td align="right"><?php echo "$ " . number_format(abs($ingygan) - abs($gasyper), 0, ',', '.'); ?></td>
              <td align="right">100%</td>
            </tr>
            <?php }
            $i++;  } // FIN WHILE ?>
            <?php if($haycosto) { ?>
            <tr class="color2">
              <td align="right">Saldo&nbsp;&nbsp;</td>
              <td align="right"><?php echo "$ " . number_format(abs($ingygan) - abs($gasyper) - abs($costo), 0, ',', '.'); ?></td>
              <td align="right">&nbsp;&nbsp;&nbsp;<?php echo "$ " . number_format(abs($ingygan) - abs($gasyper) - abs($costo), 0, ',', '.'); ?></td>
            </tr>
            <?php } else { ?>
            <tr class="color2">
              <td align="right">Saldo&nbsp;&nbsp;</td>
              <td align="right"><?php echo "$ " . number_format(abs($ingygan) - abs($gasyper), 0, ',', '.'); ?></td>
              <td align="right">&nbsp;&nbsp;&nbsp;<?php echo "$ " . number_format(abs($ingygan) - abs($gasyper), 0, ',', '.'); ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
        <br><br>
        <center><img src="../pie.php" alt="Grafico"></center>
				<center><img src="../pie2.php" alt="Grafico"></center>
	    </div>
	    <!-- <div class="page">
	        <div class="subpage">Page 2/2</div>
	    </div> -->
	</body>
</html>
