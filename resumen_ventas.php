<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Resumen de Ventas</title>
    <?php require_once('head.php'); ?>
  	<?php
  		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
  			mensaje("Debe seleccionar una empresa.");
  			ir_a("empresas.php");
  		} else {

  		}
  	?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-industry home-icon"></i>
              <a href="simempresarial.php">Simulador Empresarial</a>
            </li>
            <li class="active">Resumen de Ventas</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

  <!-- Page Content -->
  <h1 class="page-header">Resumen de Ventas</h1>
  <div class="space50"></div>
  <form role="form" data-toggle="validator" target="_blank" action="PDFs/resumen_ventas.php" method="POST">
	<div class="col-md-6 col-xs-12">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Desde</strong></div>
                <input type="text" data-mask="99/99/9999" id="desde" name="desde" class="form-control" value="<?php echo date("d/m/Y",strtotime($desde)); ?>" placeholder="Fecha Desde" maxlength="10">
          </div>
   		</div>
	</div>
	<div class="col-md-6 col-xs-12">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Hasta</strong></div>
                <input type="text" data-mask="99/99/9999" id="hasta" name="hasta" class="form-control" value="<?php echo date("d/m/Y",strtotime($hasta)); ?>" placeholder="Fecha Hasta" maxlength="10">
          </div>
   		</div>
	</div>
  <div class="col-md-6 col-md-offset-3 col-xs-12">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span><strong>&nbsp;&nbsp;Cliente</strong></div>
        <select name="cliente" class="form-control">
        <option selected value=0>Todos los Cliente</option>
          <optgroup label="Empresas Compañeros">
          <?php
            $con_com=consulta("SELECT empresas.idempresa, empresas.rsocial FROM empresas LEFT JOIN usuario ON empresas.idusuario = usuario.idusuario WHERE usuario.idprofesor='$idprofe'");
            while ($c = mysqli_fetch_array($con_com, MYSQLI_ASSOC)) {
              $idemp = $c['idempresa'];
              $rsocial = $c['rsocial'];
          ?>
            <option value = '<?php echo $idemp;?>'><?php echo $rsocial; ?></option>
          <?php } ?>
          </optgroup>
          <optgroup label="Clientes Cargados">
          <?php
            $con_cli=consulta("SELECT idcliente, rsocial FROM clientes WHERE idempresa='$idempresa'");
            while ($c = mysqli_fetch_array($con_cli, MYSQLI_ASSOC)) {
              $idcli = $c['idcliente'];
              $nombre = $c['rsocial'];
          ?>
            <option value = 'a<?php echo $idcli;?>'><?php echo $nombre; ?></option>
          <?php } ?>
          </optgroup>
        </select>
      </div>
    </div>
  </div>
	<div class="col-md-12">
		<button type="submit" id="btnAceptar" style="width:100%;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Generar Impresion</button>
	</div>
  </form>

  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

  <script type="text/javascript">
  	$(document).ready(function() {
  		//Calendarios
  		$("#desde").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  		$("#hasta").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  	});
  </script>
  <!-- ============================================================= -->
</body>
</html>
