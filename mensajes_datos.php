<?php

if ($_SERVER['HTTP_REFERER'] == ""){
		header("Location: index.php");
		exit();
}

session_start();
require_once("funciones.php");

$id = intval($_GET['id']);

$con_datos=consulta("SELECT mensajes.*, usuario.*
	FROM mensajes LEFT JOIN usuario ON mensajes.origen = usuario.idusuario
	WHERE idmensaje='$id'");
$d=mysqli_fetch_array($con_datos);

consulta("UPDATE mensajes SET visto_des = 1 WHERE idmensaje = '$id'");

if(!empty($d['nombre']) || !empty($d['apellido'])) {
	$remitente = $d['nombre'] . " " . $d['apellido'] . " (" . $d['email'].")";
} else {
	$remitente = $d['email'];
}

if($d['imagen']){ $imagenperfil = $d['imagen'];} else { $imagenperfil = "images/logo.png";}

$arr= array();
$arr['asunto'] = $d['asunto'];
$arr['fecha'] = fecha($d['fecha'],"/").", ".date("H:i",strtotime($d['hora']));
$arr['remitente'] = $remitente;
$arr['avatar'] = $imagenperfil;
$arr['mensaje'] = $d['mensaje'];
$arr['idusuario'] = $d['idusuario'];

echo json_encode($arr);

?>
