<?php
session_start();
include "funciones.php";
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM bienes_clientes WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM bienes_clientes WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$numero = trim($_POST["numero"]);
		$denominacion = trim($_POST["denominacion"]);
		$cuit = trim($_POST["cuit"]);
		$cierre = trim($_POST["cierre"]);
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO bienes_clientes(idempresa, numero, denominacion, cuit, cierre) VALUES('$idempresa','$numero','$denominacion','$cuit','$cierre');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM bienes_clientes WHERE idcliente = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idcliente"];
		$numero = trim($_POST["numero"]);
		$denominacion = trim($_POST["denominacion"]);
		$cuit = trim($_POST["cuit"]);
		$cierre = trim($_POST["cierre"]);
		//Update record in database
		$result = mysqli_query($con,"UPDATE bienes_clientes SET numero = '$numero', denominacion = '$denominacion', cuit = '$cuit', cierre = '$cierre' WHERE idcliente = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idcliente"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM bienes_clientes WHERE idcliente = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>