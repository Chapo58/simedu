<!DOCTYPE html>
<html lang="es">
<head>
  <?php require_once('head.php'); ?>
	<?php
		if(isset($_POST['idlista']) && !empty($_POST['idlista'])) {
			$idlista = $_POST['idlista'];
			$con_l=consulta("SELECT * FROM modelolistas WHERE idmodelolista='$idlista'");
            $lista=mysqli_fetch_array($con_l);
			$cabecera = $lista['denominacion'];
			$bandera = true;
			$con_nmax=consulta("SELECT * FROM modeloasientos WHERE idmodelolista = '$idlista' AND idmodelo = (SELECT MAX(idmodelo) FROM modeloasientos WHERE idmodelolista = '$idlista')");
			$cmax=mysqli_fetch_array($con_nmax);
			$ultimoasiento = $cmax['idmodelo'] + 1;
		} else {
			$idlista = 0;
			$bandera = false;
			$cabecera = "";
			$ultimoasiento = 0;
		}
	?>
    <title>Simedu | Modelo Asiento</title>
</head>

<body class="no-skin">
	<?php require_once('header.php'); ?>

  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-usd home-icon"></i>
            <a href="#">Simulador Contable</a>
          </li>
          <li><a href="asientos_lista.php">Asientos Contables</a></li>
          <li class="active">Modelo de Asiento</li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">

  <!-- Page Content -->
  <div class="bs-callout bs-callout-default col-sm-12">
  <div class="col-md-8">
	<form action="asientos_modelo.php" method="POST" class="form-horizontal" role="form">
    <select class="form-control" onchange="this.form.submit()" name="idlista">
		<?php if (!isset($_POST['idlista'])) echo "<option disabled selected>Selecciona un Modelo</option>";
			$con_modelos=consulta("SELECT * FROM modelolistas WHERE idusuario='$idusuario'");
			while ($m = mysqli_fetch_array($con_modelos, MYSQLI_ASSOC)) {
				$idmodelo = $m['idmodelolista'];
				$denominacion = $m['denominacion'];
			?>
			<option value="<?php echo $idmodelo; ?>"><?php echo $denominacion; ?></option>
		<?php } ?>
	</select>
	</form>
	</div>
	<?php if($bandera) { ?>
	<div class="col-md-2">
	<form class="delform">
        <button type="submit" class="btn btn-danger" title="Eliminar Modelo">
			<span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Eliminar Modelo
		</button>
		<input type="hidden" id="delmod" name="delmod" value="<?php echo $idlista; ?>" >
    </form>
	</div>
	<div class="col-md-2">
	<a class="btn btn-info" href="asientos_modelo.php"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Nuevo Modelo</a>
	</div>
	<?php } ?>
  </div>
  <br /><br /><br /><br /><br />
  <div class="bs-callout bs-callout-info">
	<h4>Cabecera</h4>
	<div class="row">
		<div class="col-md-8">
            <input type="text" id="denominacion" name="denominacion" placeholder="Denominacion" value="<?php echo $cabecera; ?>" class="form-control">
		</div>
	</div>
  </div>
  <input type="button" id="nuevo" class="btn btn-lg btn-primary" style="width:100%;" value="Nueva Imputacion">
  <div class="space20"></div>
  <table id="asientos" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>id</th>
				<th>Cuenta</th>
                <th>Detalle</th>
				<th></th>
            </tr>
        </thead>
		<tbody>
			<?php
				if($bandera){
				$con_asi=consulta("SELECT * FROM modeloasientos WHERE idmodelolista='$idlista'");
				while ($a = mysqli_fetch_array($con_asi, MYSQLI_ASSOC)) {
					$idasiento = $a['idmodelo'];
					$idcuenta = $a['idcuenta'];
					$con_cuenta=consulta("SELECT * FROM cuentas WHERE idcuenta='$idcuenta'");
					$c=mysqli_fetch_array($con_cuenta);
					$codigo = $c['codigo'];
			?>
            <tr>
				<td><?php echo $idasiento; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" value="<?php echo $codigo; ?>" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
				<?php }} ?>
			<tr>
				<td><?php echo $ultimoasiento; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<option selected disabled>Seleccionar Denominacion</option>
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
			<tr>
				<td><?php echo $ultimoasiento += 1; ?></td>
				<td><input type="number" placeholder="Nº Cuenta" id="ncuenta" name="ncuenta" onkeyup="getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());" class="form-control" /></td>
                <td>
					<select class='form-control' id='detalle' onchange="getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());">
					<option selected disabled>Seleccionar Denominacion</option>
					<?php
						$con_den=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
						while ($den = mysqli_fetch_array($con_den, MYSQLI_ASSOC)) {
								$denominacion = $den['denominacion'];
								$codigo = $den['codigo'];
					?>
							<option value='<?php echo $codigo; ?>'><?php echo $denominacion; ?></option>
					<?php } ?>
					</select>
				</td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
		</tbody>
    </table>
  <!-- Final Page Content -->
	<br />
	<input type="button" id="guardar" class="btn btn-lg btn-success" style="float:right;" value="Guardar Modelo Asiento">
  <br/><br/><br/><br/><br/>
  </div>
 </div>
</div><!-- /.main-content -->

 <?php require_once('footer.php'); ?>

<script type="text/javascript">
	var tasiento;
	var select = "<select class='form-control' id='detalle' onchange=" + '"' +"getCuenta($(this).closest('td').prev('td').find('input'),$(this).val());"+ '"' +">" +
	<?php
	$con_den2=consulta("SELECT * FROM cuentas WHERE idempresa='$idempresa' AND imputable = 1 ORDER BY codigo ASC");
	while ($den2 = mysqli_fetch_array($con_den2, MYSQLI_ASSOC)) {
			$denominacion2 = $den2['denominacion'];
			$codigo2 = $den2['codigo'];
	?>
		"<option value='<?php echo $codigo2; ?>'><?php echo $denominacion2; ?></option>" +
	<?php } ?>
	"<option selected disabled>Seleccionar Denominacion</option>" +
	"</select>";
	<?php echo "var ultimoasiento = $ultimoasiento;"; ?>
	function getCuenta(input, valor) {
		input.val(valor);
	};

	function getDenominacion(select, valor) {
		select.val(valor);
	};

	function remover(tr) {
		tasiento.row(tr).remove().draw();
	}

	$(document).ready(function() {
	    tasiento = $('#asientos').DataTable({
			"columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
			],
			"paging":   false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Presione "Nuevo Asiento" para comenzar a cargar asientos.',
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"bFilter": false
		});

		$('#nuevo').on( 'click', function () {
			ultimoasiento += 1;
			tasiento.row.add( [
				ultimoasiento,
				'<input type="number" placeholder="Nº Cuenta" id="ncuenta" name="ncuenta" onkeyup=' + '"' +"getDenominacion($(this).closest('td').next('td').find('select'),$(this).val());"+ '"' +' class="form-control" />',
				select,
				'<button type="button" class="btn btn-danger btn-md" onclick=' + '"' + "remover($(this).parents('tr'))" + '"' + '><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'
			] ).draw();
		} );

		<?php
			if($bandera){
		?>
		$("input[id=ncuenta]").each(function() {
			$(this).closest('td').next('td').find('select').val(this.value);
		});
		<?php } ?>
	});

	$('#guardar').click( function() {
			var vcodigos = [];
			var vdetalles = [];
			var denominacion = $('#denominacion').val();
			<?php echo "var idlista = $idlista;"; ?>
			$("input[id=ncuenta]").each(function() {
				var number3 = parseFloat(this.value) || 0;
				vcodigos.push(parseFloat(number3));
			});
			$("select[id=detalle]").each(function() {
				var detalle = $(this).find('option:selected').text() || 0;
				vdetalles.push(detalle);
			});
            $.ajax({
				type: "POST",
				url: "cargar_modeloasientos.php",
				data: { ncuenta: vcodigos, detalles: vdetalles, denominacion : denominacion, idlista : idlista }
			}).done(function( msg ) {
				BootstrapDialog.show({
				message: msg,
				buttons: [{
					label: 'Aceptar',
					action: function() {
						location.href= "asientos_lista.php";
					}
				}]
				});
			});
    } );

	$('.delform').on('submit',function(event){
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Modelo',
				message: '¿Esta seguro que desea eliminar este modelo de asiento?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
										//	window.location.reload();
										location.href= "asientos_modelo.php";
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

</script>

</body>
</html>
