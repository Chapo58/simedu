<?php
	include "funciones.php";
	session_start();

	if (isset($_POST['txtRazonSocial']) && !empty($_POST['txtRazonSocial']) &&
		isset($_POST['txtCUIT']) && !empty($_POST['txtCUIT']) &&
		isset($_POST['txtInicioActividades']) && !empty($_POST['txtInicioActividades']) &&
		isset($_POST['cboCondicionIVA']) && !empty($_POST['cboCondicionIVA']) &&
		isset($_POST['cboPuntosVenta']) && !empty($_POST['cboPuntosVenta']) &&
		isset($_POST['cboPeriodo']) && !empty($_POST['cboPeriodo']) &&
		isset($_POST['txtNroAsiento']) && !empty($_POST['txtNroAsiento'])) {
		
		// Quito espacios en blanco
		$rsocial = trim($_POST['txtRazonSocial']);
		$cuit = trim($_POST['txtCUIT']);
		$iniactividades = trim($_POST['txtInicioActividades']);
		$domicilio = trim($_POST['txtDomicilio']);
		$localidad = trim($_POST['txtLocalidad']);
		$provincia = trim($_POST['txtProvincia']);
		$pais = $_POST['pais'];
		$telefono = trim($_POST['txtTelefono']);
		$condiva = $_POST['cboCondicionIVA'];
		$ingbrutos = trim($_POST['txtIngresosBrutos']);
		$pventas = $_POST['cboPuntosVenta'];
		$cpventas = trim($_POST['txtCantidad']);
		$iniasientos = trim($_POST['txtNroAsiento']);
		$periodo = $_POST['cboPeriodo'];
		$plancuenta = $_POST['cboPlanCuentas'];
		
		// Paso a mayusculas
		$rsocial = ucfirst($rsocial);
		$domicilio = ucfirst($domicilio);
		$localidad = ucfirst($localidad);
		$provincia = ucfirst($provincia);
		
		$rutaimagenemp = "images/logo.png";
		
		$idusuario = $_SESSION['idusuario'];
		
		$con_usr=consulta("SELECT * FROM usuario WHERE idusuario='$idusuario'");
		$u=mysqli_fetch_array($con_usr);
		$idprofesor = $u['idprofesor'];
		
		if(testfecha($iniactividades)===false) {
			mensaje("La fecha ingresada no es correcta.");
			ir_a("empresas.php");
		} else {
			$iniactividades = str_replace("/","",$iniactividades);
			$año = substr($iniactividades, -4);
			$mes = substr($iniactividades, -6, 2);
			$dia = substr($iniactividades, -8, 2);
			$iniactividades = $año."-".$mes."-".$dia;
			$altaempresa = "INSERT INTO empresas (idusuario,rsocial,cuit,inicioact,domicilio,localidad,provincia,pais,telefono,condiva,ingbrutos,pventas,cantpventas,idperiodo,idpcuentas,iniasientos,imagenempresa)
									VALUES (
									'$idusuario',
									'$rsocial',
									'$cuit',
									'$iniactividades',
									'$domicilio',
									'$localidad',
									'$provincia',
									'$pais',
									'$telefono',
									'$condiva',
									'$ingbrutos',
									'$pventas',
									'$cpventas',
									'$periodo',
									'$plancuenta',
									'$iniasientos',
									'$rutaimagenemp')";
			
				$new = mysqli_query($link, $altaempresa);
				
				$idcreada = mysqli_insert_id($link);

				$cargarplanes = consulta("INSERT INTO cuentas (codigo,denominacion,rubro,imputable,sumariza)
									SELECT codigo, denominacion, rubro, imputable, sumariza FROM modeloscuentas WHERE idprofesor = '$plancuenta'");
				
				$cargarids = consulta("UPDATE cuentas SET idempresa = '$idcreada' WHERE idempresa = 0");
				
				if(!$new || !$cargarplanes || !$cargarids){
						echo "Mensaje: ".mysqli_error();
						mensaje("Error");
				}
				mensaje("Empresa creada con exito");
				acthistempresa($rsocial, "Creacion de una nueva empresa");
				ir_a("empresas.php");
		}
		
	} else {
		mensaje("No se cargaron todos los datos");
		ir_a("empresas.php");
	}
?>
