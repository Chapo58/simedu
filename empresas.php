<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Mis empresas</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-suitcase home-icon"></i>
              <a href="#">Mis Empresas</a>
            </li>
            <li class="active">Empresas</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">

	<?php
        if(isset($_POST['edemp']) && !empty($_POST['edemp'])){
            $editarempresa = $_POST['edemp'];
            $con_editar=consulta("select * from empresas where idempresa='$editarempresa'");
            $editar=mysqli_fetch_array($con_editar);
			$titulo = "Modificar Empresa " . $editar['rsocial'];
			$accion = "empresa_modificar.php";
			$txtboton = "Guardar Cambios";

			$modidempresa = $editar['idempresa'];
			$modrsocial = $editar['rsocial'];
			$modcuit = $editar['cuit'];
			$modiniact = date("d/m/Y",strtotime($editar['inicioact']));
			$moddomicilio = $editar['domicilio'];
			$modlocalidad = $editar['localidad'];
			$modprovincia = $editar['provincia'];
			$modpais = $editar['pais'];
			$modtelefono = $editar['telefono'];
			$modcondiva = $editar['condiva'];
			$modpventas = $editar['pventas'];
			$modcantpventas = $editar['cantpventas'];
			$modperiodos = $editar['idperiodo'];
			$modcuentas = $editar['idpcuentas'];
			$modiniasientos = $editar['iniasientos'];
			$modingbru = $editar['ingbrutos'];

		} else {
			$titulo = "Crear una nueva Empresa";
			$accion = "empresa_alta.php";
			$txtboton = "Aceptar";
		}
	?>


    <!-- Page Content -->
    <div id="divEmpresas">
        <h1 class="page-header">Mis empresas</h1>
		<div class="col-md-8">
		<input type="button" id="btnNueva" onclick="switchDivs();" class="btn btn-lg btn-primary" value="Crear nueva empresa">
        </div>
		<div class="col-md-4">
		<p align="right"><?php if(isset($_SESSION['rsocial'])) echo "<u>Empresa Activa</u>: ".$_SESSION['rsocial']; else echo "Ninguna Empresa Activa."; ?></p>
        </div>
		<div class="space20"></div><br /><br />
        <div class="row">
            <div class="col-xs-12">
                <table id="empresas" class="table table-striped table-bordered" cellspacing="0" style="width:80%;margin:auto;">
                    <thead>
                        <tr>
                            <th>Seleccionar</th>
                            <th></th>
                            <th>Razón Social</th>
                            <th>CUIT</th>
                            <th>Domicilio</th>
                            <th>Teléfono</th>
							<th>Editar</th>
							<th>Eliminar</th>
                        </tr>
                    </thead>

                    <tbody>
						<?php
							$con_emp2=consulta("select * from empresas where idusuario='$idusuario'");
							while ($emp2 = mysqli_fetch_array($con_emp2, MYSQLI_ASSOC)) {
								$idempresa2 = $emp2['idempresa'];
								$rsocial2 = $emp2['rsocial'];
								$cuit = $emp2['cuit'];
								$inicioact = date("d/m/Y",strtotime($emp2['inicioact']));
								$domicilio = $emp2['domicilio'];
								$telefono = $emp2['telefono'];
								$condiva = $emp2['condiva'];
								$ingbrutos = $emp2['ingbrutos'];
								$pventas = $emp2['pventas'];
								$cantpventas = $emp2['cantpventas'];
								$iniasientos = $emp2['iniasientos'];
								$web = $emp2['web'];
								$imagenemp = $emp2['imagenempresa'];
						?>
                        <tr>
                            <td align="center">
							<form action="empresas.php" method="POST">
                                <button type="submit" class="btn btn-info" title="Seleccionar empresa">
                                    <span class="fa fa-hand-o-right" aria-hidden="true"></span>
                                </button>
                                <input type="hidden" name="empr" id="empr" value="<?php echo $idempresa2; ?>" >
							</form>
							</td>
                            <td class="text-center"><img src=<?php echo $imagenemp; ?> class="img-circle img-empresa-min"></td>
                            <td><?php echo $rsocial2; ?></td>
                            <td><?php echo $cuit; ?></td>
                            <td><?php echo $domicilio; ?></td>
                            <td><?php echo $telefono; ?></td>
							<td class="text-center">
                                <form action="empresas.php" method="POST">
                                    <button type="submit" class="btn btn-primary" title="Editar">
										<span class="fa fa-pencil" aria-hidden="true"></span>
									</button>
                                <input type="hidden" name="edemp" id="edemp" value="<?php echo $idempresa2; ?>" >
                                </form>
							</td>
							<td class="text-center">
                                <form class="delform">
                                    <button type="submit" class="btn btn-danger" title="Eliminar">
										<span class="fa fa-trash-o" aria-hidden="true"></span>
									</button>
									<input type="hidden" id="delemp" name="delemp" value="<?php echo $idempresa2; ?>" >
                                </form>
                            </td>
                        </tr>
						<?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="space50"></div>

        <div class="row">
            <div class="bs-callout bs-callout-primary">
                <h4>Institucional y tienda virtual</h4>
                <p>Una vez que tengas creada una empresa, puedes crear o modificar su sección <a href="institucional.php">institucional</a> y su <a href="productos.php">tienda virtual</a>.</p>
            </div>
        </div>
    </div>
    <div id="divNuevaEditar" class="sr-only">
        <h1 class="page-header"><?php echo $titulo; ?></h1>
		<?php if(!isset($_POST['edemp'])) { ?>
        <div class="bs-callout bs-callout-warning">
            <h4>Período</h4>
            <p>Al crear tu cuenta en simedu se registro automaticamente el periodo completo del año actual, si quieres modificar dicho periodo, haz click <a href="periodos.php">aquí</a>.</p>
        </div>
		<?php } ?>
        <form role="form" data-toggle="validator" action="<?php echo $accion; ?>" method="POST">
        <div class="form-group">
            <label for="txtRazonSocial" class="sr-only">Razón Social o Nombre de Fantasia</label>
            <input type="text" id="txtRazonSocial" name="txtRazonSocial" maxlength="30" class="form-control" placeholder="Razón Social o Nombre de Fantasia" required>
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <label for="txtCUIT" class="sr-only">CUIT</label>
            <input type="text" name="txtCUIT" id="txtCUIT" data-mask="99-99999999-9" onkeypress="return permite(event, 'num')" class="form-control" placeholder="CUIT" maxlength="11" data-minlength="11" data-error="Ingresa 11 caracteres" required>
            <span class="help-block with-errors"></span>
        </div>

        <div class="form-group">
            <label class="sr-only" for="txtInicioActividades">Inicio de actividades</label>
            <input type="text" id="txtInicioActividades" data-mask="99/99/9999" name="txtInicioActividades" maxlength="10" class="form-control" placeholder="Inicio de actividades" required>
        </div>
		<input type="hidden" name="idempr" id="idempr" value="<?php echo $modidempresa; ?>" >
		<div class="col-md-3">
        <div class="form-group">
            <label for="txtDomicilio" class="sr-only">Direccion</label>
            <input type="text" id="txtDomicilio" name="txtDomicilio" class="form-control" placeholder="Domicilio">
            <div class="help-block with-errors"></div>
        </div>
		</div>
		<div class="col-md-3">
        <div class="form-group">
            <label for="txtDomicilio" class="sr-only">Localidad</label>
            <input type="text" id="txtLocalidad" name="txtLocalidad" class="form-control" placeholder="Localidad">
            <div class="help-block with-errors"></div>
        </div>
		</div>
		<div class="col-md-3">
        <div class="form-group">
            <label for="txtDomicilio" class="sr-only">Provincia/Estado</label>
            <input type="text" id="txtProvincia" name="txtProvincia" class="form-control" placeholder="Provincia/Estado">
            <div class="help-block with-errors"></div>
        </div>
		</div>
		<div class="col-md-3">
            <div class="form-group">
                <label class="sr-only" for="cboPais">País</label>
				<select class="form-control" name="pais" id="cboPais">
                        <option value="Argentina">Argentina</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Brasil">Brasil</option>
                        <option value="Traidores">Chile</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Perú</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Venezuela">Venezuela</option>
                </select>
            </div>
		</div>
        <div class="form-group">
            <label for="txtTelefono" class="sr-only">Telefono</label>
            <input type="number" id="txtTelefono" name="txtTelefono" class="form-control"  placeholder="Teléfono">
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <label for="cboCondicionIVA" class="">Condición IVA</label>
            <select class="form-control" name="cboCondicionIVA" id="cboCondicionIVA">
			<?php
				$con_per=consulta("SELECT * FROM condiva");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idcondi = $p['idiva'];
					$descr = $p['iva_descr'];
			?>
                <option value="<?php echo $idcondi;?>"><?php echo $descr;?></option>
			<?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label for="txtIngresosBrutos" class="sr-only">Ingresos brutos</label>
            <input type="number" id="txtIngresosBrutos" name="txtIngresosBrutos" class="form-control" placeholder="Ingresos brutos">
            <div class="help-block with-errors"></div>
        </div>

        <div class="form-group">
            <label for="cboPuntosVenta" class="">Puntos de Venta</label>
            <select class="form-control"  onchange="pventas();" name="cboPuntosVenta" id="cboPuntosVenta" style="width: 110px">
                <option value="1">Si</option>
                <option value="2">No</option>
            </select>
            <label for="txtCantidad" class="sr-only">Cantidad</label>
            <input type="number" id="txtCantidad" name="txtCantidad" class="form-control" placeholder="Cantidad" style="width: 110px; margin-left: 135px; margin-top: -34px;">
        </div>

        <div class="form-group">
            <label for="cboPeriodo" class="">Periodo</label>
            <select class="form-control" name="cboPeriodo" id="cboPeriodo" required>
				<option selected></option>
				<?php
				$con_per=consulta("select * from periodos where idusuario='$idusuario'");
				while ($p = mysqli_fetch_array($con_per, MYSQLI_ASSOC)) {
					$idperiodo = $p['idperiodo'];
					$descr = $p['descripcion'];
				?>
				<option value = '<?php echo $idperiodo;?>'><?php echo $descr; ?></option>
				<?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label for="cboPlanCuentas" class="">Plan de cuentas</label>
            <select class="form-control" name="cboPlanCuentas" id="cboPlanCuentas" required>
				<option selected></option>
				<option value="0">Plan de Cuentas por defecto</option>
				<?php if($idprofe != 0) { ?>
				<option value="<?php echo $idprofe; ?>">Plan de Cuentas del Profesor</option>
				<?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label for="cboPuntosVenta" class="">¿En qué número comienzan los asientos?</label>
            <label for="txtNroAsiento" class="sr-only">Numero inicio de asientos</label>
            <input type="number" id="txtNroAsiento" name="txtNroAsiento" class="form-control" value="1" style="width: 110px;">
        </div>

        <div class="form-group">
            <div class="pull-right">
                <button type="button" id="btnAtras" class="btn btn-default btn-lg" onclick="window.location.href='empresas.php';">Atrás</button>
				<button type="submit" id="btnAceptar" class="btn btn-primary btn-lg"><?php echo $txtboton; ?></button>
            </div>
        </div>
        </form>
    </div>
    <!-- Final Page Content -->
    <br><br><br><br><br>
    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

    <script type="text/javascript">

	function pventas() {
		if ($( '#cboPuntosVenta' ).val() == "1") {
			$("#txtCantidad").show();
		} else {
			$("#txtCantidad").hide();
		};
	};

    $(document).ready(function() {
        $('#empresas').dataTable({
            "paging":   false,
            "info":     false,
            "searching":   false,
			"order": [[ 2, "asc" ]],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'Seleccione "Cargar nueva empresa" para crea su primer empresa.',
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
        });

		$('.delform').on('submit',function(event){
			event.preventDefault();
			console.log($(this));
			data = $(this).serialize();

			BootstrapDialog.show({
				title: 'Eliminar Empresa',
				message: '¿Esta seguro que desea eliminar esta empresa?',
				buttons: [{
					label: ' Aceptar',
					cssClass: 'btn-primary',
					action: function(){
						$.ajax({
							type: "POST",
							url: "eliminaciones.php",
							data: data
							}).done(function( msg ) {
								BootstrapDialog.show({
									message: msg,
									buttons: [{
										label: 'Aceptar',
										action: function() {
											window.location.reload();
										}
									}]
								});
							});
						dialogItself.close();
					}
				}, {
					label: ' Cancelar',
					cssClass: 'btn-default',
					action: function(dialogItself){
						dialogItself.close();
					}
				}]
			});
		});

		//Calendario
		$("#txtInicioActividades").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y',maxDate:'0'});

		<?php
			if(isset($_POST['edemp']) && !empty($_POST['edemp'])){
				echo "switchDivs();";
				echo "$( '#txtRazonSocial' ).val( '$modrsocial' );";
				echo "$( '#txtCUIT' ).val( '$modcuit' );";
				echo "$( '#txtInicioActividades' ).val( '$modiniact' );";
				echo "$( '#txtDomicilio' ).val( '$moddomicilio' );";
				echo "$( '#txtLocalidad' ).val( '$modlocalidad' );";
				echo "$( '#txtProvincia' ).val( '$modprovincia' );";
				echo "$( '#cboPais' ).val( '$modpais' );";
				echo "$( '#txtTelefono' ).val( '$modtelefono' );";
				echo "$( '#cboCondicionIVA' ).val( '$modcondiva' );";
				echo "$( '#txtIngresosBrutos' ).val( '$modingbru' );";
				echo "$( '#cboPuntosVenta' ).val( '$modpventas' );";
				echo "pventas();";
				echo "$( '#txtCantidad' ).val( '$modcantpventas' );";
				echo "$( '#cboPeriodo' ).val( '$modperiodos' );";
				echo "$( '#cboPlanCuentas' ).val( '$modcuentas' );";
				echo "$( '#txtNroAsiento' ).val( '$modiniasientos' );";
			}
		?>

	});


    function switchDivs(){
        $("#divEmpresas").toggleClass('sr-only');
        $("#divNuevaEditar").toggleClass('sr-only');
    }
    </script>
    <!-- ============================================================= -->
</body>
</html>
