<?php
	include "funciones.php";

	if (isset($_POST['email']) && !empty($_POST['email']) &&
		isset($_POST['pass']) && !empty($_POST['pass']) &&
		isset($_POST['tipousr']) && !empty($_POST['tipousr'])) {
		
		$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
		$pass = trim($_POST['pass']);
		$tipousr = $_POST['tipousr'];
		
		/* Verifica que no exista otro usuario con el mismo email */
		$con_u=consulta("select email from usuario where email='$email'");
		$u=mysqli_num_rows($con_u);
		if($u>0){
			mensaje("El Email ingresado ya existe. Ingrese uno diferente");
			ir_a("signup.php");
		} elseif(!$email) {
			mensaje("El Email ingresado no es valido. Ingrese uno diferente");
			ir_a("signup.php");
		} else {

			$insertar = "insert into usuario 
							(email,pass,habilitado,tipousuario) values
							('$email','$pass','1','$tipousr')";
				
			$new = mysqli_query($link, $insertar);
				
			$idcreada = mysqli_insert_id($link);
			
			$año =  date("Y"); 
			$periodo = "Periodo " . $año;
			$desde = "$año-01-01";
			$hasta = "$año-12-31";
			
			$insertar_periodo = "INSERT INTO periodos (idusuario,periodo,descripcion,desde,hasta)
					VALUES ('$idcreada','$año','$periodo','$desde','$hasta')";
			
			$new_periodo = mysqli_query($link, $insertar_periodo);
				
			$idperiodo = mysqli_insert_id($link);
					
			consulta("UPDATE usuario SET idperiodo = '$idperiodo' WHERE idusuario = '$idcreada'");
			
			if($tipousr == 2){
			    consulta("INSERT INTO modeloscuentas (codigo,denominacion,rubro,imputable,sumariza)
						SELECT codigo, denominacion, rubro, imputable, sumariza FROM modeloscuentas WHERE idprofesor = 0");
				
				consulta("UPDATE modeloscuentas SET idprofesor = '$idcreada' WHERE idprofesor = -1");
				
				consulta("UPDATE usuario SET idprofesor = '$idcreada' WHERE idusuario = '$idcreada'");
			}
			
								
			if(!$new){
					echo "Mensaje: ".mysqli_error($link);
					mensaje("Error");
			} else {
				mensaje("Cuenta creada con exito ");
				acthistnuevousuario($email);
				ir_a("index.php");
			}
			
		}
	} else {
		ir_a("signup.php");
	}
?>
