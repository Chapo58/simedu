<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php" ?>
    <title>Simedu | Plan de cuentas</title>
	<link href="../jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="../jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
	<script src="../jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../jtable/js/jquery.jtable.js" type="text/javascript"></script>
</head>

<body>
	
    <!-- Page Content -->
	
    <h1 class="page-header">Planes de cuentas</h1>
    <input type="button" id="btnNueva" onclick="$('#Cuentas').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Agregar cuenta">
	<div class="space50"></div>

        <div class="row">
            <div class="col-xs-12">
				<div id="Cuentas" style="width: 100%;"></div>
            </div>
        </div>
	
	<a href="index.php" style="float:right;margin-top:20px;" class="btn btn-lg btn-info">
	<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver
	</a>
    <!-- Final Page Content -->
	
<script type="text/javascript">
	$(document).ready(function () {

		    //Prepare jTable
			$('#Cuentas').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Plan de Cuentas',
				paging: true,
				sorting: true,
				defaultSorting: 'codigo ASC',
				actions: {
					listAction: 'ModeloCuentasAct.php?action=list',
					createAction: 'ModeloCuentasAct.php?action=create',
					updateAction: 'ModeloCuentasAct.php?action=update',
					deleteAction: 'ModeloCuentasAct.php?action=delete'
				},
				fields: {
					idcuenta: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					codigo: {
						title: 'Codigo',
						width: '10%',
						visibility: 'fixed'
					},
					denominacion: {
						title: 'Denominacion',
						width: '40%'
					},
					rubro: {
						title: 'Rubro',
						width: '20%',
						options: { '1': 'Activo', '2': 'Pasivo', '3': 'Patrimonio Neto', '4': 'Ingresos y Ganancias', '5': 'Gastos y Perdidas' }
					},
					imputable: {
						title: 'Imputable',
						width: '10%',
						options: { '1': 'Si', '2': 'No' }
					},
					sumariza: {
						title: 'Sumariza',
						width: '20%'
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay registros cargados!',
					addNewRecord: 'Agregar Cuenta',
					editRecord: 'Editar Cuenta',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El registro será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando registros {0} a {1} de {2}',
					pageSizeChangeLabel: 'Registros por página',
					gotoPageLabel: 'Ir a la pagina',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});
			
			 $('#Cuentas').jtable('load');
	});
</script>
</body>
</html>