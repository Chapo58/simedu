<?php
session_start();
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM empresas;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscar']) && !empty($_POST['buscar'])) {
			$buscar = $_POST['buscar'];
		} else {
			$buscar = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM empresas WHERE rsocial LIKE '%".$buscar."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$a1 = $_POST["idusuario"];
		$a2 = $_POST["rsocial"];
		$a3 = $_POST["cuit"];
		$a4 = $_POST["inicioact"];
		$a5 = $_POST["domicilio"];
		$a6 = $_POST["telefono"];
		$a7 = $_POST["condiva"];
		$a8 = $_POST["ingbrutos"];
		$a9 = $_POST["pventas"];
		$a10 = $_POST["cantpventas"];
		$a11 = $_POST["idperiodo"];
		$a12 = $_POST["iniasientos"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO empresas(idusuario, rsocial, cuit, inicioact, domicilio, telefono, condiva, ingbrutos,pventas, cantpventas, idperiodo, iniasientos) VALUES('$a1','$a2','$a3','$a4','$a5','$a6','$a7','$a8','$a9','$a10','$a11','$a12');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM empresas WHERE idempresa = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idempresa"];
		$a1 = $_POST["idusuario"];
		$a2 = $_POST["rsocial"];
		$a3 = $_POST["cuit"];
		$a4 = date("Y-m-d", strtotime($_POST["inicioact"]));
		$a5 = $_POST["domicilio"];
		$a6 = $_POST["telefono"];
		$a7 = $_POST["condiva"];
		$a8 = $_POST["ingbrutos"];
		$a9 = $_POST["pventas"];
		$a10 = $_POST["cantpventas"];
		$a11 = $_POST["idperiodo"];
		$a12 = $_POST["iniasientos"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE empresas SET idusuario = '$a1', rsocial = '$a2', cuit = '$a3', inicioact = '$a4', domicilio = '$a5'
			, telefono = '$a6', condiva = '$a7', ingbrutos = '$a8', pventas = '$a9', cantpventas = '$a10', idperiodo = '$a11', iniasientos = '$a12' WHERE idempresa = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idempresa"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM empresas WHERE idempresa = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>