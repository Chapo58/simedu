<?php
session_start();
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM usuario;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscar']) && !empty($_POST['buscar'])) {
			$buscar = $_POST['buscar'];
		} else {
			$buscar = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM usuario WHERE email LIKE '%".$buscar."%' OR nombre LIKE '%".$buscar."%' OR apellido LIKE '%".$buscar."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$a1 = $_POST["idprofesor"];
		$a2 = $_POST["email"];
		$a3 = $_POST["nombre"];
		$a4 = $_POST["apellido"];
		$a5 = $_POST["sexo"];
		$a6 = date("Y-m-d", strtotime($_POST["fechanacimiento"]));
		$a7 = $_POST["pass"];
		$a8 = $_POST["direccion"];
		$a9 = $_POST["ciudad"];
		$a10 = $_POST["provincia"];
		$a11 = $_POST["pais"];
		$a12 = $_POST["telefono"];
		$a13 = $_POST["habilitado"];
		$a14 = $_POST["tipousuario"];
		$a15 = $_POST["facebook"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO usuario(idprofesor, email, nombre, apellido, sexo, fechanacimiento, pass, direccion, ciudad, provincia, pais, telefono, habilitado, tipousuario, faceboook) VALUES('$a1','$a2','$a3','$a4','$a5','$a6','$a7','$a8','$a9','$a10','$a11','$a12','$a13','$a14','$a15');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM usuario WHERE idusuario = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idusuario"];
		$a1 = $_POST["idprofesor"];
		$a2 = $_POST["email"];
		$a3 = $_POST["nombre"];
		$a4 = $_POST["apellido"];
		$a5 = $_POST["sexo"];
		$a6 = date("Y-m-d", strtotime($_POST["fechanacimiento"]));
		$a7 = $_POST["pass"];
		$a8 = $_POST["direccion"];
		$a9 = $_POST["ciudad"];
		$a10 = $_POST["provincia"];
		$a11 = $_POST["pais"];
		$a12 = $_POST["telefono"];
		$a13 = $_POST["habilitado"];
		$a14 = $_POST["tipousuario"];
		$a15 = $_POST["facebook"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE usuario SET idprofesor = '$a1', email = '$a2', nombre = '$a3', apellido = '$a4', sexo = '$a5'
			, fechanacimiento = '$a6', pass = '$a7', direccion = '$a8', ciudad = '$a9', provincia = '$a10', pais = '$a11', telefono = '$a12', habilitado = '$a13', tipousuario = '$a14', facebook = '$a15' WHERE idusuario = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idusuario"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM usuario WHERE idusuario = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>