<!DOCTYPE html>
<html lang="en">
<head>
    <title>Simedu | Panel de Administración</title>
	<?php include "head.php"; ?>
</head>

<body>
    <!-- Page Content -->
  <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row text-center">
                <h1 class="page-header">Panel de Administración</h1>
                <p>Bienvenido al panel de administración de Simedu. Abajo se listan las diferentes categorías disponibles.</p>
            </div>

      <div class="row text-center">
        <div class="col-md-3 col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-certificate fa-fw"></i>&nbsp;&nbsp;Datos principales</h3>
            </div>
            <ul class="list-group">
              <a href="datos.php" class="list-group-item">Datos</a>
              <a href="http://mail.simedu.com.ar" class="list-group-item">Correo Electronico</a>
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;Configuración interna</h3>
            </div>
            <ul class="list-group">
              <a href="#" class="list-group-item">Módulos</a>
              <a href="modelo_cuentas.php" class="list-group-item">Modelo Plan de Cuentas</a>
              <a href="lista_obras_sociales.php" class="list-group-item">Obras Sociales</a>
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title" style="font-size:130%;"><i class="fa fa-users"></i>&nbsp;&nbsp;Configuración Usuarios</h3>
            </div>
            <ul class="list-group">
              <a href="lista_usuarios.php" class="list-group-item">Usuarios Registrados</a>
              <a href="historial.php" class="list-group-item">Historial</a>
              <a href="lista_periodos.php" class="list-group-item">Períodos</a>
              <a href="modxusuario.php" class="list-group-item">Modulos habilitados a usuario</a>
            </ul>
          </div>
        </div>
        <div class="col-md-3 col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title" style="font-size:130%;"><i class="fa fa-industry"></i>&nbsp;&nbsp;Configuración Empresas</h3>
            </div>
            <ul class="list-group">
              <a href="lista_empresas.php" class="list-group-item">Empresas Registradas</a>
              <a href="#" class="list-group-item">Productos</a>
              <a href="lista_fichas.php" class="list-group-item">Fichas</a>
              <a href="modxempresa.php" class="list-group-item">Modulos habilitados a empresas</a>
              <a href="lista_asientos.php" class="list-group-item">Asientos</a>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
	<div class="navbar navbar-default navbar-fixed-bottom">
	  <div class="container">
      <div class="row">
        <div class="col-md-6 col-xs-12">
      		<span style="width:100%;" class="navbar-text">
      			<a style="width:100%;" href="../inicio.php" class="btn btn-lg btn-info">
      			<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver al simulador
      			</a>
      		</span>
        </div>
        <div class="col-md-6 col-xs-12">
      		<span style="width:100%;" class="navbar-text">
      		<a style="width:100%;" href="errores.php" class="btn btn-default btn-lg pull-right"><strong>REPORTES</strong></a>
      		</span>
        </div>
      </div>
	  </div>
	</div>
    <!-- Final Page Content -->

    <!-- Scripts al final del DOM para que la pagina cargue mas rapido -->
    <!-- ============================================================= -->
    <script src="../js/jquery-2.1.4.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <!-- ============================================================= -->
</body>
</html>
