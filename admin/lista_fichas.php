<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php" ?>
    <title>Simedu | Fichas Empresas</title>
	<link href="../jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="../jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
	<script src="../jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../jtable/js/jquery.jtable.js" type="text/javascript"></script>
</head>

<body>
	
    <!-- Page Content -->
	
    <h1 class="page-header">Fichas Empresas</h1>
    <input type="button" id="btnNueva" onclick="$('#Tabla').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Nueva Ficha">
	<div class="space50"></div>

        <div class="row">
            <div class="col-xs-12">
				<div id="Tabla" style="width: 100%;"></div>
            </div>
        </div>
	
	<a href="index.php" style="float:right;margin-top:20px;" class="btn btn-lg btn-info">
	<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver
	</a>
    <!-- Final Page Content -->
	
<script type="text/javascript">
	$(document).ready(function () {

		    //Prepare jTable
			$('#Tabla').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Fichas Empresas',
				paging: true,
				sorting: true,
				defaultSorting: 'idempresa ASC',
				actions: {
					listAction: 'ListaFichasAct.php?action=list',
					createAction: 'ListaFichasAct.php?action=create',
					updateAction: 'ListaFichasAct.php?action=update',
					deleteAction: 'ListaFichasAct.php?action=delete'
				},
				fields: {
					idficha: {
						title: 'ID',
						key: true,
						create: false,
						edit: false,
						list: true
					},
					idempresa: {
						title: 'Empresa',
						width: '20%',
						visibility: 'fixed',
						options: {
						<?php
						$con_e=consulta("SELECT idempresa, rsocial FROM empresas");
						while ($e = mysqli_fetch_array($con_e, MYSQLI_ASSOC)) {
						$id = $e['idempresa'];
						$rsocial = $e['rsocial'];
						echo "'".$id."'".":"."'".$rsocial."'".",";
						} ?>
						}
					},
					imagen: {
						title: 'Imagen',
						width: '20%'
					},
					titulo: {
						title: 'Titulo',
						width: '10%'
					},
					texto: {
						title: 'Texto',
						width: '50%',
						input: function (data) {
						  if (data.record) {
						  return '<textarea cols="30" id="texto" name="texto" rows="5" wrap="hard">' + data.record.texto+ '</textarea>';
						  } else {
							return '<textarea cols="30" rows="5" wrap="hard" name="texto"/>';
							   }
						}
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay registros cargados!',
					addNewRecord: 'Agregar Registro',
					editRecord: 'Editar Registro',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El registro será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando registros {0} a {1} de {2}',
					pageSizeChangeLabel: 'Registros por página',
					gotoPageLabel: 'Ir a la pagina',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});
			
			 $('#Tabla').jtable('load');
	
	});
</script>
</body>
</html>