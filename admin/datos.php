<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php" ?>
	<?php
		$con_pie=consulta("SELECT * FROM piepagina");
		$p=mysqli_fetch_array($con_pie);
		
		if(isset($_POST['linea1'])) {
			$linea1 = $_POST['linea1'];
			$linea2 = $_POST['linea2'];
			$linea3 = $_POST['linea3'];
			$linea4 = $_POST['linea4'];
			$stecnico = $_POST['stecnico'];
			consulta("UPDATE piepagina SET linea1 = '$linea1', linea2 = '$linea2', linea3 = '$linea3', linea4 = '$linea4', stecnico = '$stecnico'");
			$con_pie=consulta("SELECT * FROM piepagina");
			$p=mysqli_fetch_array($con_pie);
			mensaje("Los cambios se guardaron correctamente");
		}
	?>
    <title>Simedu | Configracion</title>
</head>

<body>
	
    <!-- Page Content -->
	
    <h1 class="page-header">Pie de Pagina</h1>
	<form role="form" data-toggle="validator" action="datos.php" method="POST">
	<div class="col-md-12">
        <div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-flag"></span><strong>&nbsp;&nbsp;Primera Linea</strong></div>
                    <input type="text" id="linea1" name="linea1" class="form-control" value="<?php echo $p['linea1']; ?>" placeholder="Primera Linea">
            </div>
		</div>
	</div>
	<div class="col-md-12">
        <div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-flag"></span><strong>&nbsp;&nbsp;Segunda Linea</strong></div>
                    <input type="text" id="linea2" name="linea2" class="form-control" value="<?php echo $p['linea2']; ?>" placeholder="Segunda Linea">
            </div>
		</div>
	</div>
	<div class="col-md-12">
        <div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-flag"></span><strong>&nbsp;&nbsp;Tercer Linea</strong></div>
                    <input type="text" id="linea3" name="linea3" class="form-control" value="<?php echo $p['linea3']; ?>" placeholder="Tercera Linea">
            </div>
		</div>
	</div>
	<div class="col-md-12">
        <div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-flag"></span><strong>&nbsp;&nbsp;Cuarta Linea</strong></div>
                    <input type="text" id="linea4" name="linea4" class="form-control" value="<?php echo $p['linea4']; ?>" placeholder="Cuarta Linea">
            </div>
		</div>
	</div>
	<div class="col-md-12">
        <div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"><span class="glyphicon glyphicon-wrench"></span><strong>&nbsp;&nbsp;Soporte Tecnico</strong></div>
                    <input type="text" id="stecnico" name="stecnico" class="form-control" value="<?php echo $p['stecnico']; ?>" placeholder="Soporte Tecnico">
            </div>
		</div>
	</div>
	<div class="col-md-offset-6 col-md-6">
        <div class="form-group">
            <button style="width:100%;" type="submit" id="btnGuardarDatos" class="btn btn-primary btn-lg">
				<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Guardar Cambios
			</button>
        </div>
	</div>
	</form>
	
	<a href="index.php" style="float:right;margin-top:20px;" class="btn btn-lg btn-info">
	<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver
	</a>
    <!-- Final Page Content -->
	
<script type="text/javascript">
	$(document).ready(function () {

	});
</script>
</body>
</html>