<?php
session_start();
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM sueldos_obras_sociales;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM sueldos_obras_sociales ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$a1 = $_POST["codigo"];
		$a2 = $_POST["descripcion"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO sueldos_obras_sociales(codigo, descripcion) VALUES('$a1','$a2');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM sueldos_obras_sociales WHERE idobrasocial = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idobrasocial"];
		$a1 = $_POST["codigo"];
		$a2 = $_POST["descripcion"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE sueldos_obras_sociales SET codigo = '$a1', descripcion = '$a2' WHERE idobrasocial = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idobrasocial"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM sueldos_obras_sociales WHERE idobrasocial = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>