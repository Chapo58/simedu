<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php" ?>
    <title>Simedu | Empresas Registradas</title>
	<link href="../jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="../jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
	<script src="../jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../jtable/js/jquery.jtable.js" type="text/javascript"></script>
</head>

<body>
	
    <!-- Page Content -->
	
    <h1 class="page-header">Empresas Registradas</h1>
    <input type="button" id="btnNueva" onclick="$('#Tabla').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Nueva Empresa">
	<div class="space50"></div>
		
		<div class="well well-sm">
			<div class="row">
				<div class="col-md-12">
					<input type="text" class="form-control" name="buscardenominacion" placeholder="Buscar" id="buscar" />
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-xs-12">
				<div id="Tabla" style="width: 100%;"></div>
            </div>
        </div>
	
	<a href="index.php" style="float:right;margin-top:20px;" class="btn btn-lg btn-info">
	<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver
	</a>
    <!-- Final Page Content -->
	
<script type="text/javascript">
	$(document).ready(function () {

		    //Prepare jTable
			$('#Tabla').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Empresas Registradas',
				paging: true,
				sorting: true,
				defaultSorting: 'idempresa ASC',
				actions: {
					listAction: 'ListaEmpresasAct.php?action=list',
					createAction: 'ListaEmpresasAct.php?action=create',
					updateAction: 'ListaEmpresasAct.php?action=update',
					deleteAction: 'ListaEmpresasAct.php?action=delete'
				},
				fields: {
					idempresa: {
						title: 'ID',
						key: true,
						create: false,
						edit: false,
						list: true
					},
					idusuario: {
						title: 'Usuario',
						width: '5%',
						visibility: 'fixed',
						options: {
						<?php
						$con_u=consulta("SELECT idusuario, email FROM usuario");
						while ($u = mysqli_fetch_array($con_u, MYSQLI_ASSOC)) {
						$id = $u['idusuario'];
						$mail = $u['email'];
						echo "'".$id."'".":"."'".$mail."'".",";
						} ?>
						}
					},
					rsocial: {
						title: 'Razon Social',
						width: '10%'
					},
					cuit: {
						title: 'C.U.I.T.',
						width: '10%'
					},
					inicioact: {
						title: 'Inicio de Actividades',
						width: '10%'
					},
					domicilio: {
						title: 'Domicilio',
						width: '10%'
					},
					telefono: {
						title: 'Telefono',
						width: '10%'
					},
					condiva: {
						title: 'Condicion de IVA',
						width: '15%',
						options: { '1': 'Responsable Inscripto', '2': 'Monotributista' }
					},
					ingbrutos: {
						title: 'Ingresos Brutos',
						width: '10%'
					},
					pventas: {
						title: 'Puntos de Venta',
						width: '10%',
						options: { '1': 'Si', '2': 'No'}
					},
					cantpventas: {
						title: 'Cantidad Puntos de Venta',
						width: '10%'
					},
					idperiodo: {
						title: 'Periodo',
						width: '8%'
					},
					iniasientos: {
						title: 'Inicio de Asientos',
						width: '10%'
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay registros cargados!',
					addNewRecord: 'Agregar Registro',
					editRecord: 'Editar Registro',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El registro será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando registros {0} a {1} de {2}',
					pageSizeChangeLabel: 'Registros por página',
					gotoPageLabel: 'Ir a la pagina',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				}
			});
			
			 $('#Tabla').jtable('load');
			 
			 $('#buscar').on('keyup', function() {
				$('#Tabla').jtable('load', {
					buscar: $(this).val(),
				});
			 });
			 
			  $.datepicker.regional['es'] = {
			 closeText: 'Cerrar',
			 prevText: '<Ant',
			 nextText: 'Sig>',
			 currentText: 'Hoy',
			 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			 weekHeader: 'Sm',
			 dateFormat: 'dd/mm/yy',
			 firstDay: 1,
			 isRTL: false,
			 showMonthAfterYear: false,
			 yearSuffix: ''
			 };
			 $.datepicker.setDefaults($.datepicker.regional['es']);
	});
</script>
</body>
</html>