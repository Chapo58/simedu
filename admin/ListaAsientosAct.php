<?php
session_start();
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM listaasientos;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscar']) && !empty($_POST['buscar'])) {
			$buscar = $_POST['buscar'];
		} else {
			$buscar = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM listaasientos WHERE denominacion LIKE '%".$buscar."%' OR nota LIKE '%".$buscar."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$a1 = $_POST["idempresa"];
		$a2 = $_POST["idperiodo"];
		$a3 = $_POST["denominacion"];
		$a4 = $_POST["fecha"];
		$a5 = $_POST["nlista"];
		$a6 = $_POST["nota"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO listaasientos(idempresa, idperiodo, denominacion, fecha, nlista, nota) VALUES('$a1','$a2','$a3','$a4','$a5','$a6');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM listaasientos WHERE idlista = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idlista"];
		$a1 = $_POST["idempresa"];
		$a2 = $_POST["idperiodo"];
		$a3 = $_POST["denominacion"];
		$a4 = $_POST["fecha"];
		$a5 = $_POST["nlista"];
		$a6 = $_POST["nota"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE listaasientos SET idempresa = '$a1', idperiodo = '$a2', denominacion = '$a3', fecha = '$a4', nlista = '$a5', nota = '$a6' WHERE idlista = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idlista"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM listaasientos WHERE idlista = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>