<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php" ?>
    <title>Simedu | Configracion</title>
	<link rel="stylesheet" type="text/css" href="css/datatable.css">
</head>

<body>

    <!-- Page Content -->

    <h1 class="page-header">Historial</h1>

	  <table id="historial" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Fecha</th>
        <th>Hora</th>
        <th>Accion</th>
        <th>Profesor</th>
				<th>Usuario</th>
        <th>Empresa</th>
				<th>IP</th>
            </tr>
        </thead>
		<tfoot>
			<tr>
				<th></th>
        <th></th>
        <th></th>
				<th></th>
        <th></th>
        <th></th>
				<th></th>
      </tr>
		</tfoot>
        <tbody>
			<?php
				$con_hist=consulta("SELECT historial.*, usuarios.alumno, usuarios.profesor
          FROM historial LEFT JOIN (SELECT usuario.idusuario, usuario.email as 'alumno', IFNULL(profesor.email,'Sin Profesor') as 'profesor' FROM usuario LEFT JOIN usuario AS profesor ON usuario.idprofesor = profesor.idusuario) AS usuarios
          ON historial.idusuario = usuarios.idusuario ORDER BY historial.idhistorial DESC");
				while ($h = mysqli_fetch_array($con_hist, MYSQLI_ASSOC)) {
					$empresa = $h['rsocialempresa'];
					$fecha = fecha($h['fecha'],'/');
					$hora = $h['hora'];
					$accion = $h['accion'];
					$alumno = $h['alumno'];
          $profesor = $h['profesor'];
					$ip = $h['ip'];
			?>
            <tr>
				<td><?php echo $fecha; ?></td>
        <td><?php echo $hora; ?></td>
        <td><?php echo $accion; ?></td>
        <td><?php echo $profesor; ?></td>
				<td><?php echo $alumno; ?></td>
        <td><?php echo $empresa; ?></td>
				<td><?php echo $ip; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>

	<a href="index.php" style="float:right;margin-top:20px;" class="btn btn-lg btn-info">
	<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver
	</a>
    <!-- Final Page Content -->

<script type="text/javascript">
	$(document).ready(function () {
		var table = $('#historial').DataTable({
			initComplete: function () {
            this.api().columns(2).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
            this.api().columns(3).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
			},
      "aaSorting": [],
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
</script>
</body>
</html>
