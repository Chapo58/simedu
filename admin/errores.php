<!DOCTYPE html>
<html lang="es">
<head>
    <?php include "head.php" ?>
    <title>Simedu | Configracion</title>
	<link rel="stylesheet" type="text/css" href="css/datatable.css">
</head>

<body>
	
    <!-- Page Content -->
	
    <h1 class="page-header">Errores</h1>

	  <table id="tabla" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Usuario</th>
                <th>Empresa</th>
                <th>Periodo</th>
				<th>Comentario</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
			<?php	
				$con_hist=consulta("SELECT errores.*, usuario.email, usuario.pass, empresas.rsocial, periodos.periodo 
				FROM errores LEFT JOIN usuario ON errores.idusuario = usuario.idusuario LEFT JOIN empresas ON errores.idempresa = empresas.idempresa
				LEFT JOIN periodos ON errores.idperiodo = periodos.idperiodo ORDER BY iderror DESC");
				while ($h = mysqli_fetch_array($con_hist, MYSQLI_ASSOC)) {
					$empresa = $h['rsocial'];
					$fecha = date("d/m/Y",strtotime($h['fecha']));
					$hora = $h['hora'];
					$periodo = $h['periodo'];
					$usuario = $h['email'] . " >> " . $h['pass'];
					$comentario = $h['comentario'];
			?>
            <tr>
				<td><?php echo $usuario; ?></td>
                <td><?php echo $empresa; ?></td>
                <td><?php echo $periodo; ?></td>
				<td><?php echo $comentario; ?></td>
                <td><?php echo $fecha . "  " . $hora; ?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
	
	<a href="index.php" style="float:right;margin-top:20px;" class="btn btn-lg btn-info">
	<span class="glyphicon glyphicon-chevron-left"></span><strong>&nbsp;&nbsp;Volver
	</a>
    <!-- Final Page Content -->
	
<script type="text/javascript">
	$(document).ready(function () {
		var table = $('#tabla').DataTable({
			"bSort": false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	});
</script>
</body>
</html>