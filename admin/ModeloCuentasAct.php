<?php
session_start();
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM modeloscuentas;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM modeloscuentas WHERE idprofesor = 0 ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$codigo = $_POST["codigo"];
		$denominacion = $_POST["denominacion"];
		$rubro = $_POST["rubro"];
		$imputable = $_POST["imputable"];
		$sumariza = $_POST["sumariza"];
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO modeloscuentas(idprofesor, codigo, denominacion, rubro, imputable, sumariza) VALUES(0,'$codigo','$denominacion','$rubro','$imputable','$sumariza');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM modeloscuentas WHERE idcuenta = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idcuenta"];
		$codigo = $_POST["codigo"];
		$denominacion = $_POST["denominacion"];
		$rubro = $_POST["rubro"];
		$imputable = $_POST["imputable"];
		$sumariza = $_POST["sumariza"];
		//Update record in database
		$result = mysqli_query($con,"UPDATE modeloscuentas SET codigo = '$codigo', denominacion = '$denominacion', rubro = '$rubro', imputable = '$imputable', sumariza = '$sumariza' WHERE idcuenta = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idcuenta"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM modeloscuentas WHERE idcuenta = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>