<?php
session_start();
try
{
	include "../funciones.php";

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM usuario;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];
		
		if (isset($_POST['buscar']) && !empty($_POST['buscar'])) {
			$buscar = $_POST['buscar'];
		} else {
			$buscar = "";
		}

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM usuario WHERE email LIKE '%".$buscar."%' OR nombre LIKE '%".$buscar."%' OR apellido LIKE '%".$buscar."%' ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idusuario"];
		$a1 = $_POST["modcontable"];
		$a2 = $_POST["modempresarial"];
		$a3 = $_POST["modludico"];
		$a4 = $_POST["impexp"];
		
		//Update record in database
		$con_usr=consulta("SELECT tipousuario FROM usuario WHERE idusuario='$id'");
		$u=mysqli_fetch_array($con_usr);
		if($u['tipousuario'] == 2){ 
			// Si es profesor modifico tambien a sus alumnos
			$result = mysqli_query($con,"UPDATE usuario SET modcontable = '$a1', modempresarial = '$a2', modludico = '$a3', impexp = '$a4' WHERE idprofesor = $id;");
		} else {
			// Si es alumno modifico unicamente ese registro
			$result = mysqli_query($con,"UPDATE usuario SET modcontable = '$a1', modempresarial = '$a2', modludico = '$a3', impexp = '$a4' WHERE idusuario = $id;");
		}
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idusuario"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM usuario WHERE idusuario = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>