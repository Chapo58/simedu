<!DOCTYPE html>
<html lang="es">
<head>
  <title>Simedu | Productos</title>
  <?php require_once('head.php'); ?>
	<?php
		if(isset($_POST['idemp']) && !empty($_POST['idemp'])){
			$idemp = $_POST['idemp'];
			$con_demp=consulta("SELECT * FROM empresas WHERE idempresa='$idemp'");
			$e=mysqli_fetch_array($con_demp);
      $idusu = $e['idusuario'];
      $con_dusu=consulta("SELECT email, nombre, apellido FROM usuario WHERE idusuario='$idusu'");
			$du=mysqli_fetch_array($con_dusu);
		} else {
			ir_a('index.php');
		};
	?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-home home-icon"></i>
              <a href="inicio.php">Inicio</a>
            </li>
            <li><a href="perfil.php">Perfil</a></li>
            <li><a href="grupo.php">Grupo</a></li>
            <li>
              <form id="perfilform" action="perfil_usuarios.php" method="POST" class="pull-right">
                  <a href="javascript:{}" onclick="document.getElementById('perfilform').submit();">Perfil de <?php echo $du['nombre'] . " " . $du['apellido']; ?></a>
                  <input type="hidden" name="idusu" id="idusu" value="<?php echo $idusu; ?>" >
              </form>
            </li>
            <li class="active">Productos de <?php echo $e['rsocial']; ?></li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Productos de <?php echo $e['rsocial']; ?></h1>

  <div class="container">
    <div class="row">
    	<div class="col-md-12">
		<?php
		$con_productos=consulta("SELECT productos.*, marcas.descripcion as marca FROM productos
		LEFT JOIN marcas ON productos.idmarca = marcas.idmarca
		WHERE productos.idempresa='$idemp' AND productos.habilitado = 1 AND productos.stock > 0
		ORDER BY productos.nombre ASC");
		if(mysqli_num_rows($con_productos)>9){$t = 3;} else {$t = 3;}
		while ($p = mysqli_fetch_array($con_productos, MYSQLI_ASSOC)) {
		?>
			<div class="col-sm-6 col-md-<?php echo $t; ?>">
				<div class="thumbnail" >
					<h4 class="text-center"><span class="label label-info"><?php echo $p['marca']; ?></span></h4>
					<img src="<?php echo $p['imagen']; ?>" class="img-responsive">
					<div class="caption">
						<div class="row">
							<div class="col-md-6 col-xs-6">
								<h3><?php echo $p['nombre']; ?></h3>
							</div>
							<div class="col-md-6 col-xs-6 price">
								<h3>
								<label>$<?php echo number_format($p['preciovta'], 2, ',', '.'); ?></label></h3>
							</div>
						</div>
						<p><?php echo $p['descripcion']; ?></p>
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-6">
							<button class="btn btn-success btn-product" onclick="cargarProducto(<?php echo $p['idproducto']; ?>);" >
								<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;Comprar
							</button>
							</div>
						</div>
						<p> </p>
					</div>
				</div>
			</div>
		<?php } ?>
        </div>
	</div>
</div>



  <!-- Final Page Content -->

  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
<script type="text/javascript">
	function cargarProducto(id) {
		var botoncarrito = "<a href='carrito.php' class='btn btn-info' style='float:right;'><span class='glyphicon glyphicon-eye-open'></span>&nbsp;&nbsp;Ver Carrito</a>";
		$.ajax({
			type: "POST",
			url: 'carrito_cargar.php',
			data: { idproducto : id },
				success: function(mensaje) {
					BootstrapDialog.show({
					message: mensaje+botoncarrito,
						buttons: [{
							label: 'Aceptar',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				}
		});
	}
	$(document).ready(function() {

	});
</script>
  <!-- ============================================================= -->
</body>
</html>
