<?php
session_start();
include "funciones.php";
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM bienes_grupos WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM bienes_grupos WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$numero = trim($_POST["numero"]);
		$formulario = trim($_POST["formulario"]);
		$detalle = trim($_POST["detalle"]);
		$año_valuacion = trim($_POST["año_valuacion"]);
		$valuacion_fiscal = trim($_POST["valuacion_fiscal"]);
		$base_imponible = trim($_POST["base_imponible"]);
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO bienes_grupos(idempresa, numero, formulario, detalle, año_valuacion,valuacion_fiscal,base_imponible) VALUES('$idempresa','$numero','$formulario','$detalle','$año_valuacion','$valuacion_fiscal','$base_imponible');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM bienes_grupos WHERE idgrupo = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idgrupo"];
		$numero = trim($_POST["numero"]);
		$formulario = trim($_POST["formulario"]);
		$detalle = trim($_POST["detalle"]);
		$año_valuacion = trim($_POST["año_valuacion"]);
		$valuacion_fiscal = trim($_POST["valuacion_fiscal"]);
		$base_imponible = trim($_POST["base_imponible"]);
		//Update record in database
		$result = mysqli_query($con,"UPDATE bienes_grupos SET numero = '$numero', formulario = '$formulario', detalle = '$detalle', año_valuacion = '$año_valuacion', valuacion_fiscal = '$valuacion_fiscal', base_imponible = '$base_imponible' WHERE idgrupo = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idgrupo"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM bienes_grupos WHERE idgrupo = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>