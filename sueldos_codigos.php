<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Codigos</title>
    <?php require_once('head.php'); ?>
    <?php
      if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
        mensaje("Debe seleccionar una empresa.");
        ir_a("empresas.php");
      }
    ?>
	<link href="jtable/css/jquery-ui.css" rel="stylesheet" type="text/css" />
	<link href="jtable/css/themes/lightcolor/gray/jtable.css" rel="stylesheet" type="text/css" />
</head>

<body class="no-skin">
    <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Liquidación de Sueldos</a>
            </li>
            <li class="active">Codigos</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
    <!-- Page Content -->
    <h1 class="page-header">Codigos</h1>
    <input type="button" id="btnNueva" onclick="$('#Codigos').jtable('showCreateForm'); " class="btn btn-lg btn-primary" value="Agregar codigo">
	<div class="bs-callout bs-callout-info">
		<form>
			<div class="row">
				<div class="col-md-4">
					<input type="text" class="form-control" name="buscardescripcion" placeholder="Descripción" id="buscardescripcion" />
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" id="LoadRecordsButton"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Buscar</button>
			</div>
		</form>
	</div>
        <div class="row">
            <div class="col-xs-12">
				<div id="Codigos" style="width: 100%;"></div>
            </div>
        </div>

    <!-- Final Page Content -->

    </div>
   </div>
  </div><!-- /.main-content -->

  <?php require_once('footer.php'); ?>

    <script src="jtable/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="jtable/js/jquery.jtable.js" type="text/javascript"></script>
<script type="text/javascript">
	var descripcion = [];
	var numero = [];
	var codigovalidar = [];
	var valorprevio = 0;
	<?php
		$con_sum=consulta("SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa ORDER BY numero ASC");
		while ($sum = mysqli_fetch_array($con_sum, MYSQLI_ASSOC)) {
			$des = addslashes($sum['descripcion']);
			$num = addslashes($sum['numero']);
			echo "descripcion.push(\"$des\");";
			echo "numero.push(\"$num\");";
		};
		$con_sum=consulta("SELECT * FROM sueldos_codigos WHERE idempresa = $idempresa");
		while ($sum = mysqli_fetch_array($con_sum, MYSQLI_ASSOC)) {
			$num = $sum['numero'];
			echo "codigovalidar.push(\"$num\");";
		};
	?>
	$(document).ready(function () {

			$('#Codigos').jtable({
				dialogShowEffect: 'puff',
				dialogHideEffect: 'drop',
				title: 'Codigos',
				paging: true,
				sorting: true,
				defaultSorting: 'numero ASC',
				actions: {
					listAction: 'CodigosAct.php?action=list',
					createAction: 'CodigosAct.php?action=create',
					updateAction: 'CodigosAct.php?action=update',
					deleteAction: 'CodigosAct.php?action=delete'
				},
				fields: {
					idcodigo: {
						key: true,
						create: false,
						edit: false,
						list: false
					},
					numero: {
						title: 'Numero',
						width: '10%',
						visibility: 'fixed',
						input: function (data) {
							if (data.record) {
								valorprevio = parseInt(data.record.numero);
								return '<input type="number" name="numero" id="numero" onblur="validarcodigo($(this));" OnKeyUp="sumar(this.value);" value="' + data.record.numero + '" />';
							} else {
								return '<input type="number" name="numero" onblur="validarcodigo($(this));" OnKeyUp="sumar(this.value);" id="numero" />';
							}
						}
					},
					sueldo: {
						title: 'Es Sueldo',
						width: '10%',
						options: { '1': 'Si', '2': 'No' }
					},
					descripcion: {
						title: 'Descripción',
						width: '40%'
					},
					tipo_haberes: {
						title: 'Tipos de Haberes',
						width: '20%',
						options: { '1': 'Haberes', '2': 'Haberes sin retención', '3': 'Retenciones' }
					},
					obtencion: {
						title: 'Obtención',
						width: '20%',
						options: { '1': 'Haberes', '2': 'Haberes sin retención', '3': 'Retenciones', '4': 'Grupo', '5': 'Ninguno' }
					},
					cantidad: {
						title: 'Cantidad',
						width: '10%'
					},
					multiplica: {
						title: 'Multiplica',
						width: '10%',
					},
					divide: {
						title: 'Divide',
						width: '10%',
					},
					aguinaldo: {
						title: 'Concepto sujeto a aguinaldo',
						width: '10%',
						options: { '1': 'Si', '2': 'No' }
					}
				},
				messages: {
					serverCommunicationError: 'Ocurrió un error en la comunicación con el servidor.',
					loadingMessage: 'Cargando Registros...',
					noDataAvailable: 'No hay registros cargados!',
					addNewRecord: 'Agregar Codigo',
					editRecord: 'Editar Codigo',
					areYouSure: '¿Estas seguro?',
					deleteConfirmation: 'El registro será eliminado. ¿Esta Seguro?',
					save: 'Guardar',
					saving: 'Guardando',
					cancel: 'Cancelar',
					deleteText: 'Eliminar',
					deleting: 'Eliminando',
					error: 'Error',
					close: 'Cerrar',
					cannotLoadOptionsFor: 'No se pueden cargar las opciones para el campo {0}',
					pagingInfo: 'Mostrando {0} a {1} de {2}',
					pageSizeChangeLabel: 'Mostrar',
					gotoPageLabel: 'Ir a',
					canNotDeletedRecords: 'No se puedieron eliminar {0} de {1} registros!',
					deleteProggress: 'Eliminando {0} de {1} registros, procesando...'
				},
				formCreated(event, data) {
					for (var x = 0; x < numero.length; x++) {
						if (data.record.numero > parseInt(numero[x])) {
							$('#codigo_superior').append($('<option>', {
								value: numero[x],
								text: descripcion[x]
							}));
						};
					};
					$('#codigo_superior').val(data.record.codigo_superior);
				},
				recordAdded(event, data){
					valorprevio = 0;
				}
			});

			 $('#Codigos').jtable('load');

			$('#LoadRecordsButton').click(function (e) {
				e.preventDefault();
				$('#Codigos').jtable('load', {
					buscardescripcion: $('#buscardescripcion').val()
				});
			});
	});
	function sumar(valor) {
		var val = parseInt(valor);
		$('#codigo_superior').empty();
    $('#codigo_superior').append($('<option>', {
      value: 0,
      text: 'Ninguno'
    }));
		for (var x = 0; x < numero.length; x++) {
			if (val > parseInt(numero[x])) {
				$('#codigo_superior').append($('<option>', {
					value: numero[x],
					text: descripcion[x]
				}));
			};
		};
	};
	function validarcodigo(inp) {
		var val = parseInt(inp.val());
		for (var x = 0; x < codigovalidar.length; x++) {
			if(valorprevio != parseInt(codigovalidar[x])){
				if (val == parseInt(codigovalidar[x])) {
					var dialog = new BootstrapDialog({
						message: function(){
									var $message = $('<div>Ya existe un codigo con este numero, por favor ingrese uno diferente.</div>');
									return $message;
								}
							});
					dialog.realize();
					dialog.getModalHeader().hide();
					dialog.getModalFooter().hide();
					dialog.getModalBody().css('background-color', '#FF0000');
					dialog.getModalBody().css('color', '#fff');
					dialog.open();
					inp.val('');
				};
			};
		};
	};
</script>
</body>
</html>
