<?php
	session_start();
	include "funciones.php";
	if (isset($_POST['txtRazonSocial']) && !empty($_POST['txtRazonSocial']) &&
		isset($_POST['txtCUIT']) && !empty($_POST['txtCUIT']) &&
		isset($_POST['txtInicioActividades']) && !empty($_POST['txtInicioActividades']) &&
		isset($_POST['cboCondicionIVA']) && !empty($_POST['cboCondicionIVA']) &&
		isset($_POST['idempr']) && !empty($_POST['idempr']) &&
		isset($_POST['cboPuntosVenta']) && !empty($_POST['cboPuntosVenta']) &&
		isset($_POST['cboPeriodo']) && !empty($_POST['cboPeriodo']) &&
		isset($_POST['txtNroAsiento']) && !empty($_POST['txtNroAsiento'])) {
		
			// Quito espacios en blanco
			$idempresa = $_POST['idempr'];
			$rsocial = trim($_POST['txtRazonSocial']);
			$cuit = trim($_POST['txtCUIT']);
			$iniactividades = trim($_POST['txtInicioActividades']);
		@	$domicilio = trim($_POST['txtDomicilio']);
		@	$localidad = trim($_POST['txtLocalidad']);
		@	$provincia = trim($_POST['txtProvincia']);
		@	$pais = $_POST['pais'];
			$telefono = trim($_POST['txtTelefono']);
			$condiva = $_POST['cboCondicionIVA'];
		@	$ingbrutos = trim($_POST['txtIngresosBrutos']);
			$pventas = $_POST['cboPuntosVenta'];
			$cpventas = trim($_POST['txtCantidad']);
			$iniasientos = trim($_POST['txtNroAsiento']);
			$periodo = $_POST['cboPeriodo'];
			$plancuenta = $_POST['cboPlanCuentas'];

			// Paso a mayusculas
			$rsocial = ucfirst($rsocial);
			$domicilio = ucfirst($domicilio);
			$localidad = ucfirst($localidad);
			$provincia = ucfirst($provincia);
			
			// Busco el plan de cuentas para chequear si lo cambiaron o no
			$con_idpcuentas=consulta("SELECT idpcuentas FROM empresas WHERE idempresa='$idempresa'");
			$pc=mysqli_fetch_array($con_idpcuentas);
			$idpcuentas = $pc['idpcuentas'];
			
		if(testfecha($iniactividades)===false) {
			mensaje("La fecha ingresada no es correcta.");
			ir_a("empresas.php");
		} else {
			$iniactividades = str_replace("/","",$iniactividades);
			$año = substr($iniactividades, -4);
			$mes = substr($iniactividades, -6, 2);
			$dia = substr($iniactividades, -8, 2);
			$iniactividades = $año."-".$mes."-".$dia;
			$editar = "UPDATE empresas
								SET
								rsocial = '$rsocial',
								cuit = '$cuit',
								inicioact = '$iniactividades',
								domicilio = '$domicilio',
								localidad = '$localidad',
								provincia = '$provincia',
								pais = '$pais',
								telefono = '$telefono',
								condiva = '$condiva',
								ingbrutos = '$ingbrutos',
								pventas = '$pventas',
								cantpventas = '$cpventas',
								idperiodo = '$periodo',
								idpcuentas = '$plancuenta',
								iniasientos = '$iniasientos'
								WHERE idempresa = '$idempresa'";
								
			// Si modificaron el plan de cuentas lo actualizo en la tabla cuentas
			if($idpcuentas != $plancuenta){
				consulta("DELETE FROM cuentas WHERE idempresa = '$idempresa'");
				
				consulta("INSERT INTO cuentas (codigo,denominacion,rubro,imputable,sumariza)
						SELECT codigo, denominacion, rubro, imputable, sumariza FROM modeloscuentas WHERE idprofesor = '$plancuenta'");
						
				$cargarids = consulta("UPDATE cuentas SET idempresa = '$idempresa' WHERE idempresa = 0");
			}
			
			
			$actualizardatos=consulta($editar);
			if(!$actualizardatos){
				echo "Mensaje: ".mysqli_error();
				mensaje("Error");
			} else {
				mensaje("Los datos de $rsocial se actualizaron con exito.");
				acthistempresa($rsocial, "Se ha modificado la informacion de la empresa");
				ir_a("empresas.php");	
			}
		}
	} else {
		mensaje("No se completaron todos los campos");
		ir_a("empresas.php");
	}
?>