ALTER TABLE `facturas` CHANGE `conceptosng` `conceptosng` DOUBLE NULL;
ALTER TABLE `facturas` CHANGE `retenciones` `retenciones` DOUBLE NULL;

update `facturas` set `conceptosng` = null where conceptosng = 0
update `facturas` set `retenciones` = null where retenciones = 0