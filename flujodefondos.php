<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Flujo de Fondos</title>
    <?php require_once('head.php'); ?>
</head>

<body class="no-skin">

  <?php require_once('header.php'); ?>

    <div class="main-content">
      <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
          <ul class="breadcrumb">
            <li>
              <i class="ace-icon fa fa-usd home-icon"></i>
              <a href="#">Simulador Contable</a>
            </li>
            <li class="active">Flujo de Fondos</li>
          </ul><!-- /.breadcrumb -->

          <div class="nav-search" id="nav-search">
            <form class="form-search">
              <span class="input-icon">
                <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                <i class="ace-icon fa fa-search nav-search-icon"></i>
              </span>
            </form>
          </div><!-- /.nav-search -->
        </div>
      <div class="page-content">
	<?php
		if(!isset($_SESSION['idempresa']) || empty($_SESSION['idempresa'])) {
			mensaje("Debe seleccionar una empresa.");
			ir_a("empresas.php");
		} else {
			$idempresa = $_SESSION['idempresa'];
			$con_emp=consulta("SELECT * FROM empresas WHERE idempresa='$idempresa'");
      $emp=mysqli_fetch_array($con_emp);
		}
	?>
  <!-- Page Content -->
  <h1 class="page-header">Flujo de Fondos</h1>
  <div class="space20"></div>
  <form role="form" id="formulario" data-toggle="validator" target="_blank" action="PDFs/flujo_de_fondos.php" method="POST">
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Desde</strong></div>
                <input type="text" data-mask="99/99/9999" id="desde" name="desde" class="form-control" value="<?php echo date("d/m/Y",strtotime($desde)); ?>" placeholder="Fecha Desde" maxlength="10" required>
          </div>
   		</div>
	</div>
	<div class="col-md-6">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Hasta</strong></div>
                <input type="text" data-mask="99/99/9999" id="hasta" name="hasta" class="form-control" value="<?php echo date("d/m/Y",strtotime($hasta)); ?>" placeholder="Fecha Hasta" maxlength="10" required>
          </div>
   		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<div class="input-group" style="width:70%;">
		<label for="activo" style="width:70%;font-weight:bold;" class="btn btn-info"><div align="right">ACTIVO <input type="checkbox" id="activo" name="activo" class="badgebox"><span class="badge">&check;</span></div></label>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<div class="input-group" style="width:70%;">
		<label for="pasivo" style="width:70%;font-weight:bold;" class="btn btn-info"><div align="right">PASIVO <input type="checkbox" id="pasivo" name="pasivo" class="badgebox"><span class="badge">&check;</span></div></label>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<div class="input-group" style="width:70%;">
		<label for="pneto" style="width:70%;font-weight:bold;" class="btn btn-info"><div align="right">PATRIMONIO NETO <input type="checkbox" id="pneto" name="pneto" class="badgebox"><span class="badge">&check;</span></div></label>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<div class="input-group" style="width:100%;">
		<label for="resumen" style="width:100%;font-weight:bold;" class="btn btn-warning">GENERAR RESUMEN <input type="checkbox" id="resumen" name="resumen" class="badgebox"><span class="badge">&check;</span></label>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<div class="input-group" style="width:70%;">
		<label for="ingygan" style="width:70%;font-weight:bold;" class="btn btn-info"><div align="right">INGRESOS Y GANANCIAS <input type="checkbox" id="ingygan" name="ingygan" class="badgebox"><span class="badge">&check;</span></div></label>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<div class="input-group" style="width:70%;">
		<label for="gyp" style="width:70%;font-weight:bold;" class="btn btn-info"><div align="right">GASTOS Y PERDIDAS <input type="checkbox" id="gyp" name="gyp" class="badgebox"><span class="badge">&check;</span></div></label>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<div class="input-group" style="width:70%;">
		<label for="costo" style="width:70%;font-weight:bold;" class="btn btn-info"><div align="right">COSTO <input type="checkbox" id="costo" name="costo" class="badgebox"><span class="badge">&check;</span></div></label>
			</div>
		</div>
	</div>
    <!--<center><img src="pie.php" alt="Grafico"></center>-->
	<div class="col-md-12">
		<button type="submit" id="btnAceptar" style="width:100%;" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Generar Informe</button>
	</div>
  </form>
  <!-- Final Page Content -->
  <br><br><br><br><br><br><br><br><br><br>
  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>
  <script type="text/javascript">
  	$(document).ready(function() {
  		//Calendarios
  		$("#desde").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});
  		$("#hasta").datetimepicker({lang:'es',timepicker:false,format:'d/m/Y'});

      $("#resumen").change(function() {
          if(this.checked) {
            $('#formulario').attr('action', 'PDFs/flujo_de_fondos_printable.php');
          } else {
            $('#formulario').attr('action', 'PDFs/flujo_de_fondos.php');
          }
      });
  	});
  </script>
  <!-- ============================================================= -->
</body>
</html>
