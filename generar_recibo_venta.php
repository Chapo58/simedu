<!DOCTYPE html>
<html lang="es">
<head>
    <title>Simedu | Comprobantes</title>
    <?php require_once('head.php'); ?>
	  <link href="css/select2.css" rel="stylesheet">
</head>

<body class="no-skin">
	<?php require_once('header.php'); ?>

  <div class="main-content">
    <div class="main-content-inner">
      <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
          <li>
            <i class="ace-icon fa fa-industry home-icon"></i>
            <a href="#">Simulador Empresarial</a>
          </li>
          <li><a href="comprobantes.php">Comprobantes</a></li>
          <li class="active">Comprobante de Venta</li>
        </ul><!-- /.breadcrumb -->

        <div class="nav-search" id="nav-search">
          <form class="form-search">
            <span class="input-icon">
              <input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
              <i class="ace-icon fa fa-search nav-search-icon"></i>
            </span>
          </form>
        </div><!-- /.nav-search -->
      </div>
    <div class="page-content">
  <!-- Page Content -->
  <h1 class="page-header">Generar Comprobante de Venta</h1>
    <!--<div class="bs-callout bs-callout-danger">
        <h4>Productos</h4>
        <p>Debes cargar al menos un producto para poder generar un comprobante. Para cargar nuevos productos haz click <a href="productos.php">aquí</a>.</p>
    </div>-->
  <div class="row">
	<div class="col-md-5">
        <div class="form-group">
		  <div class="input-group">
			<div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span><strong>&nbsp;&nbsp;Fecha</strong></div>
                <input type="text" data-mask="99/99/9999" id="fecha" name="fecha" class="form-control" value="<?php echo date("d/m/Y"); ?>" placeholder="Fecha" maxlength="10" required>
          </div>
   		</div>
	</div>
  <div class="col-md-7">
	<select class="form-control" id = "tcomprobante">
		<option disabled>Tipo de Comprobante</option>
		<option value="F">Factura</option>
		<option value="P">Presupuesto</option>
		<option value="R">Remito</option>
		<option value="NC">Nota de Credito</option>
		<option value="ND">Nota de Debito</option>
    <option value="RC" selected>Recibo de Venta</option>
	</select>
	</div>
  </div>
  <br>
  <input type="button" id="nuevo" class="btn btn-lg btn-primary" style="width:100%;" value="Agregar Factura">
  <br><br>
  <!-- Final Page Content -->
	<table id="tabla" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
				<th>Nº Factura</th>
        <th></th>
				<th>Total</th>
				<th></th>
      </tr>
    </thead>
		<tbody>
			<tr>
				<td>
					<select class='form-control' id='facturas' onchange="getPrecio($(this).closest('td').next('td').next('td').find('input'),$(this).val());">
					<option selected disabled>Seleccionar Factura</option>
					<?php
						$con_comp=consulta("SELECT facturas.*, empresas.rsocial, clientes.rsocial as 'rsocialc' FROM facturas
    				LEFT JOIN empresas ON facturas.idcomprador = empresas.idempresa
    				LEFT JOIN clientes ON facturas.idcliente = clientes.idcliente
    				WHERE (idvendedor = '$idempresa' AND facturas.fecha BETWEEN '$desde' AND '$hasta') AND (tipo = 'A' OR tipo = 'B' OR tipo = 'C')
    				ORDER BY facturas.fecha DESC");
						while ($c = mysqli_fetch_array($con_comp, MYSQLI_ASSOC)) {
              $idfactura = $c['idfactura'];
              if($c['rsocial'] == ""){
                $comprador = $c['rsocialc'];
              } else {
                $comprador = $c['rsocial'];
              }
              $pventa = $c['puntofacturacion'];
    					$nfactura = $c['numero'];
              $tipoc = "FACTURA " . $c['tipo'];
					?>
							<option value='<?php echo $idfactura; ?>'><?php echo str_pad($pventa, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura, 8, "0", STR_PAD_LEFT); ?></option>
					<?php } ?>
					</select>
				</td>
        <td></td>
				<td><input type="number" placeholder="Total" name="totalcomp" onblur="calTotal();" id="totalcomp" class="form-control" /></td>
				<td><button type="button" class="btn btn-danger btn-md" onclick="remover($(this).parents('tr'))"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td>
            </tr>
		</tbody>
		<tfoot>
      <tr>
				<th></th>
        <th></th>
				<th><div id="total"></div></th>
        <th></th>
      </tr>
    </tfoot>
    </table>
    <br><br><br>
    <strong>RECIBI DE:&nbsp;&nbsp;&nbsp;</strong>
      <select id="cliente">
    	<option selected disabled>Seleccionar Cliente</option>
    	  <optgroup label="Empresas Compañeros">
    		<?php
    			$con_com=consulta("SELECT empresas.idempresa, empresas.rsocial FROM empresas LEFT JOIN usuario ON empresas.idusuario = usuario.idusuario WHERE usuario.idprofesor='$idprofe'");
    			while ($c = mysqli_fetch_array($con_com, MYSQLI_ASSOC)) {
    				$idemp = $c['idempresa'];
    				$rsocial = $c['rsocial'];
    		?>
    			<option value = '<?php echo $idemp;?>'><?php echo $rsocial; ?></option>
    		<?php } ?>
    	  </optgroup>
    	  <optgroup label="Clientes Cargados">
    		<?php
    			$con_cli=consulta("SELECT idcliente, rsocial FROM clientes WHERE idempresa='$idempresa'");
    			while ($c = mysqli_fetch_array($con_cli, MYSQLI_ASSOC)) {
    				$idcli = $c['idcliente'];
    				$nombre = $c['rsocial'];
    		?>
    			<option value = 'a<?php echo $idcli;?>'><?php echo $nombre; ?></option>
    		<?php } ?>
    	  </optgroup>
    	</select>
      <strong>&nbsp;&nbsp;&nbsp;EN CONCEPTO DE:&nbsp;&nbsp;&nbsp;</strong>
  	   <input type="text" id="comentario" placeholder="Comentario"></input>
      <strong>&nbsp;&nbsp;&nbsp;LA SUMA DE:&nbsp;&nbsp;&nbsp;</strong>
       <input type="number" id="importe" placeholder="Importe"></input>
    <br>
  <br/>
	<button class="btn btn-success btn-lg" style="float:right;" id="generar">
		<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Generar Comprobante
	</button>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
  </div>
 </div>
</div><!-- /.main-content -->

<?php require_once('footer.php'); ?>

<script type="text/javascript">
  var tabla;
	var ids = [];
	var precios = [];
	<?php
		$con_pro=consulta("SELECT idfactura, total
							FROM facturas WHERE idvendedor='$idempresa'");
		while ($p = mysqli_fetch_array($con_pro, MYSQLI_ASSOC)) {
			$id = $p['idfactura'];
			$precio = number_format($p['total'], 2, '.', '');
			echo "ids.push(\"$id\");";
			echo "precios.push(\"$precio\");";
		}
	?>

  var select = "<select class='form-control' id='facturas' onchange=" + '"' +"getPrecio($(this).closest('td').next('td').next('td').find('input'),$(this).val());"+ '"' +">" +
  <?php
    $con_comp2=consulta("SELECT facturas.*, empresas.rsocial, clientes.rsocial as 'rsocialc' FROM facturas
    LEFT JOIN empresas ON facturas.idcomprador = empresas.idempresa
    LEFT JOIN clientes ON facturas.idcliente = clientes.idcliente
    WHERE (idvendedor = '$idempresa' AND facturas.fecha BETWEEN '$desde' AND '$hasta') AND (tipo = 'A' OR tipo = 'B' OR tipo = 'C')
    ORDER BY facturas.fecha DESC");
    while ($c2 = mysqli_fetch_array($con_comp2, MYSQLI_ASSOC)) {
      $idfactura2 = $c2['idfactura'];
      if($c['rsocial'] == ""){
        $comprador = $c2['rsocialc'];
      } else {
        $comprador = $c2['rsocial'];
      }
      $pventa2 = $c2['puntofacturacion'];
      $nfactura2 = $c2['numero'];
      $tipoc = "FACTURA " . $c2['tipo'];
  ?>
		"<option value='<?php echo $idfactura2; ?>'><?php echo str_pad($pventa2, 4, "0", STR_PAD_LEFT) . "-" . str_pad($nfactura2, 8, "0", STR_PAD_LEFT); ?></option>" +
	<?php } ?>
	"<option selected disabled>Seleccionar Factura</option>" +
	"</select>";

	function remover(tr) {
		tabla.row(tr).remove().draw();
		calTotal();
	}

	function getPrecio(input, id){
		for(var x = 0 ; x < ids.length ; x++) {
			if(ids[x] == id) {
				input.val(precios[x]);
			}
		};
		calTotal();
		
		// Traer datos factura
		$.ajax({
          type: "GET",
          url: "traerDatos.php",
          data: {idfactura:id},
          contentType: "application/json",
          dataType: "json",
          success: function (data) {
			  if(data['idcliente'] > 0){
				  $("#cliente").val('a'+data['idcliente']);
			  } else {
				  $("#cliente").val(data['idcomprador']);
			  }
          }
      });
	}

  $('#tcomprobante').on('change', function() {
    if(this.value != "RC"){
      window.location = "generar_comprobante_venta.php";
    }
  });

	function calTotal(){
		var sumaprecios = 0;
		$("input[id=totalcomp]").each(function() {
			var number = parseFloat(this.value) || 0;
			sumaprecios += parseFloat(number);
		});
		$('#total').html("Total: " + sumaprecios);
    $('#importe').val(sumaprecios);
	}

	$('#generar').click( function() {
		var fecha = $("#fecha").val();
    var cliente = $("#cliente").val();
    var comentario = $("#comentario").val();
    var importe = $("#importe").val();
  /*  if (!comentario) {
			BootstrapDialog.show({
				message: "Debe escribir un detalle para el recibo.",
				buttons: [{
					label: 'Aceptar',
					action: function(dialogItself) {
						dialogItself.close();
					}
				}]
			});
		} else {*/
			var ftotales = [];
      var facturas = [];

			$("input[id=totalcomp]").each(function() {
				var number = parseFloat(this.value) || 0;
				ftotales.push(parseFloat(number));
			});

			$("select[id=facturas]").each(function() {
				var f = $(this).find('option:selected').val() || 0;
				facturas.push(f);
			});

			if(importe > 0) {
				$.ajax({
					type: "POST",
					url: "PDFs/generar_recibo_venta.php",
					data: { importes:ftotales, facturas:facturas, fecha:fecha, comentario:comentario, cliente:cliente, importe:importe },
					beforeSend: function(){
						$("#generar").addClass("m-progress");
						$("#generar").prop('disabled', true);
					},
					success: function(msg) {
						$("#generar").removeClass("m-progress");
						$("#generar").prop('disabled', false);
						BootstrapDialog.show({
						message: msg,
						buttons: [{
							label: 'Aceptar',
							action: function() {
								location.href= "comprobantes.php";
							}
						}]
						});
					}
				});
			} else {
				BootstrapDialog.show({
					message: "El importe no puede ser 0 o negativo.",
					buttons: [{
						label: 'Aceptar',
						action: function(dialogItself) {
							dialogItself.close();
						}
					}]
				});
			}

		// }
    } );

	$(document).ready(function() {

		tabla = $('#tabla').DataTable({
			"paging":   false,
			language: {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     'No hay productos cargados!.',
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			"bFilter": false,
			"bSort" : false
		});

		$('#nuevo').on( 'click', function () {
			tabla.row.add( [
				select,
				'',
				'<input type="number" placeholder="Total" name="totalcomp" onblur="calTotal();" id="totalcomp" class="form-control" />',
				'<button type="button" class="btn btn-danger btn-md" onclick=' + '"' + "remover($(this).parents('tr'))" + '"' + '><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'
			] ).draw();
		} );
	});
</script>
  <!-- ============================================================= -->
</body>
</html>
