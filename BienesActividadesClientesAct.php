<?php
session_start();
include "funciones.php";
try
{
	$idusuario = $_SESSION['idusuario'];
	$idempresa = $_SESSION['idempresa'];
	$rsocialemp = $_SESSION['rsocial'];

	//Getting records (listAction)
	if($_GET["action"] == "list")
	{
		//Get record count
		$result = mysqli_query($con,"SELECT COUNT(*) AS RecordCount FROM bienes_actividades_clientes WHERE idempresa = $idempresa;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($con,"SELECT * FROM bienes_actividades_clientes WHERE idempresa = $idempresa ORDER BY " . $_GET["jtSorting"] . " LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		
		//Add all records to an array
		$rows = array();
		while($row = mysqli_fetch_array($result))
		{
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($_GET["action"] == "create")
	{
		$idcliente = trim($_POST["idcliente"]);
		$orden = trim($_POST["orden"]);
		$codigo = trim($_POST["codigo"]);
		$descripcion = trim($_POST["descripcion"]);
		$formulario = trim($_POST["formulario"]);
		$agropecuario = trim($_POST["agropecuario"]);
		//Insert record into database
		$result = mysqli_query($con,"INSERT INTO bienes_actividades_clientes(idempresa, idcliente, orden, codigo, descripcion, formulario, agropecuario) VALUES('$idempresa','$idcliente','$orden','$codigo','$descripcion','$formulario','$agropecuario');");
		
		//Get last inserted record (to return to jTable)
		$result = mysqli_query($con,"SELECT * FROM bienes_actividades_clientes WHERE idactividad = LAST_INSERT_ID();");
		$row = mysqli_fetch_array($result);
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $row;
		print json_encode($jTableResult);
	}
	//Updating a record (updateAction)
	else if($_GET["action"] == "update")
	{
		$id = $_POST["idactividad"];
		$orden = trim($_POST["orden"]);
		$codigo = trim($_POST["codigo"]);
		$descripcion = trim($_POST["descripcion"]);
		$formulario = trim($_POST["formulario"]);
		$agropecuario = trim($_POST["agropecuario"]);
		//Update record in database
		$result = mysqli_query($con,"UPDATE bienes_actividades_clientes SET orden = '$orden', codigo = '$codigo', descripcion = '$descripcion', formulario = '$formulario', agropecuario = '$agropecuario' WHERE idactividad = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($_GET["action"] == "delete")
	{
		$id = $_POST["idactividad"];
		//Delete from database
		$result = mysqli_query($con,"DELETE FROM bienes_actividades_clientes WHERE idactividad = $id;");

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		print json_encode($jTableResult);
	}

	//Close database connection
	mysqli_close($con);

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>