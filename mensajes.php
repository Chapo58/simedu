<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Mensajes</title>
		<?php require_once('head.php'); ?>
		<style>
		.autocomplete-suggestions { border: 1px solid #999; font-size: 120%; background: #FFF; overflow: auto; }
		.autocomplete-suggestion { padding: 0px 5px; white-space: nowrap; overflow: hidden; cursor: pointer; }
		.autocomplete-selected { background: #F0F0F0; }
		.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
		.autocomplete-group { padding: 2px 5px; }
		.autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
		.autocomplete-suggestion { cursor: pointer; }
		</style>
	</head>

	<body class="no-skin">

		<?php require_once('header.php'); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="inicio.php">Inicio</a>
							</li>
							<li class="active">Mensajes</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
				<div class="page-content">
					<div class="page-header">
						<h1>
							Mensajes
							<small>
								<i class="ace-icon fa fa-angle-double-right"></i>
								Casilla de mensajes personal
							</small>
						</h1>
					</div><!-- /.page-header -->

					<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="row">
								<div class="col-xs-12">
									<div class="tabbable">
										<ul id="inbox-tabs" class="inbox-tabs nav nav-tabs padding-16 tab-size-bigger tab-space-1">
											<li class="li-new-mail pull-right">
												<a data-toggle="tab" onclick="nuevoMensaje()" href="#write" data-target="write" class="btn-new-mail">
													<span class="btn btn-purple no-border">
														<i class="ace-icon fa fa-envelope bigger-130"></i>
														<span class="bigger-110">Nuevo Mensaje</span>
													</span>
												</a>
											</li><!-- /.li-new-mail -->

											<li class="active">
												<a data-toggle="tab" href="#inbox" data-target="inbox">
													<i class="blue ace-icon fa fa-inbox bigger-130"></i>
													<span class="bigger-110">Mensajes</span>
												</a>
											</li>
										</ul>

										<div class="tab-content no-border no-padding">
											<div id="inbox" class="tab-pane in active">
												<div class="message-container">
													<div id="id-message-list-navbar" class="message-navbar clearfix">
														<div class="message-bar">
															<div class="message-infobar" id="id-message-infobar">
																<span class="blue bigger-150">Mensajes</span>
																<span class="grey bigger-110">(<?php echo $sinleer; ?> mensajes sin leer)</span>
															</div>

															<div class="message-toolbar hide">
																<button type="button" class="btn btn-xs btn-white btn-primary">
																	<i class="ace-icon fa fa-trash-o bigger-125 orange"></i>
																	<span class="bigger-110">Eliminar</span>
																</button>
															</div>
														</div>

														<div>
															<div class="messagebar-item-left">
																<label class="inline middle">
																	<input type="checkbox" id="id-toggle-all" class="ace" />
																	<span class="lbl"></span>
																</label>

																&nbsp;
																<div class="inline position-relative">
																	<a href="#" data-toggle="dropdown" class="dropdown-toggle">
																		<i class="ace-icon fa fa-caret-down bigger-125 middle"></i>
																	</a>

																	<ul class="dropdown-menu dropdown-lighter dropdown-100">
																		<li>
																			<a id="id-select-message-all" href="#">Todos</a>
																		</li>

																		<li>
																			<a id="id-select-message-none" href="#">Ninguno</a>
																		</li>

																		<li class="divider"></li>

																		<li>
																			<a id="id-select-message-unread" href="#">Sin Leer</a>
																		</li>

																		<li>
																			<a id="id-select-message-read" href="#">Leidos</a>
																		</li>
																	</ul>
																</div>
															</div>
															<div class="nav-search minimized">
																<form class="form-search">
																	<span class="input-icon">
																		<input type="text" autocomplete="off" class="input-small nav-search-input" placeholder="Buscar ..." />
																		<i class="ace-icon fa fa-search nav-search-icon"></i>
																	</span>
																</form>
															</div>
														</div>
													</div>

													<div id="id-message-item-navbar" class="hide message-navbar clearfix">
														<div class="message-bar">
															<div class="message-toolbar">
																<button type="button" onclick="responderMensaje()" class="btn btn-xs btn-white btn-primary">
																	<i class="ace-icon fa fa-reply bigger-125 green"></i>
																	<span class="bigger-110">Responder</span>
																</button>
																<button onclick="eliminarMensaje()" type="button" class="btn btn-xs btn-white btn-primary">
																	<i class="ace-icon fa fa-trash-o bigger-125 orange"></i>
																	<span class="bigger-110">Eliminar</span>
																</button>
															</div>
														</div>

														<div>
															<div class="messagebar-item-left">
																<a href="#" class="btn-back-message-list">
																	<i class="ace-icon fa fa-arrow-left blue bigger-110 middle"></i>
																	<b class="bigger-110 middle">Volver</b>
																</a>
															</div>

															<div class="messagebar-item-right">
																<i class="ace-icon fa fa-clock-o bigger-110 orange"></i>
																<span id="fechamensajetop" class="grey">Today, 7:15 pm</span>
															</div>
														</div>
													</div>

													<div id="id-message-new-navbar" class="hide message-navbar clearfix">
														<div class="message-bar">
															<div class="message-toolbar">

															</div>
														</div>

														<div>
															<div class="messagebar-item-left">
																<a href="#" class="btn-back-message-list">
																	<i class="ace-icon fa fa-arrow-left bigger-110 middle blue"></i>
																	<b class="middle bigger-110">Volver</b>
																</a>
															</div>

															<div class="messagebar-item-right">
																<span class="inline btn-send-message">
																	<button type="button" onclick="$('form#id-message-form').submit();" class="btn btn-sm btn-primary no-border btn-white btn-round">
																		<span class="bigger-110">Enviar</span>

																		<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
																	</button>
																</span>
															</div>
														</div>
													</div>

													<div class="message-list-container">
														<div class="message-list" id="message-list">
															<?php
																$con_mensajes=consulta("SELECT mensajes.*, usuario.nombre, usuario.apellido, usuario.email
																	FROM mensajes LEFT JOIN usuario ON mensajes.origen = usuario.idusuario
																	WHERE mensajes.destino='$idusuario'");
																while ($m = mysqli_fetch_array($con_mensajes, MYSQLI_ASSOC)) {
																	if($m['visto_des'] == 0) $visto = 'message-unread'; else $visto = '';
																	if(!empty($m['nombre']) || !empty($m['apellido'])) {
									                  $remitente = $m['nombre'] . " " . $m['apellido'] . " (" . $m['email'].")";
									                } else {
									                  $remitente = $m['email'];
									                }
																	$fecha = fecha($m['fecha'],"/").", ".date("H:i",strtotime($m['hora']));
															?>
															<div class="message-item message-unread">
																<label class="inline">
																	<input type="checkbox" class="ace" />
																	<span class="lbl"></span>
																</label>

																<span class="sender" title="<?php echo $remitente ?>"><?php echo $remitente ?></span>
																<span class="time"><?php echo $fecha ?></span>

																<span class="summary">
																	<span id=<?php echo $m['idmensaje']; ?> class="text">
																		<?php echo $m['asunto']; ?>
																	</span>
																</span>
															</div>
															<?php } ?>
														</div>
													</div>

													<div class="message-footer clearfix">
														<div class="pull-left"><?php echo $cantmensajes; ?> Mensajes en total </div>

														<div class="pull-right">
															<div class="inline middle"> Pagina 1 de 16 </div>

															&nbsp; &nbsp;
															<ul class="pagination middle">
																<li class="disabled">
																	<span>
																		<i class="ace-icon fa fa-step-backward middle"></i>
																	</span>
																</li>

																<li class="disabled">
																	<span>
																		<i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
																	</span>
																</li>

																<li>
																	<span>
																		<input value="1" maxlength="3" type="text" />
																	</span>
																</li>

																<li>
																	<a href="#">
																		<i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
																	</a>
																</li>

																<li>
																	<a href="#">
																		<i class="ace-icon fa fa-step-forward middle"></i>
																	</a>
																</li>
															</ul>
														</div>
													</div>

													<div class="hide message-footer message-footer-style2 clearfix">
														<div class="pull-left"></div>

														<div class="pull-right">
															<div class="inline middle"> Mensaje 1 de 151 </div>

															&nbsp; &nbsp;
															<ul class="pagination middle">
																<li class="disabled">
																	<span>
																		<i class="ace-icon fa fa-angle-left bigger-150"></i>
																	</span>
																</li>

																<li>
																	<a href="#">
																		<i class="ace-icon fa fa-angle-right bigger-150"></i>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div><!-- /.tab-content -->
									</div><!-- /.tabbable -->
								</div><!-- /.col -->
							</div><!-- /.row -->

							<form id="id-message-form" class="hide form-horizontal message-form col-xs-12">
								<div>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-recipient">Destinatario:</label>

										<div class="col-sm-6 col-xs-12">
											<span class="input-icon block col-xs-12 no-padding">
												<input type="text" class="col-xs-12" name="destinatario" id="destinatario" placeholder="Destinatario" />
												<i class="ace-icon fa fa-user"></i>
											</span>
										</div>
									</div>

									<div class="hr hr-18 dotted"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-subject">Asunto:</label>

										<div class="col-sm-6 col-xs-12">
											<div class="input-icon block col-xs-12 no-padding">
												<input maxlength="100" type="text" class="col-xs-12" name="subject" id="asunto" placeholder="Asunto" />
												<i class="ace-icon fa fa-comment-o"></i>
											</div>
										</div>
									</div>

									<div class="hr hr-18 dotted"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right">
											<span class="inline space-24 hidden-480"></span>
											Mensaje:
										</label>

										<div class="col-sm-9">
											<div id="mensaje" class="wysiwyg-editor"></div>
										</div>
									</div>

									<div class="hr hr-18 dotted"></div>

									<div class="form-group no-margin-bottom">
										<label class="col-sm-3 control-label no-padding-right">Adjuntos:</label>

										<div class="col-sm-9">
											<div id="form-attachments">
												<input type="file" name="attachment[]" />
											</div>
										</div>
									</div>

									<div class="align-right">
										<button id="id-add-attachment" type="button" class="btn btn-sm btn-danger">
											<i class="ace-icon fa fa-paperclip bigger-140"></i>
											Agregar Adjunto
										</button>
									</div>

									<div class="space"></div>
								</div>
							</form>

							<div class="hide message-content" id="id-message-content">
								<div class="message-header clearfix">
									<div class="pull-left">
										<span id="asuntomensaje" class="blue bigger-125"> </span>

										<div class="space-4"></div>

										&nbsp;
										<img class="middle" id="avatarmensaje" src="" width="32" />
										&nbsp;
										<a href="#" id="remitentemensaje" class="sender"></a>

										&nbsp;
										<i class="ace-icon fa fa-clock-o bigger-110 orange middle"></i>
										<span id="fechamensaje" class="time grey"></span>
									</div>

									<div class="pull-right action-buttons">
										<a onclick="responderMensaje()" href="#">
											<i class="ace-icon fa fa-reply green icon-only bigger-130"></i>
										</a>
										<a onclick="eliminarMensaje()" href="#">
											<i class="ace-icon fa fa-trash-o red icon-only bigger-130"></i>
										</a>
									</div>
								</div>

								<div class="hr hr-double"></div>

								<div id="cuerpomensaje" class="message-body">

								</div>

								<div class="hr hr-double"></div>

								<!-- <div class="message-attachment clearfix">
									<div class="attachment-title">
										<span class="blue bolder bigger-110">Attachments</span>
										&nbsp;
										<span class="grey">(2 files, 4.5 MB)</span>

										<div class="inline position-relative">
											<a href="#" data-toggle="dropdown" class="dropdown-toggle">
												&nbsp;
												<i class="ace-icon fa fa-caret-down bigger-125 middle"></i>
											</a>

											<ul class="dropdown-menu dropdown-lighter">
												<li>
													<a href="#">Download all as zip</a>
												</li>

												<li>
													<a href="#">Display in slideshow</a>
												</li>
											</ul>
										</div>
									</div>

									&nbsp;
									<ul class="attachment-list pull-left list-unstyled">
										<li>
											<a href="#" class="attached-file">
												<i class="ace-icon fa fa-file-o bigger-110"></i>
												<span class="attached-name">Document1.pdf</span>
											</a>

											<span class="action-buttons">
												<a href="#">
													<i class="ace-icon fa fa-download bigger-125 blue"></i>
												</a>

												<a href="#">
													<i class="ace-icon fa fa-trash-o bigger-125 red"></i>
												</a>
											</span>
										</li>

										<li>
											<a href="#" class="attached-file">
												<i class="ace-icon fa fa-film bigger-110"></i>
												<span class="attached-name">Sample.mp4</span>
											</a>

											<span class="action-buttons">
												<a href="#">
													<i class="ace-icon fa fa-download bigger-125 blue"></i>
												</a>

												<a href="#">
													<i class="ace-icon fa fa-trash-o bigger-125 red"></i>
												</a>
											</span>
										</li>
									</ul>

									<div class="attachment-images pull-right">
										<div class="vspace-4-sm"></div>

										<div>
											<img width="36" alt="image 4" src="assets/images/gallery/thumb-4.jpg" />
											<img width="36" alt="image 3" src="assets/images/gallery/thumb-3.jpg" />
											<img width="36" alt="image 2" src="assets/images/gallery/thumb-2.jpg" />
											<img width="36" alt="image 1" src="assets/images/gallery/thumb-1.jpg" />
										</div>
									</div>
								</div> -->
							</div><!-- /.message-content -->

							<!-- PAGE CONTENT ENDS -->
						</div><!-- /.col -->
					</div><!-- /.row -->



					<!-- Final Page Content -->
				</div>
			</div>
		</div><!-- /.main-content -->

			<?php require_once('footer.php'); ?>
			<script src="assets/js/bootstrap-tag.min.js"></script>
			<script src="assets/js/jquery.hotkeys.index.min.js"></script>
			<script src="assets/js/bootstrap-wysiwyg.min.js"></script>
			<script src="assets/js/jquery.autocomplete.js"></script>
			<script>
			var usuario = 0;
			var mensaje = 0;
			var lid;
			var usuarios = [
			<?php
				if($u['tipousuario'] == 58) {
					$con_us=consulta("SELECT * FROM usuario");
				} else {
					$con_us=consulta("SELECT * FROM usuario WHERE idprofesor = '$idprofe'");
				}
					while ($us = mysqli_fetch_array($con_us, MYSQLI_ASSOC)) {
						$id = $us['idusuario'];
						$nombre = $us['nombre']." ".$us['apellido']." (".$us['email'].")";
			?>
			    { value: "<?php echo $nombre; ?>", data: "<?php echo $id; ?>" },
			<?php } ?>
			];

			<?php if(isset($_POST['idusu']) && !empty($_POST['idusu'])) {
				$idus = $_POST['idusu'];
				$con_res=consulta("SELECT * FROM usuario WHERE idusuario='$idus'");
				$res=mysqli_fetch_array($con_res);
				$nombre = $res['nombre']." ".$res['apellido']." (".$res['email'].")";
			?>
			$(window).on("load", function() {
			    $('#inbox-tabs a[href="#write"]').tab('show');
			    <?php echo "usuario = $idus;"; ?>
				$("#destinatario").css("background-color", "#D1FFD1");
				$("#destinatario").attr("value", "<?php echo $nombre; ?>");
			});
			<?php } ?>

			function responderMensaje(){
				$('#inbox-tabs a[href="#write"]').tab('show');
			}

			function nuevoMensaje(){
				$('#destinatario').prop('disabled', false);
				$("#asunto").attr("value", "");
				$("#destinatario").css("background-color", "#FFF");
				$("#destinatario").attr("value", "");
				usuario = 0;
			}

			function eliminarMensaje(){
				BootstrapDialog.show({
					title: 'Eliminar Mensaje',
					type: BootstrapDialog.TYPE_WARNING,
					message: '¿Esta seguro que desea eliminar este mensaje?',
					buttons: [{
						icon: 'glyphicon glyphicon-trash',
						label: ' Eliminar',
						cssClass: 'btn-warning',
						action: function(dialogItself){
							dialogItself.close();
							$.ajax({
								type: "POST",
								url: "eliminaciones.php",
								data: {idmensaje:mensaje}
							}).done(function( resp ) {
									window.location.reload();
							});
						}
					}, {
						icon: 'glyphicon glyphicon-remove',
						label: ' Cancelar',
						cssClass: 'btn-default',
						action: function(dialogItself){
							dialogItself.close();
						}
					}]
				});
		 };

			$('#destinatario').autocomplete({
				lookup: usuarios,
				onSelect: function (suggestion) {
					usuario = suggestion.data;
					$("#destinatario").css("background-color", "#D1FFD1");
				}
			});

			jQuery(function($){

				//handling tabs and loading/displaying relevant messages and forms
				//not needed if using the alternative view, as described in docs
				$('#inbox-tabs a[data-toggle="tab"]').on('show.bs.tab', function (e) {
					var currentTab = $(e.target).data('target');
					if(currentTab == 'write') {
						Inbox.show_form();
					}
					else if(currentTab == 'inbox') {
						Inbox.show_list();
					}
				})



				//basic initializations
				$('.message-list .message-item input[type=checkbox]').removeAttr('checked');
				$('.message-list').on('click', '.message-item input[type=checkbox]' , function() {
					$(this).closest('.message-item').toggleClass('selected');
					if(this.checked) Inbox.display_bar(1);//display action toolbar when a message is selected
					else {
						Inbox.display_bar($('.message-list input[type=checkbox]:checked').length);
						//determine number of selected messages and display/hide action toolbar accordingly
					}
				});


				//check/uncheck all messages
				$('#id-toggle-all').removeAttr('checked').on('click', function(){
					if(this.checked) {
						Inbox.select_all();
					} else Inbox.select_none();
				});

				//select all
				$('#id-select-message-all').on('click', function(e) {
					e.preventDefault();
					Inbox.select_all();
				});

				//select none
				$('#id-select-message-none').on('click', function(e) {
					e.preventDefault();
					Inbox.select_none();
				});

				//select read
				$('#id-select-message-read').on('click', function(e) {
					e.preventDefault();
					Inbox.select_read();
				});

				//select unread
				$('#id-select-message-unread').on('click', function(e) {
					e.preventDefault();
					Inbox.select_unread();
				});

				/////////

				//display first message in a new area
				$('.message-list .message-item .text').on('click', function() {

					mensaje = $(this).attr('id');
					$.ajax({
	          type: "GET",
	          url: "mensajes_datos.php",
	          data: {id:mensaje},
	          contentType: "application/json",
	          dataType: "json",
						beforeSend: function(){
							//show the loading icon
							$('.message-container').append('<div class="message-loading-overlay"><i class="fa-spin ace-icon fa fa-spinner orange2 bigger-160"></i></div>');
						},
	          success: function (data) {
							$("#asuntomensaje").html(data['asunto']);
							$("#remitentemensaje").html(data['remitente']);
							$("#avatarmensaje").attr("src",data['avatar']);
							$("#fechamensaje").html(data['fecha']);
							$("#fechamensajetop").html(data['fecha']);
							$("#cuerpomensaje").html(data['mensaje']);
							usuario = data['idusuario'];
							$("#destinatario").css("background-color", "#D1FFD1");
							$("#destinatario").attr("value", data['remitente']);
							$("#asunto").attr("value", data['asunto']);

							$('.message-inline-open').removeClass('message-inline-open').find('.message-content').remove();
	          }
	        });

					var message_list = $(this).closest('.message-list');

					$('#inbox-tabs a[href="#inbox"]').parent().removeClass('active');
					//some waiting
					setTimeout(function() {

						//hide everything that is after .message-list (which is either .message-content or .message-form)
						message_list.next().addClass('hide');
						$('.message-container').find('.message-loading-overlay').remove();

						//close and remove the inline opened message if any!

						//hide all navbars
						$('.message-navbar').addClass('hide');
						//now show the navbar for single message item
						$('#id-message-item-navbar').removeClass('hide');

						//hide all footers
						$('.message-footer').addClass('hide');
						//now show the alternative footer
						$('.message-footer-style2').removeClass('hide');


						//move .message-content next to .message-list and hide .message-list
						$('.message-content').removeClass('hide').insertAfter(message_list.addClass('hide'));

						//add scrollbars to .message-body
						$('.message-content .message-body').ace_scroll({
							size: 150,
							mouseWheelLock: true,
							styleClass: 'scroll-visible'
						});

					}, 500 + parseInt(Math.random() * 500));
				});

				//back to message list
				$('.btn-back-message-list').on('click', function(e) {

					e.preventDefault();
					$('#inbox-tabs a[href="#inbox"]').tab('show');
				});



				//hide message list and display new message form
				/**
				$('.btn-new-mail').on('click', function(e){
					e.preventDefault();
					Inbox.show_form();
				});
				*/




				var Inbox = {
					//displays a toolbar according to the number of selected messages
					display_bar : function (count) {
						if(count == 0) {
							$('#id-toggle-all').removeAttr('checked');
							$('#id-message-list-navbar .message-toolbar').addClass('hide');
							$('#id-message-list-navbar .message-infobar').removeClass('hide');
						}
						else {
							$('#id-message-list-navbar .message-infobar').addClass('hide');
							$('#id-message-list-navbar .message-toolbar').removeClass('hide');
						}
					}
					,
					select_all : function() {
						var count = 0;
						$('.message-item input[type=checkbox]').each(function(){
							this.checked = true;
							$(this).closest('.message-item').addClass('selected');
							count++;
						});

						$('#id-toggle-all').get(0).checked = true;

						Inbox.display_bar(count);
					}
					,
					select_none : function() {
						$('.message-item input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');
						$('#id-toggle-all').get(0).checked = false;

						Inbox.display_bar(0);
					}
					,
					select_read : function() {
						$('.message-unread input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');

						var count = 0;
						$('.message-item:not(.message-unread) input[type=checkbox]').each(function(){
							this.checked = true;
							$(this).closest('.message-item').addClass('selected');
							count++;
						});
						Inbox.display_bar(count);
					}
					,
					select_unread : function() {
						$('.message-item:not(.message-unread) input[type=checkbox]').removeAttr('checked').closest('.message-item').removeClass('selected');

						var count = 0;
						$('.message-unread input[type=checkbox]').each(function(){
							this.checked = true;
							$(this).closest('.message-item').addClass('selected');
							count++;
						});

						Inbox.display_bar(count);
					}
				}

				//show message list (back from writing mail or reading a message)
				Inbox.show_list = function() {
					$('.message-navbar').addClass('hide');
					$('#id-message-list-navbar').removeClass('hide');

					$('.message-footer').addClass('hide');
					$('.message-footer:not(.message-footer-style2)').removeClass('hide');

					$('.message-list').removeClass('hide').next().addClass('hide');
					//hide the message item / new message window and go back to list
				}

				//show write mail form
				Inbox.show_form = function() {
					if($('.message-form').is(':visible')) return;
					if(!form_initialized) {
						initialize_form();
					}


					var message = $('.message-list');
					$('.message-container').append('<div class="message-loading-overlay"><i class="fa-spin ace-icon fa fa-spinner orange2 bigger-160"></i></div>');

					setTimeout(function() {
						message.next().addClass('hide');

						$('.message-container').find('.message-loading-overlay').remove();

						$('.message-list').addClass('hide');
						$('.message-footer').addClass('hide');
						$('.message-form').removeClass('hide').insertAfter('.message-list');

						$('.message-navbar').addClass('hide');
						$('#id-message-new-navbar').removeClass('hide');


						//reset form??
						$('.message-form .wysiwyg-editor').empty();

						$('.message-form .ace-file-input').closest('.file-input-container:not(:first-child)').remove();
						$('.message-form input[type=file]').ace_file_input('reset_input');

						$('.message-form').get(0).reset();

					}, 300 + parseInt(Math.random() * 300));
				}




				var form_initialized = false;
				function initialize_form() {
					if(form_initialized) return;
					form_initialized = true;

					//intialize wysiwyg editor
					$('.message-form .wysiwyg-editor').ace_wysiwyg({
						toolbar:
						[
							'bold',
							'italic',
							'strikethrough',
							'underline',
							null,
							'justifyleft',
							'justifycenter',
							'justifyright',
							null,
							'createLink',
							'unlink',
							null,
							'undo',
							'redo'
						]
					}).prev().addClass('wysiwyg-style1');



					//file input
					$('.message-form input[type=file]').ace_file_input()
					.closest('.ace-file-input')
					.addClass('width-90 inline')
					.wrap('<div class="form-group file-input-container"><div class="col-sm-7"></div></div>');

					//Add Attachment
					//the button to add a new file input
					$('#id-add-attachment')
					.on('click', function(){
						var file = $('<input type="file" name="attachment[]" />').appendTo('#form-attachments');
						file.ace_file_input();

						file.closest('.ace-file-input')
						.addClass('width-90 inline')
						.wrap('<div class="form-group file-input-container"><div class="col-sm-7"></div></div>')
						.parent().append('<div class="action-buttons pull-right col-xs-1">\
							<a href="#" data-action="delete" class="middle">\
								<i class="ace-icon fa fa-trash-o red bigger-130 middle"></i>\
							</a>\
						</div>')
						.find('a[data-action=delete]').on('click', function(e){
							//the button that removes the newly inserted file input
							e.preventDefault();
							$(this).closest('.file-input-container').hide(300, function(){ $(this).remove() });
						});
					});
				}//initialize_form

				$("#id-message-form").on('submit',function(event){
						event.preventDefault();
						var asunto = $("#asunto").val();
						var hidden_input =
					  $('<input type="hidden" name="mensaje" />')
					  .appendTo('#id-message-form');

					  var html_content = $('#mensaje').html();
					  hidden_input.val( html_content );
						var mensaje = hidden_input.val();
						$.ajax({
							type: "POST",
							url: 'mensajes_enviar.php',
							data: { mensaje : mensaje, asunto : asunto, destino : usuario },
							beforeSend: function(){
								$('#inbox-tabs a[href="#inbox"]').tab('show');
								$('.message-container').append('<div class="message-loading-overlay"><i class="fa-spin ace-icon fa fa-spinner orange2 bigger-160"></i></div>');
							},
							success: function(mensaje) {
								$('.message-container').find('.message-loading-overlay').remove();
								alerta("Mensaje Enviado","Mensaje Enviado");
							},
							error: function (xhr, status, error) {
								var err = eval("(" + xhr.responseText + ")");
								alert(err.Message);
							}
						});

					});

			});
			</script>
	</body>
</html>
