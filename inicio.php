<!DOCTYPE html>
<html lang="es">
	<head>
		<title>Simedu | Inicio</title>
		<?php include_once('head.php'); ?>
		<?php consulta("DELETE FROM historial WHERE fecha < DATE_SUB(NOW(), INTERVAL 1 MONTH);"); ?>
		<link href="menus/css/style4.css" rel="stylesheet">
		<link href="css/inicio.css" rel="stylesheet">
	</head>

	<body class="no-skin">

		<?php include_once('header.php'); ?>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Inicio</a>
							</li>
							<li class="active">Pagina de Inicio</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Buscar ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
				<div class="page-content">
					<article>
			        <h1 class="page-header">Inicio</h1>

			        <span>Bienvenido a la página principal de Simedu. Desde aquí podrá configurar todos los servicios disponibles de la aplicación.</span>
			        <span>Sobre la izquierda de la pantalla encontrará el menú principal completo y más abajo puedes ver los principales accesos directos.</span>
			        <span>Si tienes alguna duda, puedes visitar el <a href="http://ayuda.simedu.com.ar">Centro de Ayuda</a>.</span>
			        <p>Elige por dónde quieres comenzar y manos a la obra!</p>
			    </article>

			    <article>
			        <div class="row">

			<div class="col-md-6 col-sm-12">
				<div id="divWheelnav" class="wheelNav" data-wheelnav data-wheelnav-slicepath="Pie" data-wheelnav-navangle="90">
			        <div data-wheelnav-navitemtext="Perfil" onmouseup="navegar('#nperfil')"></div>
			        <div data-wheelnav-navitemtext="Institucional" onmouseup="navegar('#ninstitucional')"></div>
			        <div data-wheelnav-navitemicon="Comprobantes" onmouseup="navegar('#ncomprobantes')"></div>
					<div data-wheelnav-navitemicon="Libros de IVA" onmouseup="navegar('#niva')"></div>
					<div data-wheelnav-navitemicon="Mercado" onmouseup="navegar('#nmercado')"></div>
					<div data-wheelnav-navitemicon="Flujo de Fondos" onmouseup="navegar('#nfondos')"></div>
					<div data-wheelnav-navitemicon="Balances" onmouseup="navegar('#nbalances')"></div>
					<div data-wheelnav-navitemicon="Plan de Cuentas" onmouseup="navegar('#ncuentas')"></div>
					<div data-wheelnav-navitemicon="Asientos" onmouseup="navegar('#nasientos')"></div>
				</div>
			</div>

				<div class="col-md-6 col-sm-12">
				  <div id="nperfil" align="justify">
					<h1 class="page-header">Perfil<i class="fa fa-user pull-right"></i></h1>
					<p>Completa tus datos personales.</p>
					<a href="perfil.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Actualizar Perfil</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="ninstitucional" style="display: none;" align="justify">
					<h1 class="page-header">Institucional<i class="fa fa-industry pull-right"></i></h1>
					<p>A través de esta opción podemos  cumplir con los siguientes módulos:</p>
						<ul>
							<li><strong>Módulo de generación de la empresa virtual:</strong> Se realiza una definición conceptual y a nivel informativo (razón social, nombre de fantasía, cuit,  dirección, etc) de cada empresa virtual.</li>
							<li><strong>Módulo de generación de vista operativa de la empresa virtual:</strong> permite completar la información de la empresa, definir la misión, visión, mercados y valores de la misma. </li>
							<li><strong>Módulo de definición de vista comercial de la empresa virtual:</strong> permite definir objetivos comerciales, productos y/o servicios a comercializar, categorizarlos. Permite armar y manejar conceptos de catálogos de los productos que se van a ofrecer, definición de lineamientos de imagen.</li>
						</ul>
						<p>
						Una vez que cargamos esta información podemos visualizarla accediendo a nuestro <a href="perfil.php">Perfil</a> en la opcion "Ver Perfil".
						Desde el ítem "Institucional" de cada empresa podemos ver información categorizada por solapas según fuimos incorporando para relacionarla con la empresa en cuestión, como Quienes Somos, Misión, Mercados, Valores, etc.
						El máximo de solapas es cuatro (4).
						</p>
						<div class="row">
						<?php
							$con_empresa=consulta("SELECT empresas.*, usuario.idusuario
							FROM usuario INNER JOIN empresas ON empresas.idusuario = usuario.idusuario
							WHERE usuario.tipousuario=85");
							while ($em = mysqli_fetch_array($con_empresa, MYSQLI_ASSOC)) {
								if ($em['imagenempresa'] == "") {
									$imagen = "images/empresa-default.png";
								} else {
									$imagen = $em['imagenempresa'];
								}
							$rsoc = str_replace(' ', '', $em['rsocial']);
						?>
			                <div class="col-xs-12 col-sm-4 col-md-4">
			                    <div class="grid wow zoomIn">
			                    	<a href="#" data-toggle="modal" data-target="<?php echo "#" . $rsoc ?>">
				                        <figure class="effect-bubba">
				                            <img src="<?php echo $imagen; ?>" alt="img"/>
				                            <figcaption>
				                                <h2><?php echo $em['rsocial']; ?></h2>
				                                <!--<p>Slogan</p>-->
				                            </figcaption>
				                        </figure>
			                        </a>
			                    </div>
			                </div>
						<?php } ?>
			            </div>
						<a href="institucional.php" style="float:right;margin-top:20px;" class="btn btn-info">
							<strong>Ir</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
						</a>
				  </div>
				  <div id="ncomprobantes" style="display: none;">
					<h1 class="page-header">Comprobantes<i class="fa fa-file-pdf-o pull-right"></i></h1>
					<p>En esta Seccion podemos visualizar todos nuestros comprobantes de compras y ventas.</p>
					<p>Desde aqui podemos generar tambien nuevos comprobantes de <a href="generarcomprobante_compra.php">compras</a> y <a href="generarcomprobante.php">ventas</a>.
					<a href="comprobantes.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Ir</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="niva" style="display: none;">
					<h1 class="page-header">Libros de IVA<i class="fa fa-file-text pull-right"></i></h1>
					<p>En estas secciones podemos visualizar tantos nuestros libros de iva de compra como de ventas.</p>
					<a href="ivacompra.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Libro de IVA Compra</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
					<a href="ivaventa.php" style="float:left;margin-top:20px;" class="btn btn-info">
						<strong>Libro de IVA Venta</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="nmercado" style="display: none;">
					<h1 class="page-header">Mercado<i class="fa fa-shopping-cart pull-right"></i></h1>
					<p>Desde aqui podremos visualizar los productos disponibles de las empresas propias y de nuestros compañeros para agregarlos al carrito de compras de la empresa.</p>
					<a href="mercado.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Ir al Mercado</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="nfondos" style="display: none;">
					<h1 class="page-header">Flujo de Fondos<i class="fa fa-area-chart pull-right"></i></h1>
					<p>Desde aqui podremos visualizar los flujos de fondos que hubo en un determinado periodo filtrandolos por Rubro o simplemente generando un resumen general de los fondos.</p>
					<a href="flujodefondos.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Flujo de Fondos</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="nbalances" style="display: none;">
					<h1 class="page-header">Balances<i class="fa fa-bar-chart  pull-right"></i></h1>
					<p>Desde aqui podremos visualizar los balances generales y balances de sumas y saldos de nuestras empresas.</p>
					<a href="balancegeneral.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Balance General</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
					<a href="balancesumasysaldos.php" style="float:left;margin-top:20px;" class="btn btn-info">
						<strong>Balance de sumas y saldos</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="ncuentas" style="display: none;">
					<h1 class="page-header">Plan de Cuentas<i class="fa fa-archive pull-right"></i></h1>
					<p>Al definir una empresa seleccionamos un modelo por defecto, el mismo nos sirve para tener una idea de cómo empezar a trabajar.</p>
								<p>Cada alumno tendrá la tarea a acondicionar el mismo a sus necesidades según la empresa creada, pudiendo modificar, eliminar y dar de alta otras cuentas.</p>
								<p>Sugerimos cargar las cuentas imputables en minúsculas y la cuentas integradoras en mayúsculas, contamos con una opción de <a href="cuentas.php">Imprimir Plan de Cuentas</a></p>
								<p><strong>Modelo de Plan de cuentas default:</strong> Este modelo está cargado en forma genérica respetando las cuentas integradoras y de movimiento llamadas imputables.</p>
					<a href="cuentas.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Ver Plan de Cuentas</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				  <div id="nasientos" style="display: none;">
					<h1 class="page-header">Listados de Asientos<i class="fa fa-pencil pull-right"></i></h1>
					<p>A través de esta opción podemos definir asientos contables, recordar que un asiento tiene un número, una denominación, Fecha de imputación y movimientos llamadas imputaciones.</p>
								<p>El sistema te habilita dos imputaciones como mínimo pero a través de la opción <strong><i>Nueva Imputación</i></strong> podemos incorporar tantas imputaciones como necesitemos para definir nuestro asiento.</p>
					<a href="asientos_lista.php" style="float:right;margin-top:20px;" class="btn btn-info">
						<strong>Listado de Asientos</strong>&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-chevron-right"></span>
					</a>
				  </div>
				</div>


			        </div>
				<div class="row">
				 <h1 class="page-header">Accesos directos</h1>
					<ul class="ca-menu">
						<?php if($u['modcontable'] == 1 && $emodcontable == 1){ ?>
			    <div class="col-md-2 col-xs-12 col-sm-6">
						<li>
							<a href="simcontable.php">
								<span class="ca-icon"><i class="fa fa-book"></i></span>
								<div class="ca-content">
									<h2 class="ca-main">Simulador Contable</h2>
								</div>
							</a>
						</li>
			    </div>
						<?php } if($u['modempresarial'] == 1 && $emodempresarial == 1){ ?>
			    <div class="col-md-2 col-xs-12 col-sm-6">
						<li>
							<a href="simempresarial.php">
								<span class="ca-icon"><i class="fa fa-industry"></i></span>
								<div class="ca-content">
									<h2 class="ca-main">Simulador Empresarial</h2>
								</div>
							</a>
						</li>
			    </div>
						<?php } ?>
						<?php if($u['modludico'] == 1 && $u['tipousuario'] == 58 || $u['tipousuario'] == 2){ ?>
			    <div class="col-md-2 col-xs-12 col-sm-6">
						<li>
							<a href="planteo.php">
								<span class="ca-icon"><i class="fa fa-cubes"></i></span>
								<div class="ca-content">
									<h2 class="ca-main">Planteos</h2>
								</div>
							</a>
						</li>
			    </div>
						<?php } ?>
			    <div class="col-md-2 col-xs-12 col-sm-6">
						<li>
							<a href="empresas.php">
								<span class="ca-icon"><i class="fa fa-suitcase"></i></span>
								<div class="ca-content">
									<h2 class="ca-main">Empresas</h2>
								</div>
							</a>
						</li>
			    </div>
			    <div class="col-md-2 col-xs-12 col-sm-6">
						<li>
							<a href="comienzo.php">
								<span class="ca-icon"><i class="fa fa-question"></i></span>
								<div class="ca-content">
									<h2 class="ca-main">¿Por donde comienzo?</h2>
								</div>
							</a>
						</li>
			    </div>
			    <div class="col-md-2 col-xs-12 col-sm-6">
						<li>
							<a href="http://ayuda.simedu.com.ar">
								<span class="ca-icon"><i class="fa fa-flag"></i></span>
								<div class="ca-content">
									<h2 class="ca-main">Centro de Ayuda</h2>
								</div>
							</a>
						</li>
			    </div>
					</ul>
				</div>
			    </article>
			    <!-- Final Page Content -->
						</div>
					</div>
				</div>

			<script src="js/wheel/raphael.min.js"></script>
			<script src="js/wheel/raphael.icons.min.js"></script>
			<script src="js/wheel/wheelnav.min.js"></script>

			<?php require_once('footer.php'); ?>

		</div><!-- /.main-content -->

			<!-- Modal de las empresas -->
			<?php include "modal-index.php" ?>

			<script type="text/javascript">

				var idactual = '#nperfil';

						window.onload = function () {
								new wheelnav("divWheelnav");
						};

				function navegar(id){
					$(idactual).css("display", "none");
					$(id).css("display", "");
					idactual = id;
				}
				<?php if($sinempresa){ ?>
					BootstrapDialog.show({
						title: 'Bienvenido a Simedu',
						message: "Bienvenido a Simedu. Para poder utilizar todas las funcionalidades que el Simulador Educativo tiene para ofrecerte lo primero que deberas hacer es crear tu primer empresa.",
						type: BootstrapDialog.TYPE_WARNING,
						buttons: [{
							label: ' CREAR EMPRESA',
							cssClass: 'btn-primary',
							action: function(){
								window.location = "empresas.php";
							}
						}, {
							label: ' Cancelar',
							cssClass: 'btn-default',
							action: function(dialogItself){
								dialogItself.close();
							}
						}]
					});
				<?php } ?>
			</script>
	</body>
</html>
